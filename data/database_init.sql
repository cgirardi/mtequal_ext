DELETE FROM roletype;
INSERT INTO `roletype` (`name`, `description`, `can_edit`, `can_delete`, `can_invite`, `can_evaluate`, `can_judge`) VALUES ('admin','You was invited to admin a project created by other',1,0,1,1,1);
INSERT INTO `roletype` (`name`, `description`, `can_edit`, `can_delete`, `can_invite`, `can_evaluate`, `can_judge`) VALUES ('evaluator','You can evaluate a project',0,0,0,1,0);
-- INSERT INTO `roletype` (`name`, `description`, `can_edit`, `can_delete`, `can_invite`, `can_evaluate`, `can_judge`) VALUES ('judge','You can see the all evaluations of the project and its agreement',0,0,1,0,1);
INSERT INTO `roletype` (`name`, `description`, `can_edit`, `can_delete`, `can_invite`, `can_evaluate`, `can_judge`) VALUES ('reviewer','You can check the work of the judges',0,0,0,0,1);
INSERT INTO `roletype` (`name`, `description`, `can_edit`, `can_delete`, `can_invite`, `can_evaluate`, `can_judge`) VALUES ('owner','You can do everything on own projects',1,1,1,1,1);
INSERT INTO `roletype` (`name`, `description`, `can_edit`, `can_delete`, `can_invite`, `can_evaluate`, `can_judge`) VALUES ('vendor','You can invite someone to annotate or judge a project',0,0,1,1,1);
