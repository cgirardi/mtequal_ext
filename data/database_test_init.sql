DELETE FROM annotation;
DELETE FROM agreement;
DELETE FROM comment;
DELETE FROM corpus;
DELETE FROM document;
DELETE FROM sentence;

DELETE FROM user;
INSERT INTO `user` (`id`, `username`, `email`, `password`, `affiliation`, `active`, `hash`, `verified`, `registered`, `modified`, `logged`, `notes`, `roles`, `firstname`, `lastname`) VALUES (1,'mary','mtequalfbk@gmail.com','9003d1df22eb4d3820015070385194c8','AS/eBay',1,'e416fd9fb5152ed2761ad937794c4ab3','2015-12-11 10:24:00','2015-11-05 14:30:01','2015-11-05 14:30:01','2015-12-16 15:08:58','She creates a project','[\"admin\"]','Mary','');
INSERT INTO `user` (`id`, `username`, `email`, `password`, `affiliation`, `active`, `hash`, `verified`, `registered`, `modified`, `logged`, `notes`, `roles`, `firstname`, `lastname`) VALUES (2,'john','mtequalfbk@gmail.com','9003d1df22eb4d3820015070385194c8','eBay',1,'e416fd9fb5152ed2761ad937794c4ab3','2015-12-11 10:24:00','2015-11-05 14:30:01','2015-11-05 14:30:01','2015-12-16 15:08:58','He is invited as admin by Mary','[\"admin\"]','John','');
INSERT INTO `user` (`id`, `username`, `email`, `password`, `affiliation`, `active`, `hash`, `verified`, `registered`, `modified`, `logged`, `notes`, `roles`, `firstname`, `lastname`) VALUES (3,'bob','mtequalfbk@gmail.com','9003d1df22eb4d3820015070385194c8','Vendor',1,'e416fd9fb5152ed2761ad937794c4ab3','2015-12-11 10:24:00','2015-11-05 14:30:01','2015-11-05 14:30:01','2015-12-16 15:08:58','He is invited as vendor by John','[\"vendor\"]','Bob','');
INSERT INTO `user` (`id`, `username`, `email`, `password`, `affiliation`, `active`, `hash`, `verified`, `registered`, `modified`, `logged`, `notes`, `roles`, `firstname`, `lastname`) VALUES (4,'ellen','mtequalfbk@gmail.com','9003d1df22eb4d3820015070385194c8','MTLS/eBay',1,'e416fd9fb5152ed2761ad937794c4ab3','2015-12-11 10:24:00','2015-11-05 14:30:01','2015-11-05 14:30:01','2015-12-16 15:08:58','She is invited as reviewer by John','[\"reviewer\"]','Ellen','');
INSERT INTO `user` (`id`, `username`, `email`, `password`, `affiliation`, `active`, `hash`, `verified`, `registered`, `modified`, `logged`, `notes`, `roles`, `firstname`, `lastname`) VALUES (5,'tom','mtequalfbk@gmail.com','9003d1df22eb4d3820015070385194c8','Vendor',1,'e416fd9fb5152ed2761ad937794c4ab3','2015-12-11 10:24:00','2015-11-05 14:30:01','2015-11-05 14:30:01','2015-12-16 15:08:58','He is invited as reviewer by Bob','[\"reviewer\"]','Tom','');
INSERT INTO `user` (`id`, `username`, `email`, `password`, `affiliation`, `active`, `hash`, `verified`, `registered`, `modified`, `logged`, `notes`, `roles`, `firstname`, `lastname`) VALUES (7,'mark','mtequalfbk@gmail.com','9003d1df22eb4d3820015070385194c8','Vendor',1,'e416fd9fb5152ed2761ad937794c4ab3','2015-12-11 10:24:00','2015-11-05 14:30:01','2015-11-05 14:30:01','2015-12-16 15:08:58','He is invited as judge by Bob','[\"judge\"]','Mark','');
INSERT INTO `user` (`id`, `username`, `email`, `password`, `affiliation`, `active`, `hash`, `verified`, `registered`, `modified`, `logged`, `notes`, `roles`, `firstname`, `lastname`) VALUES (8,'davide','mtequalfbk@gmail.com','9003d1df22eb4d3820015070385194c8','Vendor',1,'e416fd9fb5152ed2761ad937794c4ab3','2015-12-11 10:24:00','2015-11-05 14:30:01','2015-11-05 14:30:01','2015-12-16 15:08:58','He invited as evaluators by Bob','[\"evaluator\"]','Davide','');
INSERT INTO `user` (`id`, `username`, `email`, `password`, `affiliation`, `active`, `hash`, `verified`, `registered`, `modified`, `logged`, `notes`, `roles`, `firstname`, `lastname`) VALUES (9,'chris','mtequalfbk@gmail.com','9003d1df22eb4d3820015070385194c8','Vendor',1,'e416fd9fb5152ed2761ad937794c4ab3','2015-12-11 10:24:00','2015-11-05 14:30:01','2015-11-05 14:30:01','2015-12-16 15:08:58','He invited as evaluators by Bob','[\"evaluator\"]','Chris','');


DELETE FROM project;
INSERT INTO `project` (`id`, `author_id`, `name`, `type`, `description`, `instructions`, `ranges`, `randout`, `requiref`, `sentlist`, `maxdontknow`, `created`, `modified`) VALUES (1,NULL,'__task2test__','scale','','','[{\"val\": \"1\",\"label\": \"1\",\"color\": \"FFFF99\"},{\"val\": \"2\",\"label\": \"2\",\"color\": \"D9FF99\"},{\"val\": \"3\",\"label\": \"3\",\"color\": \"B2FF99\"},{\"val\": \"4\",\"label\": \"4\",\"color\": \"8CFF99\"},{\"val\": \"5\",\"label\": \"5\",\"color\": \"66FF99\"}]',1,1,1,0,'2015-11-16 12:25:45','2015-11-16 12:25:45');

DELETE FROM progress;
INSERT INTO `progress` (`task`, `num`, `project`, `user`, `completed`, `disabled`, `modified`, `duedate`) VALUES ('creation',0,1,1,1,1,'2016-04-29 10:46:52',NULL);
INSERT INTO `progress` (`task`, `num`, `project`, `user`, `completed`, `disabled`, `modified`, `duedate`) VALUES ('evalformat',1,1,1,0,0,'2016-04-29 10:46:52',NULL);
INSERT INTO `progress` (`task`, `num`, `project`, `user`, `completed`, `disabled`, `modified`, `duedate`) VALUES ('import_corpus',2,1,1,0,1,'2016-04-29 10:46:52',NULL);
INSERT INTO `progress` (`task`, `num`, `project`, `user`, `completed`, `disabled`, `modified`, `duedate`) VALUES ('guidelines',4,1,1,0,1,'2016-04-29 10:46:52',NULL);
INSERT INTO `progress` (`task`, `num`, `project`, `user`, `completed`, `disabled`, `modified`, `duedate`) VALUES ('check_corpus',5,1,1,0,1,'2016-04-29 10:46:52',NULL);
INSERT INTO `progress` (`task`, `num`, `project`, `user`, `completed`, `disabled`, `modified`, `duedate`) VALUES ('evaluation',10,1,9,0,1,'2016-04-29 10:51:57',NULL);
INSERT INTO `progress` (`task`, `num`, `project`, `user`, `completed`, `disabled`, `modified`, `duedate`) VALUES ('evaluation',10,1,8,0,1,'2016-04-29 10:52:13',NULL);
INSERT INTO `progress` (`task`, `num`, `project`, `user`, `completed`, `disabled`, `modified`, `duedate`) VALUES ('review',11,1,4,0,1,'2016-04-29 10:52:13',NULL);
INSERT INTO `progress` (`task`, `num`, `project`, `user`, `completed`, `disabled`, `modified`, `duedate`) VALUES ('finalize',12,1,1,0,1,'2016-04-29 10:52:13',NULL);


DELETE FROM role;
INSERT INTO `role` (`user`, `project`, `role`, `created`, `activated`, `created_by`, `status`, `hash`) VALUES (1,1,'owner','2015-11-16 12:25:45','2015-12-11 00:27:17',0,'created','9a357222efc782f2a6d59c9af0e50f67');
INSERT INTO `role` (`user`, `project`, `role`, `created`, `activated`, `created_by`, `status`, `hash`) VALUES (1,1,'admin','2015-11-16 12:25:45','2015-12-11 00:27:17',0,'accepted','9a357222efc782f2a6d59c9af0e50f67');
INSERT INTO `role` (`user`, `project`, `role`, `created`, `activated`, `created_by`, `status`, `hash`) VALUES (2,1,'admin','2015-12-17 00:18:26','2015-12-17 00:18:26',1,'accepted','caa97dea1dd1619793561871d3997f58');
INSERT INTO `role` (`user`, `project`, `role`, `created`, `activated`, `created_by`, `status`, `hash`) VALUES (3,1,'vendor','2015-12-17 00:21:54','2015-12-17 00:21:54',2,'accepted','a8c9d59bffef37a23dfc90de46442ae2');
INSERT INTO `role` (`user`, `project`, `role`, `created`, `activated`, `created_by`, `status`, `hash`) VALUES (4,1,'reviewer','2015-12-17 00:21:39','2015-12-17 00:21:39',2,'accepted','0282811ba0375cce9005d90baf1de5d7');
INSERT INTO `role` (`user`, `project`, `role`, `created`, `activated`, `created_by`, `status`, `hash`) VALUES (5,1,'reviewer','2015-12-17 00:30:51','2015-12-17 00:30:51',3,'accepted','dbe9d9b8ad9e9d9fbb96443c9270cc08');
INSERT INTO `role` (`user`, `project`, `role`, `created`, `activated`, `created_by`, `status`, `hash`) VALUES (8,1,'evaluator','2015-12-17 00:31:24','2015-12-17 00:31:24',3,'accepted','ddede7fa46b63af93bfdf4ea68c3dc0d');
INSERT INTO `role` (`user`, `project`, `role`, `created`, `activated`, `created_by`, `status`, `hash`) VALUES (9,1,'evaluator','2015-12-17 00:31:47','2015-12-17 00:31:47',3,'accepted','7596c157ab16086d4d08cbc68d546475');

