package steps;

import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import objects.EvalData;
import objects.EvalRow;

import org.junit.Assert;

import pages.ReviewPage;
import net.thucydides.core.annotations.Step;

public class ReviewSteps {

	private ReviewPage reviewPage;
	
	@Step
	public int getRowsNumber() {
		return reviewPage.getActualRowsNumber();
	}

	@Step
	public void verifyReviewModalIsVisible() {
		waitASecond();
		Assert.assertTrue("Review modal should be visible", reviewPage.reviewModal.isVisible());
	}
	
	@Step
	public void waitReviewModalClosed() {
		if (reviewPage.reviewModal.isCurrentlyVisible())
			reviewPage.reviewModal.withTimeoutOf(30, TimeUnit.SECONDS).waitUntilNotVisible();
		Assert.assertTrue("Review modal should not be visible", !reviewPage.reviewModal.isCurrentlyVisible());
	}

	@Step
	public void clickOnRow(int i) {
		reviewPage.getRow(i).click();
	}

	@Step
	public void verifyKappa(double expectedKappa) {
		String kappaString = reviewPage.kscore.getText();
		System.out.println(kappaString);
		double actualKappa = getValueFromKappaString(kappaString);
		double delta = 0.01;
		Assert.assertEquals(expectedKappa, actualKappa, delta);
	}

	private double getValueFromKappaString(String kappaString) {
		String patternText = "k = ([\\d\\.]+) .*";
		Pattern pattern = Pattern.compile(patternText);
		Matcher matcher = pattern.matcher(kappaString);
		String result = null;
		if (matcher.find()) {
			result = matcher.group(1); 
		} else {
			Assert.fail("Pattern " + patternText + " was not found in text: " + kappaString);
		}
		return Double.parseDouble(result);
	}

	@Step
	public void performScaleReview(EvalData reviewData) {
		performScaleOrRankingReview(reviewData);
	}

	@Step
	public void performRankingReview(EvalData reviewData) {
		performScaleOrRankingReview(reviewData);
	}
	
	private void performScaleOrRankingReview(EvalData reviewData) {
		EvalRow[] rows = reviewData.getRows();
		EvalRow currentRow;
		for (int i = 0; i < rows.length; i++) {
			currentRow = rows[i];
			openReviewStep(currentRow);
			verifyReviewModalIsVisible();
			if (currentRow.dontKnow) 
				chooseAllCurrentScaleReviewAnswers("don't know");
			else
				chooseAllCurrentScaleReviewAnswers(rows[i].answers[0]);
			confirmReviewStep();
			waitReviewModalClosed();
		}
	}

	@Step
	public void confirmReviewStep() {
		reviewPage.confirmButton.click();
	}

	@Step
	public void chooseAllCurrentScaleReviewAnswers(String answer) {
		reviewPage.chooseReviewAnswers(answer);
	}

	@Step
	public void openReviewStep(EvalRow row) {
		reviewPage.getRow(row).click();
	}

	@Step
	public void completeReviewTask() {
		openProjectMonitor();
		markCompleteReviewingTask();
	}

	@Step
	public void openProjectMonitor() {
		reviewPage.buttonProjectMonitor.click();
	}
	
	@Step
	public void markCompleteReviewingTask() {
		reviewPage.checkboxReviewing.click();
		reviewPage.dialogConfirmCompleteTask.shouldBeVisible();
		reviewPage.dialogConfirmCompleteTask.findBy(".//button[contains(.,'OK')]").click();
	}
	
	private void waitASecond() {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
