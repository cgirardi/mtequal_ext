package steps;

import objects.User;
import pages.MainPage;

import org.junit.Assert;

import net.thucydides.core.annotations.Step;

public class UserSteps {

	private MainPage page = new MainPage();
	
	@Step
	public void openMainPage() {
		page.open();
	}

	@Step
	public void loginAsUser(User user) {
		openLoginPopup();
		inputUsername(user.name);
    	inputPassword(user.password);
    	clickLoginButton();
	}
	
	@Step
	public void inputUsername(String username) {
		page.usernameInput.type(username);
	}

	@Step
	public void inputPassword(String password) {
		page.passwordInput.type(password);
	}

	@Step
	public void clickLoginButton() {
		page.submitLoginButton.click();
	}

	@Step
	public void verifyUserLoggedIn(String username) {
		Assert.assertTrue("Page content for logged in user should be visible", page.content.isVisible());
		Assert.assertEquals(username, page.userSettingsLink.getText());
	}

	@Step
	public void openLoginPopup() {
		if (page.loginTitle.isVisible())
			return;
		openMainPage();
		vefiryLoginFormVisible();
	}

	@Step
	public void vefiryLoginFormVisible() {
		Assert.assertTrue("Login form should be visible", page.loginTitle.isVisible());
	}

	@Step
	public void clickForgotPassword() {
		page.forgotPasswordLink.click();
	}

	@Step
	public void verifyForgotPasswordVisible() {
		Assert.assertTrue("Forgot password popup should be visible", page.forgotPasswordTitle.isVisible());
	}

	@Step
	public void inputUserReferenceInForgotPassword(String userReference) {
		page.referenceInput.type(userReference);
	}

	@Step
	public void submitForgotPassword() {
		page.submitForgotPassword.click();
	}

	@Step
	public void openUrl(String url) {
		page.getDriver().get(url);
	}

	@Step
	public void verifyResetPasswordOpened() {
		Assert.assertTrue("Reset password title should be visible", page.resetPasswordTitle.isVisible());
	}
	
	@Step
	public void inputNewPassword(String password) {
		page.newPassword.type(password);
	}

	@Step
	public void confirmNewPassword(String password) {
		page.confirmPassword.type(password);
	}

	@Step
	public void submitNewPassword() {
		page.submitNewPassword.click();
	}

	@Step
	public void verifyLoginFailed() {
		Assert.assertTrue("Login failed message should be visible", page.errorLoginFailed.isVisible());
		Assert.assertFalse("Content for logged in user should not be visible", page.content.isVisible());
	}

	@Step
	public void closeLoginPopup() {
		page.closeLoginPopupButton.click();
		page.loginTitle.waitUntilNotVisible();
	}

	@Step
	public void openSignUpForm() {
		closeLoginPopup();
		clickSignUpButton();
		verifySignUpFormVisible();
	}

	@Step
	public void clickSignUpButton() {
		page.signUpButton.click();
	}

	@Step
	public void verifySignUpFormVisible() {
		Assert.assertTrue("Sign up title should be visible", page.signUpTitle.isVisible());
	}

	@Step
	public void inputSignUpData(User user) {
		inputSignUpUsername(user.name);
		inputSignUpEmail(user.email);
		inputSignUpPassword(user.password);
		inputSignUpConfirmPassword(user.password);
		checkAgreeToTerms();
	}

	@Step
	public void inputSignUpUsername(String name) {
		page.signUpUsername.type(name);
	}
	
	@Step
	public void inputSignUpEmail(String email) {
		page.signUpEmail.type(email);
	}

	@Step
	public void inputSignUpPassword(String password) {
		page.newPassword.type(password);
	}
	
	@Step
	public void inputSignUpConfirmPassword(String password) {
		page.confirmPassword.type(password);
	}

	@Step
	public void checkAgreeToTerms() {
		page.agreeCheckbox.click();
	}

	@Step
	public void submitSignUp() {
		page.submitSignUpButton.click();
	}

	@Step
	public void verifyInfoMessageContains(String message) {
		page.findBy("//div[contains(.,'" + message + "')]").isVisible();
	}

	@Step
	public void logout() {
		page.userSettingsLink.click();
		page.logoutLink.click();
	}

	@Step
	public void changePasswordInProfile(User user, String oldPassword, String newPassword) {
		openUserProfile();
		openChangePasswordTab();
		insertOldPassword(oldPassword);
		inputNewPassword(newPassword);
		confirmNewPassword(newPassword);
		submitNewPassword();
		verifyInfoMessageContains("The password has been changed");
	}

	@Step
	public void openUserProfile() {
		page.userSettingsLink.click();
		page.userProfileLink.click();
	}
	
	@Step
	public void openChangePasswordTab() {
		page.changePasswordTab.click();
	}

	@Step
	public void insertOldPassword(String password) {
		page.oldPassword.type(password);
	}

}
