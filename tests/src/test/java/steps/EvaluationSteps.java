package steps;

import objects.EvalData;
import objects.EvalRow;

import org.junit.Assert;

import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Step;
import pages.EvaluationPage;

public class EvaluationSteps {

	private EvaluationPage evalPage = new EvaluationPage();

	@Step
	public void answerTreeEvaluationQuestions(EvalRow row) { 
		WebElementFacade snippet = findSnippetWithOutput(row.output);
		evalPage.completeTreeAnswers(snippet, row.answers);
	}

	@Step
	public void completeEvaluationStep() {
		evalPage.waitFor(2000);
		evalPage.confirmAndGotoNext.click();
	}

	@Step
	public int getOutputsNumber() {
		return evalPage.getDecisionTables().size();
	}

	@Step
	public void performDecisionTreeEvaluation(EvalData evalData) {
		EvalRow[] rows = evalData.getRows();
		EvalRow currentRow;
		for (int i = 0; i < rows.length; i=i+3) {
			currentRow = rows[i];
			if (currentRow.dontKnow) {
				answerDontKnow();
			}
			else {
				answerTreeEvaluationQuestions(rows[i]);
				answerTreeEvaluationQuestions(rows[i+1]);
				answerTreeEvaluationQuestions(rows[i+2]);
			}
			completeEvaluationStep();
		}
	}

	@Step
	public void answerDontKnow() {
		evalPage.dontKnowButton.click();
	}

	@Step
	public void performScaleEvaluation(EvalData evalData) {
		EvalRow[] rows = evalData.getRows();
		EvalRow currentRow;
		for (int i = 0; i < rows.length; i=i+3) {
			currentRow = rows[i];
			if (currentRow.dontKnow) {
				answerDontKnow();
			}
			else {
				answerScaleQuestion(rows[i]);
				answerScaleQuestion(rows[i+1]);
				answerScaleQuestion(rows[i+2]);
			}
			completeEvaluationStep();
		}
	}

	@Step
	public void answerScaleQuestion(EvalRow row) {
		WebElementFacade snippet = findSnippetWithOutput(row.output);
		evalPage.chooseScaleAnswer(snippet, row.answers[0]);
	}
	
	private WebElementFacade findSnippetWithOutput(String output) {
		return evalPage.getSnippet(output);
	}

	@Step
	public void confirmWorkIsFinished() {
		Assert.assertTrue(evalPage.dialogWorkFinished.isVisible());
		evalPage.dialogWorkFinished.findBy(".//button[contains(.,'OK')]").click();
	}

	@Step
	public void performRankingEvaluation(EvalData evalData) {
		EvalRow[] rows = evalData.getRows();
		EvalRow currentRow;
		for (int i = 0; i < rows.length; i=i+3) {
			currentRow = rows[i];
			if (currentRow.dontKnow) {
				answerDontKnow();
			}
			else {
				answerRankingQuestion(rows[i]);
				answerRankingQuestion(rows[i+1]);
				answerRankingQuestion(rows[i+2]);
			}
			completeEvaluationStep();
		}
	}

	@Step
	private void answerRankingQuestion(EvalRow row) {
		WebElementFacade snippet = findSnippetWithOutput(row.output);
		evalPage.chooseRankingAnswer(snippet, row.answers[0]);
	}

}
