package steps;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.Message;
import javax.mail.MessagingException;

import org.junit.Assert;

import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.StepEventBus;
import utils.DataProperties;
import utils.MailServer;

public class EmailSteps {
	
	private MailServer mailServer;
	private Message message;
	
	public EmailSteps() throws Exception {
		String host = "pop.gmail.com";
		String emailUser = DataProperties.get("emailUser");
		String password = DataProperties.get("emailPassword");
		mailServer = new MailServer(host, emailUser, password);
	}
	
	@Step
	public void clearOldMessages() throws Exception {
		mailServer.deleteAllMessages();
	}
	
	@Step
	public String getResetPasswordUrlFromEmail() throws Exception {
		getNewMessage();
		checkMessageSubjectContains("[MT-EQuAl] Password recovery");
		return getUrlFromMessage("[^\']*password/forgot[^\']*");
	}

	@Step
	public String getActivationUrlFromEmail() throws Exception {
		getNewMessage();
		checkMessageSubjectContains("[MT-EQuAl] User registration");
		return getUrlFromMessage("[^\"]*user/verify[^\"]*");
	}
	
	@Step
	public void getNewMessage() throws Exception {
		if (StepEventBus.getEventBus().aStepInTheCurrentTestHasFailed()) 
			return;
		message = mailServer.getNewMessage(60);
		if (message == null)
			fail("Message not received");
	}

	@Step
	public void checkMessageSubjectContains(String expectedSubject) throws MessagingException {
		String subject = message.getSubject();
		assertTrue("Expected that subject contains " + expectedSubject + "; actual subject: " + subject,
				subject.contains(expectedSubject));
	}
	
	@Step
	public String getUrlFromMessage(String patternText) throws Exception {
		String messageText = message.getContent().toString();
		String url = null;
		url = getPatternFromMessage(messageText, patternText);
		System.out.println("Url extracted from message: " + url); 
		return url;
	}
	
	private String getPatternFromMessage(String messageText, String patternText) {
		Pattern pattern = Pattern.compile(patternText);
		Matcher matcher = pattern.matcher(messageText);
		String result = null;
		if (matcher.find()) {
			result = matcher.group(); 
		} else {
			Assert.fail("Pattern " + patternText + " was not found in text: " + messageText);
		}
		return result;
	}

	@Step
	public String getAcceptInvitationUrlFromEmail() throws Exception {
		getNewMessage();
		checkMessageSubjectContains("[MT-EQuAl] Access invitation");
		return getUrlFromMessage("[^\"]*role/reply[^\"]*action=accepted");
	}

	@Step
	public void checkReportMessage() throws Exception {
		getNewMessage();
		checkMessageSubjectContains("Evaluation report");
	}

}
