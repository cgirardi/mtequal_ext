package steps;

import org.junit.Assert;
import org.openqa.selenium.Keys;
import net.thucydides.core.annotations.Step;
import objects.User;
import pages.AccessPage;

public class AccessManagementSteps {
	
	private AccessPage accessPage;

	@Step
	public void addUser(User user) {
		typeUserEmail(user.email);
		clickUserName(user.name);
		clickAddButton();
		checkUserAdded(user.name);
	}

	@Step
	public void checkUserAdded(String name) {
		Assert.assertTrue(accessPage.isUserPresentInTable(name));
	}

	@Step
	public void clickAddButton() {
		accessPage.buttonAdd.click();
	}

	@Step
	public void clickUserName(String name) {
		accessPage.clickNameInAutocomplete(name);
	}

	@Step
	public void typeUserEmail(String email) {
		try {
			accessPage.searchInput.type(email);
			Thread.sleep(2000);
			accessPage.searchInput.sendKeys(Keys.ENTER);
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Step
	public void setEvaluatorRole(User user) {
		clickAccessButton(user, "evaluator");
		confirmRoleActivation();
		verifyEvaluatorButtonIsOrange(user.name);
	}

	@Step
	public void verifyEvaluatorButtonIsOrange(String userName) {
		Assert.assertTrue(accessPage.isButtonOrange(userName, "evaluator"));
	}

	@Step
	public void clickAccessButton(User user, String role) {
		accessPage.clickAccessButton(user.name, role);
	}

	@Step
	public void confirmRoleActivation() {
		String questionText = "Do you want to activate this user's role for the project?";
		confirmAction(questionText);
	}

	@Step
	public void confirmRevoke() {
		String questionText = "Do you really want to revoke this user's role from the project?";
		confirmAction(questionText);
	}
	
	@Step
	public void confirmAction(String questionText) {
		verifyDialogWithText(questionText);
		clickConfirmOK();
	}

	@Step
	public void clickConfirmOK() {
		accessPage.clickConfirmOK();
	}

	@Step
	public void verifyDialogWithText(String text) {
		Assert.assertTrue(accessPage.isPopupWithTextVisible(text));
	}

	@Step
	public void verifyEvaluatorButtonIsGreen(String userName) {
		Assert.assertTrue(accessPage.isButtonGreen(userName, "evaluator"));
	}

	@Step
	public void revokeRole(User user, String role) {
		clickAccessButton(user, role);
		confirmRevoke();
	}

	@Step
	public void removeUserFromProject(User user) {
		clickRemoveUser(user.name);
		confirmAction("Do you really want to remove this user from the project?");
	}

	@Step
	public void clickRemoveUser(String userName) {
		accessPage.clickRemoveUserButton(userName);
	}

}
