package steps;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import utils.DataProperties;
import net.thucydides.core.annotations.Step;

public class AuxiliarySteps {
	
	@Step
	public void openUrl(String url) throws MalformedURLException, IOException {
		URL urlObject = new URL(url);
		HttpURLConnection connection = (HttpURLConnection) urlObject.openConnection();
		
		BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		String inputLine;
		while ((inputLine = br.readLine()) != null) {
			System.out.println(inputLine);
		}
		br.close();
	}

	@Step
	public void deleteUserIfExists(String name) throws MalformedURLException, IOException {
		String apiUrl = DataProperties.get("deleteObjectUrl") + "&type=user&name=" + name;
		openUrl(apiUrl);
	}
}
