package steps;

import org.junit.Assert;

import net.thucydides.core.annotations.Step;
import pages.ProjectPage;

public class ProjectSteps {

	private ProjectPage projectPage;

	@Step
	public void openProject(String projectName) {
		openDashboard();
		clickRowWithProject(projectName); 
		verifyProjectOpened(projectName);
	}

	@Step
	public void openDashboard() {
		projectPage.dashboardLink.click();
	}
	
	@Step
	public void clickRowWithProject(String projectName) {
		projectPage.clickRowWithProject(projectName);
	}
	
	@Step
	public void verifyProjectOpened(String projectName) {
		Assert.assertEquals(projectName, projectPage.contentHeader.getText());
	}

	@Step
	public void verifyEvaluationTabOpened() {
		Assert.assertTrue("Evaluation tab should be visible", projectPage.isEvaluationTabOpened());
	}

	@Step
	public void openTab(String tabName) {
		projectPage.clickOnTab(tabName);
	}

	@Step
	public void verifyReviewTabOpened() {
		Assert.assertTrue("Review tab should be visible", projectPage.reviewCompleteButton.isVisible());
	}

	@Step
	public void openUserInvitation() {
		openTab("Settings");
		clickUserInvitationLink();
	}

	@Step
	public void clickUserInvitationLink() {
		projectPage.userInvitationLink.click();
	}

	@Step
	public void sendReport(String reportEmail) {
		clickSendAReportButton();
		insertReportEmail(reportEmail);
		clickSendButton();
	}

	@Step
	public void clickSendAReportButton() {
		projectPage.sendReportButton.click();
	}

	@Step
	public void insertReportEmail(String reportEmail) {
		projectPage.sendReportEmailInput.type(reportEmail);
	}

	@Step
	public void clickSendButton() {
		projectPage.finalSendButton.click();
	}

}
