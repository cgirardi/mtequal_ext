package requirements;

import java.util.Arrays;
import java.util.Collection;

import net.serenitybdd.junit.runners.SerenityParameterizedRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import net.thucydides.junit.annotations.TestData;
import objects.EvalData;
import objects.User;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.WebDriver;

import steps.AuxiliarySteps;
import steps.EvaluationSteps;
import steps.ProjectSteps;
import steps.ReviewSteps;
import steps.UserSteps;
import utils.DataProperties;
import utils.DataProvider;

@RunWith(SerenityParameterizedRunner.class)  
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestScaleProject {

    private String projectName;

	@TestData                                                   
    public static Collection<Object[]> testData(){
        return Arrays.asList(new Object[][]{
                {"data/scale 0.333", 0.333},
                {"data/scale 0.888", 0.888}
        });
    }
	
	private final String testDataFolder;
	private final double expectedKappa;
    
	public TestScaleProject(String testDataFolder, double expectedKappa) {
		this.testDataFolder = testDataFolder;
		this.expectedKappa = expectedKappa;
		this.projectName = DataProvider.getScaleProject();
	}
	
	@Managed
	WebDriver driver;
	
	@Steps                                                                       
    UserSteps userSteps;
	@Steps 
	EvaluationSteps evalSteps;
	@Steps
	ProjectSteps projectSteps;
	@Steps 
	ReviewSteps reviewSteps;
	@Steps
	AuxiliarySteps auxSteps;
	
	private final int scaleAnswersNumber = 1;
	
	@Before
	public void maximizeWindow() {
		driver.manage().window().maximize();
	}
	
	@Test
	public void beforeTestResetDb() throws Exception {
		String resetDbUrl = DataProperties.get("resetDbUrl");
		auxSteps.openUrl(resetDbUrl);
	}

	@Test
	public void test1ScaleEvaluation1() {
		User user = DataProvider.getUserEvaluator1();
		String filePath = testDataFolder + "/eval1.csv";
		EvalData evalData = new EvalData(filePath, scaleAnswersNumber);	
		scaleEvaluation(user, evalData);
		userSteps.logout();
	}
	
	@Test
	public void test2ScaleEvaluation2() {
		User user = DataProvider.getUserEvaluator2();
		String filePath = testDataFolder + "/eval2.csv";
		EvalData evalData = new EvalData(filePath, scaleAnswersNumber);	
		scaleEvaluation(user, evalData);
		userSteps.logout();
	}
	
	@Test
	public void test3ReviewScale() {
		User user = DataProvider.getUserReviewer();
		String filePath = testDataFolder + "/review.csv";
		EvalData reviewData = new EvalData(filePath, scaleAnswersNumber);
		
		userSteps.openMainPage();
		userSteps.loginAsUser(user);
		projectSteps.openProject(projectName);
		projectSteps.openTab("Review");
		
		reviewSteps.verifyKappa(expectedKappa);
		reviewSteps.performScaleReview(reviewData);
		reviewSteps.verifyKappa(1.0);
		
		reviewSteps.completeReviewTask();
		
		userSteps.logout();
	}
	
	private void scaleEvaluation(User user, EvalData evalData) {
		userSteps.openMainPage();
		userSteps.loginAsUser(user);
		projectSteps.openProject(projectName);
		projectSteps.verifyEvaluationTabOpened();
		evalSteps.performScaleEvaluation(evalData);
		evalSteps.confirmWorkIsFinished();
	}
	
	@Override
	public String toString() {
		return "TestScaleProject: " + testDataFolder + ", k = " + expectedKappa;
	}
}
