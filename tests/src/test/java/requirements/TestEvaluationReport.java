package requirements;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import objects.User;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import steps.EmailSteps;
import steps.ProjectSteps;
import steps.UserSteps;
import utils.DataProvider;

@RunWith(SerenityRunner.class) 
public class TestEvaluationReport {

	@Managed                            
    WebDriver driver;

    @Steps                                                                       
    UserSteps userSteps;
	@Steps
	ProjectSteps projectSteps;
    @Steps
    EmailSteps emailSteps;
    
    @Test
    public void sendEvaluationReport() throws Exception {
    	emailSteps.clearOldMessages();
		
		User user = DataProvider.getUserReviewer();
		String projectName = DataProvider.getProjectNameForEvaluationReport();
		String reportEmail = DataProvider.getEmailForReport();
		
		userSteps.openMainPage();
		userSteps.loginAsUser(user);
		projectSteps.openProject(projectName);
		projectSteps.openTab("Evaluation report");
		projectSteps.sendReport(reportEmail);
		
		emailSteps.checkReportMessage();
    }
}
