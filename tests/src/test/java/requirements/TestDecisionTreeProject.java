package requirements;

import java.util.Arrays;
import java.util.Collection;

import net.serenitybdd.junit.runners.SerenityParameterizedRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import net.thucydides.junit.annotations.TestData;
import objects.EvalData;
import objects.User;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.WebDriver;

import steps.AuxiliarySteps;
import steps.ProjectSteps;
import steps.EvaluationSteps;
import steps.ReviewSteps;
import steps.UserSteps;
import utils.DataProperties;
import utils.DataProvider;

@RunWith(SerenityParameterizedRunner.class)  
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestDecisionTreeProject {

    @TestData                                                   
    public static Collection<Object[]> testData(){
        return Arrays.asList(new Object[][]{
                {"data/tree", 0.888},
        });
    }
	
	private final String testDataFolder;
	private final double expectedKappa;
	private final int treeAnswersNumber = 5;
	private String projectName;
	
	public TestDecisionTreeProject(String testDataFolder, double expectedKappa) {
		this.testDataFolder = testDataFolder;
		this.expectedKappa = expectedKappa;
		this.projectName = DataProvider.getTreeProject();
	}
	
	@Managed                            
    WebDriver driver;
	
	@Steps                                                                       
    UserSteps userSteps;
	@Steps 
	EvaluationSteps evalSteps;
	@Steps
	ProjectSteps projectSteps;
	@Steps
	AuxiliarySteps auxSteps;
	@Steps 
	ReviewSteps reviewSteps;
	
	@Test
	public void test0ResetDb() throws Exception {
		String resetDbUrl = DataProperties.get("resetDbUrl");
		auxSteps.openUrl(resetDbUrl);
	}
	
	@Test
	public void test1DecisionTreeEvaluation1() {
		User user = DataProvider.getUserEvaluator1();
		String filePath = testDataFolder + "/eval1.csv";
		EvalData evalData = new EvalData(filePath, treeAnswersNumber);	
		desicionTreeEvaluation(user, evalData);
	}

	@Test
	public void test2DecisionTreeEvaluation2() {
		User user = DataProvider.getUserEvaluator2();
		String filePath = testDataFolder + "/eval2.csv";
		EvalData evalData = new EvalData(filePath, treeAnswersNumber);
		desicionTreeEvaluation(user, evalData);
	}

	@Test
	public void test3DecisionTreeReview() {
		User user = DataProvider.getUserReviewer();
		
		userSteps.openMainPage();
		userSteps.loginAsUser(user);
		projectSteps.openProject(projectName);
		projectSteps.openTab("Review");
		reviewSteps.verifyKappa(expectedKappa);
		
	}
	
	private void desicionTreeEvaluation(User user, EvalData evalData) {
		userSteps.openMainPage();
		userSteps.loginAsUser(user);
		projectSteps.openProject(projectName);
		projectSteps.verifyEvaluationTabOpened();
		evalSteps.performDecisionTreeEvaluation(evalData);
	}
	
	@Override
	public String toString() {
		return "TestDecisionTreeProject: " + testDataFolder + ", k = " + expectedKappa;
	}
}
