package requirements;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import objects.User;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import steps.AccessManagementSteps;
import steps.EmailSteps;
import steps.ProjectSteps;
import steps.UserSteps;
import utils.DataProvider;

@RunWith(SerenityRunner.class)  
public class TestUserInvitation {
	
	@Managed                            
    WebDriver driver;

    @Steps                                                                       
    UserSteps userSteps;
	@Steps
	ProjectSteps projectSteps;
    @Steps
    EmailSteps emailSteps;
    @Steps
    AccessManagementSteps accessSteps;
    
	@Test
	public void inviteEvaluatorByVendor() throws Exception {
		
		User vendor = DataProvider.getUserVendor();
		User evaluator = DataProvider.getUserEvaluator1();
		User admin = DataProvider.getUserAdmin();
		String projectName = DataProvider.getProjectForAccessTest();
		
		emailSteps.clearOldMessages();
		
		// Invite user
		userSteps.openMainPage();
		userSteps.loginAsUser(vendor);
		projectSteps.openProject(projectName);
		projectSteps.openUserInvitation();
		accessSteps.addUser(evaluator);
		accessSteps.setEvaluatorRole(evaluator);
		userSteps.logout();
		
		// Accept invitation from email
		String acceptUrl = emailSteps.getAcceptInvitationUrlFromEmail();
		userSteps.openUrl(acceptUrl);
		accessSteps.verifyDialogWithText("Well done! Your account has been successfully linked to a new project. Please login to continue!");
		accessSteps.clickConfirmOK();
		
		// Login as new evaluator and check access to the project
		userSteps.loginAsUser(evaluator);
		projectSteps.openProject(projectName);
		projectSteps.verifyEvaluationTabOpened();
		userSteps.logout();
		
		// Login as vendor and check evaluator status
		userSteps.loginAsUser(vendor);
		projectSteps.openProject(projectName);
		projectSteps.openUserInvitation();
		accessSteps.verifyEvaluatorButtonIsGreen(evaluator.name);
		userSteps.logout();
		
		// Login as admin and delete new evaluator from the project
		userSteps.loginAsUser(admin);
		projectSteps.openProject(projectName);
		projectSteps.openUserInvitation();
		accessSteps.revokeRole(evaluator, "evaluator");
		accessSteps.removeUserFromProject(evaluator);
	}
	
}
