package requirements;

import java.util.Arrays;
import java.util.Collection;

import net.serenitybdd.junit.runners.SerenityParameterizedRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import net.thucydides.junit.annotations.TestData;
import objects.EvalData;
import objects.User;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.WebDriver;

import steps.AuxiliarySteps;
import steps.EvaluationSteps;
import steps.ProjectSteps;
import steps.ReviewSteps;
import steps.UserSteps;
import utils.DataProperties;
import utils.DataProvider;

@RunWith(SerenityParameterizedRunner.class)  
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestRankingProject {

	private final String testDataFolder;
	private final double expectedKappa;
	private final int rankingAnswersNumber = 1;
	private String projectName;

	@TestData                                                   
    public static Collection<Object[]> testData(){
        return Arrays.asList(new Object[][]{
                {"data/ranking 0.888", 0.888}
        });
    }
	    
	public TestRankingProject(String testDataFolder, double expectedKappa) {
		this.testDataFolder = testDataFolder;
		this.expectedKappa = expectedKappa;
		this.projectName = DataProvider.getRankingProject();
	}
	
	@Managed
	WebDriver driver;
	
	@Steps                                                                       
    UserSteps userSteps;
	@Steps 
	EvaluationSteps evalSteps;
	@Steps
	ProjectSteps projectSteps;
	@Steps 
	ReviewSteps reviewSteps;
	@Steps
	AuxiliarySteps auxSteps;
	
	@Test
	public void beforeTestResetDb() throws Exception {
		String resetDbUrl = DataProperties.get("resetDbUrl");
		auxSteps.openUrl(resetDbUrl);
	}

	@Test
	public void test1RankingEvaluation1() {
		User user = DataProvider.getUserEvaluator1();
		String filePath = testDataFolder + "/eval1.csv";
		EvalData evalData = new EvalData(filePath, rankingAnswersNumber);	
		rankingEvaluation(user, evalData);
		userSteps.logout();
	}
	
	@Test
	public void test2RankingEvaluation2() {
		User user = DataProvider.getUserEvaluator2();
		String filePath = testDataFolder + "/eval2.csv";
		EvalData evalData = new EvalData(filePath, rankingAnswersNumber);	
		rankingEvaluation(user, evalData);
		userSteps.logout();
	}
	
	@Test
	public void test3ReviewRanking() {
		User user = DataProvider.getUserReviewer();
		String filePath = testDataFolder + "/review.csv";
		EvalData reviewData = new EvalData(filePath, rankingAnswersNumber);
		
		userSteps.openMainPage();
		userSteps.loginAsUser(user);
		projectSteps.openProject(projectName);
		projectSteps.openTab("Review");
		
		reviewSteps.verifyKappa(expectedKappa);
		reviewSteps.performRankingReview(reviewData);
		reviewSteps.verifyKappa(1.0);
		
		reviewSteps.completeReviewTask();
		userSteps.logout();
	}
	
	private void rankingEvaluation(User user, EvalData evalData) {
		userSteps.openMainPage();
		userSteps.loginAsUser(user);
		projectSteps.openProject(projectName);
		projectSteps.verifyEvaluationTabOpened();
		evalSteps.performRankingEvaluation(evalData);
		evalSteps.confirmWorkIsFinished();
	}
	
	@Override
	public String toString() {
		return "TestRankingProject: " + testDataFolder + ", k = " + expectedKappa;
	}
}
