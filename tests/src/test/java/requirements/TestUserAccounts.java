package requirements;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import steps.AuxiliarySteps;
import steps.EmailSteps;
import steps.UserSteps;
import utils.DataProvider;
import objects.User;

@RunWith(SerenityRunner.class)
public class TestUserAccounts {

	@Managed                            
    WebDriver driver;

    @Steps                                                                       
    UserSteps userSteps;
    @Steps
    EmailSteps emailSteps;
	@Steps
	AuxiliarySteps auxSteps;
    
    @Test
    public void loginUserEvaluator() {
    	// Get test data
    	User user = DataProvider.getUserEvaluator1();
    	
    	//Perform login
    	userSteps.openMainPage();
    	userSteps.loginAsUser(user);
    	userSteps.verifyUserLoggedIn(user.name);
    	userSteps.verifyInfoMessageContains(user.name);
    }
    
    @Test
    public void loginUserReviewer() {
    	// Get test data
    	User user = DataProvider.getUserReviewer();
    	
    	//Perform login
    	userSteps.openMainPage();
    	userSteps.loginAsUser(user);
    	userSteps.verifyUserLoggedIn(user.name);
    	userSteps.verifyInfoMessageContains(user.name);
    }
    
    @Test
    public void testForgotPasswordByUsername() throws Exception {
    	// Get test data
    	User user = DataProvider.getUserForChangePassword();
    	String newPassword = "123";
    	String oldPassword = user.password;
    	
    	// Prepare mailbox
    	emailSteps.clearOldMessages();
    	
    	// Initiate forgot password
    	userSteps.openMainPage();
    	userSteps.clickForgotPassword();
    	userSteps.verifyForgotPasswordVisible();
    	userSteps.inputUserReferenceInForgotPassword(user.name);
    	userSteps.submitForgotPassword(); 
    	
    	// Open forgot url from email
    	String resetPasswordUrl = emailSteps.getResetPasswordUrlFromEmail();
    	userSteps.openUrl(resetPasswordUrl);
    	userSteps.verifyResetPasswordOpened();
    	
    	// Set new password
    	userSteps.inputNewPassword(newPassword);
    	userSteps.confirmNewPassword(newPassword);
    	userSteps.submitNewPassword();
    	
    	// Try to log in with old password (fail expected)
    	userSteps.loginAsUser(user);
    	userSteps.verifyLoginFailed();
    	
    	// Login with new password
    	user.setPassword(newPassword);
    	userSteps.loginAsUser(user);
    	userSteps.verifyUserLoggedIn(user.name);
    	
    	// Change password back in user profile
    	userSteps.changePasswordInProfile(user, newPassword, oldPassword); 	
    }
    
    @Test
    public void testForgotPasswordByEmail() throws Exception {
    	// Get test data
    	User user = DataProvider.getUserForChangePassword();
    	String newPassword = "123";
    	String oldPassword = user.password;
    	
    	// Prepare mailbox
    	emailSteps.clearOldMessages();
    	
    	// Initiate forgot password
    	userSteps.openMainPage();
    	userSteps.clickForgotPassword();
    	userSteps.verifyForgotPasswordVisible();
    	userSteps.inputUserReferenceInForgotPassword(user.email);
    	userSteps.submitForgotPassword(); 
    	
    	// Open forgot url from email
    	String resetPasswordUrl = emailSteps.getResetPasswordUrlFromEmail();
    	userSteps.openUrl(resetPasswordUrl);
    	userSteps.verifyResetPasswordOpened();
    	
    	// Set new password
    	userSteps.inputNewPassword(newPassword);
    	userSteps.confirmNewPassword(newPassword);
    	userSteps.submitNewPassword();
    	
    	// Trying to log in with old password
    	userSteps.loginAsUser(user);
    	userSteps.verifyLoginFailed();
    	
    	// Login with new password
    	user.setPassword(newPassword);
    	userSteps.loginAsUser(user);
    	userSteps.verifyUserLoggedIn(user.name);
    	
    	// Change password back in user profile
    	userSteps.changePasswordInProfile(user, newPassword, oldPassword);
    }
    
    @Test
    public void testSignUpAndActivation() throws Exception {
    	// Get test data
    	User user = DataProvider.getNewUser();
    	
    	// Make sure user doesn't exist
    	auxSteps.deleteUserIfExists(user.name);
    	
    	// Prepare mailbox
    	emailSteps.clearOldMessages();
    	
    	// Register new user
    	userSteps.openMainPage();
    	userSteps.openSignUpForm();
    	userSteps.inputSignUpData(user);
    	userSteps.submitSignUp();
    	userSteps.verifyInfoMessageContains("The instruction for logging in has been sent to your email address.");
    	
    	// Open user activation url from email
    	String activationUrl = emailSteps.getActivationUrlFromEmail();
    	userSteps.openUrl(activationUrl);
    	userSteps.verifyInfoMessageContains("Thanks for registering");
    	
    	// Login as new user
    	userSteps.loginAsUser(user);
    	userSteps.verifyUserLoggedIn(user.name);
    }
}
