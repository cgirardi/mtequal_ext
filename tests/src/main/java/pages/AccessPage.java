package pages;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class AccessPage extends PageObject {

	@FindBy(id = "search")
	public WebElementFacade searchInput;
	
	@FindBy(id = "add")
	public WebElementFacade buttonAdd;
	
	@FindBy(id = "users")
	public WebElementFacade usersTable;

	public void clickNameInAutocomplete(String name) {
		WebElementFacade autocomplete = findBy("//ul[contains(@class, 'ui-autocomplete')]");
		autocomplete.findBy(".//li[contains(.,'" + name + "')]").click();
	}

	public boolean isUserPresentInTable(String name) {
		try {
			usersTable.findBy(".//tr[contains(.,'" + name + "')]");
			return true;
		} catch (NoSuchElementException | TimeoutException e) {
			return false;
		}
	}

	public void clickAccessButton(String userName, String buttonName) {
		getRoleButton(userName, buttonName).click();
	}

	public WebElementFacade getRoleButton(String userName, String buttonName) {
		WebElementFacade userRow = findRowWithUser(userName);
		WebElementFacade roleButton = userRow.findBy(".//a[contains(.,'" + buttonName + "')]");
		return roleButton;
	}

	private WebElementFacade findRowWithUser(String userName) {
		return usersTable.findBy(".//tr[contains(.,'" + userName + "')]");
	}

	public boolean isPopupWithTextVisible(String text) {
		return findBy("//div[contains(@class, 'ajs-dialog')][contains(.,\"" + text + "\")]").isVisible();
	}

	public void clickConfirmOK() {
		findBy("//div[contains(@class, 'ajs-dialog')]//button[contains(.,'OK')]").click();
	}

	public boolean isButtonOrange(String userName, String buttonName) {
		WebElementFacade button = getRoleButton(userName, buttonName);
		return button.getAttribute("class").contains("btn-warning");
	}

	public boolean isButtonGreen(String userName, String buttonName) {
		WebElementFacade button = getRoleButton(userName, buttonName);
		return button.getAttribute("class").contains("btn-success");
	}

	public void clickRemoveUserButton(String userName) {
		WebElementFacade row = this.findRowWithUser(userName);
		row.findBy(".//button[contains(@id, 'remove')]").click();
	}

}
