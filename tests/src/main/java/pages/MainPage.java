package pages;

import org.openqa.selenium.NoSuchElementException;

import utils.DataProperties;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class MainPage extends PageObject {

	public MainPage() {
		this.setDefaultBaseUrl(DataProperties.get("baseUrl"));
	}
	
	@FindBy(id = "user")
	public WebElementFacade usernameInput;
	
	@FindBy(id = "myLogin")
	public WebElementFacade loginTitle;
	
	@FindBy(xpath = "//*[@id='loginform']//input[@name='password']")
	public WebElementFacade passwordInput;
	
	@FindBy(xpath = "//*[@id='loginform']//*[@type='submit']")
	public WebElementFacade submitLoginButton;

	@FindBy(id = "content")
	public WebElementFacade content;

	@FindBy(xpath = "//*[@id='login']//a[contains(.,'Forgot')]")
	public WebElementFacade forgotPasswordLink;
	
	@FindBy(id = "myForgot")
	public WebElementFacade forgotPasswordTitle;
	
	@FindBy(id = "reference")
	public WebElementFacade referenceInput;

	@FindBy(xpath = "//*[@id='forgotform']//*[@type='submit']")
	public WebElementFacade submitForgotPassword;

	@FindBy(xpath = "//*[contains(.,'Reset password')]")
	public WebElementFacade resetPasswordTitle;

	@FindBy(id = "submit")
	public WebElementFacade submitNewPassword;

	@FindBy(xpath = "//div[contains(.,'Warning! The login failed.')]")
	public WebElementFacade errorLoginFailed;

	@FindBy(id = "loginButton")
	public WebElementFacade loginButton;

	@FindBy(xpath = "//*[@id='login']//button[@class='close']")
	public WebElementFacade closeLoginPopupButton;

	@FindBy(xpath = "//a[contains(.,'Sign up')]")
	public WebElementFacade signUpButton;

	@FindBy(id = "mySignup")
	public WebElementFacade signUpTitle;
	
	@FindBy(id = "username")
	public WebElementFacade signUpUsername;
	
	@FindBy(id = "email")
	public WebElementFacade signUpEmail;
	
	@FindBy(id = "password")
	public WebElementFacade newPassword;
	
	@FindBy(id = "password_confirm")
	public WebElementFacade confirmPassword;
	
	@FindBy(id = "old_password")
	public WebElementFacade oldPassword;
	
	@FindBy(xpath = "//*[@id='signup']//input[@value='agree']")
	public WebElementFacade agreeCheckbox;

	@FindBy(id = "signupbutton")
	public WebElementFacade submitSignUpButton;
	
	@FindBy(xpath = "//*[@id='navbar']//a[@title='User settings']")
	public WebElementFacade userSettingsLink;

	@FindBy(xpath = "//*[@id='navbar']//a[contains(.,'Logout')]")
	public WebElementFacade logoutLink;

	@FindBy(xpath = "//*[@id='navbar']//a[contains(.,'User Profile')]")
	public WebElementFacade userProfileLink;

	@FindBy(xpath = "//*[@id='content']//a[contains(.,'Change password')]")
	public WebElementFacade changePasswordTab;
	
	public boolean isWelcomeMessageVisible(String username) {
		try {
			WebElementFacade welcome = findBy("//div[contains(.,'Welcome " + username +"!')]");
			return welcome.isVisible();
		} catch (NoSuchElementException e) {
			return false;
		}
	}

}
