package pages;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.NoSuchElementException;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class EvaluationPage extends PageObject {

	@FindBy(id = "done")
	public WebElementFacade confirmAndGotoNext;
	
	@FindBy(xpath = "//*[@class='ajs-modal']")
	public WebElementFacade updateInProgress;

	@FindBy(id = "dontknow")
	public WebElementFacade dontKnowButton;

	@FindBy(xpath = "//*[@class='ajs-modal'][contains(.,'Your work is finished')]")
	public WebElementFacade dialogWorkFinished;
	
	public List<WebElementFacade> getDecisionTables() {
		return findAll("//*[@id='ajx_content']//table");
	}

	public WebElementFacade getSnippet(String output) {
		return findBy("//*[@id='ajx_content']/div[contains(.,'" + output + "')]");
	}

	public void completeTreeAnswers(WebElementFacade snippet, String[] answers) {
		List<WebElementFacade> evalButtons = snippet.findBy(".//table").thenFindAll(".//select[contains(@class, 'evalbutton')]");
		for (int i=0; i<evalButtons.size(); i++) {
			evalButtons.get(i).selectByVisibleText(answers[i]);
			waitWhileUpdateInProgress();	
		}
	}

	public void chooseScaleAnswer(WebElementFacade snippet, String answer) {
		waitWhileUpdateInProgress();
		WebElementFacade evalButton = snippet.findBy(".//*[@class='evalbutton'][contains(.,'" + answer + "')]");
		if (!isEvalButtonChosen(evalButton))
			evalButton.click();
	}

	public void chooseRankingAnswer(WebElementFacade snippet, String answer) {
		waitWhileUpdateInProgress();
		WebElementFacade evalButton = snippet.findBy(".//*[contains(@class, 'rating-box')]//a[@class='evalbutton'][" + answer + "]");
		if (!isEvalButtonChosen(evalButton))
			evalButton.click();
	}

	private boolean isEvalButtonChosen(WebElementFacade evalButton) {
		return evalButton.getCssValue("border").contains("red");
	}

	private void waitWhileUpdateInProgress() {
		try {
			while (updateInProgress.withTimeoutOf(500, TimeUnit.SECONDS).isPresent())
				waitFor(1000);
		} catch (NoSuchElementException e) {}
	}

}
