package pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class ProjectPage extends PageObject {

	@FindBy(xpath = "//*[@id='navbar']//a[contains(.,'Dashboard')]")
	public WebElementFacade dashboardLink;
	
	@FindBy(xpath = "//*[@id='content']//h3")
	public WebElementFacade contentHeader;

	@FindBy(xpath = "//*[@id='nav-main']//li[contains(@class, 'active')]/*[contains(.,'Evaluation')]")
	public WebElementFacade evaluationTabActive;

	@FindBy(id = "review")
	public WebElementFacade reviewCompleteButton;

	@FindBy(xpath = "//*[@id='content']//a[contains(.,'User Invitation')]")
	public WebElementFacade userInvitationLink;

	@FindBy(id = "sendbutton")
	public WebElementFacade sendReportButton;

	@FindBy(id = "sendReportEmail")
	public WebElementFacade sendReportEmailInput;
	
	@FindBy(xpath = "//*[@id='sendpanel']/input[@value='Send']")
	public WebElementFacade finalSendButton;
	
	public void clickRowWithProject(String projectName) {
		findBy("//*[@id='items']/tr//*[.='" + projectName +"']/..").click();
	}

	public void clickOnTab(String tabName) {
		findBy("//*[@id='nav-main']//a[contains(.,'" + tabName + "')]").click();
	}

	public boolean isEvaluationTabOpened() {
		return evaluationTabActive.isVisible();
	}

}
