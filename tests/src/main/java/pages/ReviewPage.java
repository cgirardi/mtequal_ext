package pages;

import java.util.List;
import java.util.concurrent.TimeUnit;

import objects.EvalRow;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class ReviewPage extends PageObject {

	@FindBy(id = "outputs")
	public WebElementFacade outputsTable;
	
	@FindBy(xpath = "//*[@class='modal-content']")
	public WebElementFacade reviewModal;
	
	@FindBy(id = "kscore")
	public WebElementFacade kscore;
	
	@FindBy(xpath = "//*[@class='modal-content']//table")
	public WebElementFacade answersTable;
	
	@FindBy(id = "econfirm")
	public WebElementFacade confirmButton;

	@FindBy(xpath = "//*[@id='log'][contains(.,'saved')]")
	public WebElementFacade logSaved;
	
	@FindBy(xpath = "//*[@class='ajs-dialog'][contains(.,'Do you really want to mark this task complete?')]")
	public WebElementFacade dialogConfirmCompleteTask;

	@FindBy(xpath = "//*[@id='projectMonitor']//button[contains(.,'Project monitor')]")
	public WebElementFacade buttonProjectMonitor;

	@FindBy(xpath = "//*[@id='tasks']//tr[contains(.,'reviewing')]//input[@class='checktask']/..")
	public WebElementFacade checkboxReviewing;
	
	public int getActualRowsNumber() {
		return getActualRows().size();
	}

	public List<WebElementFacade> getActualRows() {
		return outputsTable.thenFindAll("./tr[@id]");
	}

	public WebElementFacade getRow(int rowIndex) {
		return getActualRows().get(rowIndex);
	}

	public WebElementFacade getRow(EvalRow row) {
		WebElementFacade sourceRow = outputsTable.findBy("./tr[contains(.,'" + row.source +  "')]");
		return sourceRow.findBy("./following-sibling::tr[contains(.,'" + row.output  + "')][1]");
	}

	public void chooseReviewAnswers(String answer) {
		List<WebElementFacade> rows = answersTable.thenFindAll(".//tr");
		for (WebElementFacade row : rows) {
			WebElementFacade answerButton = row.findBy("./td[contains(.,'" + answer + "')]/div");
			if (!isButtonChosen(answerButton)) {
				answerButton.click();
				waitForSavedMessage();
			}
		}
	}

	private void waitForSavedMessage() {
		delay(500);
		withTimeoutOf(10, TimeUnit.SECONDS).find("//*[@id='log'][contains(.,'saved')]");
	}

	private void delay(int millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private boolean isButtonChosen(WebElementFacade button) {
		String border = button.getCssValue("border");
		// format may vary in different browsers
		return border.contains("red") || border.contains("blue") || border.contains("rgb(255, 0, 0)") || border.contains("rgb(0, 0, 255)");
	}
	
}
