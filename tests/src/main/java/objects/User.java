package objects;

public class User {

	public String name;
	public String password;
	public String email;
	public String role;
	
	public User setName(String name) {
		this.name = name;
		return this;
	}
	
	public User setPassword(String password) {
		this.password = password;
		return this;
	}
	
	public User setRole(String role) {
		this.role = role;
		return this;
	}

	public User setEmail(String email) {
		this.email = email;
		return this;
	}
	
	@Override
	public String toString() {
		String result = name;
		if (role != null )
			result = result + " (" + role + ")";
		return result;
	}
}
