package objects;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

public class EvalData {

	private EvalRow[] rows;
	private String filePath;
	
	private final int rowsNumber = 12;
	private int answersNumber;
	
	public EvalRow[] getRows() {
		return rows;
	}
	
	public EvalData(String filePath, int answersNumber)  {
		this.answersNumber = answersNumber;
		rows = new EvalRow[rowsNumber];
		this.filePath = filePath;
		BufferedReader br = null;
		String line = "";
		int index = 0;

		try {
	        br = new BufferedReader(new InputStreamReader(new FileInputStream(filePath), StandardCharsets.UTF_8));
			// the first line is table's head
			line = br.readLine();
			while ((line = br.readLine()) != null) {
				rows[index] = getRowFromCsvString(line);
				index++;
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	private EvalRow getRowFromCsvString(String line) {
		String csvSplitBy = ";";
		String[] cells = line.split(csvSplitBy);
		EvalRow row = new EvalRow(answersNumber);
		row.source = cells[0];
		row.output = cells[1];
		row.dontKnow = (cells[2].contains("1"));
		for (int i=0; i<answersNumber; i++) {
			row.answers[i] = cells[i+3];
		}
		return row;
	}
	
	public String toString() {
		return "Eval data from " + filePath;
	}
	
}
