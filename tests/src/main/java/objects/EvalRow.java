package objects;

public class EvalRow {
	public String source;
	public String output;
	public boolean dontKnow;
	public String[] answers;
	
	public EvalRow(int answersNumber) {
		answers = new String[answersNumber];
	}
	
	public String toString() {
		return "Eval '" + source + "' - '" + output + "'";
	}
}
