package utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

public class DataProperties {

	private static Properties properties;
	
	static {
		properties = new Properties();
		
		InputStream input;
		String filePath = getPropertiesFilePath();
		System.out.println("Loading properties from " + filePath);
		try {
			input = new FileInputStream(filePath);
			properties.load(input);
			input.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static String getPropertiesFilePath() {
		String filePath =  ".//local.properties";
		if (new File(filePath).exists())
			return filePath;
		System.out.println("[Warning] File local.properties not found! Trying to use global.properties");
		filePath = ".//global.properties";
		if (new File(filePath).exists())
			return filePath;
		System.out.println("[Error] File global.properties not found too! Impossible to load properties");
		return null;
	}
	
	public static String get(String key) {
		return properties.getProperty(key);
	}

	public static boolean getBoolean(String key) {
		String value = properties.getProperty(key);
		return value.contains("true");
	}

	public static int getInteger(String key) {
		String value = properties.getProperty(key);
		return Integer.parseInt(value);
	}
	
}