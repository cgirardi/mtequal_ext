package utils;

import java.util.Properties;

import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;

import com.sun.mail.pop3.POP3Store;

public class MailServer {

	private String host;
	private String user;
	private String password;
	
	private Folder folder;
	private POP3Store emailStore;
	private Session session;
	
	public MailServer(String host, String user, String password) throws Exception {
		this.host = host;
		this.user = user;
		this.password = password;
		establishConnection();
		openInboxFolder();
	}
	
	private void openInboxFolder() throws MessagingException {
		folder = emailStore.getFolder("INBOX");
		folder.open(Folder.READ_WRITE);
	}

	private void establishConnection() throws NoSuchProviderException,
			MessagingException {
		session = Session.getDefaultInstance(new Properties());
		emailStore = (POP3Store) session.getStore("pop3s");
		emailStore.connect(host, user, password);
	}
	
	public void deleteAllMessages() throws Exception {
		Message[] messages = folder.getMessages();
		System.out.println("Deleting " + messages.length + " messages...");
		for (int i = 0; i < messages.length; i++) {
			messages[i].setFlag(Flags.Flag.DELETED, true);
		}
		folder.close(true);
		folder.open(Folder.READ_WRITE);
	}

	public Message getLastMessage() throws Exception {	
		Message[] messages = folder.getMessages();
		Message message = messages[messages.length-1];
		return message;
	}
	
	public Message getNewMessage(int timeoutSeconds) throws Exception {
		Message[] messages = folder.getMessages();
		if (messages.length > 0)
			return messages[0];
		folder.close(false);
		System.out.println("Waiting for messages... ");
		for (int i = 0; i < timeoutSeconds; i++) {
			folder.open(Folder.READ_WRITE);
			messages = folder.getMessages();
			if (messages.length > 0) {
				System.out.println("Message received!");
				return messages[0];
			}
			folder.close(false);
			Thread.sleep(1000); // waiting for a second
		}
		System.out.println("Timeout expired! No new messages.");
		return null;
	}
	
}
