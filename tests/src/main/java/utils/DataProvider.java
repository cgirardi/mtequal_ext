package utils;

import objects.User;

public class DataProvider {

	public static User getUserAdmin() {
		return new User()
			.setName(DataProperties.get("adminName"))
			.setPassword(DataProperties.get("adminPassword"))
			.setEmail(DataProperties.get("emailUser"))
			.setRole("admin");
	}
	
	public static User getUserEvaluator1() {
		return new User()
			.setName(DataProperties.get("evaluatorName1"))
			.setPassword(DataProperties.get("evaluatorPassword1"))
			.setEmail(DataProperties.get("emailUser"))
			.setRole("evaluator");
	}

	public static User getUserEvaluator2() {
		return new User()
			.setName(DataProperties.get("evaluatorName2"))
			.setPassword(DataProperties.get("evaluatorPassword2"))
			.setEmail(DataProperties.get("emailUser"))
			.setRole("evaluator");
	}
	
	public static User getUserReviewer() {
		return new User()
			.setName(DataProperties.get("reviewerName"))
			.setPassword(DataProperties.get("reviewerPassword"))
			.setEmail(DataProperties.get("emailUser"))
			.setRole("reviewer");
	}
	
	public static User getUserVendor() {
		return new User()
			.setName(DataProperties.get("vendorName"))
			.setPassword(DataProperties.get("vendorPassword"))
			.setEmail(DataProperties.get("emailUser"))
			.setRole("vendor");
	}

	public static User getNewUser() {
		return new User()
			.setName(DataProperties.get("newUserName"))
			.setPassword(DataProperties.get("newUserPassword"))
			.setEmail(DataProperties.get("newUserEmail"));
	}

	public static User getUserForChangePassword() {
		return getUserAdmin();
	}

	public static String getEmailForReport() {
		return DataProperties.get("emailUser");
	}

	public static String getProjectNameForEvaluationReport() {
		return "test access";
	}

	public static String getProjectForAccessTest() {
		return DataProperties.get("projectTestAccess");
	}

	public static String getScaleProject() {
		return DataProperties.get("projectScale");
	}
	
	public static String getRankingProject() {
		return DataProperties.get("projectRanking");
	}
	
	public static String getTreeProject() {
		return DataProperties.get("projectTree");
	}
}
