# MT-EQuAl extension #

Authors: Christian Girardi (cgirardi@fbk.eu), Claudio Giuliano (giuliano@fbk.eu).

License: Apache 2.0 (https://www.apache.org/licenses/LICENSE-2.0.txt)

Last update: July 2016, 19.


## Requirements

* PHP 5.6+
* Apache 2.4+
* MySQL 5.5+
* Git (optional) 2.5.4+

## Quick start

This installation and configuration steps are specifically built and tested for Ubuntu (14.04).

### INSTALLATION of MySQL, Apache, PHP ###

In order install the required software on Ubuntu run this command:
```
sudo apt-get -y install mysql-server mysql-client apache2 libapache2-mod-php5 php5 php5-cli php5-mysql php5-gd php5-mcrypt php5-sqlite php5-curl php5-json
```

In order to use Apache mod_rewrite you can type the following command in the terminal:
```
sudo a2enmod rewrite
```

Now restart services:
```
sudo /etc/init.d/apache2 restart
/usr/bin/mysqld_safe > /dev/null 2>&1 &
```

or

```
sudo service apache2 restart
sudo service mysql start
```

If you don't use it, please consider the installation of a Apache–MySQL–PHP packages: there are LAMPs (for Linux); WAMPs (for Windows); MAMPs (for Macintosh),...


### INSTALLATION of MT-EQuAl ###

Create a new directory where to install the application, for example:

```
$ mkdir /var/www/mtequal
```

Get the the last version of MT-EQuAl:

* decompress the downloaded file from https://bitbucket.org/cgirardi/mtequal_ext/downloads
* or to use the following git command:

```
git clone https://bitbucket.org/cgirardi/mtequal_ext/ /var/www/mtequal/html/
```

Run the installation script:

```
cd /var/www/mtequal/html/
./INSTALL.sh
```

Link the directory /var/www/mtequal/html/web/ in the Apache htdocs (usually located in /etc/apache2/htdocs/):

```
cd /etc/apache2/htdocs/
ln -s /var/www/mtequal/html/web/ mtequal
```

In order to change some configuration parameters you can edit the file:

```
nano app/config/parameters.yml
```

Finally you can open the web application on your browser at the address:

http://localhost/mtequal/



### APACHE CONFIGURATION ###

Set up the Apache virtual host on Ubuntu.

```
nano /etc/apache2/sites-available/mtequal.conf
```

Add the following information.

```
<VirtualHost *:80>
    ServerName <HOSTNAME>
    #ServerAdmin <ADMIN EMAIL>

    DocumentRoot /var/www/mtequal/html/web
    <Directory /var/www/mtequal/html/web/>
        Options Indexes FollowSymLinks MultiViews
        AllowOverride None
        Order allow,deny
        allow from all
            RewriteEngine On
            RewriteCond %{REQUEST_FILENAME} !-f
            RewriteRule ^(.*)$ /app.php [QSA,L]
    </Directory>
</VirtualHost>
```

Enable the new virtual host:

```
sudo a2ensite mtequal
```

Restart the Apache server:

```
sudo service apache2 restart
```

If the installation and configuration steps are successful, MT-EQuAl is running at http://<HOSTNAME>/.

## Testing

In order to test the application the required software are the following:

* Java 1.8+
* Apache Maven 3.0.4+
* Firefox <= 46.1

After the needed software installation you can run the following script:
```
./TEST.sh
```

Basically, this module will launch a set of tests, in a browser (it uses Firefox by default).

At the end you can check a report page opening the last address showed by TEST.sh script.
