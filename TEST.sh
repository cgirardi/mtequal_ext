#!/bin/bash

if [ -d "tests" ]
then
    cd tests
    echo "loading tests/local.properties ..."
    echo

    if [ -f "local.properties" ]
    then

        counter="$(php ../app/console doctrine:query:sql 'select count(*) from user')"
        if [[ ! $counter =~ 'string(1) "0"' ]]
        then
            echo "WARNING!! The testing needs to initialize the database but it doesn't seem empty."
            read -r -p "Do you wish to reset the database? [Y/n] " response
            if [[ $response =~ ^(Y)$ ]]
            then
                if [ -f "../data/database_test_init.sql" ]
                then
                    echo "Testing database initialization..."

                    while read line
                    do
                        php ../app/console doctrine:query:sql "$line" -q
                    done < ../data/database_test_init.sql
                fi
            else
                echo "Database initialization has been skipped by user."
                echo
                exit
            fi
        fi
        echo "Start testing..."

        # Load config values
        . local.properties

        mvn clean verify

        path="$(pwd)";
        echo
        echo "The result of testing can be checked from a browser at the following address:"
        echo "file://"${path}"/target/site/serenity/index.html"
        echo

        cd - >& /dev/null
    else
        echo "Sorry! The file tests/local.properties doesn't found."
    fi
else
	echo "Sorry! The tests directory doesn't found."
fi
echo