#!/bin/sh

if [ -f .git/config ]; then
    echo "calling repository ..."
    git pull
else
    echo "Sorry! No repository config file found."
    echo
    exit
fi

echo "updating ..."

# uncomment the next line if you would reset the cache files as apache user
#curl "https://mtequaldev.fbk.eu/api/admin/resetcache?secret=<SECRET CODE FROM app/config/parameters.yml";
\rm -rf app/cache/*
php app/console doctrine:schema:update --force

php app/console cache:clear --env=prod
php app/console assets:install

echo "DONE!"