#!/bin/sh

echo "Testing database initialization..."
while read line; do
    php app/console doctrine:query:sql "$line" -q
done < data/database_init.sql

while read line; do
    php app/console doctrine:query:sql "$line" -q
done < data/database_test_init.sql

java -jar bin/selenium-server-standalone-2.47.1.jar & echo $! > /tmp/selenium-mtequal.pid &
sleep 5
php bin/phpunit -v --debug -c app/ -v src/AppBundle/Tests/TestSelenium.php >> test_report.log
kill -9 `cat /tmp/selenium-mtequal.pid`
