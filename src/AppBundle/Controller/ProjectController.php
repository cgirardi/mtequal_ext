<?php
/**
 * Project: mtequal_ext
 * User: cgirardi
 * Date: 11/11/15
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Document;
use AppBundle\Entity\User;
use AppBundle\Repository\AnnotationRepository;
use AppBundle\Repository\CorpusRepository;
use AppBundle\Repository\ProgressRepository;
use AppBundle\Repository\ProjectRepository;
use AppBundle\Repository\RoleRepository;
use AppBundle\Repository\SentenceRepository;
use AppBundle\Repository\UserRepository;
use Doctrine\ORM\Query;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Project;
use Symfony\Component\HttpFoundation\Response;

/**
 * Project controller.
 *
 * @Route("/project")
 */
class ProjectController extends Controller
{
    /**
     * returns the list of projects
     * @Route ("/list", name="project_list", defaults={"_format" = "json"})
     * @return Response
     */
    public function getProjecList()
    {
        if ($this->get("session")->has("mysession")) {
            $userid = $this->get("session")->get("mysession")["userid"];

            /** @var ProjectRepository $repository */
            $repository = $this->getDoctrine()->getRepository('AppBundle:Project');
            $items = $repository->findProject($userid);

            $json = $this->get('serializer')->serialize(array('projects' => $items), 'json');
            return new Response($json);
        }
        return null;
    }


    /**
     * Check the last update of the project
     *
     * @Route("/check", name="project_check", defaults={"_format" = "json"})
     * @Method("GET")
     */
    public function checkAction(Request $request) {
        $data = array("modified" => "0000-00-00");
        $projectSession = $this->get("session")->get("project");
        if ($projectSession != null && isset($projectSession["id"])) {
            $dql = "SELECT p.modified
                    FROM AppBundle:Project AS p
                    WHERE p.id=:pid";
            /** @var Query $query */
            $query = $this->getDoctrine()->getManager()->createQuery($dql);
            $query->setParameter('pid', $projectSession["id"]);

            $result = $query->getResult();
            if (count($result) > 0) {
                $data["modified"] = $result[0]["modified"];
            }
        }

        return new Response(json_encode($data), 200, array('Content-Type' => 'application/json'));
    }

    /**
     * Project progress
     *
     * @Route("/monitor", name="project_monitor")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        /** @var ProgressRepository $repository_progress */
        $repository_progress = $this->getDoctrine()->getRepository("AppBundle:Progress");

        $projectSession = $this->get("session")->get("project");
        $data = array();
        if ($projectSession != null && isset($projectSession["id"])) {
            $userid = null;
        	if ($this->get("session")->has("mysession")) {
        	    $userid = $this->get("session")->get("mysession")["userid"];
        	}
        	$data = $repository_progress->taskReport($projectSession["id"], $userid);
            $data["isEvaluationStarted"] = $repository_progress->isCompletedTask(ProgressRepository::CHECKCORPUS_TASK, $projectSession["id"]);
        }


        //refresh session
        if ($this->get("session")->has("mysession")) {
            $userid = $this->get("session")->get("mysession")["userid"];

            /** @var RoleRepository $role_repository */
            $role_repository = $this->getDoctrine()->getRepository("AppBundle:Role");
            $data["evaluators"] = $role_repository->getUsers($projectSession["id"], $userid);


            /** @var SentenceRepository $sentence_repository */
            $sentence_repository = $this->getDoctrine()->getRepository("AppBundle:Sentence");

            /** @var ProjectRepository $project_repository */
            $project_repository = $this->getDoctrine()->getRepository("AppBundle:Project");


            $array_data = $project_repository->getProject($projectSession["id"]);
            if ($array_data && $projectSession["modified"] != $array_data["modified"]) {
                //get user roles
                $array_data["roles"] = $role_repository->getUserRoles($userid, $projectSession["id"]);

                if (count($array_data["roles"]) > 0) {
                    $array_data["tus"] = $sentence_repository->countTUs($projectSession["id"]);
                    $array_data["targets"] = $sentence_repository->targetLabels($projectSession["id"]);

                    /** @var CorpusRepository $corpus_repository */
                    $corpus_repository = $this->getDoctrine()->getRepository("AppBundle:Corpus");
                    $array_data["corpus"] = $corpus_repository->statsCorpus($projectSession["id"]);

                    /** @var ProgressRepository $progress_repository */
                    $progress_repository = $this->getDoctrine()->getRepository("AppBundle:Progress");
                    $array_data["etasks"] = $progress_repository->enableTasks($projectSession["id"], $userid);

                    $session = $request->getSession();
                    $session->set("project", $array_data);
                }
            }
        }

        return $this->render('project/progress.twig', $data);
    }

    /**
     * Creates a new Project entity.
     *
     * @Route("/new", name="project_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        //reset project session
        $this->get("session")->remove("project");

        return new Response($this->renderView('project/new.twig'));
    }

    /**
     * Update a Project entity.
     *
     * @Route("/update", name="project_update")
     * @return Response
     */
    public function updateAction (Request $request)
    {
        $params = $request->request->all();
        if(!$this->isCsrfTokenValid('authenticate', $params["_csrf_token"])) {
            // invalid token
            return new Response($this->renderView('exception/error-500.twig'));
        }

        if (!array_key_exists("id", $params)) {
            $params["id"] = -1;
        }

        $messagetype = "error";
        $message = "The project has not been created correctly.";

        //get user session info
        $userid = null;
        if ($this->get("session")->has("mysession")) {
            $userid = $this->get("session")->get("mysession")["userid"];
        }
        if ($userid == null) {
            $this->addFlash("warning", "Sorry, but your account doesn't have sufficient privileges to perform this action!");
            return $this->redirectToRoute('admin', $params);
        }

        //check project session info
        if ($this->get("session")->has("project")) {
            $pid = $this->get("session")->get("project")["id"];
            if ($pid != null) {
                if ($params["id"] != -1 && $params["id"] != $pid) {
                    $this->addFlash("warning", "Sorry, but your account doesn't have sufficient privileges to perform this action!");
                    return $this->redirectToRoute('admin', $params);
                } else {
                    $params["id"] = $pid;
                }
            }
        }
        /** @var ProjectRepository $repository */
        $repository = $this->getDoctrine()->getRepository("AppBundle:Project");

        //check if it is using an already exists project name
        if (array_key_exists("name", $params)) {
            $params["name"] = trim($params["name"]);
            $project_same_name = $repository->findOneBy(array("name" => $params["name"], "active" => 1));
            if ($project_same_name && $project_same_name->getId() != $params["id"]) {
                $messagetype = "alert";
                $message = "A project with this name already exists! Please, change the name and save it again.";

                $this->addFlash($messagetype, $message);
                if ($params["id"] == -1) {
                    return new Response($this->renderView('project/new.twig', $params));
                }
                return new Response($this->renderView('default/admin.twig', $params));
            }
        }
        
        /** @var Project $project */
        $project = $repository->createProject($params);
        if ($project) {
            if ($params["id"] > 0) {
            //update an already existing project
                $messagetype = "success";
                $message = "The project has been updated successfully.";
                $this->addFlash($messagetype, $message);
                return $this->redirectToRoute('admin', array("pid" => $params["id"], "view" => $params["view"]));
            } else {
                //create e new project
                $params["id"] = $project->getId();
                if ($userid != null) {
                    /** @var RoleRepository $role_repository */
                    $role_repository = $this->getDoctrine()->getRepository("AppBundle:Role");
                    $role_repository->updateRole($userid, $userid, $params["id"], "owner", "accepted");
                    //$role_repository->updateRole($userid, $userid, $params["id"], "admin", "accepted");

                    //init the progress status
                    /** @var ProgressRepository $progress_repository */
                    $progress_repository = $this->getDoctrine()->getRepository("AppBundle:Progress");
                    $progress_repository->initProgress($params["id"], $userid);

                    $messagetype = "success";
                    $message = "The project has been updated successfully.";
                }
                $this->addFlash($messagetype, $message);
                return $this->redirectToRoute('admin', array('pid' => $project->getId()));

            }
        } else {
            $messagetype = "alert";
            $message = "Sorry, the project has not been saved correctly!";
        }

        $this->addFlash($messagetype, $message);
        return new Response($this->renderView('default/admin.twig', $params));
    }


    /**
     * Import a document to set evalformat or guidelines of a project.
     *
     * @Route("/import", name="project_import")
     * @return Response
     */
    public function importAction (Request $request)
    {
        $params = $request->query->all();
        if (!$this->isCsrfTokenValid('authenticate', $params["_csrf_token"])) {
            // invalid token
            return new Response($this->renderView('exception/error-500.twig'));
        }

        $messagetype = "error";
        $message = "The import wizard failed.";

        //check project session info
        if ($this->get("session")->has("project")) {
            $pid = $this->get("session")->get("project")["id"];
            if (array_key_exists("id", $params)) {
                /** @var Document $document */
                $document = $this->getDoctrine()->getRepository("AppBundle:Document")->importDocument($params["id"], $pid);

                $messagetype = "success";
                $message = "The import has been performed successfully.";

                $this->addFlash($messagetype, $message);
                return $this->redirectToRoute('admin', array('pid' => $pid, "view" => $params["view"]));
            }
        } else {
            $message = "user_without_privileges";
        }
        $this->addFlash($messagetype, $message);
        return $this->redirectToRoute('admin', array("view" => $params["view"]));
    }

    /**
     * set a project as not active
     * @Route("/delete", name="delete_project")
     * @return Response
     */
    public function deleteProject(Request $request)
    {
        $user = $this->get("session")->get("mysession");;
        $project = $this->get("session")->get("project");
        $messagetype = "error";
        $message = "project_delete_error";
        if ($user != null && $project != null) {
            /** @var ProjectRepository $project_repository */
            $project_repository = $this->getDoctrine()->getRepository("AppBundle:Project");
            $permission = $project_repository->can("delete", $project["id"], $user["userid"]);
            if ($permission == 1) {
                if ($project_repository->deleteProject($project["id"])) {
                    $messagetype = "success";
                    $message = "The project has been delete!";
                    $this->get("session")->remove("project");

                    $this->addFlash($messagetype, $message);
                    return $this->redirectToRoute("dashboard");
                }
            } else {
                $message = "user_without_privileges";
            }
        } else {
            $message = "user_without_privileges";
        }
        $this->addFlash($messagetype, $message);
        return new Response($this->renderView("default/admin.twig",
            array("view" => "settings.delete")));
    }


    /**
     * @Route("/users", name="project_users", defaults={"_format" = "json"})
     * @Method("GET")
     */
    public function invitedProjectUsers(Request $request)
    {
        $result = array();
        $userid = $this->get("session")->get("mysession")["userid"];
        $projectSession = $this->get("session")->get("project");
        /** @var RoleRepository $role_repository */
        $role_repository = $this->getDoctrine()->getRepository('AppBundle:Role');
        /** @var UserRepository $user_repository */
        $user_repository = $this->getDoctrine()->getRepository('AppBundle:User');
        if ($projectSession != null && isset($projectSession["id"])) {
            //access panel
            //TODO get all user with the status equals to "accepted" as role for this project
            $result["myroles"] = $projectSession["roles"];
            $result["roles"] = $role_repository->getRoles($projectSession["id"], $userid);
            $result["users"] = array();
            foreach ($result["roles"] as $jUser) {
                /** @var User $user */
                $user = $user_repository->getUserFromID($jUser["inviter"]);
                if ($user) {
                    $result["users"][$jUser["inviter"]] = $user->getUsername();
                }
            }

            /** @var ProgressRepository $repository_progress */
            $repository_progress = $this->getDoctrine()->getRepository('AppBundle:Progress');

            $result["disableinvitation"] = 1;
            if ($repository_progress->isCompletedTask($repository_progress::CHECKCORPUS_TASK, $projectSession["id"])) {
                $result["disableinvitation"] = 0;
            }
        } else {
            //request panel
            $result = $role_repository->getUserRequests($userid);
        }
        return new Response(
          json_encode($result), 200,  array('Content-Type' => 'application/json')
        );
    }

    /**
     * @Route("/export/evalformat", name="evalformat_export")
     * @Method("GET")
     */
    public function exportEvalFormat(Request $request)
    {
        $format = $request->query->get("format");

        $projectSession = $this->get("session")->get("project");
        if ($projectSession != null && isset($projectSession["id"])) {
            /** @var ProjectRepository $repository */
            $repository = $this->getDoctrine()->getRepository('AppBundle:Project');
            /** @var Project $project */
            $project = $repository->find($projectSession["id"]);
            if ($project) {
                if ($format == "html") {
                    $report = "<body><h3>Evaluation format</h3><li>Type: " . $projectSession["type"] . "\n";
                    $report .= "\n<li>Customized values: \n";
                    $evals = json_decode($project->getRanges(), TRUE);
                    if (count($evals) > 0) {
                        foreach ($evals as $eval) {
                            $report .= "<br>- button " . $eval["val"] . ": label \"" . $eval["label"] . "\", color: #" . $eval["color"] . "\n";
                        }
                    } else {
                        $report .= "<i>&lt;not defined yet&gt;</i>";
                    }
                    $report .= "\n<li>Include reference: ";
                    if ($project->getRequiref()) {
                        $report .= "YES";
                    } else {
                        $report .= "NO";
                    }
                    $report .= "</body>";

                    /** @var $response Response */
                    $response = new Response();
                    $response->headers->set('Cache-Control', 'public, must-revalidate, post-check=0, pre-check=0');
                    $response->headers->set('Expires', '-1');
                    $response->headers->set('Pragma', 'public');
                    $response->headers->set('Content-Type', 'text/html;charset=utf-8');
                    $response->headers->set('Content-Disposition', 'attachment; filename=' . $project->getName() . '-evaluation_format.html');
                    $response->headers->set('Content-Length', strlen($report));
                    $response->setContent($report);

                    return $response;
                } else if ($format == "json") {
                    $data = array("evalformat"
                    => array("type" => $project->getType(),
                        "ranges" => json_decode($project->getRanges(), true),
                        "requiref" => $project->getRequiref()));
                    $json = json_encode($data, JSON_PRETTY_PRINT);

                    $response = new Response();
                    $response->headers->set('Pragma', 'public');
                    $response->headers->set('Cache-Control', 'public, must-revalidate, post-check=0, pre-check=0');
                    //$response->headers->set('Cache-Control: private',false); // required for certain browsers
                    $response->headers->set('Expires', '-1');
                    $response->headers->set('Content-Type', 'application/json');
                    $response->headers->set('Content-Transfert-Encoding', 'binary');
                    $response->headers->set('Content-Disposition', 'attachment; filename=' . $project->getName() . '-evaluation_format.json');
                    $response->headers->set('Content-Length', strlen($json));
                    $response->setContent($json);
                    return $response;
                }
            }
        }
        $this->addFlash("error", "Sorry, this action is not valid!");
        return $this->redirectToRoute('admin', array('view' => 'settings.export'));
    }

    /**
     * @Route("/export/guidelines", name="guidelines_export")
     * @Method("GET")
     */
    public function exportGuidelines(Request $request)
    {
        $format = $request->query->get("format");

        $projectSession = $this->get("session")->get("project");
        if ($projectSession != null && isset($projectSession["id"])) {
            /** @var ProjectRepository $repository */
            $repository = $this->getDoctrine()->getRepository('AppBundle:Project');
            /** @var Project $project */
            $project = $repository->find($projectSession["id"]);

            if ($project) {
                if ($format == "html") {
                    $report="<body><h3>Project specifications</h3><li>Randomize target presentation order: ";
                    if ($project->getRandout()) {
                        $report.="YES";
                    } else {
                        $report.="NO";
                    }
                    $report.="\n<li>Keep evaluators anonymous to judges/reviewers: ";
                    if ($project->getBlindrev()) {
                        $report.="YES";
                    } else {
                        $report.="NO";
                    }
                    $report.="\n<li>Alert on don't know answers: ";
                    if ($project->getMaxdontknow() >= 0) {
                        $report.="active on more than ".$project->getMaxdontknow();
                    } else {
                        $report.="disabled";
                    }
                    $report.="\n<li>Intra-annotator agreement: ";
                    if ($project->getIntragree() > 0) {
                        $report.=$project->getIntragree() . "% of TUs to be evaluated twice by the evaluator";
                    } else {
                        $report.="disabled";
                    }
                    $report.="\n<li>Pooling: ";
                    if ($project->getPooling() != "") {
                        $report.= "active using the ".$project->getPooling()." algorithm";
                    } else {
                        $report.="disabled";
                    }

                    $report.="<br><h3>Evaluation instructions</h3>";
                    if ($project->getInstructions() != "") {
                        $report.=$project->getInstructions();
                    } else {
                        $report.="<i>&lt;empty&gt;</i>";
                    }
                    $report.="</body>";

                    /** @var $response Response */
                    $response = new Response();
                    $response->headers->set('Cache-Control', 'public, must-revalidate, post-check=0, pre-check=0');
                    $response->headers->set('Expires', '-1');
                    $response->headers->set('Pragma', 'public');
                    $response->headers->set('Content-Type', 'text/html;charset=utf-8');
                    $response->headers->set('Content-Disposition', 'attachment; filename='.$project->getName().'-guidelines.html');
                    $response->headers->set('Content-Length', strlen($report));
                    $response->setContent($report);
                    return $response;
                } else if ($format == "json") {
                    $data = array("guidelines" => array(
                      "specification"
                      => array("randout" => $project->getRandout(),
                        "blindrev" => $project->getBlindrev(),
                        "maxdontknow" => $project->getMaxdontknow(),
                        "intragree" => $project->getIntragree() / 100,
                        "pooling" => $project->getPooling()
                      ),
                      "instructions" => $project->getInstructions()));
                    $json = json_encode($data, JSON_PRETTY_PRINT);

                    $response = new Response();
                    $response->headers->set('Pragma', 'public');
                    $response->headers->set('Cache-Control', 'public, must-revalidate, post-check=0, pre-check=0');
                    //$response->headers->set('Cache-Control: private',false); // required for certain browsers
                    $response->headers->set('Expires', '-1');
                    $response->headers->set('Content-Type', 'application/json');
                    $response->headers->set('Content-Transfert-Encoding', 'binary');
                    $response->headers->set('Content-Disposition', 'attachment; filename='.$project->getName().'-guidelines.json');
                    $response->headers->set('Content-Length', strlen($json));
                    $response->setContent($json);
                    return $response;
                }
            }


        }
        $this->addFlash("error", "Sorry, this action is not valid!");
        return $this->redirectToRoute('admin', array('view' => 'settings.export'));
    }
}
