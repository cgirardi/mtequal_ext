<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Repository\AnnotationRepository;
use AppBundle\Repository\SentenceRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Sentence;
use AppBundle\Form\SentenceType;

/**
 * Sentence controller.
 *
 * @Route("/sentence")
 */
class SentenceController extends Controller
{
    /**
     * Lists all Sentence entities.
     *
     * @Route("/", name="sentence_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        if ($this->get("session")->has("project") && $this->get("session")->has("mysession")) {
            $array_data = array();
            //$userid = $this->get("session")->get("mysession")["userid"];
            $projectid = $this->get("session")->get("project")["id"];

            /** @var AnnotationRepository $repository_annotation */
            $repository_annotation = $this->getDoctrine()->getRepository('AppBundle:Annotation');
            $array_data["done"] = $repository_annotation->listOfDone($projectid);

            /** @var SentenceRepository $repository_sentence */
            $repository_sentence = $this->getDoctrine()->getRepository('AppBundle:Sentence');
            if ($request->query->has("corpus") && trim($request->query->get("corpus")) != "") {
                $array_data["corpus"]=trim($request->query->get("corpus"));
                $array_data["sentences"] = $repository_sentence->findBy(array("corpus" => $array_data["corpus"], "project" => $projectid), array("simto" => "ASC",  "num" => "ASC", "isintra" => "ASC"));
            } else {
                $array_data["sentences"] = $repository_sentence->findBy(array("project" => $projectid), array("simto" => "ASC", "num" => "ASC", "isintra" => "ASC"));
            }
            $array_data["tus"] = $repository_sentence->countTUs($projectid);
            $array_data["targets"] = $repository_sentence->targetLabels($projectid);
            return $this->render('listing/sentence.twig', $array_data);
        }
        return $this->redirectToRoute('admin');
    }

    /**
     * Creates a new Sentence entity.
     *
     * @Route("/new", name="sentence_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $sentence = new Sentence();
        $form = $this->createForm('AppBundle\Form\SentenceType', $sentence);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($sentence);
            $em->flush();

            return $this->redirectToRoute('sentence_show', array('id' => $sentence->getId()));
        }

        return $this->render('sentence/new.twig', array(
            'sentence' => $sentence,
            'form' => $form->createView(),
        ));
    }


    /**
     * Deletes a Sentence entity.
     *
     * @Route("/{id}", name="sentence_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Sentence $sentence)
    {
        $form = $this->createDeleteForm($sentence);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($sentence);
            $em->flush();
        }

        return $this->redirectToRoute('sentence_index');
    }

    /**
     * Creates a form to delete a Sentence entity.
     *
     * @param Sentence $sentence The Sentence entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Sentence $sentence)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('sentence_delete', array('id' => $sentence->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
