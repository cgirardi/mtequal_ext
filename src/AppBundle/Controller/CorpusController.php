<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Project;
use AppBundle\Entity\Sentence;
use AppBundle\Importing\FileValidator;
use AppBundle\Repository\AnnotationRepository;
use AppBundle\Repository\CorpusRepository;
use AppBundle\Repository\RoleRepository;
use AppBundle\Repository\SentenceRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Corpus;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use ZipArchive;


/**
 * Corpus controller.
 *
 * @Route("/corpus")
 */

class CorpusController extends Controller
{

  public function getSession ($key)
  {
    /** @var Session $session */
    $session = $this->get("session");
    if ($session) {
      if ($session->has($key)) {
        return $session->get($key);
      }
    }
    return null;
  }

  /**
   * Lists all Corpus entities.
   *
   * @Route("/", name="corpus_index")
   * @Method("GET")
   */
  public function indexAction(Request $request)
  {
    $session = $this->getSession("project");
    if ($session) {
      /** @var CorpusRepository $repository_corpus */
      $repository_corpus = $this->getDoctrine()->getRepository('AppBundle:Corpus');
      $files = $repository_corpus->findBy(array("project" => $session["id"]), array('date' => 'DESC'));

      return $this->render('listing/corpus.twig', array(
        'files' => $files
      ));
    }
    return $this->redirectToRoute('admin');
  }


  /**
   * Creates a new Corpus entity.
   *
   * @Route("/new", name="corpus_new")
   * @Method("POST")
   */
  public function newAction(Request $request)
  {
    if ($request->getMethod() == 'POST') {
      if(!$this->isCsrfTokenValid('authenticate', $request->query->get("_csrf_token"))) {
        // invalid token
        return new Response($this->renderView('exception/error-500.twig'));
      }

      if ($request->query->has("pid")) {
        $projectId = $request->query->get("pid");
        /** @var UploadedFile $upfile */
        //$upfile = $request->files->get('Filedata');
        $upfile = $request->files->get('upfile');

        if ($upfile instanceof UploadedFile) {
          $filename = $upfile->getClientOriginalName();
          /** @var CorpusRepository $repository_corpus */
          $repository_corpus = $this->getDoctrine()->getRepository('AppBundle:Corpus');
          /** @var Corpus $corpus */
          $corpus = $repository_corpus->findCorpus($filename, $projectId);
          if ($corpus != null && $corpus->getSentences() > 0) {
            return new Response(json_encode(
              array("success" => false,
                    "filename" => $filename,
                    "alert" => "<li>The file <label>$filename</label> is already present!",
                    "suggest" => $this->get('translator')->trans ("mess#delete_file_before_upload"))));
          } else {
            /** @var FileValidator $fileValidator */
            $fileValidator = $repository_corpus->validateFile($upfile, $projectId);

            if ($fileValidator != null) {
              if (!$fileValidator->isWrong) {
                $targetfile = self::backupFile($upfile, $projectId);
                if ($targetfile == null) {
                  array_push($fileValidator->message, "ERROR: Failed to backup file on the server!");
                  $fileValidator->isWrong = true;
                } else {
                  $fileValidator->uploadFile = new UploadedFile($targetfile, $fileValidator->origFilename);
                }
              }

              if ($corpus == null) {
                $corpus = $repository_corpus->newCorpus($fileValidator->uploadFile,
                  $fileValidator->origFilename, $projectId, -1, implode("\n", $fileValidator->message));
              } else {
                $corpus->setFilepath($fileValidator->uploadFile);
                $corpus->setErrors(implode("\n", $fileValidator->message));
                $corpus->setDate(new \DateTime("now"));
                $em = $this->getDoctrine()->getManager();
                $em->persist($corpus);
                $em->flush();

                //update the modified time of the project
                $em->getRepository('AppBundle:Project')->updateModifiedTime($projectId);
              }

              $response = new Response(json_encode(
                array('success' => true,
                  'corpusid' => $corpus->getId(),
                  'sentnum' => 0)));
              //$response->headers->set('Content-Type', 'application/json; charset=UTF-8');
              return $response;
            } else {
              $response = new Response(json_encode(
                array('success' => false,
                  'alert' => "fileValidator is null or wrong! " . implode("\n", $fileValidator->message))));
              return $response;
            }
          }
        }
      }
    }
    /*$this->addFlash(
      'error',
      'Uploading file failed!'
    );*/
    $response = new Response(json_encode(
      array('success' => false,
        'alert' => 'Uploading file failed!')));
    return $response;
  }

  /**
   * check the progress of corpus storing
   * @Route("/parse", name="corpus_parse")
   * @Method("GET")
   */
  public function parseFile (Request $request) {
    $sentences = 0;
    $message = "Parsing file failed";
    $success = false;
    if ($request->query->has("id")) {
      /** @var CorpusRepository $repository_corpus */
      $repository_corpus = $this->getDoctrine()->getRepository('AppBundle:Corpus');
      /** @var Corpus $corpus */
      $corpus = $repository_corpus->getCorpusFromID($request->query->get("id"));

      if ($corpus) {
        /** @var Corpus $corpus */
        $sentences = $repository_corpus->addFileData($corpus);
        if ($sentences > 0) {
          $success = true;
          $message = "";
          //$message = $corpus->getErrors();

          //update session project info
          $session = $request->getSession();
          $array_data = $session->get("project");
          $array_data["corpus"] = $repository_corpus->statsCorpus($corpus->getProject());

          /** @var SentenceRepository $repository_sentence */
          $repository_sentence = $this->getDoctrine()->getRepository("AppBundle:Sentence");
          $array_data["tus"] = $repository_sentence->countTUs($corpus->getProject());
          $array_data["targets"] = $repository_sentence->targetLabels($corpus->getProject());

          $session->set("project", $array_data);
        }
      }
    }
    //$sentences = self::processFile($project, $fileValidator);

    $response = new Response(json_encode(
      array('success' => $success,
        'sentences' => $sentences,
        'message' => $message)));
    //$response->headers->set('Content-Type', 'application/json; charset=UTF-8');
    return $response;
  }


  /**
   * check the progress of corpus storing
   * @Route("/check", name="corpus_check")
   * @Method("GET")
   */
  public function checkFile (Request $request) {
    if ($request->query->has("id")) {
      $result = self::getStatus($request->query->get("id"));

      if ($result) {
        return new Response(json_encode(
          array("success" => true,
            "corpusid" => $request->query->get("id"),
            "sentnum" => $result[0]["num"],
            "lastupdate" => $result[0]["lastupdate"])));
      }
    }

    return new Response(json_encode(
        array("success" => false,
          "message" => "Import corpus failed!")));
  }

  /**
   * Download corpus.
   *
   * @Route("/download", name="corpus_download")
   * @Method("GET")
   */
  public function downloadAction (Request $request)
  {
    $messagetype = "error";
    $message = "Failed zip download!";
    $session = $this->getSession("project");
    if ($session) {
      $pid = $session["id"];

      $zipName = '/tmp/mtequal_'.$pid.'_orig-'.time().'.zip';

      $zip = new \ZipArchive();
      if ($zip->open($zipName, \ZipArchive::CREATE) !== TRUE) {
        $message = "ERROR! Sorry, zip archive creation failed.";
      } else {
        $target_path = $this->container->getParameter('uploaded_files_dir');
        $target_path .= $pid . DIRECTORY_SEPARATOR;
        //$message .= "<br>ADDED: $target_path<br>";

        if (file_exists($target_path)) {
          $files = scandir($target_path);
          //var_dump($files);
          //unset($files[0],$files[1]);
          foreach ($files as $file) {
            if (!preg_match('/^\\..*/',$file)) {
              $zip->addFile($target_path . DIRECTORY_SEPARATOR .$file, $file);
            }
            //$message .= "ADDED: $file<br>";
          }
          $zip->close();

          //$message .= "<br>TO: ".realpath($zipName) . " [".filesize($zipName)."Kb]";
          if (file_exists($zipName)) {

            /** @var $response Response */
            $response = new Response();
            $response->headers->set('Cache-Control', 'public, must-revalidate, post-check=0, pre-check=0');
            $response->headers->set('Expires', '-1');
            $response->headers->set('Pragma', 'public');
            $response->headers->set('Content-Type', 'application/zip');
            $response->headers->set('Content-Transfert-encoding', 'binary');
            $response->headers->set('Content-Disposition', 'attachment; filename='.basename($zipName));
            $response->headers->set('Content-Length', filesize($zipName));
            $response->setContent(file_get_contents($zipName));
            unlink($zipName);

            //$this->addFlash($messagetype, $message);
            return $response;
          } else {
            $message .= " The zip archive hasn't been created correctly!";
          }
        }
      }
    } else {
      $message = "Sorry, this action is not valid!";
    }

    $this->addFlash($messagetype, $message);
    return $this->redirectToRoute('admin', array('view' => 'settings.export'));
  }

  /**
   * Export annotations.
   *
   * @Route("/export", name="evaluation_export")
   * @Method("GET")
   */
  public function exportAction (Request $request) {
    if ($request->query->has("format")) {
      $format = $request->query->get("format");
      $project = $this->getSession("project");
      if ($project) {
        $user = $this->getSession("mysession");
        if ($user) {
          /** @var AnnotationRepository $repository_annotation */
          $repository_annotation = $this->getDoctrine()->getRepository("AppBundle:Annotation");
          return $repository_annotation->exportEvaluation($project["id"], $project["type"],$user["userid"], $format);
        } else {
          $message = "Sorry! You do not have enough permissions to download.";
        }
      } else {
        $message = "ERROR! No project found.";
      }
    } else {
      $message = "WARNING! The requested format is not valid!";
    }

    $response = new Response($message, 404);

    return $response;
  }


  /**
   * Export annotations.
   *
   * @Route("/source_export", name="source_export")
   * @Method({"GET", "POST"})
   */
  public function exportSourceAction (Request $request)
  {
    $messagetype = "error";
    $message = "An error occurred during export data!";

    if ($request->query->has("format")) {
      $format = $request->query->get("format");
      $project = $this->getSession("project");
      if ($project) {
        $result = "";

        /** @var Sentence $sentence */
        $sources = $this->getDoctrine()->getRepository('AppBundle:Sentence')->findBy(array("project" => $project["id"], "type" => "source"));
        $hashSource = [];
        foreach ($sources as $sentence) {
          $hashSource[$sentence->getId()] = $sentence->getText();
          $result .= $sentence->getId(). "," . $sentence->getText() . "\n";
        }

        if (trim($result) != "") {
          $zipName = '/tmp/mtequal_prj-'.$project["id"].'_eval-'.time().".zip";

          $zip = new \ZipArchive();
          if ($zip->open($zipName, \ZipArchive::CREATE) !== TRUE) {
            $message = "ERROR! Sorry zip archive creation failed.";
          } else {
            if ($format == "csv") {
              $zip->addFromString("mtequal_prj-".$project["id"]."_evdata.csv", $result);
              $zip->close();

              if (file_exists($zipName)) {
                /** @var $response Response */
                $response = new Response();
                $response->headers->set('Cache-Control', 'public, must-revalidate, post-check=0, pre-check=0');
                $response->headers->set('Expires', '-1');
                $response->headers->set('Pragma', 'public');
                $response->headers->set('Content-Type', 'application/zip');
                $response->headers->set('Content-Transfert-encoding', 'binary');
                $response->headers->set('Content-Disposition', 'attachment; filename='.basename($zipName));
                $response->headers->set('Content-Length', filesize($zipName));
                $response->setContent(file_get_contents($zipName));
                unlink($zipName);

                //$this->addFlash($messagetype, $message);
                return $response;
              } else {
                $message .= "The zip archive hasn't been created correctly!";
              }
            }
          }
        } else {
          $message = "No data found!";
        }
      }

      $this->addFlash($messagetype, $message);
      return $this->redirectToRoute('admin', array('view' => 'settings.export'));
    }
  }


  /**
   * Deletes a Corpus entity.
   *
   * @Route("/delete", name="corpus_delete")
   * @Method("GET")
   */
  public function deleteAction(Request $request)
  {
    $flist = $request->query->get("flist");
    $countFile = 0;
    $deletedFile = 0;

    if ($flist) {
      $fileids = explode(",", trim($flist));
      if (count($fileids) > 0) {
        /** @var CorpusRepository $repository_corpus */
        $repository_corpus = $this->getDoctrine()->getRepository('AppBundle:Corpus');
        foreach ($fileids as $fileid) {
          $countFile++;
          /** @var Corpus $corpus */
          $corpus = $repository_corpus->findOneBy(array("id" => $fileid));

          if ($corpus) {
            if ($repository_corpus->deleteCorpus($fileid)) {
              //update session project info
              $session = $request->getSession();
              $array_data = $session->get("project");
              $array_data["corpus"] = $repository_corpus->statsCorpus($corpus->getProject());

              /** @var SentenceRepository $repository_sentence */
              $repository_sentence = $this->getDoctrine()->getRepository("AppBundle:Sentence");
              $array_data["tus"] = $repository_sentence->countTUs($corpus->getProject());
              $array_data["targets"] = $repository_sentence->targetLabels($corpus->getProject());

              $session->set("project", $array_data);

              $deletedFile++;
            } else {
              $this->addFlash(
                'warning',
                'An error occurred removing the original file ' . $corpus->getFilename() . '!');
            }
          }
        }
      }
    }
    if ($countFile == $deletedFile) {
      if ($countFile == 1) {
        $this->addFlash(
          'success',
          'Deleting file has been performed successfully'
        );
      } else if ($countFile > 1) {
        $this->addFlash(
          'success',
          'Deleting files has been performed successfully'
        );
      }
    } else {
      $this->addFlash(
        'success',
        'Deleting $deletedFile/$countFile files has been performed successfully'
      );
    }
    return $this->redirectToRoute('admin', array('view' => 'settings.import'));
 }

  /**
   * Creates a form to delete a Corpus entity.
   *
   * @param Corpus $corpus The Corpus entity
   *
   * @return \Symfony\Component\Form\Form The form
   */
  private function createDeleteForm(Corpus $corpus)
  {
    return $this->createFormBuilder()
      ->setAction($this->generateUrl('corpus_delete', array('id' => $corpus->getId())))
      ->setMethod('DELETE')
      ->getForm();
  }


  public function ___processFile(Project $project, FileValidator $file)
  {
    $filepath = $file->uploadFile;
    $filename = $file->origFilename;
    /** @var Corpus $corpus */
    $corpus = null;
    $totSentences = 0;
    if (preg_match("/\\.zip$/", $filename)) {
      /** @var CorpusRepository $repository_corpus */
      $repository_corpus = $this->getDoctrine()->getRepository('AppBundle:Corpus');
      $corpus = $repository_corpus->newCorpus($filepath,$filename,$project->getId(),-1,"");
      /** @var \ZipArchive $zip1 */
      $zip1 = new \ZipArchive;
      $extract1 = $zip1->open($filepath);
      if ($extract1 === TRUE) {
        //Extract the archive contents
        $zip1->extractTo(dirname($filepath));
        for ($i = 0; $i < $zip1->numFiles; $i++) {
          $f = dirname($filepath) . DIRECTORY_SEPARATOR . $zip1->getNameIndex($i);
          if (file_exists($f) && is_file($f) && preg_match("/__MACOSX/\\.", $f)) {
            $sentences = $repository_corpus->addFileData($corpus);
            if ($sentences > 0) {
              $totSentences += $sentences;
            } else {
              //TODO: get back alert!!
              $corpus->setErrors($corpus->getErrors() . "WARNING: No valid data found in the file " . $zip1->getNameIndex($i) ."\n");
            }
          }
        }
        $zip1->close();
      } else {
        $this->addFlash(
          'error',
          "Failed to open zip file (code: $extract1)."
        );
      }
    }
    return $totSentences;
  }


  /**
   * Get the number of already stored data and the last update time
   * @param $corpusId
   */
  public function getStatus ($corpusId) {
    $em = $this->getDoctrine()->getManager();

    $dql = "SELECT COUNT(s.corpus) AS num, s.updated AS lastupdate
        FROM AppBundle:Sentence AS s
        WHERE s.corpus=:id AND s.type='source'
        ORDER BY s.updated ASC";
    /** @var Query $query */
    $query = $em->createQuery($dql);
    $query->setParameter('id', $corpusId);

    return $query->getResult();
  }


  private function backupFile(UploadedFile $file, $projectId)
  {
    $target_path = $this->container->getParameter('uploaded_files_dir');
    if ($target_path != null) {
      $target_path .= $projectId . DIRECTORY_SEPARATOR;
      if (!file_exists($target_path)) {
        mkdir($target_path, 0777);
      }

      $target_path .= $file->getClientOriginalName();

      move_uploaded_file($file, $target_path);
      return $target_path;
    }
    return null;
  }

  private function downloadOriginalFile($projectId)
  {
    $target_path = $this->container->getParameter('uploaded_files_dir');
    if ($target_path != null) {
      $target_path .= DIRECTORY_SEPARATOR . $projectId . DIRECTORY_SEPARATOR;

      $date = date('Ymd_his', time());

      if (file_exists($target_path)) {
        $filezip = $target_path . "mtequal_" . $projectId . "-" . $date . "-uploaded.zip";

        $zip = new ZipArchive();
        if ($zip->open($filezip, ZIPARCHIVE::CREATE) !== TRUE) {
          print "ERROR! Sorry ZIP creation failed.";
        }
        $files = scandir($target_path);
        //var_dump($files);
        //unset($files[0],$files[1]);
        foreach ($files as $file) {
          #print "ADD to zip: $file<br>";
          if ($file != "." && $file != ".." && !preg_match('/^\\.*/',$file)) {
            $zip->addFile($target_path . $file, "mtequal-origfile_" . $projectId . "-" . $date . DIRECTORY_SEPARATOR . $file);
          }
        }
        $zip->close();

        if (file_exists($filezip)) {
          #print $filezip . " (" . file_exists($filezip) .")";
          readfile($filezip);
          unlink($filezip);
        }
        //deleteDirectory($target_path);
        exit(0);
      }
    } else {

    }
  }
}
