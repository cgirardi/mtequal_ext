<?php
/**
 * User: cgirardi
 * Date: 04/10/15
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Role;
use AppBundle\Entity\User;
use AppBundle\Repository\AnnotationRepository;
use AppBundle\Repository\RoleRepository;
use AppBundle\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class UserController extends Controller
{
  /**
   * @Route("/user/login", name="app_user_login")
   */
  public function loginAction(Request $request)
  {
    if ($this->get("session")->has("mysession")) {
      return $this->redirectToRoute('admin');
    }
    return new Response($this->renderView('default/login.twig',
      array("view" => "login",
        'username' => $request->query->get("username"),
        'email' => $request->query->get("email")
      )));
  }

  /**
   * @Route("/user/verify", name="app_user_verify")
   */
  public function verifyAction(Request $request)
  {
    $message = "Warning! E-mail validation failed.";
    $messagetype = "error";

    $email = $request->query->get("email");
    $hash = $request->query->get("hash");
    /** @var UserRepository $repository */
    $repository = $this->getDoctrine()->getRepository("AppBundle:User");

    /** @var User $user */
    $user = $repository->findOneBy(array('hash' => $hash, 'email' => $email));
    if (!$user) {
      $message = "Invalid or expired hash.";
    } else {
      //set user as active and update verified time
      if ($repository->verifyUser($email, $hash)) {
        $message = "Thanks for registering! You can now log in.";
        $messagetype = "success";
      }
    }
    $this->addFlash(
      $messagetype,
      $message
    );
    return new Response($this->renderView('default/login.twig', array('email' => $email)));
  }

  /**
   * @Route("/user/check", name="app_user_check")
   * @Method("POST")
   */
  public function checkAction(Request $request)
  {
    $data = array_map("strip_tags", $request->request->all());

    $message = "Warning! The login failed.";
    $messagetype = "error";
    /** @var UserRepository $repository */
    $repository = $this->getDoctrine()->getRepository("AppBundle:User");
    if (array_key_exists('user', $data)) {
      if (array_key_exists('password', $data)) {

        //check username and password and verified (not null)
        $query = $repository->createQueryBuilder('users')
          ->where('(users.username = :username OR users.email = :username)')
          ->andWhere('users.password = :password')
          ->andWhere('users.verified IS NOT NULL')
          ->andWhere('users.active = 1')
          ->setParameters(array('username' => trim($data['user']), 'password' => md5(trim($data['password']))))
          ->getQuery();

        $result = $query->getResult();
        if ($result && count($result) > 0) {
          //update logged time
          /** @var User $user */
          $user = $result[0];
          if ($repository->updateUser($user->getId(), array("logged" => new \DateTime("now")))) {
            $message = "Welcome " . $user->getUsername() . "!";
            $messagetype = "success";

            //TODO: remove lines when the legacy code wont be needed
            $request->getSession()->set("mysession",
              array("userid" => $user->getId(),
                "username" => $user->getUsername(),
                "firstname" => $user->getFirstname(),
                "lastname" => $user->getLastname(),
                "affiliation" => $user->getAffiliation(),
                "email" => $user->getEmail(),
                "roles" => $user->getRoles(),
                "access_token" => $user->getApiKey(),
                "sessionid" => session_id()));

            $this->addFlash(
              $messagetype,
              $message
            );
            return $this->redirectToRoute('dashboard');
          }
        }
      }
    } else {
      $data['user'] = "";
    }

    $this->addFlash(
      $messagetype,
      $message
    );

    return new Response($this->renderView('default/login.twig', array("email" => $data['user'])));
  }

  /**
   * @Route("/user/logout", name="app_user_logout")
   */
  public function logoutAction(Request $request)
  {
    //$request->getSession()->set("mysession", array());
    //$request->getSession()->set("logged", false);
    //$this->get("session")->remove('logged');
    if ($this->get("session")->has("mysession")) {
      $this->get("session")->remove('mysession');
    }
    $this->get("session")->clear();
    //$request->getSession()->clear();
    return $this->redirectToRoute('app_user_login');
  }

  /**
   * @Route("/user/refresh_token", name="app_refresh_token")
   * @Method("GET")
   */
  public function refreshToken(Request $request)
  {
    $params = $request->query->all();
    if(!$this->isCsrfTokenValid('authenticate', $params["_csrf_token"])) {
      // invalid token
      return new Response($this->renderView('exception/error-500.twig'));
    }
    $session = $this->get("session");
    $message = "Sorry, access token generation failed!";
    $messagetype = "error";

    /** @var UserRepository $repository */
    $repository = $this->getDoctrine()->getRepository('AppBundle:User');
    /** @var User $user */
    if ($repository->updateUser($session->get("mysession")["userid"], $params)) {
      $user = $repository->getUser(array('id' => $session->get("mysession")["userid"]));

      $this->get("session")->set("mysession",
        array("userid" => $user->getId(),
          "username" => $user->getUsername(),
          "firstname" => $user->getFirstname(),
          "lastname" => $user->getLastname(),
          "affiliation" => $user->getAffiliation(),
          "email" => $user->getEmail(),
          "roles" => $user->getRoles(),
          "access_token" => $user->getApiKey(),
          "sessionid" => session_id()));
      $message = "A new access token has been generated successfully.";
      $messagetype = "success";
    }

    $this->addFlash(
      $messagetype,
      $message
    );

    return $this->redirectToRoute('admin', array('view' => 'account.details'));
  }

  /**
   * @Route("/user/update", name="app_user_update")
   */
  public function updateUser(Request $request)
  {
    $message = "Sorry! Updating the user information failed.";
    $messagetype = "error";

    $params = $request->request->all();
    if(!$this->isCsrfTokenValid('authenticate', $params["_csrf_token"])) {
      // invalid token
      return new Response($this->renderView('exception/error-500.twig'));
    }

    if (!array_key_exists('roles', $params)) {
      $params["roles"] = array();
    }
    $session = $this->get("session");

    /** @var UserRepository $repository */
    $repository = $this->getDoctrine()->getRepository('AppBundle:User');
    /** @var User $user */
    $user = $repository->getUser(array('username' => $params["username"]));
    if ($params["username"] == "" || ($user != null && $user->getId() != $session->get("mysession")["userid"])) {
      $message = "Sorry! The filled username is not unique. Updating failed!";
      $messagetype = "alert";
    } else {
      $user = $repository->getUser(array('email' => $params["email"]));
      if ($params["email"] == "" || ($user != null && $user->getId() != $session->get("mysession")["userid"])) {
        $message = "Sorry! The filled email address is not unique. Updating failed!";
        $messagetype = "alert";
      } else {
        if ($repository->updateUser($session->get("mysession")["userid"], $params)) {
          $user = $repository->getUser(array('id' => $session->get("mysession")["userid"]));

          $this->get("session")->set("mysession",
            array("userid" => $user->getId(),
              "username" => $user->getUsername(),
              "firstname" => $user->getFirstname(),
              "lastname" => $user->getLastname(),
              "affiliation" => $user->getAffiliation(),
              "email" => $user->getEmail(),
              "roles" => $user->getRoles(),
              "access_token" => $user->getApiKey(),
              "sessionid" => session_id()));
          $message = "The user details have been changed.";
          $messagetype = "success";
        }
      }
    }
    $this->addFlash(
      $messagetype,
      $message
    );

    return $this->redirectToRoute('admin', array('view' => 'account.details'));
  }

  /**
   * @Route("/user/create", name="app_user_create")
   */
  public function createUser(Request $request)
  {
    $messagetype="error";
    $params = array_map("strip_tags",$request->request->all());

    if (array_key_exists("username", $params) && array_key_exists("email", $params)) {
      /** @var UserRepository $repository */
      $repository = $this->getDoctrine()->getRepository('AppBundle:User');
      //check if it already exists
      /** @var User $user */
      $user = $repository->getUser(array("email" => $params["email"]));
      if ($user && $user->getVerified() != null) {
        $message = "Warning! This user is already exists!";
      } else {
        //remove not active user
        if ($user && $user->getVerified() == null) {
          $repository->deleteUser($user->getId());
        }
        $user = $repository->createUser($params);
        if ($user) {
          try {
            //send the registration email
            $this->sendRegistrationMail($user->getEmail(), $user->getHash(), $this->container->getParameter('hostname'));
            $messagetype="success";
            $message = "The instruction for logging in has been sent to your email address.";
          } catch (\Exception $e) {
            $message = "Warning! E-mail delivery failed!";
          }
        } else {
          $message = "Warning! The user does not exists!";
        }
      }
      $this->addFlash(
        $messagetype,
        $message);
      return new Response($this->renderView('default/login.twig', array('username' => $params["username"], 'email' => $params["email"])));
    }
    return $this->redirectToRoute('app_user_login');
  }

  /**
   * @Route("/user/revokeall", name="app_user_revokeall")
   */
  public function revokeAllInvitations(Request $request) {
    $params = $request->query->all();

    $session = $this->get("session");
    if ($session->has("project") && array_key_exists("userid", $params)) {
      /** @var  User $user */
      $user = $this->getDoctrine()->getRepository('AppBundle:User')->findOneBy(array("id" => $params["userid"]));

      if ($user) {
        /** @var RoleRepository $repository */
        $repository = $this->getDoctrine()->getRepository('AppBundle:Role');
        if ($repository->removeInvitation($params["userid"], $session->get("project")["id"])) {
          $this->revokeInvitationMail($user->getEmail(), $user->getUsername(), $session->get("mysession")["username"], $session->get("project")["name"], null);
        }
      }
    }
    return $this->redirectToRoute('admin', array("view" => "settings.access"));
  }

  /**
   * @Route("/user/invite", name="app_user_invite")
   */
  public function inviteUser(Request $request)
  {
    $params = $request->query->all();
    if(!$this->isCsrfTokenValid('authenticate', $params["_csrf_token"])) {
      // invalid token
      return new Response($this->renderView('exception/error-500.twig'));
    }

    $result = array();
    $session = $this->get("session");
    if ($session->has("project")) {
      /** @var UserRepository $userRepository */
      $userRepository = $this->getDoctrine()->getRepository('AppBundle:User');
      /** @var User $user */
      $user = null;
      if (array_key_exists("userid", $params)) {
        $user = $userRepository->getUser(array("id" => $params["userid"]));
      } else if (array_key_exists("search", $params)) {
        $user = $userRepository->getUser(array("username" => $params["search"]));
      }

      if ($user) {
        $projectId = $session->get("project")["id"];
        //check if the user has not any evaluations
        /** @var AnnotationRepository $annotationRepository */
        $annotationRepository = $this->getDoctrine()->getRepository('AppBundle:Annotation');
        $evalCount = $annotationRepository->countEvaluation($projectId, $params["userid"]);
        if ($evalCount > 0 && $params["force"] == "false") {
          return new Response(json_encode(array("role" => $params["role"], "status" => "userwithevals")));
        } else {
          /** @var RoleRepository $roleRepository */
          $roleRepository = $this->getDoctrine()->getRepository('AppBundle:Role');
          /** @var Role $role */
          $myuserid = $session->get("mysession")["userid"];
          if ($user->getId() == $myuserid) {
            $role = $roleRepository->getRole($user->getId(), $projectId, $params["role"]);
            $status = "revoked";
            if (!$role || $role->getStatus() != "accepted") {
              $status = "accepted";
            }
            $role = $roleRepository->updateRole($myuserid, $user->getId(), $projectId, trim($params["role"]), $status);
            if ($role) {
              $result["status"] = $role->getStatus();
              $result["refresh"] = true;
            }
            //return $this->redirectToRoute('admin', array("pid" => $projectId, "view" => "settings.access"));
          } else {
            $role = $roleRepository->updateRole($myuserid, $user->getId(), $projectId, trim($params["role"]), null);
            if ($role == null) {
              $this->addFlash(
                'warning',
                'This user cannot be invited or s/he could be already involved in this project!'
              );
              return $this->redirectToRoute('admin', array("view" => "settings.access"));

            } else if ($role->getRole() != "") {
              try {
                if ($role && $role->getStatus() == "pending") {
                  if ($this->sendInvitationMail($user->getEmail(), $user->getUsername(), $session->get("mysession")["username"], $session->get("project")["name"], $role->getRole(), $role->getHash())) {
                    $result["sentmail"] = $user->getUsername();
                    if ($role->getCreatedBy() != $session->get("mysession")["userid"]) {
                      /** @var User $inviteeUser */
                      $inviteeUser = $userRepository->getUser(array("id" => $role->getCreatedBy()));
                      if ($inviteeUser) {
                        if ($this->sendInvitationMail($inviteeUser->getEmail(), $user->getUsername(), $session->get("mysession")["username"], $session->get("project")["name"], $role->getRole(), $role->getHash(), $inviteeUser->getUsername())) {
                          $result["sentmail"] .= " and " .$inviteeUser->getUsername();
                        }
                      }
                    }
                  }
                } else {
                  if ($roleRepository->removeInvitation($params["userid"], $projectId, $role->getRole())) {
                    $result["sentmail"] = "";
                    if ($this->revokeInvitationMail($user->getEmail(), $user->getUsername(), $session->get("mysession")["username"], $session->get("project")["name"], $role->getRole())) {
                      $result["sentmail"] = $user->getUsername();
                      //send an email to who invited the user if he is not who call the request
                      if ($role->getCreatedBy() != $session->get("mysession")["userid"]) {
                        /** @var User $inviteeUser */
                        $inviteeUser = $userRepository->getUser(array("id" => $role->getCreatedBy()));
                        if ($inviteeUser) {
                          if ($this->revokeInvitationMail($inviteeUser->getEmail(), $user->getUsername(), $session->get("mysession")["username"], $session->get("project")["name"], $role->getRole(), $inviteeUser->getUsername())) {
                            $result["sentmail"] .= " and " .$inviteeUser->getUsername();
                          }
                        }
                      }
                    }
                  }
                }
                $result["status"] = $role->getStatus();

              } catch (Exception $e) {
                $result["status"] = "error";
                $result["message"] = 'Errors sending the email message!';
              }

            } else {
              return $this->redirectToRoute('admin', array("view" => "settings.access"));
            }
          }
        }
      } else {
        $result["status"] = "error";
        $result["message"] = 'The invited user seems not valid!';
      }
    }
    return new Response(json_encode($result));
  }

  /**
   * @Route("/password/forgot", name="app_password_forgot")
   */
  public function forgotPassword(Request $request)
  {
    $messagetype = "warning";
    $message = "Can't find the username or email, sorry!";

    $params = array_map("strip_tags",$request->query->all());

    if (array_key_exists("reference", $params)) {
      $reference = trim($params["reference"]);
      $hash = trim($params["hash"]);

      /** @var UserRepository $repository */
      $repository = $this->getDoctrine()->getRepository('AppBundle:User');

      /** @var User $user */
      if (!empty($hash)) {
        $user = $repository->getUser(array("email" => $reference, "hash" => $hash));
        if ($user) {
          return new Response($this->renderView('default/resetpassword.twig', array('email' => $reference, 'hash' => $hash)));
        }
      } else if (!empty($reference)) {
        $isOK = false;
        $user = $repository->getUser(array("email" => $reference));
        if ($user && $user->getVerified() != null) {
          $isOK = true;
        } else {
          $user = $repository->getUser(array("username" => $reference));
          if ($user && $user->getVerified() != null) {
            $isOK = true;
          }
        }

        if ($isOK) {
          try {
            $hash = $repository->resetUserHash($user->getId());
            if ($hash != null) {
              /* send the email to recover the password */
              $this->sendForgotPasswordMail($user->getEmail(), $hash, $this->container->getParameter('hostname'));
              $messagetype = "success";
              $message = "The instruction to recover the password has been sent to your email address.";
            }
          } catch (\Exception $e) {
            $message = "Warning! E-mail delivery failed!";
          }
        }
      }
    }
    $this->addFlash($messagetype, $message);
    return new Response($this->renderView('default/login.twig'));
  }

  /**
   * @Route("/password/reset", name="app_password_reset")
   */
  public function resetPassword(Request $request)
  {
    $message = "Sorry! Password reset failed.";
    $messagetype = "error";

    $email = $request->query->get("email");
    $hash = $request->query->get("hash");
    $password = $request->query->get("password");

    if (!empty(trim($email))) {
      /** @var UserRepository $repository */
      $repository = $this->getDoctrine()->getRepository("AppBundle:User");

      /** @var User $user */
      $user = $repository->findOneBy(array('hash' => $hash, 'email' => $email));
      if ($user) {
        if ($repository->verifyUser($email, $hash)) {
          $repository->updateUser($user->getId(), array("password" => $password));
          $messagetype = "success";
          $message = "The password has been changed! You can now log in.";
        }
      } else {
        $message = "Sorry! This user is not valid.";
      }
    }
    $this->addFlash($messagetype, $message);

    return new Response($this->renderView('default/login.twig',array('username' => $email,
        'email' => $email)));
  }

  /**
   * @Route("/password/change", name="app_password_change")
   */
  public function changePassword(Request $request)
  {
    $message = "Sorry! Password changing failed.";
    $messagetype = "error";

    $userid = $this->get("session")->get("mysession")['userid'];
    $params = $request->request->all();

    if (array_key_exists("old_password", $params) && array_key_exists("password", $params)) {
      if ($userid) {
        /** @var UserRepository $repository */
        $repository = $this->getDoctrine()->getRepository("AppBundle:User");

        /** @var User $user */
        $user = $repository->findOneBy(array('password' => md5(trim($params['old_password'])), 'id' => $userid));
        if ($user) {
          $repository->updateUser($user->getId(), array("password" => $params['password']));
          $message = "The password has been changed.";
          $messagetype = "success";
        }
      }
    }
    $this->addFlash($messagetype, $message);
    return new Response($this->renderView('default/admin.twig', array('view' => 'account.changepassword')));
  }

  /**
   * @Route("/user/exist", name="app_user_exist")
   * return "invalid" for not valid, "unknown" if the email or username is not present in the db, "ok" it is exists
   */
  public function checkExist(Request $request)
  {
    $what = trim($request->query->get("what"));
    $search = trim($request->query->get("search"));
    if (!empty($search)) {
      if ($what == "email" && !filter_var($search, FILTER_VALIDATE_EMAIL)) {
        return new Response("invalid");
      } else {
        /** @var UserRepository $repository */
        $repository = $this->getDoctrine()->getRepository('AppBundle:User');
        /** @var User $user */
        $user = $repository->getUser(array($what => $search));
        if ($user && $user->getVerified() != null) {
          return new Response("used");
        }
      }
    }
    return new Response("unknown");
  }

  //SENDING EMAIL FUNCTIONS

  /**
   * send registration email
   */
  private function sendRegistrationMail($email, $hash, $hostname)
  {
    /** @var \Swift_Mailer $mailer */
    $mailer = $this->get('mailer');
    $time = new \DateTime("now");

    $mail = \Swift_Message::newInstance()
      ->setSubject('[MT-EQuAl] User registration (' . $time->format('Y-m-d H:i:s') .')')
      ->setFrom($this->container->getParameter('mailer_user'))
      ->setTo($email)
      ->setBody(
        $this->renderView('mail/signup.email.twig',
          array('email' => urlencode($email), 'hash' => $hash, 'hostname' => $hostname)
        ),'text/html'
      );

    $result = $mailer->send($mail, $failures);
    if (!$result) {
      throw new \Exception($failures);
    }
    /* else {
      printf("Sent %d messages\n", $result);

      $this->get('session')->getFlashBag()->add(
      'notice',
      'The email was successfully sent. Thank you!'
    );
    }*/
  }

  /**
   * send forgot password email
   */
  private function sendForgotPasswordMail($email, $hash, $hostname)
  {
    /** @var \Swift_Mailer $mailer */
    $mailer = $this->get('mailer');
    $time = new \DateTime("now");

    $mail = \Swift_Message::newInstance()
      ->setSubject('[MT-EQuAl] Password recovery (' . $time->format('Y-m-d H:i:s') .')')
      ->setFrom($this->container->getParameter('mailer_user'))
      ->setTo($email)
      ->setBody(
        $this->renderView('mail/forgotpassword.email.twig',
          array('email' => $email, 'hash' => $hash, 'hostname' => $hostname)
        ), 'text/html'
      );

    $result = $mailer->send($mail, $failures);
    if (!$result) {
      throw new \Exception($failures);
    }
    /*$this->get('session')->getFlashBag()->add(
  'notice',
  'The email was successfully sent. Thank you!'
);*/
  }

  /**
   * send an invitation
   */
  public function sendInvitationMail($emailTo, $userTo, $userFrom, $projectname, $role_type, $hash, $invitee = null)
  {
    /** @var \Swift_Mailer $mailer */
    $mailer = $this->get('mailer');
    $time = new \DateTime("now");

    $mail = \Swift_Message::newInstance()
      ->setSubject('[MT-EQuAl] Access invitation (' . $time->format('Y-m-d H:i:s') .')')
      ->setFrom($this->container->getParameter('mailer_user'))
      ->setTo($emailTo)
      ->setBody(
        $this->renderView('mail/inviteuser.email.twig',
          array('userTo' => $userTo, 'userFrom' => $userFrom, 'projectname' => $projectname, 'role' => $role_type, 'hash' => $hash, 'invitee' => $invitee)
        ), 'text/html'
      );

    $result = $mailer->send($mail, $failures);
    if (!$result) {
      //throw new \Exception($failures);
      return false;
    }
    return true;
  }

  /**
   * send a revoked invitation
   */
  public function revokeInvitationMail($emailTo, $userTo, $userFrom, $projectName, $roleType, $invitee = null)
  {
    /** @var \Swift_Mailer $mailer */
    $mailer = $this->get('mailer');
    $time = new \DateTime("now");

    $mail = \Swift_Message::newInstance()
      ->setSubject('[MT-EQuAl] Revoke notification (' . $time->format('Y-m-d H:i:s') .')')
      ->setFrom($this->container->getParameter('mailer_user'))
      ->setTo($emailTo)
      ->setBody(
        $this->renderView('mail/revokeuser.email.twig',
          array('userTo' => $userTo, 'userFrom' => $userFrom, 'projectname' => $projectName, 'role' => $roleType, 'invitee' => $invitee)
        ), 'text/html'
      );

    if (isset($mail)) {
      $result = $mailer->send($mail, $failures);
      if (!$result) {
        //throw new \Exception($failures);
        return false;
      }
    }
    return true;
  }
}
?>