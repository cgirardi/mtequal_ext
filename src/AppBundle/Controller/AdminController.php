<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Progress;
use AppBundle\Entity\Project;
use AppBundle\Entity\Role;
use AppBundle\Entity\User;
use AppBundle\Entity\ViewedData;
use AppBundle\Repository\AgreementRepository;
use AppBundle\Repository\AnnotationRepository;
use AppBundle\Repository\CommentRepository;
use AppBundle\Repository\CorpusRepository;
use AppBundle\Repository\ProgressRepository;
use AppBundle\Repository\RoleRepository;
use AppBundle\Repository\ProjectRepository;
use AppBundle\Repository\SentenceRepository;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Validator\Constraints\DateTime;

class AdminController extends Controller
{

  /**
   * @Route("/", name="homepage")
   */
  public function indexAction(Request $request)
  {
    if (!$this->get("session")->has("mysession")) {
      return $this->redirectToRoute('app_user_login');
    }
    /* replace this example code with whatever you need */
    return $this->redirectToRoute('admin');
  }

  /**
   * @Route("/admin", name="admin")
   */
  public function adminAction (Request $request)
  {
    if (!$this->get("session")->has("mysession")) {
      return $this->redirectToRoute('app_user_login');
    }
    $userid = $this->get("session")->get("mysession")["userid"];

    /** @var RoleRepository $role_repository */
    $role_repository = $this->getDoctrine()->getRepository("AppBundle:Role");
    /** @var SentenceRepository $sentence_repository */
    $sentence_repository = $this->getDoctrine()->getRepository("AppBundle:Sentence");

    $params = $request->query->all();
    if (array_key_exists("pid", $params)) {
      /** @var ProjectRepository $project_repository */
      $project_repository = $this->getDoctrine()->getRepository("AppBundle:Project");

      $array_data = $project_repository->getProject($params["pid"]);
      if ($array_data) {
        //get user roles
        $array_data["roles"] = $role_repository->getUserRoles($userid, $params["pid"]);

        if (count($array_data["roles"]) > 0) {
          $array_data["tus"] = $sentence_repository->countOutput($params["pid"]);
          $array_data["targets"] = $sentence_repository->targetLabels($params["pid"]);

          /** @var CorpusRepository $corpus_repository */
          $corpus_repository = $this->getDoctrine()->getRepository("AppBundle:Corpus");
          $array_data["corpus"] = $corpus_repository->statsCorpus($params["pid"]);

          /** @var ProgressRepository $progress_repository */
          $progress_repository = $this->getDoctrine()->getRepository("AppBundle:Progress");
          $array_data["etasks"] = $progress_repository->enableTasks($params["pid"], $userid);

          $session = $request->getSession();
          $session->set("project", $array_data);
        }
      }
    }

    if (array_key_exists("view", $params)) {
      $projectid = $this->get("session")->get("project")["id"];

      $roles = $this->get("session")->get("project")["roles"];
      if (preg_match('/^settings.delete/', $params["view"])) {
        /** @var AnnotationRepository $annotation_repository */
        $annotation_repository = $this->getDoctrine()->getRepository("AppBundle:Annotation");
        $joinUsers = $role_repository->getRoles($projectid, $userid);
        $params["users"] = array();
        foreach ($joinUsers as $jUser) {
          $params["users"][$jUser["id"]] = $jUser["username"];
        }
        $params["annotations"] = $annotation_repository->getAnnotationProgress($this->get("session")->get("project")["id"]);
      } else if (preg_match('/^settings.guidelines/', $params["view"])) {
        /** @var AnnotationRepository $annotation_repository */
        $annotation_repository = $this->getDoctrine()->getRepository("AppBundle:Annotation");
        $params["countIntraEvals"] = $annotation_repository->countIntraEvaluation($projectid);
        $params["countEvals"] = $annotation_repository->countEvaluation($projectid);
      } /* else if ((preg_match('/^settings.access/', $params["view"]) || preg_match('/^settings.export/', $params["view"])) &&
          in_array("vendor", $roles)) {
        //do nothing
      } else if (preg_match('/^settings/', $params["view"])) {
        if (!in_array("admin", $roles) && !in_array("owner", $roles)) {
          $params["view"] = "dashboard";
        }
      } */

      if ($params["view"] == "dashboard" || preg_match('/^account/', $params["view"])) {
        $session = $request->getSession();
        $session->remove("project");
      }
    } else {
      //project overview
      //$params["targets"] = $sentence_repository->targetLabels($this->get("session")->get("project")["id"]);
      $params['roles'] = $role_repository->statsRoles($this->get("session")->get("project")["id"], "accepted");

      $myroles = $this->get("session")->get("project")['roles'];
      if (count($myroles) == 1) {
        if (in_array("evaluator", $myroles)) {
          $params["view"] = "evaluation";
        }
      }
    }

    return new Response($this->renderView('default/admin.twig', $params));
  }

  /**
   * @Route("/dashboard", name="dashboard")
   */
  public function dashboardAction (Request $request) {
    if (!$this->get("session")->has("mysession")) {
      return $this->redirectToRoute('app_user_login');
    }
    $userid = $this->get("session")->get("mysession")["userid"];
    $params["view"] = "overview";
    if ($request->query->has("view")) {
      $params["view"] = $request->query->get("view");
    }

    /** @var RoleRepository $repository */
    $repository = $this->getDoctrine()->getRepository('AppBundle:Role');
    $params["pending"] = $repository->pendingCount($userid);

    $this->get("session")->remove("project");
    if ($params["view"] == "overview") {
      /** @var ProjectRepository $project_repository */
      $project_repository = $this->getDoctrine()->getRepository("AppBundle:Project");
      $params["projects"] = $project_repository->findProject($userid);

      /** @var ProgressRepository $progress_repository */
      $progress_repository = $this->getDoctrine()->getRepository('AppBundle:Progress');
      $params["duetasks"] = $progress_repository->dueTasks($userid);
      //$this->get("session")->set("projects", $params["projects"]);
    } else if ($params["view"] == "requests") {
    } else if ($params["view"] == "apidoc") {
      return $this->render('default/apidoc.twig', $params);
    }
    return $this->render('default/admin.twig', $params);
  }

  public function pageNotFoundAction($path)
  {
    //throw new NotFoundHttpException('No route found for ' . $path);
    return new Response($this->renderView('exception/error-500.twig'));
  }

  /**
   * @Route("/evaluation", name="evaluation")
   * @method: GET
   */
  public function evaluationAction (Request $request)
  {
    $session = $this->get("session");
    $params = array();
    if ($session->has("project") && $session->has("mysession")) {
      $userid = $session->get("mysession")["userid"];
      $projectid = $session->get("project")["id"];

      $em = $this->getDoctrine();
      /** @var ProgressRepository $repository_progress */
      $repository_progress = $em->getRepository('AppBundle:Progress');

      //check if the evaluation task is enable (not disabled and not completed)
      /** @var Progress $progress */
      $progress = $repository_progress->getProgress($repository_progress::EVALUATION_TASK, $projectid, $userid);
      if ($progress) {
        /** @var  User $user */
        $user = $em->getRepository('AppBundle:User')->findOneBy(array("id" => $userid));

        /** @var  SentenceRepository $repository_sentence */
        $repository_sentence = $em->getRepository('AppBundle:Sentence');

        /** @var  AnnotationRepository $repository_annotation */
        $repository_annotation = $em->getRepository('AppBundle:Annotation');

        $params{"countTUs"} = $repository_sentence->countTUs($projectid);
        $params{"countDone"} = $repository_annotation->countDone($projectid, $user);

        if (!$progress->getDisabled() && $params{"countDone"} < $params{"countTUs"}) {

          //click on "don't know" button
          if ($request->query->has("dontknow") && $request->query->get("dontknow") != "") {
            $outputid = $request->query->get("dontknow");
            try {
              $repository_annotation->dontKnow($outputid, $projectid, $user);

              //set all annotations as not confirmed
              $repository_annotation->unconfirmEvaluation($outputid, $projectid, $user);
            } catch (Exception $e) {
              return new Response(json_encode(array("success" => false)));
            }

            return new Response(json_encode(array("success" => true)));

            //click on confirm button
          } elseif ($request->query->has("done") && $request->query->get("done") != "") {
            $outputid = $request->query->get("done");

            //set as complete the progress
            $repository_annotation->confirmEvaluation($outputid, $projectid, $user);

            //check the number of the don't know then it send an alert if the TU list hasn't been activated
            if ($session->get("project")["maxdontknow"] > 0) {
              //check the maxdontknow if the TU index was allowed
              if (!$session->get("project")["sentlist"]) {
                $dontknow = $repository_annotation->countDontKnow($projectid, $user);
                if ($dontknow == $session->get("project")["maxdontknow"]) {
                  //send an email
                  $this->sendDontKnowAlert($projectid, $user);
                  //$this->addFlash('alert', 'Max number of don\'t know has been reached!');
                }
              }
            }

            //get the next unconfirmed TU
            $next = $repository_sentence->getFirstUndoneSentences($projectid, $userid);
            if ($next) {
              $params{"id"} = $next[0]{"id"};
            } else {
              //TODO: Save explicitly the progress of evaluation as completed for user X, project Y
              if ($session->get("project")["sentlist"]) {
                //enable the progress of the task "evaluation"
                $repository_progress->updateProgress($repository_progress::EVALUATION_TASK, $projectid, FALSE, $userid, "evaluator", null);
              } else {
                $this->addFlash("alert", "CONGRATULATIONS! Your work is finished.");
                //complete the task
                $repository_progress->updateProgress($repository_progress::EVALUATION_TASK, $projectid, TRUE, $userid, "evaluator", null);

                //send an email to the all reviewers, admins and the vendor who invited the evaluator
                self::sendEvaluationComplete($projectid, $user);

                //if there is at least a reviewer and all evaluations were done send an email to reviewers
                $undoneEvaluationTasks = $repository_progress->findBy(array("project" => $projectid, "task" => ProgressRepository::EVALUATION_TASK, "completed" => 0));
                if (count($undoneEvaluationTasks) == 0) {
                  self::sendReviewStart($projectid);
                }

                //update the session var etask
                $session = $this->get("session");

                $projectSession = $this->get("session")->get("project");
                $projectSession["etasks"] = $repository_progress->enableTasks($projectid, $userid);
                $session->set("project", $projectSession);
              }
            }

            $params{"view"} = "evaluation";
            return $this->redirectToRoute('admin', $params);
          } else {
            $params{"id"} = "-1";
            $randout = $this->get("session")->get("project")["randout"];

            //get a specific TU
            if ($request->query->has("id") && $request->query->get("id") != "") {
              $params{"id"} = $request->query->get("id");
              //get the first undone sentence (go next without passing by the list)
            } else {
              $next = $repository_sentence->getFirstUndoneSentences($projectid, $userid);
              if ($next) {
                $params{"id"} = $next[0]{"id"};
              }
            }
            $params{"sentences"} = $repository_sentence->getTUSentences($params{"id"}, $projectid, $randout);

            if ($repository_annotation->isDone($params{"id"}, $projectid, $user)) {
              $params{"done"} = 1;
            }
            $params{"annotations"} = $repository_annotation->getUserAnnotations($params{"id"}, $projectid, $user);
          }
        } else if ($params{"countDone"} == $params{"countTUs"}) {
          $params = array("info" => "You have already evaluated all TUs for this project.");
        } else {
          $params = array("warning" => "The evaluation can't start yet. You will receive a notification by email when it is ready.");
        }
        return new Response($this->renderView('listing/evaluation.twig', $params));
      }
    }

    $session = $request->getSession();
    $session->remove("project");
    return new Response($this->renderView("exception/invalid.twig"));
  }

  /**
   * @Route("/review", name="review")
   * @method: GET
   */
  public function reviewAction (Request $request)
  {
    $params = array();
    if ($this->get("session")->has("project") && $this->get("session")->has("mysession")) {
      $userId = $this->get("session")->get("mysession")["userid"];
      $projectId = $this->get("session")->get("project")["id"];

      $em = $this->getDoctrine();
      /** @var ProgressRepository $progress_repository */
      $progress_repository = $em->getRepository("AppBundle:Progress");
      if (array_key_exists("review", $progress_repository->enableTasks($projectId, $userId))) {
        /** @var  User $user */
        $user = $em->getRepository('AppBundle:User')->findOneBy(array("id" => $userId));

        /** @var  SentenceRepository $repository_sentence */
        $repository_sentence = $em->getRepository('AppBundle:Sentence');

        /** @var  AnnotationRepository $repository_annotation */
        $repository_annotation = $em->getRepository('AppBundle:Annotation');

        //click on "don't know" button
        if ($request->query->has("dontknow") && $request->query->get("dontknow") != "") {
          $outputid = $request->query->get("dontknow");
          try {
            $repository_annotation->dontKnow($outputid, $projectId, $user);

            //set all annotations as not confirmed
            $repository_annotation->unconfirmEvaluation($outputid, $projectId, $user);
          } catch (Exception $e) {
            return new Response(json_encode(array("success" => false)));
          }
          return new Response(json_encode(array("success" => true)));

          //click on confirm button
        } elseif ($request->query->has("done") && $request->query->get("done") != "") {
          $outputid = $request->query->get("done");

          //set as complete the progress
          $repository_annotation->confirmEvaluation($outputid, $projectId, $user);

          //check the number of the don't know then it send an alert if the TU list hasn't been activated
          if ($this->get("session")->get("project")["maxdontknow"] > 0) {
            //check the maxdontknow if the TU index was allowed
            if (!$this->get("session")->get("project")["sentlist"]) {
              $dontknow = $repository_annotation->countDontKnow($projectId, $user);
              if ($dontknow == $this->get("session")->get("project")["maxdontknow"]) {
                //send an email
                $this->sendDontKnowAlert($projectId, $user);
                $this->addFlash('alert',
                  'Max number of don\'t know has been reached!');
              }
            }
          }

          $params{"view"} = "evaluation";
          return $this->redirectToRoute('admin', $params);
        } else {
          //get a specific TU
          if ($request->query->has("id") && $request->query->get("id") != "" &&
              $request->query->has("num") && $request->query->get("num") != "") {
            $params{"id"} = $request->query->get("id");
            $params{"num"} = $request->query->get("num");
            //get the first undone sentence (go next without passing by the list)
            /** @var  RoleRepository $repository_role */
            $repository_role = $em->getRepository("AppBundle:Role");
            $params{"sentences"} = $repository_sentence->getTUOneSentence($params{"num"}, $params{"id"}, $projectId);
            $params{"countTUs"} = $repository_sentence->countTUs($projectId);
            $params{"countDone"} = $repository_annotation->countDone($projectId, $user);

            $params["evaluators"] = $repository_role->getUsers($projectId, $userId, "evaluator");

            if ($repository_annotation->isDone($params{"id"}, $projectId, $user)) {
              $params{"done"} = 1;
            }

            $params{"annotations"} = $repository_annotation->getTargetAnnotations($params{"num"});
            /** @var ViewedData $wd */
            $wd = $em->getRepository('AppBundle:ViewedData')->findOneBy(
              array("num" => $params{"num"}, "project" =>  $projectId, "user" => $userId));
            if ($wd) {
              $wd->setModified(new \DateTime("now"));
            } else {
              $wd = new ViewedData($params{"num"}, $projectId, $userId);
            }
            $mng = $this->getDoctrine()->getManager();
            $mng->persist($wd);
            $mng->flush();
          }
        }
        return new Response($this->renderView('listing/review.twig', $params));
      } else {
        return new Response("The access to review panel is disabled!");
      }
    }

    $session = $request->getSession();
    $session->remove("project");
    return new Response($this->renderView("exception/invalid.twig"));
  }


  /**
   * @Route("/agreement", name="agreement")
   * @method: GET
   */
  public function agreementAction (Request $request)
  {
    if ($this->get("session")->has("project") && $this->get("session")->has("mysession")) {
      $userId = $this->get("session")->get("mysession")["userid"];
      $projectId = $this->get("session")->get("project")["id"];

      $em = $this->getDoctrine();
      /** @var ProgressRepository $repository_progress */
      $repository_progress = $em->getRepository("AppBundle:Progress");

      //check if all evaluation tasks are completed
      if (count($repository_progress->findBy(array("task" => $repository_progress::EVALUATION_TASK,
                                              "project" => $projectId,
                                              "completed" => 0))) > 0) {
        return new Response($this->renderView('listing/agreement.twig', array("message" => "The review can't start before all evaluators have finished!")));
      }

      /** @var AgreementRepository $repository_agreement */
      $repository_agreement = $em->getRepository("AppBundle:Agreement");

      /** @var RoleRepository $repository_role */
      $repository_role = $em->getRepository("AppBundle:Role");
      $evaluators = $repository_role->getUserByRole($projectId, "evaluator");

      $evaluatorCount = -1;
      if ($request->query->has("complete") && $request->query->get("complete") == "on") {
        $evaluatorCount = count($evaluators);
      }

      if ($request->query->has("disagreement") && $request->query->get("disagreement") == "on") {
        $agreement = $repository_agreement->getDisagreement($projectId, $evaluatorCount);
        $evalCounter = $repository_agreement->getDisagreementCountEvaluation($projectId, $evaluatorCount);
      } else {
        $agreement = $repository_agreement->getAgreement($projectId, $evaluatorCount);
        $evalCounter = $repository_agreement->getAgreementCountEvaluation($projectId, $evaluatorCount);
      }

      /** @var CommentRepository $repository_comment */
      $repository_comment = $em->getRepository("AppBundle:Comment");
      $comments = $repository_comment->getCommentsDistribution($projectId);

      /** @var AnnotationRepository $repository_annotation */
      $repository_annotation = $em->getRepository("AppBundle:Annotation");
      $k = $repository_annotation->calculateKappaQuality($projectId, false);
      if ($k != 0) {
        $k = round($k ,3);
      }

      //viewed reviews
      $viewedrev = $repository_annotation->getViewedEvaluations($projectId, $userId);

      return new Response($this->renderView('listing/agreement.twig',
        array("data" => $agreement,
              "evalcounter" => $evalCounter,
              "evaluators" => count($evaluators),
              "k" => $k,
              "viewedrev" => $viewedrev,
              "comments" => $comments)));
    }

    $session = $request->getSession();
    $session->remove("project");
    return new Response($this->renderView("exception/invalid.twig"));
  }

  /**
   * @Route("/comment/save", name="save_comment")
   * @method: POST
   */
  public function commentAction (Request $request)
  {
    $message = "error";
    $params = $request->request->all();
    if(!$this->isCsrfTokenValid('authenticate', $params["_csrf_token"])) {
      // invalid token
      return new Response($this->renderView('exception/error-500.twig'));
    } else {
      $userid = $this->get("session")->get("mysession")["userid"];
      $projectid = $this->get("session")->get("project")["id"];

      if ($this->getDoctrine()->getRepository("AppBundle:Comment")->saveComment(
        $projectid, $userid, $params["comment"], $params["obj"], $params["refid"])) {
        $message = "OK";
      }
    }
    return new Response(json_encode(array("message" => $message)));
  }

 /*
 * send a template as a report
 * @method: GET
 */
  /**
   * @Route("/report", name="send_report")
   * @method: POST
   */
  public function sendReport (Request $request)
  {
    $project = $this->get("session")->get("project");
    $success = false;

    if ($project && $request->query->has("mailto")) {
      $template = null;
      $subject = "";
      $mailTo = urldecode(strip_tags($request->query->get("mailto")));
      $view = $request->query->get("view");
      $projectName = $project["name"];

      if ($view == "corpus_report") {
        $template = 'listing/corpus.twig';
        $subject = "Import corpus report";
        /** @var CorpusRepository $repository_corpus */
        $repository_corpus = $this->getDoctrine()->getRepository('AppBundle:Corpus');
        $params["files"] = $repository_corpus->findBy(array("project" => $project["id"]), array('date' => 'DESC'));
      } else if ($view == "eval_report") {
        $template = 'listing/evalreport.twig';
        $subject = "Evaluation report";

        /** @var AnnotationRepository $annotation_repository */
        $annotation_repository = $this->getDoctrine()->getRepository('AppBundle:Annotation');
        $params = $annotation_repository->getEvalReportParams($project["id"], $this->get("session")->get("mysession")["userid"]);
      }
      $params["mode"] = "report";

      if ($template != null) {
        $time = new \DateTime("now");
        /** @var \Swift_Mailer $mailer */
        $mailer = $this->get('mailer');

        $mail = \Swift_Message::newInstance()
          ->setSubject("[MT-EQuAl] Project $projectName - ". $subject ." (". $time->format('Y-m-d H:i:s') .")")
          ->setFrom($this->container->getParameter('mailer_user'))
          ->setTo($mailTo)
          ->setBody("<h4><small>project:</small> <label>$projectName</label></h4>" .
            $this->renderView($template, $params), 'text/html'
          );

        $failures = array();
        $result = $mailer->send($mail, $failures);
        if ($result) {
          $success = true;
        }
      }
    }
    $response = new Response(json_encode(array("success" => $success)));
    /*
    $response = new Response(json_encode(
      array("mailto" => $mailTo))); */
    return $response;
  }

  //EMAIL SEND

  /**
   * send a message to each admin and vendor of the project about the "don't know" evaluation
   * @param $projectid
   * @param $user
   * @return int
   * @throws Exception
   */
  public function sendDontKnowAlert ($projectid, User $user) {
    $countSent = 0;
    /** @var \Swift_Mailer $mailer */
    $mailer = $this->get('mailer');
    $time = new \DateTime("now");

    $em = $this->getDoctrine();
    /** @var Project $project */
    $project = $em->getRepository('AppBundle:Project')->findOneBy(array("id" => $projectid));

    /** @var RoleRepository $role_repository */
    $role_repository = $em->getRepository('AppBundle:Role');
    $admins = $role_repository->getUserByRole($projectid, "admin");
    $vendor = $role_repository->getInviter($projectid, $user->getId(), "evaluator");
    $users = array_merge($admins, $vendor);

    /** @var Role $admin */
    $emailAlreadySent = array();
    foreach ($users as $key => $userRow) {
      if (!in_array($userRow["id"], $emailAlreadySent)) {
        array_push($emailAlreadySent, $userRow["id"]);

        $mail = \Swift_Message::newInstance()
          ->setSubject('[MT-EQuAl] Alert for the "don\'t know" limit exceeded (' . $time->format('Y-m-d H:i:s') . ')')
          ->setFrom($this->container->getParameter('mailer_user'))
          ->setTo($userRow["email"])
          ->setBody(
            $this->renderView('mail/dontknowalert.email.twig',
              //blind user
              //array('hostname' => $this->container->getParameter('hostname'), 'projectid' => $projectid, 'projectname' => $project->getName())
              array('userTo' => $userRow["username"], 'projectid' => $projectid, 'projectname' => $project->getName(), 'username' => $user->getUsername(), 'hostname' => $this->container->getParameter('hostname'))
            ), 'text/html'
          );

        $failures = array();
        $result = $mailer->send($mail, $failures);
        if (!$result) {
          throw new \Exception($failures);
        } else {
          $countSent++;
        }
      }
    }
    return $countSent;
  }


  /**
   * send a message to all admins, reviewers and vendor PjM about a complete evaluation
   * @param $projectid
   * @throws \Exception
   * @return integer
   */
  private function sendEvaluationComplete($projectid, User $user)
  {
    $countSent = 0;
    /** @var \Swift_Mailer $mailer */
    $mailer = $this->get('mailer');
    $time = new \DateTime("now");

    $em = $this->getDoctrine();
    /** @var Project $project */
    $project = $em->getRepository('AppBundle:Project')->findOneBy(array("id" => $projectid));

    if ($project) {
      /** @var RoleRepository $role_repository */
      $role_repository = $em->getRepository('AppBundle:Role');
      $admins = $role_repository->getUserByRole($projectid, "admin");
      $reviewers = $role_repository->getUserByRole($projectid, "reviewer");
      $vendor = $role_repository->getInviter($projectid, $user->getId(), "evaluator");
      $users = array_merge($admins, $reviewers, $vendor);

      $emailAlreadySent = array();
      foreach ($users as $key => $userRow) {
        if (!in_array($userRow["id"], $emailAlreadySent)) {
          array_push($emailAlreadySent, $userRow["id"]);
          $arrayParams = array('userTo' => $userRow["username"], 'hostname' => $this->container->getParameter('hostname'), 'projectid' => $projectid, 'projectname' => $project->getName());
          if ($userRow["role"] != "reviewer") {
            //not blind user
            $arrayParams['username'] = $user->getUsername();
          }

          $mail = \Swift_Message::newInstance()
            ->setSubject('[MT-EQuAl] Evaluation complete (' . $time->format('Y-m-d H:i:s') . ')')
            ->setFrom($this->container->getParameter('mailer_user'))
            ->setTo($userRow["email"])
            ->setBody(
              $this->renderView('mail/evaluationcomplete.email.twig',
                $arrayParams
              ), 'text/html'
            );

          $failures = array();
          $result = $mailer->send($mail, $failures);
          if (!$result) {
            throw new \Exception($failures);
          } else {
            $countSent++;
          }
        }
      }
    }
    return $countSent;
  }

  /**
   * send a message to each reviewer involved in the project about start reviewing
   * @param $projectid
   * @return int
   * @throws \Exception
   */
  private function sendReviewStart($projectid)
  {
    $countSent = 0;
    /** @var \Swift_Mailer $mailer */
    $mailer = $this->get('mailer');
    $time = new \DateTime("now");

    $em = $this->getDoctrine();
    /** @var Project $project */
    $project = $em->getRepository('AppBundle:Project')->findOneBy(array("id" => $projectid));

    /** @var RoleRepository $role_repository */
    $role_repository = $em->getRepository('AppBundle:Role');

    $reviewer = $role_repository->getUserByRole($projectid, "reviewer");
    $admin = $role_repository->getUserByRole($projectid, "admin");
    $users = array_merge($reviewer, $admin);
    $emailAlreadySent = array();
    foreach ($users as $key => $userRow) {
      if (!in_array($userRow["id"], $emailAlreadySent)) {
        array_push($emailAlreadySent, $userRow["id"]);

        $mail = \Swift_Message::newInstance()
          ->setSubject('[MT-EQuAl] A project is ready for review (' . $time->format('Y-m-d H:i:s') . ')')
          ->setFrom($this->container->getParameter('mailer_user'))
          ->setTo($userRow["email"])
          ->setBody(
            $this->renderView('mail/reviewstart.email.twig',
              //blind user
              //array('hostname' => $this->container->getParameter('hostname'), 'projectid' => $projectid, 'projectname' => $project->getName())
              array('hostname' => $this->container->getParameter('hostname'), 'username' => $userRow["username"], 'projectid' => $projectid, 'projectname' => $project->getName())
            ), 'text/html'
          );

        $failures = array();
        $result = $mailer->send($mail, $failures);
        if (!$result) {
          throw new \Exception($failures);
        } else {
          $countSent++;
        }
      }
    }
    return $countSent;
  }
}
