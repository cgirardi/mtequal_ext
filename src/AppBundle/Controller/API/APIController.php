<?php
/**
 * Project: mtequal_ext
 * User: cgirardi
 * Date: 12/10/15
 */
namespace AppBundle\Controller\API;

use AppBundle\Entity\Annotation;
use AppBundle\Entity\Corpus;
use AppBundle\Entity\Progress;
use AppBundle\Entity\Role;
use AppBundle\Entity\Project;
use AppBundle\Entity\Sentence;
use AppBundle\Entity\User;
use AppBundle\Importing\FileValidator;
use AppBundle\Repository\AgreementRepository;
use AppBundle\Repository\AnnotationRepository;
use AppBundle\Repository\CommentRepository;
use AppBundle\Repository\CorpusRepository;
use AppBundle\Repository\ProgressRepository;
use AppBundle\Repository\SentenceRepository;
use AppBundle\Repository\RoleRepository;
use AppBundle\Repository\UserRepository;
use AppBundle\Repository\ProjectRepository;
use Doctrine\ORM\Query;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Controller\UserController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;


class APIController extends UserController
{
  static $ROLES = ["admin", "reviewer", "evaluator", "vendor"];

  static $ENDPOINTS = [
    "help" => ["GET", "help"],
    "refresh_token" => ["GET", "refresh_token"],
    "admin/remove" => ["GET", "admin/remove"],
    "admin/show" => ["GET", "admin/show"],
    "project/list" => ["GET", "project/list"],
    "project/show" => ["GET", "project/show"],
    "project/create" => ["POST", "project/create"],
    "project/update" => ["POST", "project/update"],
    "project/evalformat" => ["POST", "project/evalformat"],
    "project/specifications" => ["POST", "project/specifications"],
    "project/instructions" => ["POST", "project/instructions"],
    "project/import" => ["GET", "project/import"],
    "project/delete" => ["GET", "project/delete"],
    "project/report" => ["GET", "project/report"],
    "project/export" => ["GET", "project/export"],
    "user/list" => ["GET", "user/list"],
    "user/invite" => ["GET", "user/invite"],
    "user/create" => ["GET", "user/create"],
    "user/update" => ["GET", "user/update"],
    "user/delete" => ["GET", "user/delete"],
    "user/show" => ["GET", "user/show"],
    "user/revoke" => ["GET", "user/revoke"],
    "user/replyInvitation" => ["GET", "user/replyInvitation"],
    "user/search" => ["GET", "user/search"],
    "user/requests" => ["GET", "user/requests"],
    "corpus/show" => ["GET", "corpus/show"],
    "corpus/delete" => ["GET", "corpus/delete"],
    "corpus/import" => ["POST", "corpus/import"],
    "task/update" => ["GET", "task/update"],

    /*
     * under development
    "authorize" => ["POST","authorize"]
    */
  ];


  private function getProjectSession($key)
  {
    /** @var Session $session */
    if ($this->get("session")->has("project")) {
      $projectSession = $this->get("session")->get("project");
      if (array_key_exists($key, $projectSession)) {
        return $projectSession[$key];
      }
    }
    return null;
  }

  private function setProjectSession($key, $value)
  {
    if ($this->get("session")->has("project")) {
      /** @var Session $session */
      $session = $this->get("session");

      $projectSession = $session->get("project");
      $projectSession[$key] = $value;
      $session->set("project", $projectSession);
    }
    return null;
  }

  private function getUserSession($key)
  {
    if ($this->get("session")->has("mysession")) {
      $mysession = $this->get("session")->get("mysession");
      if (array_key_exists($key, $mysession)) {
        return $mysession[$key];
      }
    }
    return null;
  }

  /**
   * @param $projectId
   * @param $role
   * @param $request
   * @return int (get the request user ID or 0 otherwise)
   */
  private function hasUserRole($projectId, $role, $request)
  {
    $user = self::isAuthorizedUser($request);
    if ($user) {
      /** @var RoleRepository $roleRepository */
      $roleRepository = $this->getDoctrine()->getRepository('AppBundle:Role');

      if ($roleRepository->getRole($user->getId(), $projectId, $role)) {
        return $user;
      }
    }
    return null;
  }

  /**
   * @param Request $request
   * @return int
   */
  private function isAuthorizedUser($request)
  {
    /** @var UserRepository $userRepository */
    $userRepository = $this->getDoctrine()->getRepository('AppBundle:User');
    /** @var User $user */
    $user = null;

    if ($request->headers != null && $request->headers->has("X-Api-Key")) {
      $user = $userRepository->findOneBy(array("apiKey" => $request->headers->get("X-Api-Key")));
    } else if (self::getUserSession("userid")) {
      $user = $userRepository->findOneBy(array("id" => self::getUserSession("userid")));
    }

    if ($user) {
      return $user;
    }
    return null;
  }

  // @param $action can be "show", "edit", "delete", "invite", "evaluate"
  private function isUserEnable($user, $projectId, $action)
  {
    if ($user) {
      /** @var RoleRepository $roleRepository */
      $roleRepository = $this->getDoctrine()->getRepository('AppBundle:Role');

      if ($action == "show") {
        if ($roleRepository->findBy(array("user" => $user->getId(), "project" => $projectId, "status" => "accepted"))) {
          return 1;
        }
      } else {
        /** @var ProjectRepository $projectRepository */
        $projectRepository = $this->getDoctrine()->getRepository('AppBundle:Project');

        return $projectRepository->can($action, $projectId, $user->getId());
      }
    }
    return 0;
  }

  private function initJSONArray($description = null)
  {
    $time = new \DateTime("now");
    $data = array();
    $data["header"] = array("date" => $time->format('l, Y-m-d H:i:s e'));
    if ($description != null)
      $data["header"]["description"] = $description;

    $data["result"] = array();
    return $data;
  }

  // save orignal file into upload directory
  private function backupFile(UploadedFile $file, $projectId)
  {
    $target_path = $this->container->getParameter('uploaded_files_dir');
    if ($target_path != null) {
      $target_path .= $projectId . DIRECTORY_SEPARATOR;
      if (!file_exists($target_path)) {
        mkdir($target_path, 0777);
      }

      $target_path .= $file->getClientOriginalName();

      move_uploaded_file($file, $target_path);
      return $target_path;
    }
    return null;
  }

  //update a task as completed
  private function doneTask($task, $projectid, $userid)
  {
    /** @var ProgressRepository $repository_progress */
    $repository_progress = $this->getDoctrine()->getRepository('AppBundle:Progress');

    /** @var Progress $progress */
    $progress = $repository_progress->updateProgress($task, $projectid, TRUE, $userid, null, null);
    if ($progress) {
      if ($progress->getTask() == ProgressRepository::EVALUATION_TASK) {
        //the code was moved in AdminController file (at the end of function evaluationAction)
      } else {
        /** @var SentenceRepository $repository_sentence */
        $repository_sentence = $this->getDoctrine()->getRepository("AppBundle:Sentence");

        if ($progress->getTask() == ProgressRepository::CHECKCORPUS_TASK) {
          self::sendEvaluationStart($projectid);
        } else if ($progress->getTask() == ProgressRepository::GUIDELINES_TASK) {
          $repository_sentence->applyPooling($projectid);
          $repository_sentence->createIntraCorpus($projectid);
        }
      }
    }
    return $progress;
  }

  // reset a task
  private function undoneTask($task, $projectid, $userTo, $usernameFrom)
  {
    /** @var ProgressRepository $repository_progress */
    $repository_progress = $this->getDoctrine()->getRepository('AppBundle:Progress');

    /** @var Progress $progress */
    $progress = $repository_progress->updateProgress($task, $projectid, FALSE, $userTo->getId(), null, null);
    if ($progress) {
      if ($task == ProgressRepository::CHECKCORPUS_TASK) {
        self::sendEvaluationStop($projectid);
      } else if ($task == ProgressRepository::EVALUATION_TASK) {
        //put to null all user annotation
        if ($userTo) {
          $unsetConfirmed = $this->getDoctrine()->getRepository('AppBundle:Annotation')->unconfirmUserEvaluations($projectid, $userTo);
          if ($unsetConfirmed > 0) {
            self::sendEvaluationNotConfirmed($projectid, $userTo);
          }
        }
      } else if ($task == ProgressRepository::REVIEW_TASK) {
        self::sendTaskNotComplete($projectid, $task, $usernameFrom, "reviewer");
      }
      // The task has been reset by ...  to all admins
      self::sendTaskNotComplete($projectid, $task, $usernameFrom, "admin");
    }
    return $progress;
  }


  /**
   * @Route("/api/", name="api")
   */
  public function apiIndex(Request $request)
  {
    $params = $request->query->all();

    return new Response($this->renderView('default/apidoc.twig', $params));
  }

  /**
   * @Route("/api/help", name="help", defaults={"_format" = "json"})
   */
  public function apiHelp(Request $request)
  {
    $jsonArray = array();
    foreach (self::$ENDPOINTS as $endpoint => $route) {
      $jsonArray[$endpoint] = ["method" => $route[0],
        "url" => $this->generateUrl($route[1], array(), true),
        "docs" => $request->getUriForPath("/") . "bundles/app/docs/web-api/" . $route[1] . ".html"];
    }

    $data = self::initJSONArray("Endpoint list");
    $data["endpoints"] = $jsonArray;

    return new Response(json_encode($data, JSON_PRETTY_PRINT));
  }

  /**
   * @Route("/api/project/list", name="project/list", defaults={"_format" = "json"})
   */
  public function apiProjects(Request $request)
  {
    $data = self::initJSONArray("Get a list of the available project");
    $user = self::isAuthorizedUser($request);
    if ($user) {
      /** @var ProjectRepository $repository */
      $repository = $this->getDoctrine()->getRepository('AppBundle:Project');
      $projectList = $repository->findProject($user->getId());

      /** @var Project $project */
      if (count($projectList) > 0) {
        $data["result"]["size"] = count($projectList);
        $p = array();

        foreach ($projectList as $projectInfo) {
          $project = $repository->find($projectInfo["id"]);
          if ($project) {
            $created = $project->getCreated()->format('l, Y-m-d H:i:s e');
            array_push($p,
              array("id" => $project->getId(),
                "name" => $project->getName(),
                "description" => $project->getDescription(),
                "myroles" => explode(", ", $projectInfo["roles"]),
                "created" => $created)
            );
          }
        }

        $data["header"]["status"] = "200";
        $data["result"]["projects"] = $p;
      } else {
        $data["header"]["status"] = "204";
      }

      /* $projects = $repository->findBy(array("active" => 1));
      if (count($projects) > 0) {
        $data["result"]["size"] = count($projects);
        $p = array();

        foreach ($projects as $project) {
          $created = $project->getCreated()->format('l, Y-m-d H:i:s e');
          array_push($p,
              array("id" => $project->getId(),
                  "name" => $project->getName(),
                  "description" => $project->getDescription(),
                  "created" => $created
              ));

        }
        $data["header"]["status"] = "200";
        $data["result"]["projects"] = $p;
      } else {
        $data["header"]["status"] = "204";
      }*/


    } else {
      $data["header"]["status"] = "401";
      $data["header"]["description"] = "Unauthorized - You need to use your access token";
    }

    return new Response(json_encode($data, JSON_PRETTY_PRINT));
  }

  /**
   * @Route("/api/project/show", name="project/show", defaults={"_format" = "json"})
   */
  public function apiShowProject(Request $request)
  {
  	// Implemented by Claudio
  	
    $data = self::initJSONArray("Get all information about a project");

    if ($request->query->has("id")) {
      /** @var ProjectRepository $repository */
      $repository = $this->getDoctrine()->getRepository('AppBundle:Project');
      /** @var Project $project */
      $project = $repository->getProjectFromID($request->query->get("id"));

      if ($project) {
        $user = self::isAuthorizedUser($request);
        if (self::isUserEnable($user, $project->getId(), "show")) {
          $p = array();
          $created = $project->getCreated()->format('l, Y-m-d H:i:s e');
          $p["settings"] =
            array("id" => $project->getId(),
              "name" => $project->getName(),
              "description" => $project->getDescription(),
              "evalformat"
              => array("type" => $project->getType(),
                "ranges" => json_decode($project->getRanges(), true),
                "requiref" => $project->getRequiref()
              ),
              "guidelines"
              => array(
                "specification"
                => array("randout" => $project->getRandout(),
                  "blindrev" => $project->getBlindrev(),
                  "maxdontknow" => $project->getMaxdontknow(),
                  "intragree" => $project->getIntragree() / 100,
                  "pooling" => $project->getPooling()
                ),
                "instructions" => $project->getInstructions()),
              "created" => $created);

          $user = self::hasUserRole($project->getId(), "admin", $request);
          if ($user != null) {
            /** @var RoleRepository $roleRepository */
            $roleRepository = $this->getDoctrine()->getRepository('AppBundle:Role');
            $p["users"] =
               $roleRepository->getRoles($project->getId(), $user->getId());

            $dql = "SELECT p.id, p.task, p.completed, p.disabled, p.modified, p.duedate, p.user, u.username
                    FROM AppBundle:Progress AS p
                    INNER JOIN AppBundle:User AS u
                    WHERE p.user=u.id and p.project=:pid
                    ORDER BY p.num, p.id";
            /** @var Query $query */
            $query = $this->getDoctrine()->getManager()->createQuery($dql);
            $query->setParameter('pid', $project->getId());

            $p["tasks"] =
               $query->getResult();

            $p["progress"]=
             $this->getDoctrine()->getRepository('AppBundle:Annotation')->getAnnotationProgress($project->getId());
          }
          $data["result"]["project"] = $p;
          $data["header"]["status"] = "200";
        } else {
          $data["header"]["status"] = "401";
          $data["header"]["description"] = "You don't have permission to access to this project.";
        }
      } else {
        $data["header"]["status"] = "500";
        $data["header"]["description"] = "Project not found.";
      }
    } else {
      $data["header"]["status"] = "400";
      $data["header"]["description"] = "The parameter project 'id' is missing.";
    }

    return new Response(json_encode($data, JSON_PRETTY_PRINT));
  }
  
  /**
   * @Route("/api/project/oldshow", name="project/oldshow", defaults={"_format" = "json"})
   */
  public function apiProject(Request $request)
  {
    $data = self::initJSONArray("Get all information about a project");

    if ($request->query->has("id")) {
      /** @var ProjectRepository $repository */
      $repository = $this->getDoctrine()->getRepository('AppBundle:Project');
      /** @var Project $project */
      $project = $repository->getProjectFromID($request->query->get("id"));

      if ($project) {
        $user = self::isAuthorizedUser($request);
        if (self::isUserEnable($user, $project->getId(), "show")) {
          $p = array();
          $created = $project->getCreated()->format('l, Y-m-d H:i:s e');
          array_push($p,
            array("id" => $project->getId(),
              "name" => $project->getName(),
              "description" => $project->getDescription(),
              "evalformat"
              => array("type" => $project->getType(),
                "ranges" => json_decode($project->getRanges(), true),
                "requiref" => $project->getRequiref()
              ),
              "guidelines"
              => array(
                "specification"
                => array("randout" => $project->getRandout(),
                  "blindrev" => $project->getBlindrev(),
                  "maxdontknow" => $project->getMaxdontknow(),
                  "intragree" => $project->getIntragree() / 100,
                  "pooling" => $project->getPooling()
                ),
                "instructions" => $project->getInstructions()),
              "created" => $created));

          $user = self::hasUserRole($project->getId(), "admin", $request);
          if ($user != null) {
            /** @var RoleRepository $roleRepository */
            $roleRepository = $this->getDoctrine()->getRepository('AppBundle:Role');
            array_push($p,
              array("users" => $roleRepository->getRoles($project->getId(), $user->getId())));

            $dql = "SELECT p.id, p.task, p.completed, p.disabled, p.modified, p.duedate, p.user, u.username
                    FROM AppBundle:Progress AS p
                    INNER JOIN AppBundle:User AS u
                    WHERE p.user=u.id and p.project=:pid
                    ORDER BY p.num, p.id";
            /** @var Query $query */
            $query = $this->getDoctrine()->getManager()->createQuery($dql);
            $query->setParameter('pid', $project->getId());

            array_push($p,
              array("tasks" => $query->getResult()));

            array_push($p,
              array("evaluationProgress" => $this->getDoctrine()->getRepository('AppBundle:Annotation')->getAnnotationProgress($project->getId())));
          }
          $data["result"]["projects"] = $p;
          $data["header"]["status"] = "200";
        } else {
          $data["header"]["status"] = "401";
          $data["header"]["description"] = "You don't have permission to access to this project.";
        }
      } else {
        $data["header"]["status"] = "500";
        $data["header"]["description"] = "Project not found.";
      }
    } else {
      $data["header"]["status"] = "400";
      $data["header"]["description"] = "The parameter project 'id' is missing.";
    }

    return new Response(json_encode($data, JSON_PRETTY_PRINT));
  }

  /**
   * @Route("/api/project/create", name="project/create", defaults={"_format" = "json"})
   * @Method("POST")
   */
  public function apiCreateProject(Request $request)
  {
    /** @var User $user */
    $user = self::isAuthorizedUser($request);
    if ($user) {
      $data = self::initJSONArray("Create a new project");
      $data["header"]["status"] = "500";

      if ($request->query->has("name")) {
        $data["result"]["name"] = $request->query->get("name");

        /** @var ProjectRepository $repository */
        $repository = $this->getDoctrine()->getRepository('AppBundle:Project');
        /** @var Project $project */
        $project = $repository->findOneBy(array("name" => $data["result"]["name"], "active" => 1));
        if ($project) {
          $data["header"]["status"] = "500";
          $data["header"]["description"] = "A project with this name already exists";
        } else {
          $project = $repository->createProject(array("name" => $data["result"]["name"], "description" => ""));
          if ($project) {
            /** @var RoleRepository $role_repository */
            $role_repository = $this->getDoctrine()->getRepository("AppBundle:Role");
            $role_repository->updateRole($user->getId(), $user->getId(), $project->getId(), "owner", "accepted");
            //$role_repository->updateRole($user->getId(), $user->getId(), $project->getId(), "admin", "accepted");

            //init the progress status
            /** @var ProgressRepository $progress_repository */
            $progress_repository = $this->getDoctrine()->getRepository("AppBundle:Progress");
            $progress_repository->initProgress($project->getId(), $user->getId());

            $data["header"]["status"] = "200";
            $data["result"]["id"] = $project->getId();
            $data["result"]["link"] = $request->getSchemeAndHttpHost() . $this->get('router')->generate('admin') . "?pid=" . $project->getId();
          } else {
            $data["header"]["description"] = "An error occur during the project creation";
          }
        }
      } else {
        $data["header"]["status"] = "400";
        $data["header"]["description"] = "The parameter 'name' is missing.";
      }
    } else {
      $data["header"]["status"] = "401";
      $data["header"]["description"] = "Unauthorized";
    }
    return new Response(json_encode($data, JSON_PRETTY_PRINT));
  }

  /**
   * @Route("/api/project/update", name="project/update", defaults={"_format" = "json"})
   * @Method("POST")
   */
  public function apiUpdateProject(Request $request)
  {
    $data = self::initJSONArray("Update a project");
    $data["header"]["status"] = "500";

    if ($request->query->has("id")) {
      $user = self::hasUserRole($request->query->get("id"), "admin", $request);
      if ($user != null) {
        $paramList = array("id", "description");
        foreach ($paramList as $param) {
          if ($request->query->has($param)) {
            $data["result"][$param] = $request->query->get($param);
          }
        }

        /** @var ProjectRepository $projectRepository */
        $projectRepository = $this->getDoctrine()->getRepository('AppBundle:Project');
        if ($projectRepository->createProject($data["result"])) {
          $data["header"]["status"] = "201";
        }
      } else {
        $data["header"]["status"] = "401";
        $data["header"]["description"] = "Unauthorized";
      }
    } else {
      $data["header"]["status"] = "400";
      $data["header"]["description"] = "The parameter project 'id' is missing.";
    }
    return new Response(json_encode($data, JSON_PRETTY_PRINT));
  }

  /**
   * @Route("/api/project/delete", name="project/delete", defaults={"_format" = "json"})
   * @Method("GET")
   */
  public function apiDeleteProject(Request $request)
  {
    $data = self::initJSONArray("Remove a project from the list");
    $data["header"]["status"] = "500";
    $data["header"]["description"] = "Delete project failed";

    if ($request->query->has("id")) {
      /** @var ProjectRepository $projectRepository */
      $projectRepository = $this->getDoctrine()->getRepository('AppBundle:Project');
      /** @var Project $project */
      $project = $projectRepository->getProjectFromID($request->query->get("id"));

      if ($project) {
        $user = self::isAuthorizedUser($request);
        if (self::isUserEnable($user, $project->getId(), "delete")) {
          if ($projectRepository->deleteProject($project->getId()) == 1) {
            $data["header"]["status"] = "200";
            unset($data["header"]["description"]);
          }
        } else {
          $data["header"]["status"] = "401";
          $data["header"]["description"] = "You don't have the right permission! Only owner can delete own project.";
        }
      }
    } else {
      $data["header"]["status"] = "400";
      $data["header"]["description"] = "The parameter project 'id' is missing.";
    }

    return new Response(json_encode($data, JSON_PRETTY_PRINT));
  }

  //Claudio: old version
  public function apiProjectReport(Request $request)
  {
    $data = self::initJSONArray("Get the project report about the done evaluations and the reviewing");
    $data["header"]["status"] = "500";
    $data["header"]["description"] = "Report generation failed";

    if ($request->query->has("id")) {
      $projectId = $request->query->get("id");
      $user = self::isAuthorizedUser($request);
      if (self::isUserEnable($user, $projectId, "show")) {
        $annotation_repository = $this->getDoctrine()->getRepository("AppBundle:Annotation");
        $data["result"]["project"] = $projectId;
        $data["result"]["report"] = $annotation_repository->getEvalReportParams($projectId);
      } else {
        $data["header"]["status"] = "401";
        $data["header"]["description"] = "You don't have permission to access to the project.";
      }
    } else {
      $data["header"]["status"] = "400";
      $data["header"]["description"] = "The parameter project 'id' is missing.";
    }

    return new Response(json_encode($data, JSON_PRETTY_PRINT));
  }
  
    /**
   * @Route("/api/project/report", name="project/report", defaults={"_format" = "json"})
   * @Method("GET")
   */
  public function apiMyProjectReport(Request $request)
  {
  	// implemented by Claudio
  	
    $data = self::initJSONArray("Get the project report about the done evaluations and the reviewing");
    $data["header"]["status"] = "500";
    $data["header"]["description"] = "Report generation failed";

    if ($request->query->has("id")) {
      $projectId = $request->query->get("id");
      $user = self::isAuthorizedUser($request);
      if (self::isUserEnable($user, $projectId, "show")) {
      
        /** @var ProjectRepository $projectRepository */
        $projectRepository = $this->getDoctrine()->getRepository('AppBundle:Project');
		$project = $projectRepository->getProjectFromID($projectId);
		
        $annotation_repository = $this->getDoctrine()->getRepository("AppBundle:Annotation");
        

        /** @var SentenceRepository $sentence_repository */
        $sentence_repository = $this->getDoctrine()->getRepository('AppBundle:Sentence');
        $targetLabels = $sentence_repository->targetLabels($projectId);

        /** @var RoleRepository $repository */
        $roleRepository = $this->getDoctrine()->getRepository('AppBundle:Role');

        $ranges = json_decode($project->getRanges(), TRUE);

		$report = array();
		$summary = array();
		$summary["id"] = $projectId;
		$summary["type"] = $project->getType();
		$summary["countTUs"] = $sentence_repository->countTUs($projectId);
		$summary["countIntraTUs"] = $project->getIntratus();
		
		//Claudio: how can I get anonymized evaluators if needed?
		$evaluators = $roleRepository->getUserByRole($projectId,"evaluator");		
		$summary["countEvaluators"] = count($evaluators);		
     	
		$evalNum = 1;
		$evals = array();
		foreach ($evaluators as $evaluator) {
		  $eval = array();
		  //Claudio: discover the anonymization algorithm
          if ($project->getBlindrev()) {
            $eval["username"] = "Evaluator " . $evalNum;
            $evalNum++;
          } else {
            $eval["username"] = $evaluator["username"];
            $eval["id"] = $evaluator["id"];            
          }
          array_push($evals, $eval);
        }
        $summary["evaluators"] = $evals;
		$summary["countTargets"] = count($targetLabels);
		$summary["targetLabels"] = $targetLabels;
		$summary["countRanges"] = count($ranges);
		$summary["ranges"] = $ranges;
		$report["summary"] = $summary;
		
		
		$before = array();
		$before["results"]= $annotation_repository->evaluationSummary($project, $targetLabels, $ranges, false);
		$before["quality"]["kStat"] = $annotation_repository->calculateKappaQuality($projectId, true);
		$trustability = array();
		
		$evalNum = 1;
		foreach ($evaluators as $evaluator) {
		  $inter = array();
		  $Pnorm = $annotation_repository->getObservedIntraAgreement($projectId, $evaluator["id"], true);
          $Pe = $annotation_repository->getExpectedIntraAgreement($projectId, $evaluator["id"], true);
          $kappa=1;
          if ((1 - $Pe) != 0) {
      	    $kappa= round((($Pnorm - $Pe)/(1 - $Pe)), 3);
          }
          //Claudio: how can I get anonymized evaluators if needed?
          if ($project->getBlindrev()) {
            $inter["username"] = "Evaluator " . $evalNum;
            $evalNum++;
          } else {
            $inter["username"] = $evaluator["username"];
            $inter["id"] = $evaluator["id"];
          }
		  $inter["Pnorm"] = $Pnorm;
		  $inter["Pe"] = $Pe;
		  $inter["kStat"] = $kappa;
		  array_push($trustability, $inter);
		}
	    $before["trustability"]= $trustability;				
        $report["before"] = $before;
        
        if ($annotation_repository->countRevAnnotation($projectId) > 0) {
		  $after = array();
		  $after["results"]= $annotation_repository->evaluationSummary($project, $targetLabels, $ranges, true);
		  $after["quality"]["kStat"] = $annotation_repository->calculateKappaQuality($projectId, false);
          //reviewer evaluation divided by user + all
          
          //Claudio username or anonymous lable must be returned
          $after["evaluationsRev"] = $annotation_repository->reviewResults($projectId);
          $after["quality"]["totEvaluationsRev"] = 0;
          foreach ($after["evaluationsRev"] as $revInfo) {          
            $after["quality"]["totEvaluationsRev"] += $revInfo["numrev"];          
          }
		  $report["after"] = $after;
		}
        $data["result"]["report"] = $report;
        $data["header"]["status"] = "200";
        $data["header"]["description"] = "Returns the evaluation report of the specified project.";
        
      } else {
        $data["header"]["status"] = "401";
        $data["header"]["description"] = "You don't have permission to access to the project.";
      }
    } else {
      $data["header"]["status"] = "400";
      $data["header"]["description"] = "The parameter project 'id' is missing.";
    }

    return new Response(json_encode($data, JSON_PRETTY_PRINT));
  }

  /**
   * @Route("/api/project/export  ", name="project/export", defaults={"_format" = "json"})
   * @Method("GET")
   */
  public function apiProjectExport(Request $request)
  {
    $data = self::initJSONArray("Export the evalautions of the project");
    $data["header"]["status"] = "500";
    $data["header"]["description"] = "Export data failed";

    $user = self::isAuthorizedUser($request);
    if ($user) {
      if ($request->query->has("id")) {
        $projectId = $request->query->get("id");
        $user = self::isAuthorizedUser($request);
        if (self::isUserEnable($user, $projectId, "show")) {
          /** @var ProjectRepository $projectRepository */
          $projectRepository = $this->getDoctrine()->getRepository('AppBundle:Project');
          /** @var Project $project */
          $project = $projectRepository->getProjectFromID($projectId);

          $format = "csv";
          if ($request->query->has("format")) {
            $format = $request->query->get("format");
          }
          if ($format == "csv" || $format == "json") {
            /** @var AnnotationRepository $annotation_repository */
            $annotation_repository = $this->getDoctrine()->getRepository("AppBundle:Annotation");
            return $annotation_repository->exportEvaluation($project->getId(), $project->getType(), $user->getId(), $format);
          } else {
            $data["header"]["status"] = "400";
            $data["header"]["description"] = "The parameter 'format' is not valid. The available formats are 'csv' and 'json'.";
          }
        } else {
          $data["header"]["status"] = "401";
          $data["header"]["description"] = "You don't have permission to access to the project.";
        }
      } else {
        $data["header"]["status"] = "400";
        $data["header"]["description"] = "The parameter project 'id' is missing.";
      }
    } else {
      $data["header"]["status"] = "401";
      $data["header"]["description"] = "Unauthorized";
    }
    return new Response(json_encode($data, JSON_PRETTY_PRINT));
  }

  /**
   * @Route("/api/user/list", name="user/list", defaults={"_format" = "json"})
   */
  public function apiUsers(Request $request)
  {
    $data = self::initJSONArray("Get the list of the registered users");
    if (self::isAuthorizedUser($request)) {
      /** @var UserRepository $userRepository */
      $userRepository = $this->getDoctrine()->getRepository("AppBundle:User");
      $users = $userRepository->findBy(array("active" => 1), array('username' => 'DESC'));

      $p = array();
      /** @var User $user */
      foreach ($users as $user) {
        $u = array("id" => $user->getId(),
          "username" => $user->getUsername(),
          "firstname" => $user->getFirstname(),
          "lastname" => $user->getLastname(),
          "affiliation" => $user->getAffiliation(),
          "email" => $user->getEmail(),
          "roles" => $user->getRoles(),
          "registered" => $user->getRegistered()->format('l, Y-m-d H:i:s e'),
          "logged" => $user->getLogged()->format('l, Y-m-d H:i:s e'),
        );
        array_push($p, $u);
      }
      $data["header"]["status"] = "200";
      $data["result"]["users"] = $p;
    } else {
      $data["header"]["status"] = "401";
      $data["header"]["description"] = "Unauthorized - You need to use your access token";
    }
    return new Response(json_encode($data, JSON_PRETTY_PRINT));
  }

  /**
   * @Route("/api/user/search", name="user/search", defaults={"_format" = "json"})
   */
  public function apiSearchUsers(Request $request)
  {
    $data = self::initJSONArray("Get the list of the registered users");
    if (self::isAuthorizedUser($request)) {
      if ($request->query->has("q")) {
        $dql = "SELECT u.id,u.username,u.firstname,u.lastname,u.affiliation,u.email,u.roles,u.registered,u.logged
        FROM AppBundle:User AS u
        WHERE u.active = 1 AND u.username LIKE :q";

        /** @var Query $query */
        $query = $this->getDoctrine()->getManager()->createQuery($dql);
        $query->setParameter('q', '%' . $request->query->get("q") . '%');

        $data["header"]["status"] = "200";
        $data["result"]["users"] = $query->getResult();
      } else {
        $data["header"]["status"] = "400";
        $data["header"]["description"] = "The parameter 'q' is missing.";
      }
    } else {
      $data["header"]["status"] = "401";
      $data["header"]["description"] = "Unauthorized - You need to use your access token";
    }
    return new Response(json_encode($data, JSON_PRETTY_PRINT));
  }

/**
   * @Route("/api/user/update", name="user/update", defaults={"_format" = "json"})
   */
  public function apiUserUpdate(Request $request)
  {
  	// implemented by Claudio
  	$data = self::initJSONArray("Updates the specified user.");
  	
    $user = self::isAuthorizedUser($request);
    if ($user) {
      /** @var RoleRepository $repository */
      $repository = $this->getDoctrine()->getRepository('AppBundle:User');

      $a = array();
        
      if ($request->query->has("firstname")) {
        $a["firstname"] = $request->query->get("firstname");
      }
      if ($request->query->has("lastname")) {
        $a["lastname"] = $request->query->get("lastname");
      }
      
      if ($request->query->has("affiliation")) {
        $a["affiliation"] = $request->query->get("affiliation");
      }
      if ($request->query->has("email")) {
        $existingUser = $repository->getUser(array("email" => $request->query->get("email")));
        if ($existingUser && $existingUser->getVerified() != null) {
          $data["result"]["message"] = "The email cannot be changed: it's already taken!";                  
        } else {
          $a["email"] = $request->query->get("email");
        }
      }
      if ($request->query->has("username")) {
        $existingUser = $repository->getUser(array("username" => $request->query->get("username")));
        if ($existingUser && $existingUser->getVerified() != null) {
          $data["result"]["message"] = "The username cannot be changed: it's already taken!";                  
        } else {
          $a["username"] = $request->query->get("username");
        }
        
      }
      if ($request->query->has("roles")) {
        $a["roles"] = $request->query->get("roles");
      }

	  if ($repository->updateUser($user->getId(), $a)){
	  	$data["header"]["status"] = "200";
	  	$data["result"]["username"] = $user->getUsername();
	  	$data["result"]["firstname"] = $user->getFirstname();
	  	$data["result"]["lastname"] = $user->getLastname();
	  	$data["result"]["affiliation"] = $user->getAffiliation();
	  	$data["result"]["email"] = $user->getEmail();
	  	$data["result"]["roles"] = $user->getRoles();
	  } else {
	    $data["header"]["status"] = "400";
        $data["result"]["description"] = "The user cannot be updated";
	  }

    } else {
      $data["header"]["status"] = "401";
      $data["header"]["description"] = "Unauthorized";
    }
    return new Response(json_encode($data, JSON_PRETTY_PRINT));
  }
  
  /**
   * @Route("/api/user/create", name="user/create", defaults={"_format" = "json"})
   */
  public function apiUserCreate(Request $request)
  {
  	// implemented by Claudio
  	
    $data = self::initJSONArray("Creates an account in the MT-EQuAl release installed on the specified hostname.");
    
    if ($request->query->has("username") && $request->query->has("password") && $request->query->has("email")) {
      /** @var UserRepository $userRepository */
      $userRepository = $this->getDoctrine()->getRepository("AppBundle:User");

      $user = $userRepository->getUser(array("email" => $request->query->get("email")));
      if ($user && $user->getVerified() != null) {
          $data["header"]["status"] = "400";
          $data["header"]["description"] = "The user already exists!";                  
      } else {
        $a = array("username" => $request->query->get("username"),"password" => $request->query->get("password"),"email" => $request->query->get("email"));
        
        // add optional parameters
        if ($request->query->has("firstname")) {
          array_push($a, array("firstname" => $request->query->get("firstname")));
        }
        if ($request->query->has("lastname")) {
          array_push($a, array("lastname" => $request->query->get("lasttname")));
        }
        if ($request->query->has("affiliation")) {
          array_push($a, array("affiliation" => $request->query->get("affiliation")));
        }
        
        $user = $userRepository->createUser($a);
        if ($user) {
      
          if ($userRepository->verifyUser($user->getEmail(), $user->getHash())) {
      
            $data["header"]["status"] = "200";
            $data["result"]["username"] = $request->query->get("username");
            //$data["header"]["password"] = $request->query->get("password");
            $data["result"]["email"] = $request->query->get("email");
            $data["result"]["hash"] = $user->getHash();
            $data["result"]["X-Api-Key"] = $user->getApiKey();
            $data["result"]["id"] = $user->getId();
          } else {
            $data["header"]["status"] = "400";
            $data["header"]["description"] = "The user cannot be activated!";                  
          }
        } else {
          $data["header"]["status"] = "400";
          $data["header"]["description"] = "The user cannot be created!";        
        }
      }
    } else {
	  $data["header"]["status"] = "400";
      $data["header"]["description"] = "Missing required parameters.";
    }
      
    return new Response(json_encode($data, JSON_PRETTY_PRINT));
  }

  /**
   * @Route("/api/user/delete", name="user/delete", defaults={"_format" = "json"})
   */
  public function apiUserDelete(Request $request)
  {
  	// implemented by Claudio
  	
    $data = self::initJSONArray("Deletes an account in the MT-EQuAl release installed on the specified hostname.");
  	
    $user = self::isAuthorizedUser($request);
    if ($user) {

      $repository = $this->getDoctrine()->getRepository('AppBundle:User');

      $data["header"]["status"] = "200";
      $data["result"]["user"] = $user->getId();
      $data["result"]["X-Api-Key"] = $request->headers->get("X-Api-Key");
	  if ($repository->deleteUser($user->getId())) {
        $data["result"]["message"] = $user->getUsername() . "(id = " . $user->getId() . ") has been deleted";	  
      } else {
        $data["header"]["status"] = "400";
        $data["header"]["description"] = $user->getUsername() . "(id = " . $user->getId() . ") cannot be deleted";	  
      }
    } else {
      $data["header"]["status"] = "401";
      $data["header"]["description"] = "Unauthorized";
    }
    return new Response(json_encode($data, JSON_PRETTY_PRINT));
  }
   
     /**
   * @Route("/api/user/show", name="user/show", defaults={"_format" = "json"})
   */
  public function apiUserShow(Request $request)
  {
  	// implemented by Claudio
  	
    $data = self::initJSONArray("Shows an account in the MT-EQuAl release installed on the specified hostname.");
  	
    $user = self::isAuthorizedUser($request);
    if ($user) {

      $data["header"]["status"] = "200";
      $data["result"]["id"] = $user->getId();
      $data["result"]["username"] = $user->getUsername();
      $data["result"]["firstname"] = $user->getFirstname();
      $data["result"]["lastname"] = $user->getLastname();
      $data["result"]["email"] = $user->getEmail();
      $data["result"]["X-Api-Key"] = $user->getApiKey();
      $data["result"]["affiliation"] = $user->getAffiliation();
      $data["result"]["registered"] = $user->getRegistered();
      $data["result"]["logged"] = $user->getLogged();
      $data["result"]["roles"] = $user->getRoles();
    } else {
      $data["header"]["status"] = "401";
      $data["header"]["description"] = "Unauthorized";
    }
    return new Response(json_encode($data, JSON_PRETTY_PRINT));
  }
   
  /**
   * @Route("/api/user/requests", name="user/requests", defaults={"_format" = "json"})
   */
  public function apiUserRequests(Request $request)
  {
    $user = self::isAuthorizedUser($request);
    if ($user) {
      /** @var RoleRepository $repository */
      $repository = $this->getDoctrine()->getRepository('AppBundle:Role');

      $data["header"]["status"] = "200";
      $data["result"]["requests"] = $repository->getUserRequests($user->getId());
    } else {
      $data["header"]["status"] = "401";
      $data["header"]["description"] = "Unauthorized";
    }
    return new Response(json_encode($data, JSON_PRETTY_PRINT));
  }

  /**
   * @Route("/api/refresh_token", name="api_refresh_token")
   */
  public function adminRefreshToken(Request $request)
  {
    $data["header"]["status"] = "500";
    $data["header"]["description"] = "Refresh the user access token.";
    /** @var User $user */
    $user = self::isAuthorizedUser($request);
    if ($user != null) {
      /** @var UserRepository $repository_user */
      $repository = $this->getDoctrine()->getRepository('AppBundle:User');
      if ($repository->updateUser($user->getId(), array("api_key" => "refresh"))) {
        $user = $repository->getUser(array('id' => $user->getId()));
        if ($user) {
          $data["header"]["status"] = "200";
          $data["result"]["token"] = $user->getApiKey();
        }
      }
    } else {
      $data["header"]["status"] = "400";
      $data["header"]["description"] = "Unauthorized";
    }
    return new Response(json_encode($data, JSON_PRETTY_PRINT));
  }

  /**
   * update task
   * @Route("/api/task/update", name="task/update")
   */
  public function setTaskUpdate(Request $request)
  {
    $data["header"]["status"] = "500";
    $data["header"]["description"] = "Task update failed!";
    /** @var User $user */
    $user = self::isAuthorizedUser($request);
    if ($user != null) {
      if ($request->query->has("id")) {
        $projectid = $request->query->get("id");
        /** @var ProgressRepository $repository_progress */
        $repository_progress = $this->getDoctrine()->getRepository('AppBundle:Progress');

        if ($request->query->has("task")) {
          $task = $request->query->get("task");

          if (array_key_exists($task, $repository_progress::$TASK_DEPENDENCIES)) {
            /** @var Progress $progress */
            $searchParams = array("project" => $projectid, "task" => $task);
            if (in_array($task, $repository_progress::$USER_TASKS)) {
              $searchParams{"user"} = $user->getId();
            }
            /** @var RoleRepository $repository_role */
            $repository_role = $this->getDoctrine()->getRepository('AppBundle:Role');

            $progress = $repository_progress->findOneBy($searchParams);
            if ($progress &&
              ($progress->getUser() == $user->getId() ||
                $repository_role->getRole($user->getId(), $projectid, "admin") ||
                $repository_role->getRole($user->getId(), $projectid, $repository_progress::$TASK_DEPENDENCIES[0]))
            ) {
              if ($request->query->has("action")) {
                $action = $request->query->get("action");
                if ($action == "done") {
                  if ($progress->getCompleted()) {
                    $data["header"]["status"] = "400";
                    $data["header"]["description"] = "This task has been already completed.";
                  } else if ($progress->getDisabled()) {
                    $data["header"]["status"] = "400";
                    $data["header"]["description"] = "This task can't start before the preceding dependent task/s is/are finished.";
                  } else {
                    $progress = self::doneTask($task, $projectid, $user->getId());
                    if ($progress->getCompleted()) {
                      $data["header"]["status"] = "200";
                      $data["header"]["description"] = "The task has been updated.";
                      $data["result"]["project"] = $progress->getProject();
                      $data["result"]["task"] = $task;
                      $data["result"]["disabled"] = $progress->getDisabled();
                      $data["result"]["completed"] = $progress->getCompleted();
                    }
                  }
                } else if ($action == "undone") {
                  if (!$progress->getCompleted()) {
                    $data["header"]["status"] = "400";
                    $data["header"]["description"] = "This task can't be reset because its status is not completed yet.";
                  } else if ($progress->getDisabled()) {
                    $data["header"]["status"] = "400";
                    $data["header"]["description"] = "This task can't start before the preceding dependent task/s is/are finished.";
                  } else {
                    $progress = self::undoneTask($task, $projectid, $user);
                    if (!$progress->getCompleted()) {
                      $data["header"]["status"] = "200";
                      $data["header"]["description"] = "The task has been updated.";
                      $data["result"]["project"] = $progress->getProject();
                      $data["result"]["task"] = $task;
                      $data["result"]["disabled"] = $progress->getDisabled();
                      $data["result"]["completed"] = $progress->getCompleted();
                    }
                  }
                } else {
                  $data["header"]["status"] = "400";
                  $data["header"]["description"] = "The action values must be 'done' or 'undone'.";
                }
              } else {
                $data["header"]["status"] = "400";
                $data["header"]["description"] = "The parameter 'action' is missing.";
              }
            } else {
              $data["header"]["status"] = "401";
              $data["header"]["description"] = "You have no permission to change this task.";
            }
          } else {
            $data["header"]["status"] = "400";
            $data["header"]["description"] = "A valid task name must be one of the following: " . join(", ", array_keys($repository_progress::$TASK_DEPENDENCIES));
          }

        } else {
          $data["header"]["status"] = "400";
          $data["header"]["description"] = "The parameter 'task' is missing.";
        }
      } else {
        $data["header"]["status"] = "400";
        $data["header"]["description"] = "The parameter 'id' is missing.";
      }
    } else {
      $data["header"]["status"] = "401";
      $data["header"]["description"] = "Unauthorized";
    }
    return new Response(json_encode($data, JSON_PRETTY_PRINT));
  }

  /**
   * @Route("/api/user/replyInvitation", name="user/replyInvitation", defaults={"_format" = "json"})
   */
  public function apiUserReply(Request $request)
  {
    $data["header"]["status"] = "500";
    $data["header"]["description"] = "Reply invitation failed!";
    $user = self::isAuthorizedUser($request);
    if ($user) {
      if ($request->query->has("hash")) {
        $hash = $request->query->get("hash");

        if ($request->query->has("action")) {
          $action = $request->query->get("action");

          /** @var RoleRepository $repository */
          $repository = $this->getDoctrine()->getRepository('AppBundle:Role');

          /** @var Role $role */
          $role = $repository->findOneBy(array("hash" => $hash));
          if ($role != null) {
            if ($role->getUser() == $user->getId()) {
              if ($role->getStatus() == "pending") {
                if ($action == "accepted" || $action == "declined") {
                  $role = $repository->updateRole($role->getCreatedBy(), $role->getUser(), $role->getProject(), $role->getRole(), $action);
                  if ($role) {
                    if ($role->getStatus() == $action) {
                      $data["header"]["status"] = "200";
                      $data["header"]["description"] = "Accepts or declines an invitation to work on a specified project in the MT-EQuAl release installed on the specified hostname.";
                      $data["result"] = array("role" => $role->getRole(),
                        "project" => $role->getProject(),
                        "user" => $role->getUser(),
                        "role_status" => $role->getStatus(),
                        "hash" => $role->getHash());
                    }
                  }
                } else {
                  $data["header"]["status"] = "400";
                  $data["header"]["description"] = "The action values must be 'accepted' or 'declined'.";
                }
              } else {
                $data["header"]["status"] = "500";
                $data["header"]["description"] = 'The invitation failed because its status was not pending.';
              }
            } else {
              $data["header"]["status"] = "401";
              $data["header"]["description"] = "You have no permission to change this invitation.";
            }
          } else {
            $data["header"]["status"] = "403";
            $data["header"]["description"] = "This request is not valid or it has been already performed.";
          }
        } else {
          $data["header"]["status"] = "400";
          $data["header"]["description"] = "The parameter 'action' is missing.";
        }
      } else {
        $data["header"]["status"] = "400";
        $data["header"]["description"] = "The parameter 'hash' is missing.";
      }
    } else {
      $data["header"]["status"] = "401";
      $data["header"]["description"] = "Unauthorized";
    }
    return new Response(json_encode($data, JSON_PRETTY_PRINT));
  }

  /**
   * @Route("/api/user/invite", name="user/invite", defaults={"_format" = "json"})
   */
  public function apiInviteUser(Request $request)
  {
    $data = self::initJSONArray("Invite a user in a project");
    $invitation = null;
    if ($request->query->has("id")) {
      $projectId = $request->query->get("id");

      /** @var User $requestUser */
      $requestUser = self::hasUserRole($projectId, "admin", $request);
      if ($requestUser == null) {
        $requestUser = self::hasUserRole($projectId, "owner", $request);
      }
      if ($requestUser == null) {
        $requestUser = self::hasUserRole($projectId, "vendor", $request);
      }
      if ($requestUser != null) {
        if ($request->query->has("username")) {
          $role = "";
          if ($request->query->has("role") && in_array($request->query->get("role"), self::$ROLES)) {
            $role = $request->query->get("role");
          }
          if ($role != "") {
            /** @var UserRepository $userRepository */
            $userRepository = $this->getDoctrine()->getRepository("AppBundle:User");
            /** @var User $user */
            $user = $userRepository->getUser(array("username" => $request->query->get("username")));
            if ($user != null) {
              /** @var RoleRepository $roleRepository */
              $roleRepository = $this->getDoctrine()->getRepository("AppBundle:Role");

              if (!$roleRepository->getRole($user->getId(), $projectId, $role)) {
                $invitation = $roleRepository->updateRole($requestUser->getId(), $user->getId(), $projectId, $role, null);

                if ($invitation != null) {
                  $data["header"]["status"] = "200";
                  $data["result"]["role"] = $invitation->getRole();
                  $data["result"]["role_status"] = $invitation->getStatus();
                  $data["result"]["user"] = $invitation->getUser();

                  try {
                    /** @var ProjectRepository $projectRepository */
                    $projectRepository = $this->getDoctrine()->getRepository("AppBundle:Project");
                    /** @var Project $project */
                    $project = $projectRepository->getProjectFromID($invitation->getProject());

                    if ($project) {
                      if (UserController::sendInvitationMail($user->getEmail(), $user->getUsername(), $requestUser->getUsername(), $project->getName(), $role, $user->getHash())) {
                        $data["header"]["message"] = "The email has been sent to " . $user->getUsername();
                        //send an email to who invited the user if he is not who call the request
                        if ($invitation->getCreatedBy() != $requestUser->getId()) {
                          /** @var User $inviteeUser */
                          $inviteeUser = $userRepository->getUser(array("id" => $invitation->getCreatedBy()));
                          if ($inviteeUser) {
                            if ($this->sendInvitationMail($inviteeUser->getEmail(), $user->getUsername(), $requestUser->getUsername(), $project->getName(), $role, $user->getHash(), $inviteeUser->getUsername())) {
                              $data["header"]["message"] .= " and " . $inviteeUser->getUsername();
                            } else {
                              $data["header"]["message"] = "Sending email failed!";
                            }
                          }
                        }
                      } else {
                        $data["header"]["status"] = "403";
                        $data["header"]["message"] = "Sending email failed!";
                      }
                    } else {
                      $data["header"]["status"] = "500";
                      $data["header"]["description"] = "The project doesn't found.";
                    }
                  } catch (\Exception $e) {
                    $data["header"]["status"] = "500";
                    $data["header"]["description"] = 'An error occurred sending the email message!';
                  }
                } else {
                  $data["header"]["status"] = "500";
                  $data["header"]["description"] = "Invitation failed.";
                }
              } else {
                $data["header"]["status"] = "500";
                $data["header"]["description"] = "Invitation failed because this user has been already invite as $role.";
              }
            } else {
              $data["header"]["status"] = "500";
              $data["header"]["description"] = "The user doesn't found.";
            }
          } else {
            $data["header"]["status"] = "400";
            $data["header"]["description"] = "The parameter 'role' is missing or not valid.";
          }
        } else {
          $data["header"]["status"] = "400";
          $data["header"]["description"] = "The parameter 'username' is missing.";
        }
      } else {
        $data["header"]["status"] = "401";
        $data["header"]["description"] = "Unauthorized";
      }
    } else {
      $data["header"]["status"] = "400";
      $data["header"]["description"] = "The parameter project 'id' is missing.";
    }

    return new Response(json_encode($data, JSON_PRETTY_PRINT));
  }

  /**
   * @Route("/api/user/revoke", name="user/revoke", defaults={"_format" = "json"})
   */
  public function apiRevokeUser(Request $request)
  {
    $data = self::initJSONArray("Revoke a user in a project");
    $invitation = null;
    if ($request->query->has("id")) {
      $projectId = $request->query->get("id");

      /** @var User $requestUser */
      $requestUser = self::hasUserRole($projectId, "admin", $request);
      if ($requestUser == null) {
        $requestUser = self::hasUserRole($projectId, "owner", $request);
      }
      if ($requestUser == null) {
        $requestUser = self::hasUserRole($projectId, "vendor", $request);
      }
      if ($requestUser != null) {
        if ($request->query->has("username")) {
          $role = "";
          if ($request->query->has("role") && in_array($request->query->get("role"), self::$ROLES)) {
            $role = $request->query->get("role");
          }
          if ($role != "") {
            /** @var UserRepository $userRepository */
            $userRepository = $this->getDoctrine()->getRepository("AppBundle:User");
            /** @var User $user */
            $user = $userRepository->getUser(array("username" => $request->query->get("username")));
            if ($user != null) {
              /** @var RoleRepository $roleRepository */
              $roleRepository = $this->getDoctrine()->getRepository("AppBundle:Role");

              if ($roleRepository->getRole($user->getId(), $projectId, $role)) {
                $invitation = $roleRepository->updateRole($requestUser->getId(), $user->getId(), $projectId, $role, "revoked");

                if ($invitation != null) {
                  $data["header"]["status"] = "200";
                  $data["result"]["role"] = $invitation->getRole();
                  $data["result"]["role_status"] = $invitation->getStatus();
                  $data["result"]["user"] = $invitation->getUser();

                  try {
                    /** @var ProjectRepository $projectRepository */
                    $projectRepository = $this->getDoctrine()->getRepository("AppBundle:Project");
                    /** @var Project $project */
                    $project = $projectRepository->getProjectFromID($invitation->getProject());

                    if ($project) {
                      if ($this->revokeInvitationMail($user->getEmail(), $user->getUsername(), $requestUser->getUsername(), $project->getName(), $role)) {
                        $data["header"]["message"] = "The email has been sent to " . $user->getUsername();

                        //send an email to who invited the user if he is not who call the request
                        if ($role->getCreatedBy() != $requestUser->getId()) {
                          /** @var User $inviteeUser */
                          $inviteeUser = $userRepository->getUser(array("id" => $role->getCreatedBy()));
                          if ($inviteeUser) {
                            if ($this->revokeInvitationMail($inviteeUser->getEmail(), $user->getUsername(), $requestUser->getUsername(), $project->getName(), $role, $inviteeUser->getUsername())) {
                              $data["header"]["message"] .= " and " . $inviteeUser->getUsername();
                            } else {
                              $data["header"]["message"] = "Sending email failed!";
                            }
                          }
                        }
                      } else {
                        $data["header"]["message"] = "Sending email failed!";
                      }
                    } else {
                      $data["header"]["status"] = "500";
                      $data["header"]["description"] = "The project doesn't found.";
                    }
                  } catch (\Exception $e) {
                    $data["header"]["status"] = "500";
                    $data["header"]["description"] = 'An error occurred sending the email message!' . $e;
                  }
                } else {
                  $data["header"]["status"] = "500";
                  $data["header"]["description"] = "Revoke notification failed.";
                }
              } else {
                $data["header"]["status"] = "500";
                $data["header"]["description"] = "Revoke notification failed because this user has no role as $role in the project.";
              }
            } else {
              $data["header"]["status"] = "500";
              $data["header"]["description"] = "The user doesn't found.";
            }
          } else {
            $data["header"]["status"] = "400";
            $data["header"]["description"] = "The parameter 'role' is missing or not valid.";
          }
        } else {
          $data["header"]["status"] = "400";
          $data["header"]["description"] = "The parameter 'username' is missing.";
        }
      } else {
        $data["header"]["status"] = "401";
        $data["header"]["description"] = "Unauthorized";
      }
    } else {
      $data["header"]["status"] = "400";
      $data["header"]["description"] = "The parameter project 'id' is missing.";
    }

    return new Response(json_encode($data, JSON_PRETTY_PRINT));
  }


  /**
   * @Route("/api/project/evalformat", name="project/evalformat", defaults={"_format" = "json"})
   * @Method("POST")
   */
  public function apiSetEvaluationFormat(Request $request)
  {
    $data = self::initJSONArray("Set the evaluation format");
    $data["header"]["status"] = "500";
    $listOfFormats = ["binary", "scale", "ranking", "tree"];
    if ($request->query->has("id")) {
      $data["result"]["id"] = $request->query->get("id");
      $user = self::hasUserRole($data["result"]["id"], "admin", $request);
      if ($user == null) {
        $user = self::hasUserRole($data["result"]["id"], "owner", $request);
      }
      if ($user != null) {
        if ($request->query->has("type") &&
          in_array($request->query->has("type"), $listOfFormats)
        ) {
          $data["result"]["type"] = $request->query->get("type");

          // Claudio: bug fixed: check for targets: also the ranking uses the json from the content
          //if ($data["result"]["type"] == "ranking") {
          //  if ($request->query->has("targets")) {
          //    $data["result"]["targets"] = $request->query->get("targets");
          //  } else {
          //    $data["header"]["status"] = "400";
          //    $data["header"]["description"] = "The parameter 'targets' is missing. This integer tells the number of targets to be ranked.";
          //  }
          //} else {
            if (strlen($request->getContent()) > 0) {
              $error = self::json_validate($request->getContent());
              if ($error == "") {
                $data["header"]["status"] = "201";
                $data["result"]["ranges"] = $request->getContent();
              } else {
                $data["header"]["status"] = "500";
                $data["header"]["description"] = $error;
              }
            } else {
              $data["header"]["status"] = "400";
              $data["header"]["description"] = "The content of the POST request doesn't contain any JSON.";
            }
          //}
          if ($request->query->has("requiref")) {
            $data["result"]["requiref"] = $request->query->get("requiref");
          }

          if ($data["header"]["status"] == "201") {
            /** @var ProjectRepository $projectRepository */
            $projectRepository = $this->getDoctrine()->getRepository('AppBundle:Project');
            if ($projectRepository->createProject($data["result"])) {
            
              // Claudio: bug fixed: removed duplicated [ ] 
              $data["result"]["ranges"] = json_decode($request->getContent());
            } else {
              $data["header"]["status"] = "500";
              $data["header"]["description"] = "The setting of the evaluation format failed.";
            }
          }
        } else {
          $data["header"]["status"] = "400";
          $data["header"]["description"] = "The parameter evaluation 'type' is missing or not valid (use one of the following: " . join(", ", $listOfFormats) . ")";
        }
      } else {
        $data["header"]["status"] = "401";
        $data["header"]["description"] = "Unauthorized";
      }
    } else {
      $data["header"]["status"] = "400";
      $data["header"]["description"] = "The parameter project 'id' is missing.";
    }
    return new Response(json_encode($data, JSON_PRETTY_PRINT));
  }

  private function json_validate($string)
  {
    // decode the JSON data
    $result = json_decode($string);

    // switch and check possible JSON errors
    switch (json_last_error()) {
      case JSON_ERROR_NONE:
        $error = ''; // JSON is valid // No error has occurred
        break;
      case JSON_ERROR_DEPTH:
        $error = 'The maximum stack depth has been exceeded.';
        break;
      case JSON_ERROR_STATE_MISMATCH:
        $error = 'Invalid or malformed JSON.';
        break;
      case JSON_ERROR_CTRL_CHAR:
        $error = 'Control character error, possibly incorrectly encoded.';
        break;
      case JSON_ERROR_SYNTAX:
        $error = 'Syntax error, malformed JSON.';
        break;
      // PHP >= 5.3.3
      case JSON_ERROR_UTF8:
        $error = 'Malformed UTF-8 characters, possibly incorrectly encoded.';
        break;
      // PHP >= 5.5.0
      case JSON_ERROR_RECURSION:
        $error = 'One or more recursive references in the value to be encoded.';
        break;
      // PHP >= 5.5.0
      case JSON_ERROR_INF_OR_NAN:
        $error = 'One or more NAN or INF values in the value to be encoded.';
        break;
      case JSON_ERROR_UNSUPPORTED_TYPE:
        $error = 'A value of a type that cannot be encoded was given.';
        break;
      default:
        $error = 'Unknown JSON error occured.';
        break;
    }

    return $error;
  }

  /**
   * @Route("/api/project/specifications", name="project/specifications", defaults={"_format" = "json"})
   * @Method("POST")
   */
  public function apiSetSpecifications(Request $request)
  {
    $data = self::initJSONArray("Set the specifications");
    $data["header"]["status"] = "500";

    if ($request->query->has("id")) {
      $data["result"]["id"] = $request->query->get("id");
      $user = self::hasUserRole($data["result"]["id"], "admin", $request);
      if ($user == null) {
        $user = self::hasUserRole($data["result"]["id"], "owner", $request);
      }
      if ($user != null) {
        $foundParams = 0;
        $paramList = array("randout", "blindrev", "maxdontknow", "intragree", "pooling");
        foreach ($paramList as $param) {
          if ($request->query->has($param)) {
            $data["result"][$param] = $request->query->get($param);
            $foundParams++;
          }
        }

        if ($foundParams > 0) {
          /** @var ProjectRepository $projectRepository */
          $projectRepository = $this->getDoctrine()->getRepository('AppBundle:Project');
          /** @var Project $project */
          $project = $projectRepository->getProjectFromID($data["result"]["id"]);
          if ($project) {
            if ($projectRepository->createProject($data["result"])) {
              $data["header"]["status"] = "201";
            }
            unset($data["result"]["instructions"]);
          } else {
            $data["header"]["description"] = "Project not found.";
          }
        } else {
          $data["header"]["status"] = "400";
          $data["header"]["description"] = "No valid parameters found. You can set one of these: " . join(", ", $paramList);
        }
      } else {
        $data["header"]["status"] = "401";
        $data["header"]["description"] = "Unauthorized";
      }
    } else {
      $data["header"]["status"] = "400";
      $data["header"]["description"] = "The parameter project 'id' is missing.";
    }
    return new Response(json_encode($data, JSON_PRETTY_PRINT));
  }

  /**
   * @Route("/api/project/import", name="project/import", defaults={"_format" = "json"})
   * @Method({"GET"})
   */
  public function apiProjectImport(Request $request)
  {
  	// implemented by Claudio
  	
    $data = self::initJSONArray("Imports the evaluation format and the guidelines from another project");
    $data["header"]["status"] = "500";
      $data["header"]["id"] = $request->query->get("id");
            $data["header"]["basedOn"] = $request->query->get("basedOn");

    if ($request->query->has("id") && $request->query->has("basedOn")) {
      $data["result"]["id"] = $request->query->get("id");
      $data["result"]["basedOn"] = $request->query->get("basedOn");
      
      //if ($request->getContent()) {
        $user = self::hasUserRole($data["result"]["id"], "admin", $request);
        if ($user == null) {
          $user = self::hasUserRole($data["result"]["id"], "owner", $request);
        }
        if ($user != null) {
          /** @var ProjectRepository $projectRepository */
          $projectRepository = $this->getDoctrine()->getRepository('AppBundle:Project');
          
          $project = $projectRepository->getProjectFromID($request->query->get("basedOn"));
                    $pp = $projectRepository->getProject($request->query->get("basedOn"));
          if ($project) {
          $data["header"]["status"] = "200";
          $data["header"]["pp"] = $pp;
          $a = array();
            $a["id"] = $request->query->get("id");
            
            if ($request->query->has("evalformat") && $request->query->get("evalformat") == 1) {
              $a["type"] = $project->getType();
              $a["ranges"] = $project->getRanges();
              //if ($project->getRequiref()) {
                $a["requiref"] = $project->getRequiref();
              //}
              //$a["evalformat"] = $project->getEvalformat();
            }

            if ($request->query->has("guidelines") && $request->query->get("guidelines") == 1) {
              $a["pooling"] = $project->getPooling();
              $a["instructions"] = $project->getInstructions();                                    
              $a["randout"] = $project->getRandout();            
              $a["intragree"] = $project->getIntragree();            
              $a["maxdontknow"] = $project->getMaxdontknow();
              //if ($project->getBlindrev()) {
                $a["blindrev"] = $project->getBlindrev();
              //}
              $a["guidelines"] = $project->getGuidelines();
            }
            
            if ($projectRepository->createProject($a)) {
              $data["header"]["status"] = "201";
          	} 
            $data["header"]["result"] = $a;

         } else {
            $data["result"]["error"] = "project errore";
         }

        } else {
          $data["header"]["status"] = "401";
          $data["header"]["description"] = "Unauthorized";
        }
      //} else {
      //  $data["header"]["status"] = "400";
      //  $data["header"]["description"] = "The project parameters are missing.";
      //}
    } else {
      $data["header"]["status"] = "400";
      $data["header"]["description"] = "Missing required parameters";
    }
    return new Response(json_encode($data, JSON_PRETTY_PRINT));
  }
  
  /**
   * @Route("/api/project/instructions", name="project/instructions", defaults={"_format" = "json"})
   * @Method({"POST"})
   */
  public function apiSetInstructions(Request $request)
  {
    $data = self::initJSONArray("Set the instructions for evaluators");
    $data["header"]["status"] = "500";

    if ($request->query->has("id")) {
      $data["result"]["id"] = $request->query->get("id");
      if ($request->getContent()) {
        $user = self::hasUserRole($data["result"]["id"], "admin", $request);
        if ($user == null) {
          $user = self::hasUserRole($data["result"]["id"], "owner", $request);
        }
        if ($user != null) {
          $data["result"]["instructions"] = $request->getContent();

          /** @var ProjectRepository $projectRepository */
          $projectRepository = $this->getDoctrine()->getRepository('AppBundle:Project');
          if ($projectRepository->createProject($data["result"])) {
            $data["header"]["status"] = "201";
          }
        } else {
          $data["header"]["status"] = "401";
          $data["header"]["description"] = "Unauthorized";
        }
      } else {
        $data["header"]["status"] = "400";
        $data["header"]["description"] = "The project instructions are missing.";
      }
    } else {
      $data["header"]["status"] = "400";
      $data["header"]["description"] = "The parameter project 'id' is missing.";
    }
    return new Response(json_encode($data, JSON_PRETTY_PRINT));
  }

  /**
   * @Route("/api/corpus/show", name="corpus/show", defaults={"_format" = "json"})
   * @Method("GET")
   */
  public function apiCorpus(Request $request)
  {
  	// Claudio: added check on user role
  	
    $data = self::initJSONArray("Get the list of TUs from the uploaded files");
    if ($request->query->has("id")) {
      $user = self::isAuthorizedUser($request);
      if ($user) {
        if (self::isUserEnable($user, $request->query->get("id"), "show")) {
        
          /** @var CorpusRepository $repository_corpus */
          $repository_corpus = $this->getDoctrine()->getRepository('AppBundle:Corpus');

          /** @var SentenceRepository $repository_sentence */
          $repository_sentence = $this->getDoctrine()->getRepository('AppBundle:Sentence');
          $sentences = $repository_sentence->findBy(array("project" => $request->query->get("id")),
            array("corpus" => "ASC", "simto" => "ASC", "num" => "ASC", "isintra" => "ASC"));
          $corpusId = -1;
          $tuId = -1;
          $p = array();
          $s = array();
          $tus = array();
          /** @var Sentence $sentence */
          foreach ($sentences as $sentence) {
            if ($sentence->getIsintra() <= 1) {
              if ($tuId != $sentence->getId()) {
                if (count($s) > 0) {
                  //Claudio: fixed bug wrong id
                  array_push($tus, array("id" => $tuId, "sentences" => $s));
                  $s = array();
                }
                $tuId = $sentence->getId();
              }
              if ($corpusId != $sentence->getCorpus()) {
                if ($corpusId != -1) {
                  /** @var Corpus $corpus */
                  $corpus = $repository_corpus->getCorpusFromID($corpusId);
                  //$repository_corpus->statsCorpus($corpusId);
                  $c = array("filename" => $corpus->getFilename(),
                    "errors" => $corpus->getErrors(),
                    "size" => $corpus->getSentences(),
                    "TUs" => $tus);
                  array_push($p, $c);
                }
                $s = array();
                $tus = array();
                $corpusId = $sentence->getCorpus();
              }
              array_push($s, array("num" => $sentence->getNum(),
                "language" => $sentence->getLanguage(),
                "type" => $sentence->getType(),
                "text" => $sentence->getText(),
                "comment" => $sentence->getComment(),
                "updated" => $sentence->getUpdated()->format('l, Y-m-d H:i:s e')));
            }
          }
        
          //Claudio: fixed bug missing last TUs
          if (count($s) > 0) {
            array_push($tus, array("id" => $tuId, "sentences" => $s));
            $s = array();
           }
         
          if ($corpusId != -1) {
            /** @var Corpus $corpus */
            $corpus = $repository_corpus->getCorpusFromID($corpusId);
            //$repository_corpus->statsCorpus($corpusId);
            $c = array("filename" => $corpus->getFilename(),
              "errors" => $corpus->getErrors(),
              "size" => $corpus->getSentences(),
              "TUs" => $tus);
            array_push($p, $c);
          }

          if (count($p) == 0) {
            $data["header"]["status"] = "204";
          } else {
            $data["header"]["status"] = "200";
            $data["result"]["corpus"] = $p;
          }
        }
        else {
                  $data["header"]["status"] = "401";
          $data["header"]["description"] = "You don't have permission to access to the project.";

        }
      } else {
        $data["header"]["status"] = "400";
        $data["header"]["description"] = "The parameter project 'id' is missing.";
      }
    } else {
      $data["header"]["status"] = "401";
      $data["header"]["description"] = "Unauthorized";
    }
    return new Response(json_encode($data, JSON_PRETTY_PRINT));
  }

/**
   * @Route("/api/corpus/delete", name="corpus/delete", defaults={"_format" = "json"})
   * @Method("GET")
   */
  public function apiCorpusDelete(Request $request)
  {
  	// implemented by Claudio
  	
    $data = self::initJSONArray("Deletes a file from a corpus");
    $data["header"]["status"] = "500";
    $data["header"]["description"] = "File delete failed!";
    if ($request->query->has("id")) {
      $projectId = $request->query->get("id");
      $data["result"]["id"] = $projectId;
      $user = self::hasUserRole($data["result"]["id"], "admin", $request);
      if ($user == null) {
        $user = self::hasUserRole($data["result"]["id"], "owner", $request);
      }
      if ($user) {      	
        if ($request->query->has('filename')) {
          $filename = $request->query->get('filename');
          $data["result"]["filename"] = $filename;
          //$data["result"]["length"] = $request->query->get('file');

          
          $repositoryCorpus = $this->getDoctrine()->getRepository('AppBundle:Corpus');
          
          $corpus = $repositoryCorpus->findCorpus($filename, $projectId);
          
          if ($corpus) {
            $data["result"]["corpusId"] = $corpus->getId();          
            $data["result"]["filename"] = $corpus->getFilename();
            $data["header"]["status"] = "200";
            $data["header"]["description"] = "The specified file has been deleted";          
            if (!$repositoryCorpus->deleteCorpus($corpus->getId())) {            	
				$data["header"]["description"] = "The specified file has not been deleted";          
            } 
          } else {
            $data["header"]["description"] = "The specified file doesn't exist";          
		  }
        } else {
          $data["header"]["status"] = "400";
          $data["header"]["description"] = "The parameter 'filename' is missing.";
        }
      } else {
        $data["header"]["status"] = "401";
        $data["header"]["description"] = "Unauthorized";
      }
    } else {
      $data["header"]["status"] = "400";
      $data["header"]["description"] = "The parameter project 'id' is missing.";
    }
    return new Response(json_encode($data, JSON_PRETTY_PRINT));
  }
  
  /**
   * @Route("/api/corpus/import", name="corpus/import", defaults={"_format" = "json"})
   * @Method("POST")
   */
  public function apiCorpusImport(Request $request)
  {
    $data = self::initJSONArray("Import a corpus from a file");
    $data["header"]["status"] = "500";
    $data["header"]["description"] = "File upload failed!";
    if ($request->query->has("id")) {
      $projectId = $request->query->get("id");
      $data["result"]["id"] = $projectId;
      $user = self::hasUserRole($data["result"]["id"], "admin", $request);
      if ($user == null) {
        $user = self::hasUserRole($data["result"]["id"], "owner", $request);
      }
      if ($user != null) {
        $upfile = $request->files->get('file');
        if ($upfile instanceof UploadedFile) {
          $filename = $upfile->getClientOriginalName();
          $data["result"]["filename"] = $filename;
          $data["result"]["length"] = $upfile->getSize();
          $data["result"]["here"] = "A";

          /** @var CorpusRepository $repository_corpus */
          $repository_corpus = $this->getDoctrine()->getRepository('AppBundle:Corpus');
          /** @var Corpus $corpus */
          $corpus = $repository_corpus->findCorpus($filename, $projectId);
          if ($corpus != null && $corpus->getSentences() > 0) {
            $data["header"]["description"] = "The file $filename is already present! " .
              $this->get('translator')->trans("mess#delete_file_before_upload");
                        $data["result"]["here"] = "B";
          } else {
            /** @var FileValidator $fileValidator */
            $fileValidator = $repository_corpus->validateFile($upfile, $projectId);
            $data["result"]["here"] = "C";
            if ($fileValidator != null) {
                      $data["result"]["here"] = "D";
              if (!$fileValidator->isWrong) {
                        $data["result"]["here"] = "F";
                $targetfile = self::backupFile($upfile, $projectId);
                if ($targetfile == null) {
                  $data["header"]["status"] = "500";
                  $data["header"]["description"] = "Backup file on the server failed!";
                } else {
                  $fileValidator->uploadFile = new UploadedFile($targetfile, $fileValidator->origFilename);
                            $data["result"]["here"] = "G";
                  // modified by Claudio
                  $corpus = $repository_corpus->newCorpus($fileValidator->uploadFile,$fileValidator->origFilename,$projectId,-1,"");
                  if ($corpus) {
                            $data["result"]["here"] = "H";
                    /** @var Corpus $corpus */
                    $sentences = $repository_corpus->addFileData($corpus);
                    if ($sentences != null) {
                      $data["header"]["status"] = "200";
                      $data["header"]["description"] = $fileValidator->message;
                      $data["result"]["sentences"] = $sentences;
                    } else {
                      $data["header"]["status"] = "400";
                      $data["header"]["description"] = "Cannot load the sentences from the uploaed file";
                    }
				  } else {
				      $data["header"]["status"] = "400";
                      $data["header"]["description"] = "Cannot import the file";
				  }
                  // Claudio: bug fixed here?  the corpus was created twice, I had to comment the following code
                  /*if ($corpus == null) {
                  $data["header"]["step"] = "AA";
                    $corpus = $repository_corpus->newCorpus($fileValidator->uploadFile, $fileValidator->origFilename, $projectId, $sentences, implode("\n", $fileValidator->message));
                  } else {
                  $data["header"]["step"] = "BB";
                    $corpus->setFilepath($fileValidator->uploadFile);
                    $corpus->setErrors(implode("\n", $fileValidator->message));
                    $corpus->setDate(new \DateTime("now"));
                    $corpus->setSentences($sentences);
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($corpus);
                    $em->flush();

                    //update the modified time of the project
                    $this->getDoctrine()->getRepository("AppBundle:Project")->updateModifiedTime($projectId);
                  }*/
                }
              } else {
                //Claudio: added validation error message to the header
                $data["header"]["description"] = implode("\n", $fileValidator->message);
              }
            } else {
              $data["header"]["description"] = "Failed to validate the file! ";
            }
          }
        } else {
          $data["header"]["description"] = 'Uploading file failed!';
        }
      } else {
        $data["header"]["status"] = "401";
        $data["header"]["description"] = "Unauthorized";
      }
    } else {
      $data["header"]["status"] = "400";
      $data["header"]["description"] = "The parameter project 'id' is missing.";
    }
    return new Response(json_encode($data, JSON_PRETTY_PRINT));
  }

  /**
   * @Route("/api/admin/resetcache", name="api_resetcache")
   */
  public function resetCache(Request $request)
  {
    if ($request->query->has("secret")) {
      if ($request->query->get("secret") == $this->container->getParameter("secret")) {
        shell_exec("\rm -rf " . $this->get('kernel')->getRootDir() . "/cache*");
        return new Response("Resetting cache... DONE!\n");
      }
    }
    return new Response("Authorization failed! You are not an allowed user.\n");
  }

  /**
   * @Route("/api/admin/resetdb", name="api_admin_resetdb")
   */
  public function resetDB(Request $request)
  {
    if ($request->query->has("secret")) {
      if ($request->query->get("secret") == $this->container->getParameter("secret")) {
        $tableToClean = array("Annotation", "Agreement", "Comment");

        $report = "Cleaning database ...";
        /** @var Query $query */
        $em = $this->getDoctrine()->getManager();
        foreach ($tableToClean as $table) {
          $query = $em->createQuery('DELETE AppBundle:' . $table);
          $query->execute();
        }
        $report .= "DONE!\n";

        //reset from start_evaluation for each project
        /** @var ProgressRepository $repository_progress */
        $repository_progress = $em->getRepository("AppBundle:Progress");

        $projects = $em->getRepository('AppBundle:Project')->findBy(array("active" => 1));

        /** @var Project $project */
        foreach ($projects as $project) {
          //reset the evaluation task
          $report .= "Reset the start evaluation task in the project " . $project->getName() . " ...DONE!\n";
          //$progress = $repository_progress->updateProgress($repository_progress::START_EVALUATION_TASK, $project->getId(), FALSE, null, null, null);
          $progress = $repository_progress->updateProgress($repository_progress::CHECKCORPUS_TASK, $project->getId(), FALSE, null, null, null);
          if (!$progress) {
            $report .= "ERROR! Reset task failed.\n";
          }
          //start the evaluation task
          //$progress = $repository_progress->updateProgress($repository_progress::START_EVALUATION_TASK, $project->getId(), TRUE, 1, null, null);
          $progress = $repository_progress->updateProgress($repository_progress::CHECKCORPUS_TASK, $project->getId(), TRUE, 1, null, null);
          if (!$progress) {
            $report .= "ERROR! Reset task failed.\n";
          }
          //activate all evaluation tasks
          $evaluations = $repository_progress->findBy(array("project" => $project->getId(), "task" => "evaluation"));

          $em = $this->getDoctrine()->getManager();
          /** @var Progress $evaluation */
          foreach ($evaluations as $evaluation) {
            $report .= "- enable evaluation for user " . $evaluation->getUser() . "\n";

            $evaluation->setDisabled(false);
            $em->persist($evaluation);
          }
          $em->flush();
        }


        $tableToCount = array("Project", "Document", "Sentence", "User", "Role", "Progress", "Annotation", "Agreement", "Comment");
        $report .= "\n## Database report ##\n";

        foreach ($tableToCount as $table) {
          $items = $em->getRepository("AppBundle:" . $table)->findAll();
          $report .= "$table: " . count($items) . "\n";
        }

        return new Response($report);
      }
    }
    return new Response("Authorization failed! You are not an allowed user.\n");
  }

  /**
   * @Route("/api/admin/checkdb", name="api_admin_checkdb")
   */
  public function adminCheckDB (Request $request) {
    if ($request->query->has("secret") &&
      $request->query->get("secret") == $this->container->getParameter("secret")) {
      $checkResult = "";
      $em = $this->getDoctrine()->getManager();

      //check evaluations vs. reviews
      $dql = "SELECT p.id, p.name, COUNT(p.id) as num
        FROM AppBundle:Annotation AS a
        INNER JOIN AppBundle:Project AS p
        WHERE a.project=p.id AND a.active=0
        GROUP BY p.id ORDER BY p.id";
      $query = $em->createQuery($dql);
      $projects = $query->execute();
      $activeHash = array();
      foreach ($projects as $project) {
        $activeHash[$project["name"]] = $project["num"];
      }

      $dql = "SELECT p.id, p.name, COUNT(p.id) as num
          FROM AppBundle:Annotation AS a
          INNER JOIN AppBundle:Project AS p
          WHERE a.project=p.id AND a.revuser>0
          GROUP BY p.id ORDER BY p.id";
      $query = $em->createQuery($dql);
      $projects = $query->execute();
      $revuserHash = array();
      foreach ($projects as $project) {
        $revuserHash[$project["name"]] = $project["num"];
      }

      $checkResult .= "# Revised evaluation: ";
      $report="";
      foreach ($revuserHash as $pname => $num) {
        if (!array_key_exists($pname, $activeHash)) {
          $report .= "- '" .$pname. "' has ". $num . " reviews but no disabled evaluation\n";
        } else if ($num != $activeHash[$pname]) {
          $report .= "- '" . $pname . "' has ". $num . " reviews but " . $activeHash[$pname] . " disabled evaluation\n";
        }
        unset($activeHash[$pname]);
      }
      foreach ($activeHash as $pname => $num) {
        $report .= "- '" . $pname . "' has " . $num . " disabled evaluations but no review\n";
      }

      if ($report != "") {
        $checkResult .= "ERROR!\n$report";
      } else {
        $checkResult .= "OK.\n";
      }

      // check agreement
      /** @var  AgreementRepository $repository_agreement */
      $repository_agreement = $em->getRepository('AppBundle:Agreement');

      $checkResult .= "# Check agreement measure: ";
      $report="";
      $dql = "SELECT a.num AS num, COUNT(a.num) AS cnt, ag.n AS n, (a.project) AS project, (a.output_id) AS id
        FROM AppBundle:Annotation AS a
        INNER JOIN AppBundle:Agreement AS ag
        WHERE a.num=ag.num AND a.active=1 GROUP BY a.num";
      $query = $em->createQuery($dql);
      $agreements = $query->execute();
      foreach ($agreements as $agreement) {
        if ($agreement["cnt"] != $agreement["n"]) {
          $report .= "- " .$agreement["num"] ." (". $agreement["project"] . ") must be reset\n";
          $repository_agreement->updateAgreement($agreement["id"], $agreement["project"]);
        }
      }
      if ($report != "") {
        $checkResult .= "ERROR!\n$report";
      } else {
        $checkResult .= "OK.\n";
      }

      return new Response($checkResult);

    }
    return new Response("Authorization failed! You are not an allowed user.\n");
  }

  /**
   * @Route("/api/admin/show", name="api_admin_stats")
   */
  public function adminShow(Request $request)
  {
    $data = self::initJSONArray("Statistics about MT-EQuAl [by administrator access only]");
    $data["header"]["status"] = "200";

    //if ($request->query->has("secret")) {
    if ($request->headers != null && $request->headers->has("X-Api-Key")) {
      if ($request->headers->get("X-Api-Key") == $this->container->getParameter("secret")) {
        $projectRepository = $this->getDoctrine()->getRepository('AppBundle:Project');

		
        $userRepository = $this->getDoctrine()->getRepository("AppBundle:User");
        $users = $userRepository->findBy(array("active" => 1), array('username' => 'DESC'));

        $p = array();
        /** @var User $user */
        foreach ($users as $user) {
          $u = array("id" => $user->getId(),
            "username" => $user->getUsername(),
            "firstname" => $user->getFirstname(),
            "lastname" => $user->getLastname(),
            "affiliation" => $user->getAffiliation(),
            "email" => $user->getEmail(),
            "roles" => $user->getRoles(),
            "registered" => $user->getRegistered()->format('l, Y-m-d H:i:s e'),
            "logged" => $user->getLogged()->format('l, Y-m-d H:i:s e'),
          );
          array_push($p, $u);
        }
        $data["header"]["status"] = "200";
        $data["result"]["users"] = $p;
      
        $data["result"]["projects"] = $projectRepository->getProjects();
      } else {
        $data["header"]["status"] = "401";
        $data["header"]["description"] = "Unauthorized";
      }
    }
    return new Response(json_encode($data, JSON_PRETTY_PRINT));
  }

  /**
   * @Route("/api/admin/remove", name="api_admin_remove")
   */
  public function adminRemove(Request $request)
  {
    $data = self::initJSONArray("Remove a user or project from the database permanently [by administrator access only]");
    $data["header"]["status"] = "200";

    if ($request->headers != null && $request->headers->has("X-Api-Key")) {
      if ($request->headers->get("X-Api-Key") == $this->container->getParameter("secret")) {
        if ($request->query->has("type")) {
          $type = $request->query->get("type");
          if ($request->query->has("id") || $request->query->has("name")) {
            $em = $this->getDoctrine()->getManager();
            /** @var RoleRepository $repositoryRole */
            $repositoryRole = $em->getRepository('AppBundle:Role');
            if ($type == "user") {
              /** @var User $user */
              $user = null;
              if ($request->query->has("id")) {
                $user = $em->getRepository('AppBundle:User')->findOneBy(array("id" => $request->query->get("id")));
              } else if ($request->query->has("name")) {
                $user = $em->getRepository('AppBundle:User')->findOneBy(array("username" => $request->query->get("name")));
              }
              if ($user) {
                $username = $user->getUsername();
                try {
                  $roles = $repositoryRole->findBy(array("user" => $user->getId()));
                  /** @var Role $userRole */
                  foreach ($roles as $userRole) {
                    $em->getRepository('AppBundle:Progress')->deleteAllProgress($userRole->getProject(), $userRole->getUser());
                    $em->getRepository('AppBundle:Annotation')->deleteEvaluations($userRole->getProject(), $userRole->getUser(), true);

                    $em->remove($userRole);
                    $em->flush();
                  }
                  //$repositoryRole->removeInvitation($user->getId());
                  $em->remove($user);
                  $em->flush();

                  $data["header"]["description"] = "Removed an existing user.";
                  $data["result"]["type"] = $type;
                  $data["result"]["username"] = $username;
                } catch (\Exception $e) {
                  $data["header"]["status"] = "500";
                  $data["header"]["description"] .= "Some errors occurred deleting project corpus.";
                }
              } else {
                $data["header"]["status"] = "400";
                $data["header"]["description"] = "The user was not found.";
              }
            } else if ($type == "project") {
              /** @var Project $project */
              if ($request->query->has("id")) {
                $project = $em->getRepository('AppBundle:Project')->findOneBy(array("id" => $request->query->get("id")));
              } else if ($request->query->has("name")) {
                $project = $em->getRepository('AppBundle:Project')->findOneBy(array("name" => $request->query->get("name")));
              }
              if ($project) {
                $projectName = $project->getName();
                try {
                  $roles = $repositoryRole->findBy(array("project" => $project->getId()));
                  /** @var Role $userRole */
                  foreach ($roles as $userRole) {
                    $em->getRepository('AppBundle:Progress')->deleteAllProgress($userRole->getProject());
                    $em->getRepository('AppBundle:Annotation')->deleteEvaluations($userRole->getProject(), $userRole->getUser(), false);
                    $em->remove($userRole);
                    $em->flush();
                  }
                  if (!$em->getRepository('AppBundle:Corpus')->deleteAllCorpus($userRole->getProject())) {
                    $data["header"]["status"] = "500";
                    $data["header"]["description"] .= "Some errors occurred deleting project corpus.";
                  }
                  $em->remove($project);
                  $em->flush();

                  $data["header"]["description"] = "Removed an existing project.";
                  $data["result"]["type"] = $type;
                  $data["result"]["name"] = $projectName;
                } catch (\Exception $e) {
                  $data["header"]["status"] = "500";
                  $data["header"]["description"] .= "Some errors occurred deleting project corpus.";
                }
              } else {
                $data["header"]["status"] = "400";
                $data["header"]["description"] = "The project was not found.";
              }
            }
          } else {
            $data["header"]["status"] = "400";
            $data["header"]["description"] = "The parameter 'id' or 'name' is missing.";
          }
        } else {
          $data["header"]["status"] = "400";
          $data["header"]["description"] = "The parameter 'type' is missing.";
        }
      } else {
        $data["header"]["status"] = "401";
        $data["header"]["description"] = "Unauthorized";
      }
    }
    return new Response(json_encode($data, JSON_PRETTY_PRINT));
  }


  /**
   * returns json with status of a task
   * @Route("/api/task/check", name="api_check_task")
   */
  public function checkTask(Request $request)
  {
    $data = array("completed" => false, "disabled" => false, "enabletasks" => "");
    $userid = self::getUserSession("userid");
    if ($userid != null) {
      $projectid = self::getProjectSession("id");
      if ($projectid != null) {
        $task = $request->query->get("task");
        /** @var ProgressRepository $repository_progress */
        $repository_progress = $this->getDoctrine()->getRepository("AppBundle:Progress");
        if ($repository_progress->isValidTask($task)) {
          /** @var Progress $progress */
          $progress = $repository_progress->getProgress($task, $projectid, $userid);
          if ($progress) {
            $data["completed"] = $progress->getCompleted();
            $data["disabled"] = $progress->getDisabled();
          }

          //$data["enabletasks"] = join("' and '", $repository_progress->enableTasks($projectSession["id"], $userid));
          $data["enabletasks"] = join("' and '", $repository_progress->dependencyTasks($task));
        }
      }
    }
    return new Response(json_encode($data));
  }

  /**
   * change disabled status of a task (if it is true it moves to false and viceversa
   * @Route("/task/change", name="app_change_task")
   */
  public function changeStatusTask(Request $request) {
    $data = array("success" => false);

    if(!$this->isCsrfTokenValid('authenticate', $request->query->get("_csrf_token"))) {
      // invalid token
      return new Response($this->renderView('exception/error-500.twig'));
    }
    $projectId = self::getProjectSession("id");
    $userId = $request->query->get("userid");
    $task = $request->query->get("task");
    //$myuserid = self::getUserSession("userid");

    /** @var ProgressRepository $repository_progress */
    $repository_progress = $this->getDoctrine()->getRepository("AppBundle:Progress");

    /** @var Progress $progress */
    $progress = $repository_progress->findOneBy(array("task" => $task, "project" => $projectId, "user" => $userId));
    if ($progress && !$progress->getCompleted()) {
      $em = $this->getDoctrine()->getManager();

      if ($progress->getDisabled()) {
        $progress->setDisabled(false);
      } else {
        $progress->setDisabled(true);
      }

      $progress->setModified(new \DateTime("now"));

      $em->persist($progress);
      $em->flush();

      //update the modified time of the project
      $this->getDoctrine()->getRepository("AppBundle:Project")->updateModifiedTime($projectId);

      self::sendEvaluationStatus($projectId, $userId, $progress->getDisabled());
      $data["success"] = true;
    }

    return new Response(json_encode($data));
  }

  /**
   * returns json with status of a task
   * @Route("/task/reset", name="app_reset_task")
   */
  public function resetTask(Request $request)
  {
    if(!$this->isCsrfTokenValid('authenticate', $request->query->get("_csrf_token"))) {
      // invalid token
      return new Response($this->renderView('exception/error-500.twig'));
    }

    $messagetype = "error";
    $message = "Sorry, task reset failed!";
    $myuserid = self::getUserSession("userid");
    //$params["view"] = 'settings.progress';
    if ($myuserid != null) {
      $projectid = self::getProjectSession("id");
      if ($projectid != null) {
        $params["pid"] = $projectid;
        $task = $request->query->get("task");

        /** @var AnnotationRepository $repository_annotation */
        $repository_annotation = $this->getDoctrine()->getRepository("AppBundle:Annotation");
        if ($task == ProgressRepository::EVALFORMAT_TASK && $repository_annotation->countEvaluation($projectid) > 0) {
          $messagetype = "alert";
          $message = "Before to reset this task you must delete ALL EVALUATION. See the <a href='" . $this->generateUrl('admin') . "?view=settings.delete'>delete project</a> panel.";
        } else {
          /** @var User $userTo */
          $userTo = $this->getDoctrine()->getRepository('AppBundle:User')->findOneBy(array("id" => $request->query->get("userid")));
          if ($userTo) {
            /** @var Progress $progress */
            $progress = self::undoneTask($task, $projectid, $userTo, self::getUserSession("username"));
            if ($progress && !$progress->getCompleted()) {
              /** @var ProgressRepository $repository_progress */
              $repository_progress = $this->getDoctrine()->getRepository("AppBundle:Progress");
              $this->setProjectSession("etasks", $repository_progress->enableTasks($projectid, $myuserid));

              $messagetype = "success";
              $message = "The task has been reset correctly!";
            }
          }
        }
      }
    }
    $this->addFlash(
      $messagetype,
      $message
    );

    return $this->redirectToRoute('admin', $params);
  }

  /**
   * returns json with status of a task
   * @Route("/api/task/duedate", name="api_duedate_task")
   */
  public function setDueDateTask(Request $request)
  {
    $data = array();
    $userid = self::getUserSession("userid");
    if ($userid != null) {
      if (self::getProjectSession("id")) {
        $task = $request->query->get("task");
        $duedate = $request->query->get("duedate");
        if (!empty($duedate)) {
          $duedate .= " 23:59:59";
        } else {
          $duedate = null;
        }

        /** @var ProgressRepository $repository_progress */
        $repository_progress = $this->getDoctrine()->getRepository("AppBundle:Progress");
        /** @var Progress $progress */
        $progress = $repository_progress->setDueDate($task, $duedate);
        if ($progress) {
          if ($duedate != null) {
            $data["duedate"] = $progress->getDuedate()->format('Y-m-d H:i:s');
          }
        } else {
          $data["message"] = "Updating due date failed!";
        }
      }
    }
    return new Response(json_encode($data));
  }

  /**
   * update task
   * @Route("/task/complete", name="app_task_complete")
   */
  public function setTaskComplete(Request $request)
  {
    $data = array("warning" => false, "message" => "The task cannot be changed now!");
    $userid = self::getUserSession("userid");
    if ($userid != null) {
      $projectid = $this->getProjectSession("id");
      if ($projectid != null) {
        $task = $request->query->get("task");
        /** @var ProgressRepository $repository_progress */
        $repository_progress = $this->getDoctrine()->getRepository("AppBundle:Progress");
        $isDisabledTask = $repository_progress->findBy(array("project" => $projectid, "task" => $task, "disabled" => 0));
        if ($repository_progress->isValidTask($task) && $isDisabledTask != null) {
          if ($task == ProgressRepository::IMPORTCORPUS_TASK) {
            /** @var SentenceRepository $repository_sentence */
            $repository_sentence = $this->getDoctrine()->getRepository("AppBundle:Sentence");

            //check if at least a corpus was uploaded correctly
            if ($repository_sentence->countTUs($projectid) == 0) {
              $data["message"] = "This task cannot be completed with no valid corpus!";
              return new Response(json_encode($data));
            }
          }

          /** @var Progress $progress */
          $progress = self::doneTask($task, $projectid, $userid);
          if ($progress->getCompleted()) {
            $data["success"] = true;
          }
          //add in the session the list of the enable tasks
          $this->setProjectSession("etasks", $repository_progress->enableTasks($projectid, $userid));
        }
      }
    }

    return new Response(json_encode($data));
  }

  /**
   * send a message to a user to inform him that he have to confirm all done evaluations again
   * @param $projectid
   * @param $user
   * @return bool
   * @throws \Exception
   */
  private function sendEvaluationNotConfirmed($projectid, $user)
  {
    /** @var \Swift_Mailer $mailer */
    $mailer = $this->get('mailer');
    $time = new \DateTime("now");

    $em = $this->getDoctrine();
    /** @var Project $project */
    $project = $em->getRepository('AppBundle:Project')->findOneBy(array("id" => $projectid));

    if ($project) {
      /** @var User $user */
      $mail = \Swift_Message::newInstance()
          ->setSubject('[MT-EQuAl] You have some unconfirmed evaluations (' . $time->format('Y-m-d H:i:s') . ')')
          ->setFrom($this->container->getParameter('mailer_user'))
          ->setTo($user->getEmail())
          ->setBody(
              $this->renderView('mail/evalunconfirm.email.twig',
                  //blind user
                  //array('hostname' => $this->container->getParameter('hostname'), 'projectid' => $projectid, 'projectname' => $project->getName())
                  array('username' => $user->getUsername(), 'hostname' => $this->container->getParameter('hostname'), 'projectid' => $projectid, 'projectname' => $project->getName())
              ), 'text/html'
          );

      $failures = array();
      $result = $mailer->send($mail, $failures);
      if (!$result) {
        throw new \Exception($failures);
      }
      return true;
    }
    return false;
  }

  /**
   * send a message to each user involved in the project about stoping evaluation
   * @param $projectid
   * @throws \Exception
   * @return integer
   */
  private function sendEvaluationStop($projectid)
  {
    $countSent = 0;
    /** @var \Swift_Mailer $mailer */
    $mailer = $this->get('mailer');
    $time = new \DateTime("now");

    $em = $this->getDoctrine();
    /** @var Project $project */
    $project = $em->getRepository('AppBundle:Project')->findOneBy(array("id" => $projectid));

    if ($project) {
      /** @var AnnotationRepository $repository_annotation */
      $repository_annotation = $em->getRepository("AppBundle:Annotation");

      //get the counter of available TUs
      /** @var SentenceRepository $repository_sentence */
      $repository_sentence = $em->getRepository("AppBundle:Sentence");
      $tus = $repository_sentence->countTUs($projectid);

      $evaluators = $em->getRepository('AppBundle:Role')->getUserByRole($projectid, "evaluator");
      $vendors = $em->getRepository('AppBundle:Role')->getUserByRole($projectid, "vendor");
      $users = array_merge($evaluators, $vendors);
      $emailAlreadySent = array();
      foreach ($users as $key => $userRow) {
        if (!in_array($userRow["id"], $emailAlreadySent)) {
          $doneTUs = $repository_annotation->countDone($projectid, $userRow["id"]);

          array_push($emailAlreadySent, $userRow["id"]);
          if ($doneTUs < $tus) {
            $mail = \Swift_Message::newInstance()
                ->setSubject('[MT-EQuAl] The evaluation of the project "'.$project->getName().'" has been suspended (' . $time->format('Y-m-d H:i:s') . ')')
                ->setFrom($this->container->getParameter('mailer_user'))
                ->setTo($userRow["email"])
                ->setBody(
                    $this->renderView('mail/evaluationstop.email.twig',
                        //blind user
                        //array('hostname' => $this->container->getParameter('hostname'), 'projectid' => $projectid, 'projectname' => $project->getName())
                        array('username' => $userRow["username"], 'hostname' => $this->container->getParameter('hostname'), 'projectid' => $projectid, 'projectname' => $project->getName())
                    ), 'text/html'
                );

            $failures = array();
            $result = $mailer->send($mail, $failures);
            if (!$result) {
              throw new \Exception($failures);
            } else {
              $countSent++;
            }
          }
        }
      }
    }
    return $countSent;
  }


  /**
   * send a message to all admins involved in the project about reset a task
   * @param $projectid
   * @throws \Exception
   * @return integer
   */
  private function sendTaskNotComplete($projectid, $task, $userFrom, $roleType)
  {
    $countSent = 0;
    /** @var \Swift_Mailer $mailer */
    $mailer = $this->get('mailer');
    $time = new \DateTime("now");

    $em = $this->getDoctrine();
    /** @var Project $project */
    $project = $em->getRepository('AppBundle:Project')->findOneBy(array("id" => $projectid));

    if ($project) {
      $users = $em->getRepository('AppBundle:Role')->getUserByRole($projectid, $roleType);
      /* owner is not alert anymore
        $owner = $em->getRepository('AppBundle:Role')->getUserByRole($projectid, "owner");
        $users = array_merge($users, $owner);
      */
      $emailAlreadySent = array();
      foreach ($users as $key => $userRow) {
        if ($userRow["username"] != $userFrom && !in_array($userRow["id"], $emailAlreadySent)) {
          array_push($emailAlreadySent, $userRow["id"]);
          $mail = \Swift_Message::newInstance()
              ->setSubject('[MT-EQuAl] A task has been reset (' . $time->format('Y-m-d H:i:s') . ')')
              ->setFrom($this->container->getParameter('mailer_user'))
              ->setTo($userRow["email"])
              ->setBody(
                  $this->renderView('mail/tasknotcomplete.email.twig',
                      //blind user
                      //array('hostname' => $this->container->getParameter('hostname'), 'projectid' => $projectid, 'projectname' => $project->getName())
                      array('task' => $task, 'userTo' => $userRow["username"], 'userFrom' => $userFrom , 'hostname' => $this->container->getParameter('hostname'), 'projectid' => $projectid, 'projectname' => $project->getName())
                  ), 'text/html'
              );

          $failures = array();
          $result = $mailer->send($mail, $failures);
          if (!$result) {
            throw new \Exception($failures);
          } else {
            $countSent++;
          }
        }
      }
    }
    return $countSent;
  }


  /**
   * send a message to each evaluator and vendor involved in the project about start evaluation
   * @param $projectid
   * @return int
   * @throws \Exception
   */
  private function sendEvaluationStart($projectid)
  {
    $countSent = 0;
    /** @var \Swift_Mailer $mailer */
    $mailer = $this->get('mailer');
    $time = new \DateTime("now");

    $em = $this->getDoctrine();

    /** @var Project $project */
    $project = $em->getRepository('AppBundle:Project')->findOneBy(array("id" => $projectid));

    if ($project) {
      /** @var ProgressRepository $repository_progress */
      $repository_progress = $em->getRepository("AppBundle:Progress");

      /** @var AnnotationRepository $repository_annotation */
      $repository_annotation = $em->getRepository("AppBundle:Annotation");

      //get the counter of available TUs
      /** @var SentenceRepository $repository_sentence */
      $repository_sentence = $em->getRepository("AppBundle:Sentence");
      $tus = $repository_sentence->countTUs($projectid);

      $evaluators = $em->getRepository('AppBundle:Role')->getUserByRole($projectid, "evaluator");
      $vendors = $em->getRepository('AppBundle:Role')->getUserByRole($projectid, "vendor");
      $users = array_merge($evaluators, $vendors);

      $emailAlreadySent = array();
      foreach ($users as $key => $userRow) {
      	$doneTUs = $repository_annotation->countDone($projectid, $userRow["id"]);
		if ($doneTUs == $tus) {
            //update evaluation task for this user as already completed
            $repository_progress->updateProgress($repository_progress::EVALUATION_TASK, $projectid, TRUE, $userRow["id"]);
        }
        if (!in_array($userRow["id"], $emailAlreadySent)) {
          array_push($emailAlreadySent, $userRow["id"]);

          $mail = \Swift_Message::newInstance()
              ->setSubject('[MT-EQuAl] The project "'.$project->getName().'" is ready for evaluation (' . $time->format('Y-m-d H:i:s') . ')')
              ->setFrom($this->container->getParameter('mailer_user'))
              ->setTo($userRow["email"])
              ->setBody(
                  $this->renderView('mail/evaluationstart.email.twig',
                      //blind user
                      //array('hostname' => $this->container->getParameter('hostname'), 'projectid' => $projectid, 'projectname' => $project->getName())
                      array('username' => $userRow["username"], 'hostname' => $this->container->getParameter('hostname'), 'projectid' => $projectid, 'projectname' => $project->getName())
                  ), 'text/html'
              );

          $failures = array();
          $result = $mailer->send($mail, $failures);
          if (!$result) {
            throw new \Exception($failures);
          } else {
            $countSent++;
          }
        }
      }
    }
    return $countSent;
  }


  /**
   * send a message to an evaluator and vendor involved in the project about start evaluation
   * @param $projectid
   * @return int
   * @throws \Exception
   */
  private function sendEvaluationStatus($projectId, $userId, $disabled)
  {
    $countSent = 0;
    /** @var \Swift_Mailer $mailer */
    $mailer = $this->get('mailer');
    $time = new \DateTime("now");

    $em = $this->getDoctrine();

    /** @var Project $project */
    $project = $em->getRepository('AppBundle:Project')->findOneBy(array("id" => $projectId));

    if ($project) {
      /** @var RoleRepository $role_repository */
      $role_repository = $em->getRepository('AppBundle:Role');
      $inviter = $role_repository->getInviter($projectId, $userId, "evaluator");
      $evaluator = $role_repository->getRoles($projectId, $userId);

      $users = array_merge($evaluator, $inviter);

      $subject = '[MT-EQuAl] The project "'.$project->getName().'" is ready for evaluation';
      $template = 'mail/evaluationstart.email.twig';
      if ($disabled) {
        $subject = '[MT-EQuAl] The evaluation of the project "'.$project->getName().'" has been suspended';
        $template = 'mail/evaluationstop.email.twig';
      }
      $emailAlreadySent = array();
      foreach ($users as $key => $userRow) {
        if (!in_array($userRow["id"], $emailAlreadySent)) {
          array_push($emailAlreadySent, $userRow["id"]);

          $mail = \Swift_Message::newInstance()
            ->setSubject($subject.' (' . $time->format('Y-m-d H:i:s') . ')')
            ->setFrom($this->container->getParameter('mailer_user'))
            ->setTo($userRow["email"])
            ->setBody(
              $this->renderView($template,
                //blind user
                //array('hostname' => $this->container->getParameter('hostname'), 'projectid' => $projectid, 'projectname' => $project->getName())
                array('username' => $userRow["username"], 'hostname' => $this->container->getParameter('hostname'), 'projectid' => $projectId, 'projectname' => $project->getName())
              ), 'text/html'
            );

          $failures = array();
          $result = $mailer->send($mail, $failures);
          if (!$result) {
            throw new \Exception($failures);
          } else {
            $countSent++;
          }

        }
      }
    }
    return $countSent;
  }

  /**
   * returns the text about an uploadedFile
   * @Route("/api/file/content", name="api_file_content")
   */
  public function getContentFromTextUploadedFile(Request $request)
  {
    $content = "";
    $message = "Sorry, upload failed!";
    $success = false;
      $upfile = $request->files->get('upfile');
      if ($upfile && ($upfile instanceof UploadedFile) && ($upfile->getError() == '0')) {
        if (file_exists($upfile)) {
          if ($upfile->guessExtension() == "html" || $upfile->guessExtension() == "txt") {
            $content = file_get_contents($upfile);
            if ($upfile->guessExtension() == "txt") {
              $content = preg_replace("/\n/s", "<br>", $content);
            }
            $success = true;
            $message = "The file has been uploaded correctly!";
          } else {
            $message = "Sorry! Only plain text or HTML file is allowed.";
          }
        }

    }
    $response = new Response(json_encode(array('success' => $success, 'message' => $message, 'content' => $content)));
    return $response;
  }

  /**
   * returns a report about user's rows
   * @Route("/api/user/report", name="api_user_report")
   * @return Response
   */
  public function apiUserReport()
  {
    /** @var UserRepository $repository */
    $userRepository = $this->getDoctrine()->getRepository("AppBundle:User");

    $query = $userRepository->createQueryBuilder('e')->select('count(e)')->getQuery();

    $data = array(
      'user_count' => $query->getSingleScalarResult(),
    );

    return new Response(
      json_encode($data), 200, array('Content-Type' => 'application/json')
    );
  }

  /**
   * returns a report about user's rows
   * @Route("/api/roletype/report", name="api_roletype_report", defaults={"_format" = "json"})
   * @return Response
   */
  public function apiRoletypeReport()
  {
    $dql = "SELECT r.name
        FROM AppBundle:RoleType AS r
        ORDER BY r.name";
    /** @var Query $query */
    $query = $this->getDoctrine()->getManager()->createQuery($dql);

    $items = $query->getResult();
    return new Response(
      json_encode($items),
      200,
      array('Content-Type' => 'application/json')
    );

  }

  /**
   * @Route("/api/annotation/report", name="api_annotation_report")
   * @Method("GET")
   */
  public function annotationReport(Request $request)
  {
    $projectId = self::getProjectSession("id");
    if ($projectId) {
      /** @var AnnotationRepository $annotation_repository */
      $annotation_repository = $this->getDoctrine()->getRepository('AppBundle:Annotation');
      $report = $annotation_repository->getEvalReportParams($projectId, self::getUserSession("userid"));

      return new Response($this->renderView('listing/evalreport.twig', $report));
    }

    return $this->redirectToRoute('admin');
  }


  /**
   * returns all imported corpus for a project
   * @Route("/api/corpus/report", name="api_corpus_report", defaults={"_format" = "json"})
   * @return Response
   */
  public function apiCorpusReport()
  {
    $id = self::getProjectSession("id");
    if ($id != null) {
      $dql = "SELECT c.id, c.filename, s.type, count(s.type) as mycount, s.updated
        FROM AppBundle:Sentence AS s
        INNER JOIN AppBundle:Corpus AS c
        WHERE s.corpus=c.id and s.project=:pid
        GROUP BY c.id,s.type";
      /** @var Query $query */
      $query = $this->getDoctrine()->getManager()->createQuery($dql);
      $query->setParameter('pid', $id);

      $items = $query->getResult();
      $json = $this->get('serializer')->serialize($items, 'json');

      $response = new Response($json);
      return $response;
    }
    return null;
  }


  /**
   * @Route("/api/role/reply", name="api_role_reply")
   */
  public function replyInvitation(Request $request)
  {
    $message = 'ERROR! Invitation failed.';
    $userid = self::getUserSession("userid");

    $params = $request->query->all();
    $parout = array();
    $redirectPage = 'dashboard';
    if (self::getProjectSession("id")) {
      $redirectPage = 'admin';
    }
    if (array_key_exists('view', $params)) {
      $parout{"view"} = $params["view"];
    }

    if (array_key_exists('hash', $params) && !empty($params["hash"])) {
      $em = $this->getDoctrine()->getManager();
      /** @var RoleRepository $repository */
      $repository = $em->getRepository('AppBundle:Role');
      /** @var Role $role */
      $role = $repository->findOneBy(array("hash" => $params["hash"]));
      if ($role != null) {
        if ($role->getStatus() == "pending") {
          if ($params["action"] == "accepted" || $params["action"] == "declined") {
            $repository->updateRole($role->getCreatedBy(), $role->getUser(), $role->getProject(), $role->getRole(), $params["action"]);
            /*
                $role->setStatus($params["action"]);
                $role->setActivated(new \DateTime());
                $em->persist($role);
                $em->flush();
            */
            if ($params["action"] == "accepted") {
              $message = 'Well done! Your account has been successfully linked to a new project.';
            } else if ($params["action"] == "declined") {
              $message = 'You have successfully declined an invitation request.<br>Thanks anyway!';
            }

            if ($userid != null && $role->getUser() == $userid) {
              $this->addFlash("alert", $message);
              return $this->redirectToRoute('dashboard');
            } else {
              if ($params["action"] == "accepted") {
                $message .= " Please login to continue!";
              }
              $this->addFlash("alert", $message);
              return new Response($this->renderView('default/login.twig'));
            }
          } else {
            //check other values
            $message = 'WARNING! Sorry, this request is not valid.';
          }
        } else {
          $message = 'WARNING! The invitation failed because its status was not pending.';
        }
      } else {
        $message = "WARNING! This request is not valid or it has been already performed.";
      }
    }
    $this->addFlash("alert", $message);
    return $this->redirectToRoute($redirectPage, $parout);
  }

  /**
   * get roles for a project
   * @Route("/api/project/roles", name="api_project_roles", defaults={"_format" = "json"})
   * @return Response
   */
  public function apiProjectRoles(Request $request)
  {
    $pid = $request->query->get("pid");
    if ($pid > 0) {
      $dql = "SELECT u.username,u.email,r.role
        FROM AppBundle:Role AS r
        INNER JOIN AppBundle:User AS u
        WHERE r.user = u.id AND r.project = :pid AND r.role != :role";

      /** @var Query $query */
      $query = $this->getDoctrine()->getManager()->createQuery($dql);
      $query->setParameter('pid', $pid);
      $query->setParameter('role', "owner");

      $items = $query->getResult();
      $json = $this->get('serializer')->serialize(array('roles' => $items), 'json');

      /*
      $count = $role_repository->createQueryBuilder('u');
      $count->select('count(u.role)');
      $count = $count->getQuery()->getSingleScalarResult();

      $json = $this->get('serializer')->serialize(array('roles' => $items, 'total' => $count), 'json');
      */
      $response = new Response($json);
      return $response;
    }
    return new Response("[]");
  }

  /**
   * get roles for a project
   * @Route("/api/project/document", name="api_project_document", defaults={"_format" = "json"})
   * @return Response
   */
  public function apiProjectDocument(Request $request) {
    $json = "[]";
    if ($request->query->has("type")) {
      $dql = "SELECT d.id, d.format, d.url, d.used
        FROM AppBundle:Document AS d
        WHERE d.type=:type
        ORDER BY d.format ASC, d.used DESC";

      /** @var Query $query */
      $query = $this->getDoctrine()->getManager()->createQuery($dql);
      $query->setParameter('type', $request->query->get("type"));

      $items = $query->getResult();
      if (count($items) > 0) {
        $json = $this->get('serializer')->serialize($items, 'json');
      }
    }
    return new Response($json);
  }

  /**
   * now you can easily looks for users that you have already been invited to your projects or who invited you to their projects (updating the previous version that gets the list of registered users matching by substring of user email or MT-EQuAl username or role).
     In this way a user can invite the rest of users available in the system if and only s/he knows the exact her/his MT-EQuAl username (and role optionally) or email and by listing of the suggested usernames joint to him by previously invitations.
   *
   * @Route("/api/user/find", name="api_user_find", defaults={"_format" = "json"})
   * @return Response
   */
  public function apiFindUser(Request $request)
  {
    $userid = self::getUserSession("userid");

    if ($userid != null) {
      if ($request->query->has("search") || $request->query->has("role")) {
        $search = $request->query->get("search");
        $role = $request->query->get("role");
        $searchTerm = ($search == null || $search == "") ? "" : "(u.username LIKE :searchlike
            OR u.email LIKE :searchlike)";
        $roleTerm = ($role == null || $role == "") ? "" : "u.roles LIKE :rolelike";

        if ($searchTerm != "" && $roleTerm != "") {
          $roleTerm = " AND " . $roleTerm;
        }
        $dql = "SELECT DISTINCT u.id,u.username,u.email,u.firstname,u.lastname
            FROM AppBundle:User as u
            INNER JOIN AppBundle:Role AS r
            WHERE (u.id=r.user OR u.id=r.created_by OR u.email=:search) AND (r.user=:userid OR r.created_by=:userid) AND u.id!=:userid
              AND u.active = 1 AND " . $searchTerm . $roleTerm;

        /** @var Query $query */
        $query = $this->getDoctrine()->getManager()->createQuery($dql);
        $query->setParameter('userid', $userid);
        $query->setParameter('search', trim($search));

        if ($search != null) {
          $query->setParameter('searchlike', '%' . trim($search) . '%');
        }
        if ($role != null) {
          $query->setParameter('rolelike', '%' . $role . '%');
        }

        $items = $query->getResult();
        $json = $this->get('serializer')->serialize($items, 'json');

        return new Response($json);
      }
    }
    return new Response("{}");
  }

  /**
   * remove all evaluations done by evaluators in a project
   * @Route("/api/evaluations/delete", name="api_evaluations_delete")
   * @return Response
   */
  public function apiDeleteEvaluations(Request $request)
  {
    if ($request->query->has("userid")) {
      $targetUserID = $request->query->get("userid");
    }

    $refUserId = self::getUserSession("userid");
    $projectid = self::getProjectSession("id");
    $messagetype = "error";
    $message = "project_delete_error";
    if ($refUserId != null && $projectid != null) {
      /** @var ProjectRepository $project_repository */
      $project_repository = $this->getDoctrine()->getRepository("AppBundle:Project");
      $permission = $project_repository->can("delete", $projectid, $refUserId);
      if ($permission == 1) {
        /** @var AnnotationRepository $annotation_repository */
        $annotation_repository = $this->getDoctrine()->getRepository("AppBundle:Annotation");
        $annotation_repository->deleteAllEvaluations($projectid);

        $messagetype = "success";
        $message = "All evaluations have been deleted!";
      } else {
        $message = "Sorry by you don't have enough privileges!";
        //$message = "user_without_privileges";
      }
    }
    $this->addFlash($messagetype, $message);
    return $this->redirectToRoute('admin', array("pid" => $projectid, "view" => "settings.delete"));
  }


  /**
   * TODO: NOT USED!!
   * assign a role to a user for a project
   * @Route("/api/user/info", name="api_user_info")
   * @return Response
   */
  public function apiUserInfo(Request $request)
  {
    $userid = self::getUserSession("userid");
    $userInfo = array();
    if ($userid != null) {
      /** @var UserRepository $user_repository */
      $user_repository = $this->getDoctrine()->getRepository("AppBundle:User");

    }
    return new Response(
        json_encode($userInfo),
        200,
        array('Content-Type' => 'application/json')
    );
  }

  /**
   * TODO: NOT USED!!
   * assign a role to a user for a project
   * @Route("/api/project/info", name="api_project_info")
   * @return Response
   */
  public function apiInfoProject(Request $request)
  {
    $project_data = array();
    $projectid = self::getProjectSession("id");
    if ($projectid != null) {
      /** @var ProjectRepository $project_repository */
      $project_repository = $this->getDoctrine()->getRepository("AppBundle:Project");
      /** @var Project $project */
      $project_data = $project_repository->getProject($projectid);
      if ($project_data == null) {
        $project_data["id"] = $projectid;
      }
    }
    return new Response(
        json_encode($project_data),
        200,
        array('Content-Type' => 'application/json')
    );
  }

  /**
   * get an evaluation info: if there is at least a review, a comment, evals distribution and the agreement
   * @Route("/evaluation/get", name="get_evaluation")
   * @Method("GET")
   */
  public function getEvaluation(Request $request)
  {
    $html ="";
    $params = $request->query->all();
    if (array_key_exists("num", $params) &&
      array_key_exists("review", $this->get("session")->get("project")["etasks"])) {

      /** @var  AgreementRepository $agreement_repository */
      $agreement_repository = $this->getDoctrine()->getRepository('AppBundle:Agreement');
      //update Agreement
      if (array_key_exists("oid", $params) && $params["oid"] != null) {
        $agreement_repository->updateAgreement($params["oid"], $this->get("session")->get("project")["id"]);
      }

      /** @var AnnotationRepository $annotation_repository */
      $annotation_repository = $this->getDoctrine()->getRepository('AppBundle:Annotation');
      $evals = $annotation_repository->getEvaluations($params["num"]);
      $revuser = 0;
      $ranges = $this->get("session")->get("project")["ranges"];
      if (array_key_exists("0", $evals)) {
        $revuser += $evals["0"][1];
        $html .= '<td class="cell-right">'.$evals["0"][0].'</td>';
      } else {
        $html .= '<td class="cell-right">0</td>';
      }
      $decisionOptions = array(0 =>'',
                       1 => 'Yes',
                       2 => 'No',
                       3 => 'N/A');

      foreach (json_decode($ranges, TRUE) as $evalType) {
        $num=0;
        if ($this->get("session")->get("project")["type"] == "tree") {
          $num="";
          $listElem = array();
          foreach($evals as $eval => $evalInfo) {
            $revuser += $evalInfo[1];

            $ev = substr($eval,(intval($evalType["val"])-1),1);
            if ($ev != 0) {
              $listElem{$ev} += $evalInfo[0];
            }
          }
          foreach ($listElem as $eval => $cnt) {
            $num .= "$cnt<sup><b>". $decisionOptions{$eval} ."</b></sup> ";
          }
          if ($num == "") {
            $num ="--";
          }
        } else {
          if (array_key_exists($evalType["val"], $evals)) {
            $revuser += $evals[$evalType["val"]][1];
            $num = $evals[$evalType["val"]][0];
          }
        }
        $html .= "<td class='cell-right'>$num</td>";
      }

      $data = $agreement_repository->getSentenceAgreement($params["num"]);
      $text="";
      $agreement="--";
      foreach ($data as $value) {
        $text = $value["text"];
        $agreement = $value["agreement"];
      }

      /** @var CommentRepository $comment_repository */
      $comment_repository = $this->getDoctrine()->getRepository('AppBundle:Comment');
      $commentCount = $comment_repository->getCommentCount($params["num"]);
      $commentInfo = "";
      if ($commentCount > 0) {
        $commentInfo = ' <span class="pull-right"><i class="fa fa-comments-o"></i>'.$commentCount.'</span>';
      }

      //if exist at least a revision
      $revInfo = "";
      if ($revuser > 0) {
        $revInfo = "<div style='padding: 0; display: inline; position: relative; left: -8px;'><i class='fa fa-pencil bg-primary' style='padding: 2px' title='reviewed'></i></div> ";
      } else {
        $userId = $this->get("session")->get("mysession")["userid"];
        if ($userId && $annotation_repository->isViewedEvaluation($params["num"], $userId)) {
          $revInfo = "<div style='padding: 0; display: inline; position: relative; left: -8px;'><i class='fa fa-eye' style='padding: 2px; background: #BA494F; color: #fff' title='viewed'></i></div> ";
        }
      }
      $html = "<td>$revInfo$text$commentInfo</td>$html";


      if ($agreement == "1.000") {
        $agreement = "1";
      }
      $html .= '<td class="cell-right"><label>'.$agreement.'</label></td>';
    }
    return new Response($html);
  }

  /**
   * send an evaluation
   * @Route("/evaluation/save", name="save_evaluation", defaults={"_format" = "json"})
   * @Method("POST")
   */
  public function saveEvaluation(Request $request)
  {
    $params = $request->request->all();
    if(!$this->isCsrfTokenValid('authenticate', $params["_csrf_token"])) {
      // invalid token
      return new Response($this->renderView('exception/error-500.twig'));
    }

    $message = "error";
    $userid = $this->get("session")->get("mysession")["userid"];
    $projectid = $this->get("session")->get("project")["id"];

    if ($userid != null && $projectid != null &&
        array_key_exists("id", $params) &&
        array_key_exists("num", $params) &&
        array_key_exists("eval", $params)
    ) {
      $em = $this->getDoctrine()->getManager();
      /** @var User $user */
      $user = $em->getRepository('AppBundle:User')->findOneBy(array("id" => $userid));
      /** @var Project $project */
      $project = $em->getRepository('AppBundle:Project')->findOneBy(array("id" => $projectid));

      if ($user) {
        /** @var AnnotationRepository $repository */
        $repository = $this->getDoctrine()->getRepository('AppBundle:Annotation');
        //remove don't know evaluation
        $dontknows = $repository->findBy(
            array("output_id" => $params["id"], "project" => $project, "eval" => Annotation::DONTKNOW_VALUE, "user" => $user));
        if (count($dontknows) > 0) {
          foreach ($dontknows as $dontknow) {
            $em->remove($dontknow);
          }
          $em->flush();
        }
        //save the current evaluation
        /** @var Annotation $annotation */
        $annotation = $repository->findOneBy(
            array("num" => $params["num"], "user" => $user));

        if ($annotation == null) {
          $annotation = new Annotation();
          $annotation->setOutputId($params["id"]);
          $annotation->setNum(intval($params["num"]));
          $annotation->setUser($user);
          $annotation->setProject($project);
          $annotation->setEval(trim($params["eval"]));
          $em->persist($annotation);
        } else {
          if ($annotation->getEval() == trim($params["eval"])) {
            $em->remove($annotation);
          } else {
            $annotation->setEval(trim($params["eval"]));
            $em->persist($annotation);
          }
        }
        $em->flush();

        //reset progress
        $repository->unconfirmEvaluation($params["id"], $projectid, $user);

        $message = "OK";
      }
    }
    return new Response(
        json_encode(array("message" => $message)), 200, array('Content-Type' => 'application/json')
    );
  }

  /**
   * send an evaluation
   * @Route("/evaluation/resetrev", name="reset_rev_evaluation")
   * @Method("GET")
   */
  public function resetRevEvaluation(Request $request)
  {
    $params = $request->query->all();
    $message = "error";
    $userid = $this->get("session")->get("mysession")["userid"];
    $projectid = $this->get("session")->get("project")["id"];
    if ($userid != null && $projectid != null &&
        array_key_exists("num", $params)) {

      /** @var AnnotationRepository $repository */
      $repository = $this->getDoctrine()->getRepository('AppBundle:Annotation');
      if ($repository->resetReview($params["num"])) {
        $message = "OK";
      }
    }
    return new Response(
        json_encode(array("message" => $message))
    );
  }

  /**
   * send an evaluation
   * @Route("/evaluation/saverev", name="save_rev_evaluation", defaults={"_format" = "json"})
   * @Method("GET")
   */
  public function saveRevEvaluation (Request $request)
  {
    $params = $request->query->all();
    $message = "error";
    $userid = $this->get("session")->get("mysession")["userid"];
    $projectid = $this->get("session")->get("project")["id"];

    if ($userid != null && $projectid != null &&
      array_key_exists("id", $params) &&
      array_key_exists("num", $params) &&
      array_key_exists("euser", $params) &&
      array_key_exists("eval", $params))
    {
      $eval = trim($params["eval"]);
      $em = $this->getDoctrine()->getManager();
      /** @var User $user */
      $user = $em->getRepository('AppBundle:User')->findOneBy(array("id" => $userid));
      /** @var Project $project */
      $project = $em->getRepository('AppBundle:Project')->findOneBy(array("id" => $projectid));

      if ($user) {
        /** @var AnnotationRepository $repository */
        $repository = $this->getDoctrine()->getRepository('AppBundle:Annotation');
        //disable evaluation done by evaluator
        /** @var Annotation $evaluator_annotation */
        $evaluator_annotation = $repository->findOneBy(
          array("num" => $params["num"], "user" => $params["euser"], "revuser" => 0));

        if ($evaluator_annotation) {
          //save the current evaluation
          /** @var Annotation $reviewer_annotation */
          $reviewer_annotation = $repository->findOneBy(array("num" => $params["num"], "user" => $params["euser"], "active" => 1));

          if ($reviewer_annotation == null || $reviewer_annotation == $evaluator_annotation) {
            /** @var User $euser */
            $euser = $em->getRepository('AppBundle:User')->findOneBy(array("id" => $params["euser"]));

            if ($euser) {
              $evaluator_annotation->setActive(false);
              $em->persist($evaluator_annotation);

              $annotation = new Annotation();
              $annotation->setOutputId($params["id"]);
              $annotation->setNum(intval($params["num"]));
              $annotation->setUser($euser);
              $annotation->setProject($project);
              $annotation->setEval($eval);
              $annotation->setRevuser($userid);
              $annotation->setRevtime(new \DateTime());
              $annotation->setConfirmed($evaluator_annotation->getConfirmed());
              $em->persist($annotation);
            }
          } else {
            if ($reviewer_annotation->getEval() == $eval || preg_match('/^00+$/', $eval)) {
              $em->remove($reviewer_annotation);

              $evaluator_annotation->setActive(true);
              $em->persist($evaluator_annotation);
            } else {
              $reviewer_annotation->setEval($eval);
              $reviewer_annotation->setRevtime(new \DateTime());
              $em->persist($reviewer_annotation);
            }
          }
          $em->flush();

          //update Agreement
          /** @var  AgreementRepository $repository_agreement */
          $repository_agreement = $em->getRepository('AppBundle:Agreement');
          $repository_agreement->updateAgreement($params["id"], $projectid);

          $message = "OK";
        }

      }
    }
    return new Response(
      json_encode(array("message" => $message)), 200, array('Content-Type' => 'application/json')
    );
  }


  /**
   * returns a list with the latest comments
   * @Route("/api/comment", name="api_comment")
   */
  public function getComment(Request $request)
  {
    $userId = self::getUserSession("userid");
    if ($userId != null) {
      if (!in_array("admin", self::getUserSession("roles")) ||
        !in_array("reviewer", self::getUserSession("roles"))) {
        $userId = null;
      }
      $projectId = self::getProjectSession("id");
      if ($projectId != null) {
        $obj = $request->query->get("obj");
        $refId = $request->query->get("refid");
        $showall = $request->query->get("showall");
        if (!empty($obj) && !empty($refid)) {
          /** @var RoleRepository $repository_role */
          //$repository_role = $this->getDoctrine()->getRepository("AppBundle:Role");
          //$params["evaluators"] = $repository_role->getUsers($projectId, $userId, "evaluator");

          /** @var CommentRepository $repository_comment */
          $repository_comment = $this->getDoctrine()->getRepository("AppBundle:Comment");
          $comments = $repository_comment->lastComment($projectId, $obj, $refId, $userId, $showall);

          return new Response(
            json_encode($comments), 200,  array('Content-Type' => 'application/json')
          );
        }
      }
    }
    return new Response(json_encode(array("success" => false)));
  }

  /**
   * returns a current value of the kappa score (considering the reviewing)
   * @Route("/api/kappa", name="api_kappa")
   */
  public function getKappa(Request $request) {
    /** @var AnnotationRepository $repository_annotation */
    $repository_annotation = $this->getDoctrine()->getRepository("AppBundle:Annotation");
    $k = $repository_annotation->calculateKappaQuality(self::getProjectSession("id"), false);
    if ($k != 0) {
      $k = round($k ,3);
    }
    return new Response(json_encode(array("kappa" => $k)));
  }
}

?>