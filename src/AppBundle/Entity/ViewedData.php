<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * viewed data entity class
 * @ORM\Table(name="viewed",
 *              indexes={@ORM\Index(name="project", columns={"project"})})
 * @ORM\Entity()
 */

class ViewedData
{
    public function __construct($num, $project, $user)
    {
        $this->num = $num;
        $this->user = $user;
        $this->project = $project;
        $this->modified = new \DateTime();
    }

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @var \AppBundle\Entity\Sentence
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Sentence")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="index_num", referencedColumnName="num")
     * })
     */
    private $num;

    /**
     * @ORM\Column(type="integer")
     */
    private $project;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     */
    private $user;


    /**
     * @ORM\Column(type="datetime")
     */
    private $modified;


    /**
     * Set num
     *
     * @param integer $num
     */
    public function setNum($num)
    {
        $this->num = $num;
    }

    /**
     * Get num
     *
     * @return integer
     */
    public function getNum()
    {
        return $this->num;
    }

    /**
     * Set project
     *
     * @param integer $project
     */
    public function setProject($project)
    {
        $this->project = $project;
    }

    /**
     * Get project id
     *
     * @return integer
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * @return integer
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param integer $userid
     */
    public function setUser($userid)
    {
        $this->user = $userid;
    }

    /**
     * @return DateTimeInterface
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * @param DateTimeInterface $modified
     */
    public function setModified($modified)
    {
        $this->modified = $modified;
    }
}
