<?php
/**
 * Created by PhpStorm.
 * User: cgirardi
 * Date: 30/03/16
 * Time: 17:12
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *
 * Document entity class
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DocumentRepository")
 * @ORM\Table(name="document")
 */
class Document
{

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $url;

    /** @ORM\Column(type="string") */
    protected $type;

    /** @ORM\Column(type="string") */
    protected $format;

    /** @ORM\Column(type="text") */
    protected $data;

    /** @ORM\Column(type="integer", nullable=false, options={"default" = 0}) */
    protected $length;

    /** @ORM\Column(type="integer", nullable=false, options={"default" = 0}) */
    protected $width;

    /** @ORM\Column(type="integer", nullable=false, options={"default" = 0}) */
    protected $height;

    /** @ORM\Column(type="integer", nullable=false, options={"default" = 0}) */
    protected $used;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updated;



    public function __construct()
    {
        $this->data = "";
        $this->length = 0;
        $this->width = 0;
        $this->height = 0;
        $this->used = 1;
    }

    /**
     * Set id
     *
     * @param integer $id
     *
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Get id
     *
     * @return integer
     *
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set data
     *
     * @param string $data
     *
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * Get data
     *
     * @return string
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set format
     *
     * @param string $format
     *
     */
    public function setFormat($format)
    {
        $this->format = $format;
    }

    /**
     * Get format
     *
     * @return string
     */
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * Set length
     *
     * @param integer $length
     *
     */
    public function setLength($length)
    {
        $this->length = $length;

    }

    /**
     * Get length
     *
     * @return integer
     *
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * Set width
     *
     * @param integer $width
     *
     */
    public function setWidth($width)
    {
        $this->width = $width;
    }

    /**
     * Get width
     *
     * @return integer
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * Set height
     *
     * @param integer $height
     *
     */
    public function setHeight($height)
    {
        $this->height = $height;
    }

    /**
     * Get height
     *
     * @return integer
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Set the usage of document
     *
     * @param integer $used
     *
     */
    public function setUsed($used)
    {
        $this->used = $used;
    }

    /**
     * Get the usage
     *
     * @return integer
     */
    public function getUsed()
    {
        return $this->used;
    }

    /**
     * Set updated time
     *
     * @param \DateTime $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * Get updated time
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }
}
