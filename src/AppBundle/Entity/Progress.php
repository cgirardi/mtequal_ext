<?php
namespace AppBundle\Entity;

use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Progress monitor entity class
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProgressRepository")
 * @ORM\Table(name="progress", indexes={@ORM\Index(name="task", columns={"task"}),
 *                                      @ORM\Index(name="project", columns={"project"}),
 *                                      @ORM\Index(name="user", columns={"user"})})
 */

class Progress
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", unique=true)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $task;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $num;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $project;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $user;

    /**
     * @ORM\Column(type="boolean")
     */
    private $completed;

    /**
     * @ORM\Column(type="boolean")
     */
    private $disabled;

    /**
     * @ORM\Column(type="datetime")
     */
    private $modified;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $duedate;


    public function __construct()
    {
        $this->completed = false;
        $this->disabled = true;
        $this->modified = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * @param string $task
     */
    public function setTask($task)
    {
        $this->task = $task;
    }

    /**
     * @return integer
     */
    public function getNum()
    {
        return $this->num;
    }

    /**
     * @param integer $num
     */
    public function setNum($num)
    {
        $this->num = $num;
    }

    /**
     * @return integer
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * @param integer $project
     */
    public function setProject($project)
    {
        $this->project = $project;
    }

    /**
     * @return integer
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param integer $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return boolean
     */
    public function getCompleted()
    {
        return $this->completed;
    }

    /**
     * @param boolean $completed
     */
    public function setCompleted($completed)
    {
        $this->completed = $completed;
    }

    /**
     * @return boolean
     */
    public function getDisabled()
    {
        return $this->disabled;
    }

    /**
     * @param boolean $disabled
     */
    public function setDisabled($disabled)
    {
        $this->disabled = $disabled;
    }

    /**
     * @return DateTimeInterface
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * @param DateTimeInterface $modified
     */
    public function setModified($modified)
    {
        $this->modified = $modified;
    }

    /**
     * @return DateTimeInterface
     */
    public function getDuedate()
    {
        return $this->duedate;
    }

    /**
     * @param DateTimeInterface $date
     */
    public function setDuedate($date)
    {
        $this->duedate = $date; //$date->format('Y-m-d H:i:s)
    }
}
