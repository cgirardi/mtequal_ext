<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Comment entity class
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CommentRepository")
 * @ORM\Table(name="comment",
 *                      indexes={@ORM\Index(name="project", columns={"project"}),
 *                              @ORM\Index(name="userid", columns={"userid"}),
 *                              @ORM\Index(name="obj", columns={"obj"}),
 *                              @ORM\Index(name="refid", columns={"refid"}),
 *                              @ORM\Index(name="date", columns={"date"})})
 */
class Comment
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $userid;

    /**
     * @ORM\Column(type="integer")
     */
    private $project;

    /**
     * @ORM\Column(type="string")
     */
    private $obj;

    /**
     * @ORM\Column(type="integer")
     */
    private $refid;

    /**
     * @ORM\Column(type="text")
     */
    private $comment;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;


    /**
     * @param integer $userid
     * @param integer $project
     */
    function __construct($userid, $project)
    {
        $this->setUser($userid);
        $this->setProject($project);
        $this->setDate(new \DateTime());
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return integer
     */
    public function getUser()
    {
        return $this->userid;
    }

    /**
     * @param integer $id
     */
    public function setUser($id)
    {
        $this->userid = $id;
    }

    /**
     * Set project
     *
     * @param integer $project
     */
    public function setProject($project)
    {
        $this->project = $project;
    }

    /**
     * Get project id
     *
     * @return integer
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * @return string
     */
    public function getObj()
    {
        return $this->obj;
    }

    /**
     * @param string $obj
     */
    public function setObj($obj)
    {
        $this->obj = $obj;
    }

    /**
     * @return integer
     */
    public function getRefID()
    {
        return $this->refid;
    }

    /**
     * @param integer $id
     */
    public function setRefID($id)
    {
        $this->refid = $id;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }
}
