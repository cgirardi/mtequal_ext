<?php
/**
 * Project: mtequal_ext
 * User: cgirardi
 * Date: 13/02/16
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *
 * Progress entity class
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AgreementRepository")
 * @ORM\Table(name="agreement")
 */
class Agreement
{
  public function __construct(){
    $this->setN(0);
    $this->setK(0);
    $this->setAgreement(0);
    $this->setAgreementBeforeRev(0);
  }

  /**
   * @ORM\Id
   * @ORM\Column(type="integer")
   */
  private $num;

  /**
   * @ORM\Column(type="text")
   */
  private $vals;

  /**
   * number of evaluations in the values
   * @ORM\Column(type="integer", nullable=false, options={"default" = 0})
   */
  private $n;

  /**
   * number of categories
   * @ORM\Column(type="integer", nullable=false, options={"default" = 0})
   */
  private $k;

  /**
   * @ORM\Column(type="decimal", precision=10, scale=3, nullable=false, options={"default" = 0})
   */
  private $agreement;

  /**
   * @ORM\Column(type="decimal", precision=10, scale=3, nullable=false, options={"default" = 0})
   */
  private $agreementbeforerev;

  /**
   * @return integer
   */
  public function getNum()
  {
    return $this->num;
  }

  /**
   * @param integer $num
   */
  public function setNum($num)
  {
    $this->num = $num;
  }

  /**
   * @return string
   */
  public function getValues()
  {
    return $this->vals;
  }

  /**
   * @param string $vals
   */
  public function setValues($vals)
  {
    $this->vals = $vals;
  }

  /**
   * @return integer
   */
  public function getN()
  {
    return $this->n;
  }

  /**
   * @param integer $n
   */
  public function setN($n)
  {
    $this->n = $n;
  }

  /**
   * @return integer
   */
  public function getK()
  {
    return $this->k;
  }

  /**
   * @param integer $k
   */
  public function setK($k)
  {
    $this->k = $k;
  }

  /**
   * @return float
   */
  public function getAgreement()
  {
    return $this->agreement;
  }

  /**
   * @param float $agreement
   */
  public function setAgreement($agreement)
  {
    $this->agreement = floatval($agreement);
  }

/**
   * @return float
   */
  public function getAgreementBeforeRev()
  {
    return $this->agreementbeforerev;
  }

  /**
   * @param float $agreement
   */
  public function setAgreementBeforeRev($agreement)
  {
    $this->agreementbeforerev = floatval($agreement);
  }


}
?>