<?php
namespace AppBundle\Entity;

use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProjectRepository")
 * @ORM\Table(name="project")
 */
class Project
{
    function __construct()
    {
        $this->requiref = false;
        $this->randout = true;
        $this->blindrev = true;
        $this->sentlist = false;
        $this->intragree = 0;
        $this->intratus = 0;
        $this->maxdontknow = 0;
        $this->type = "";
        $this->pooling = "";
        $this->donepooling = "";
        $this->ranges = "{}";
        $this->instructions = "";
        $this->active = true;
        $this->evalformat = 0;
        $this->guidelines = 0;
        $this->created = new \DateTimeImmutable();
        $this->modified = new \DateTime('now');
    }

    /**
     * @ORM\Id
     * @ORM\Column(type="integer", nullable=false, unique=true)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=32)
     */
    private $type;

    /**
     * @ORM\Column(type="text")
     */
    private $pooling;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $donepooling;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="text")
     */
    private $instructions;

    /**
     * @ORM\Column(type="text")
     */
    private $ranges;

    /**
     * @ORM\Column(type="boolean")
     */
    private $requiref;

    /**
     * @ORM\Column(type="boolean")
     */
    private $randout;

    /**
     * @ORM\Column(type="boolean")
     */
    private $sentlist;

    /**
     * @ORM\Column(type="integer", options={"default" = 0})
     */
    private $maxdontknow;

    /**
     * @ORM\Column(type="integer", options={"default" = 0})
     */
    private $intragree;

    /**
     * @ORM\Column(type="integer", options={"default" = 0})
     */
    private $intratus;

    /**
     * @ORM\Column(type="boolean")
     */
    private $blindrev;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     */
    private $author;

    /**
     * @ORM\Column(type="boolean", options={"default" = true})
     */
    private $active;

    //* @ORM\ManyToOne(targetEntity="Document", inversedBy="projects")
    //* @ORM\JoinColumn(name="evalformat", referencedColumnName="id")
    /** @ORM\Column(type="integer", nullable=false, options={"default" = 0}) */
    public $evalformat;

    //* @ORM\ManyToOne(targetEntity="Document", inversedBy="projects")
    //* @ORM\JoinColumn(name="guidelines", referencedColumnName="id")

    /** @ORM\Column(type="integer", nullable=false, options={"default" = 0}) */
    public $guidelines;


    /**
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @ORM\Column(type="datetime")
     */
    private $modified;



    //SET+GET methods

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param integer $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getPooling()
    {
        return $this->pooling;
    }

    /**
     * @param string $pooling
     */
    public function setPooling($pooling)
    {
        $this->pooling = $pooling;
    }

    /**
     * @return string
     */
    public function getDonePooling()
    {
        return $this->donepooling;
    }

    /**
     * @param string $donepooling
     */
    public function setDonePooling($donepooling)
    {
        $this->donepooling = $donepooling;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getInstructions()
    {
        return $this->instructions;
    }

    /**
     * @param string $instructions
     */
    public function setInstructions($instructions)
    {
        $this->instructions = $instructions;
    }

    /**
     * @return string
     */
    public function getRanges()
    {
        return $this->ranges;
    }

    /**
     * @param string $ranges
     */
    public function setRanges($ranges)
    {
        $this->ranges = $ranges;
    }

    /**
     * @return boolean
     */
    public function getRandout()
    {
        return $this->randout;
    }

    /**
     * @param boolean $randout
     */
    public function setRandout($randout)
    {
        $this->randout = $randout;
    }

    /**
     * @return boolean
     */
    public function getRequiref()
    {
        return $this->requiref;
    }

    /**
     * @param boolean $requiref
     */
    public function setRequiref($requiref)
    {
        $this->requiref = $requiref;
    }

    /**
     * @return boolean
     */
    public function getSentlist()
    {
        return $this->sentlist;
    }

    /**
     * @param boolean $sentlist
     */
    public function setSentlist($sentlist)
    {
        $this->sentlist = $sentlist;
    }

    /**
     * @return integer
     */
    public function getIntragree()
    {
        return $this->intragree;
    }

    /**
     * @param integer $intragree
     */
    public function setIntragree($intragree)
    {
        $this->intragree = $intragree;
    }

    /**
     * @return integer
     */
    public function getIntratus()
    {
        return $this->intratus;
    }

    /**
     * @param integer $intratus
     */
    public function setIntratus($intratus)
    {
        $this->intratus = $intratus;
    }

    /**
     * @return integer
     */
    public function getMaxdontknow()
    {
        return $this->maxdontknow;
    }

    /**
     * @param integer $maxdontknow
     */
    public function setMaxdontknow($maxdontknow)
    {
        $this->maxdontknow = $maxdontknow;
    }

    /**
     * @return boolean
     */
    public function getBlindrev()
    {
        return $this->blindrev;
    }

    /**
     * @param boolean $blindrev
     */
    public function setBlindrev($blindrev)
    {
        $this->blindrev = $blindrev;
    }

    /**
     * @return User
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param User $author
     */
    public function setAuthor(User $author)
    {
        $this->author = $author;
    }

    /**
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param boolean $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * @return integer
     */
    public function getEvalformat()
    {
        return $this->evalformat;
    }

    /**
     * @param integer $evalformat
     */
    public function setEvalformat($evalformat)
    {
        $this->evalformat = $evalformat;
    }

    /**
     * @return integer
     */
    public function getGuidelines()
    {
        return $this->guidelines;
    }

    /**
     * @param integer $guidelines
     */
    public function setGuidelines($guidelines)
    {
        $this->guidelines = $guidelines;
    }

    /**
     * @return DateTimeInterface
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param DateTimeInterface $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return DateTimeInterface
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * @param DateTimeInterface $modified
     */
    public function setModified($modified)
    {
        $this->modified = $modified;
    }
}
