<?php
/**
 * Project: mtequal_ext
 * User: cgirardi
 * Date: 24/10/15
 */
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RoleRepository")
 * @ORM\Table(name="role")
 */
class Role
{
  function __construct()
  {
    $this->created = new \DateTimeImmutable();
    $this->created_by=0;
  }

  /**
   * @ORM\Id
   //* @ORM\ManyToMany(targetEntity="User")
   //* @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
   * @ORM\Column(type="integer", nullable=false)
   */
  private $user;

  /**
   * @ORM\Id
   //* @ORM\ManyToMany(targetEntity="Project")
   //* @ORM\JoinColumn(name="project_id", referencedColumnName="id", nullable=false)
   * @ORM\Column(type="integer", nullable=false)
   */
  private $project;

  /**
   * @ORM\Id
   //* @ORM\ManyToMany(targetEntity="RoleType")
   //* @ORM\JoinColumn(name="role", referencedColumnName="name", nullable=false)
   * @ORM\Column(type="string", nullable=false)
   */
  private $role;

  /**
   * @ORM\Column(type="datetime")
   */
  private $created;

  /**
   * @ORM\Column(type="datetime", nullable=true)
   */
  private $activated;

  /**
   * @ORM\Column(type="integer", nullable=false)
   */
  private $created_by;

  /**
   * @ORM\Column(type="string", nullable=false)
   */
  private $status;

  /**
   * @ORM\Column(type="string", length=32)
   * @Assert\NotBlank()
   * @Assert\Length(max = 32)
   */
  private $hash;

  //SET+GET methods
  /**
   * @return integer
   */
  public function getUser()
  {
    return $this->user;
  }

  /**
   * @param integer $user
   */
  public function setUser($user)
  {
    $this->user = $user;
  }

  /**
   * @return integer
   */
  public function getProject()
  {
    return $this->project;
  }

  /**
   * @param integer $project
   */
  public function setProject($project)
  {
    $this->project = $project;
  }

  /**
   * @return string
   */
  public function getRole()
  {
    return $this->role;
  }

  /**
   * @param string $role
   */
  public function setRole($role)
  {
    $this->role = $role;
  }

  /**
   * @return \DateTimeInterface
   */
  public function getCreated()
  {
    return $this->created;
  }

  /**
   * @param \DateTimeInterface $created
   */
  public function setCreated($created)
  {
    $this->created = $created;
  }

  /**
   * @return \DateTimeInterface
   */
  public function getActivated()
  {
    return $this->activated;
  }

  /**
   * @param \DateTimeInterface $activated
   */
  public function setActivated($activated)
  {
    $this->activated = $activated;
  }

  /**
   * @return integer
   */
  public function getCreatedBy()
  {
    return $this->created_by;
  }

  /**
   * @param integer $userid
   */
  public function setCreatedBy($userid)
  {
    $this->created_by = $userid;
  }

  /**
   * @return string
   */
  public function getStatus()
  {
    return $this->status;
  }

  /**
   * @param string $status
   */
  public function setStatus($status)
  {
    $this->status = $status;
  }

    /**
     * Set hash
     *
     * @param string $hash
     *
     * @return Role
     */
    public function setHash($hash)
    {
        $this->hash = $hash;

        return $this;
    }

    /**
     * Get hash
     *
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }
}
