<?php
/**
 * Project: mtequal_ext
 * User: cgirardi
 * Date: 09/12/15
 */

namespace AppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CorpusRepository")
 * @ORM\Table(name="corpus")
 */
class Corpus
{
  /**
   * @ORM\Id()
   * @ORM\Column(type="integer")
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  private $id;
  /**
   * @ORM\Column(type="string", length=255)
   * @Assert\NotBlank
   */
  private $filename;
  /**
   * @ORM\Column(type="string", length=255, nullable=true)
   */
  private $filepath;

  /**
   * @ORM\Column(type="datetime")
   */
  private $date;

  /**
   * @ORM\Column(type="integer")
   */
  private $project;

  /**
   * @ORM\Column(type="integer", nullable=false, options={"default" = 0})
   */
  private $sentences = 0;

  /**
   * @ORM\Column(type="text", nullable=true)
   */
  private $errors;

  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set filename
   *
   * @param string $filename
   */
  public function setFilename($filename)
  {
    $this->filename = $filename;
  }

  /**
   * Get filename
   *
   * @return string
   */
  public function getFilename()
  {
    return $this->filename;
  }

  /**
   * Set filepath
   *
   * @param string $filepath
   */
  public function setFilepath($filepath)
  {
    $this->filepath = $filepath;

  }

  /**
   * Get filepath
   *
   * @return string
   */
  public function getFilepath()
  {
    return $this->filepath;
  }


  /**
   * Set date
   *
   * @param \DateTime $date
   */
  public function setDate($date)
  {
    $this->date = $date;

  }

  /**
   * Get date
   *
   * @return \DateTime
   */
  public function getDate()
  {
    return $this->date;
  }

  /**
   * Set project
   *
   * @param integer $project
   */
  public function setProject($project)
  {
    $this->project = $project;
  }

  /**
   * Get project id
   *
   * @return integer
   */
  public function getProject()
  {
    return $this->project;
  }

  /**
   * Set sentences
   *
   * @param integer $sentences
   */
  public function setSentences($sentences)
  {
    $this->sentences = $sentences;
  }

  /**
   * Get sentences
   *
   * @return integer
   */
  public function getSentences()
  {
    return $this->sentences;
  }

  /**
   * Set errors
   *
   * @param string $errors
   */
  public function setErrors($errors)
  {
    $this->errors = $errors;
  }

  /**
   * Get errors
   *
   * @return string
   */
  public function getErrors()
  {
    return $this->errors;
  }

  /**
   * @return array
   */
  public function toArray()
  {
    return array(
      'filename'   => $this->filename,
      'sentences' => $this->sentences,
    );
  }
}
