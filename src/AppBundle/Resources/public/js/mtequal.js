/**
 * Created by cgirardi on 09/12/15.
 */
//Alertify init;
alertify.set('notifier','position', 'top-right');
alertify.defaults.transition = "fade";
//alertify.confirm().setting('modal', true);
//alertify.confirm().setting('defaultFocus', 'cancel');
//alertify.confirm().setting('reverseButtons', true);

var BUTTONSELCOLOR = "4px solid red";
var BUTTONSELCOLOR2 = "4px solid blue";
var BUTTONOVERCOLOR = "#999";
var RATINGSELCOLOR = "#edb867";
var RATINGBGCOLOR = "#f5d8ab";

var ELABORATION = 0;

function fadeOut (el, color) {
    el.style.cursor="normal";
    if (el.style.border == "" || el.style.border != BUTTONSELCOLOR) {
        el.style.backgroundColor="#"+color;
    }
}

function fadeIn (el) {
    el.style.cursor="pointer";
    if (el.style.border == "" || el.style.border != BUTTONSELCOLOR) {
        el.style.borderColor=el.style.backgroundColor;
        el.style.backgroundColor=BUTTONOVERCOLOR;
    }
}

function fadeOutRev (el,color) {
    if (el.style.cursor != "not-allowed") {
        el.style.cursor = "normal";

        if (el.style.border == "" || el.style.border != BUTTONSELCOLOR2) {
            el.style.backgroundColor = "#" + color;
        }
    }
}

function fadeInRev (el) {
    if (el.style.cursor != "not-allowed") {
        el.style.cursor = "pointer";
        if (el.style.border == "" || el.style.border != BUTTONSELCOLOR2) {
            el.style.borderColor = el.style.backgroundColor;
            el.style.backgroundColor = BUTTONSELCOLOR2;
        }
    }
}

function rateIn (target, idx, tot) {
    for (var i=1; i<= tot; i++) {
        var el = document.getElementById("check."+target+"."+i);

        if (el.style.border == "" || el.style.border != BUTTONSELCOLOR) {
            el.style.borderColor = RATINGBGCOLOR;
            if (i<=idx) {
                el.style.backgroundColor = RATINGSELCOLOR;
            } else {
                el.style.backgroundColor = RATINGBGCOLOR;
            }
        }
    }
}

function rateOut (target, tot) {
    for (var i=tot; i>0; i=i-1) {
        var el = document.getElementById("check."+target+"."+i);
        if (el == null) {
            return;
        } else {
            if (el.style.border != BUTTONSELCOLOR) {
                el.style.borderColor = RATINGBGCOLOR;
                el.style.backgroundColor = RATINGBGCOLOR;
            }
        }
    }
}


function isBusy () {
    if (ELABORATION) {
        alertify.alert("","Update already in progress. Please wait...");
        setTimeout('alertify.alert().destroy()', 5000);
        return true;
    }
    return false;
}

function setBusy () {
    ELABORATION = 1;
}

function unsetBusy () {
    ELABORATION = 0;
}


function notify (message, type) {
    if (!type || type == "") {
        type = "custom";
    }
    alertify.notify(message, type, 11);
    return true;
}

function toggleBox(divId, saveCoockie) {
    if ($.cookie(divId)) {
        $.removeCookie(divId);
    } else {
        if (saveCoockie) {
            $.cookie(divId, "show");
        }
    }
    $("#"+divId).toggle();
}

function initBox(divId) {
    if ($.cookie(divId)) {
        $("#"+divId).show();
    } else {
        $("#"+divId).hide();
    }
}

function renderRoletypeMenu (el, url) {
    //if (active == null) {
        var active = [];
    //}
    $.ajax({
        url: url,
        type: 'GET',
        cache: false,
        success: function (json) {
            var menu = '<select id=role class="selectpicker" multiple>';
            for (var i in json) {
                menu += '<option value="' +json[i].name+ '"';
                if (json[i].name in active) {
                    menu += " selected";
                }
                menu +='>' + json[i].name + '</option>\n';
            }
            menu += '</select>';
            $(el).html(menu);
        },
        error: function (response, xhr, err) {
            alert("No role available!");
        }
    });
}

//generate a hexadecimal colour based on a string (used to create a label for the blind user)
var stringToColour = function(str) {

    // str to hash
    for (var i = 0, hash = 0; i < str.length; hash = str.charCodeAt(i++) + ((hash << 5) - hash));

    // int/hash to hex
    for (var i = 0, colour = "#"; i < 3; colour += ("00" + ((hash >> i++ * 8) & 0xFF).toString(16)).slice(-2));

    return colour;
}




