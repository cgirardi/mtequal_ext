/**
 * Created by cgirardi on 26/10/15.
 */

var CHECKPASS = false;
var CHECKEMAIL = false;
var PASSWORD = "";
var PASSWORD_CONFIRM = "";

function checkEmail() {
    email = $('#email').val().trim();
    if (email.length> 0) {
        setTimeout(function () {
            if ($('#email').val() == email) { // Check the value searched is the latest one or not.
                $.ajax({
                    type: "GET",
                    url: "{{ path('app_check_email') }}",
                    data: {email: email},
                    cache: false,
                    beforeSend: function () {
                        $("#checkemail").html()
                    },
                    success: function (data) {
                        $("#checkemail").html(data);
                        if (data.indexOf("<img ") == 0) {
                            CHECKEMAIL = true;
                            if (CHECKPASS)
                                document.getElementById("signup").disabled = false;
                            //$("#signup").attr("disabled", false);
                        } else {
                            CHECKEMAIL = false;
                            document.getElementById("signup").disabled = true;
                        }
                    }
                })
            }
        }, 1000); // 1 sec delay to check.
    }
}

function checkPassword (el) {
    if (el.name == "password") {
        PASSWORD = el.value;
    } else if (el.name == "password_confirm") {
        PASSWORD_CONFIRM = el.value;
    }

    if (PASSWORD == PASSWORD_CONFIRM) {
        CHECKPASS = true;
        $("#checkpassword").html("");
        if (CHECKEMAIL)
            document.getElementById("signup").disabled = false;
    } else {
        CHECKPASS = false;
        $("#checkpassword").html("Mis-matched password!");
        document.getElementById("signup").disabled = true;
    }
}