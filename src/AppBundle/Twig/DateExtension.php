<?php
/**
 * Project: mtequal_ext
 * User: cgirardi
 * Date: 25/11/15

 * Extension to provide a new built-in function in the Twig template
 */
namespace AppBundle\Twig;


class DateExtension extends \Twig_Extension
{
  public function getFilters()
  {
    return array(
      new \Twig_SimpleFilter('timeago', array($this, 'timeAgo')),
    );
  }

  public function timeAgo($datetime)
  {

    if ($datetime instanceof \DateTime) {
      //do nothing
    } else if (preg_match("/\\d\\d\\d\\d-\\d\\d-\\d\\d \\d\\d:\\d\\d:\\d\\d/", $datetime)) {
      $datetime = \DateTime::createFromFormat("Y-m-d H:i:s", $datetime);
    } else {
      return strtotime($datetime);
    }
    $now = new \DateTime();

    return self::formatDateDiff($now, $datetime);
  }

  public function formatDateDiff($start, $end=null) {
    if(!($start instanceof \DateTime)) {
      $start = new \DateTime($start);
    }

    if($end === null) {
      $end = new \DateTime();
    }

    if(!($end instanceof \DateTime)) {
      $end = new \DateTime($start);
    }

    $interval = $end->diff($start);
    $doPlural = function($nb,$str){return $nb>1?$str.'s':$str;}; // adds plurals

    $format = array();
    if($interval->y !== 0) {
      $format[] = "%y ".$doPlural($interval->y, "year");
    }
    if($interval->m !== 0) {
      $format[] = "%m ".$doPlural($interval->m, "month");
    }
    if($interval->d !== 0) {
      $format[] = "%d ".$doPlural($interval->d, "day");
    }
    if($interval->h !== 0) {
      $format[] = "%h ".$doPlural($interval->h, "hour");
    }
    if($interval->i !== 0) {
      $format[] = "%i ".$doPlural($interval->i, "minute");
    }
    if($interval->s !== 0) {
      if(!count($format)) {
        return "less than a minute";
      } else {
        $format[] = "%s ".$doPlural($interval->s, "second");
      }
    }

    // We use the two biggest parts
    if(count($format) > 1) {
      $format = array_shift($format)." and ".array_shift($format);
    } else {
      $format = array_pop($format);
    }

    if ($format == "") {
      return "now";
    }
    // Prepend 'since ' or whatever you like
    return $interval->format($format . " ago");
  }

  public function getName()
  {
    return 'date_extension';
  }
}