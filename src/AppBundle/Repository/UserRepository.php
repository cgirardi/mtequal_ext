<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\User;
use Doctrine\ORM\Query;

/**
 * User repository class
 */

class UserRepository extends EntityRepository {
  /*
  * get user information from ID
  */
  public function getUserFromID($id)
  {
    return $this->findOneBy(array("id" => $id));
  }

  /*
  * get a user by column->value pairs
  */
  public function getUser(array $value)
  {
    return $this->findOneBy($value);
  }

  /**
   * returns an hash with information for all users
   */
  public function getUsers()
  {
    $em = $this->getEntityManager();

    $dql = "SELECT u.id, u.firstname, u.lastname, u.username, u.email, u.affiliation, u.roles, u.active, u.logged, u.registered
        FROM AppBundle:User AS u
        ORDER BY u.username";

    /** @var Query $query */
    $query = $em->createQuery($dql);

    return $query->execute();
  }

  /*
  * create a new user
  * @return User
  */
  public function createUser(array $values)
  {
    $user = new User();
    $user->setUsername(trim($values['username']));
    $user->setEmail(trim($values['email']));
    $user->setPassword(md5(trim($values['password'])));
    $user->setFirstname("");
    $user->setLastname("");
    $user->setAffiliation("");
    $user->setNotes("");
    $user->setApiKey(self::newAPIKey());
    $user->setActive(false);

    $user->setHash(self::getHash());
    $time = new \DateTime("now");
    $user->setRegistered($time);
    $user->setModified($time);

    $em = $this->getEntityManager();
    $em->persist($user);
    $em->flush();

    return $user;
  }

  /*
  * verify a user registration by hashing
  * @return boolean
  */
  public function verifyUser($email, $hash) {
    $em = $this->getEntityManager();
    /** @var User $user */
    $user = $this->findOneBy(array("email" => $email, "hash" => $hash));
    if ($user) {
      $user->setVerified(new \DateTime("now"));
      $user->setHash(self::getHash());
      $user->setActive(true);

      $em->persist($user);
      $em->flush();
      return 1;
    }
    return 0;
  }

  /*
  * set the user hashing
  */
  public function resetUserHash($id)
  {
    /** @var User $user */
    $hash = self::getHash();
    if ($this->updateUser($id, array('hash' => $hash)) == 1) {
      return $hash;
    }
    return null;
  }


  /*
  * update user information
  */
  public function updateUser($id, $values)
  {
    /** @var User $user */
    $user = $this->getUserFromID($id);

    if ($user) {
      if (array_key_exists('firstname',$values)) {
        $user->setFirstname($values['firstname']);
      }
      if (array_key_exists('lastname', $values)) {
        $user->setLastname($values['lastname']);
      }
      if (array_key_exists('username', $values)) {
        $user->setUsername(trim($values['username']));
      }
      if (array_key_exists('password', $values)) {
        $user->setPassword(md5(trim($values['password'])));
      }
      if (array_key_exists('email', $values)) {
        $user->setEmail(trim($values['email']));
      }
      if (array_key_exists('affiliation', $values)) {
        $user->setAffiliation($values['affiliation']);
      }
      if (array_key_exists('roles', $values)) {
        $user->setRoles($values['roles']);
      }
      if (array_key_exists('notes', $values)) {
        $user->setNotes($values['notes']);
      }
      if (array_key_exists('active', $values)) {
        $user->setActive($values['active']);
      }
      if (array_key_exists('hash', $values)) {
        $user->setHash($values['hash']);
      }
      if (array_key_exists('logged', $values)) {
        $user->setLogged($values['logged']);
      }
      $user->setModified(new \DateTime());

      if ($user->getApiKey() == "" || array_key_exists('api_key', $values)) {
        $user->setApiKey(self::newAPIKey());
      }
      $em = $this->getEntityManager();
      $em->persist($user);
      $em->flush();
      return 1;
    }
    return 0;
  }

  /*
  * delete a user
  */
  public function deleteUser($id)
  {
    $em = $this->getEntityManager();
    $user = $this->getUserFromID($id);
    if ($user != null) {
      //TODO: check dependencies: delete annotation, comment, roles, progress,...
      $em->remove($user);
      $em->flush();
      return 1;
    }
    return 0;
  }

  /**
   * get an hash starting from the current date and a random number
   */
  private function getHash() {
    $time = new \DateTime("now");
    return md5($time->format('Y-m-d H:i:s.u') ." ". rand(1, 9999));
  }

  /**
   * get user by api key
   */
  private function hasUserAPIKey ($apikey) {
    /** @var User $user */
    $user = $this->findOneBy(array("apiKey" => $apikey));
    if ($user) {
      return true;
    }
    return false;
  }

  private function newAPIKey () {
    $apikey = sprintf("%010s-%s", time(), uniqid(true));
    while (self::hasUserAPIKey($apikey)) {
      $apikey = sprintf("%010s-%s", time(), uniqid(true));
    }
    return $apikey;
  }
}