<?php
/**
 * Project: mtequal_ext
 * User: cgirardi
 * Date: 18/02/16
 */
namespace AppBundle\Repository;

use AppBundle\Entity\Progress;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

/*
 * Progress repository class
 */
class ProgressRepository extends EntityRepository
{

  const CREATION_TASK = "creation";
  const EVALFORMAT_TASK = "evalformat";
  const IMPORTCORPUS_TASK = "import_corpus";
  const GUIDELINES_TASK = "guidelines";
  const CHECKCORPUS_TASK = "check_corpus";
  const PJM_INVITATION = "admin_invitation";
  const VENDOR_INVITATION = "vendor_invitation";
  const REVIEWER_INVITATION = "reviewer_invitation";
  const EVALUATOR_INVITATION = "evaluator_invitation";
  //const START_EVALUATION_TASK = "start_evaluation";
  const EVALUATION_TASK = "evaluation";
  //const JUDGE_TASK = "judge";
  const REVIEW_TASK = "review";
  const FINALIZE_TASK = "finalize";
  //const CONFIRM_FINALIZE_TASK = "confirm_finalize";

  //list of task invitation
  static $INVITATION_TASKS = [
    self::PJM_INVITATION => ["admin", "settings.access", "admin"],
    self::VENDOR_INVITATION => ["admin", "settings.access", "vendor"],
    self::EVALUATOR_INVITATION => ["vendor", "settings.access", "vendor"],
    self::REVIEWER_INVITATION => ["admin", "settings.access", "evaluator"]
  ];

  //list of the own user task
  static $USER_TASKS = ["evaluator" => self::EVALUATION_TASK,
      "reviewer" => self::REVIEW_TASK];

  //list of the dependencies among the tasks: the value is an array in which the first element
  // is the user role enable to complete the task (empty for automatical task completation), the second is the link to the related panel
  // while the rest elements are the tasks available after its completion
  static $TASK_DEPENDENCIES = [
      self::CREATION_TASK => ["", "", self::EVALFORMAT_TASK],
      self::EVALFORMAT_TASK => ["owner", "settings.evalformat", self::IMPORTCORPUS_TASK],
      self::IMPORTCORPUS_TASK => ["owner", "settings.import", self::GUIDELINES_TASK],
      self::GUIDELINES_TASK => ["owner", "settings.guidelines", self::CHECKCORPUS_TASK],
      self::CHECKCORPUS_TASK => ["owner", "corpus", self::EVALUATION_TASK],
     // self::START_EVALUATION_TASK => ["vendor", ""],
      self::EVALUATION_TASK => ["evaluator", "evaluation", self::REVIEW_TASK],
    //self::EVALUATION_TASK => array("evaluator", self::JUDGE_TASK, self::REVIEW_TASK),
    //self::JUDGE_TASK => array("judge", self::REVIEW_TASK),
      self::REVIEW_TASK => ["reviewer", "review", self::FINALIZE_TASK],
      self::FINALIZE_TASK => ["owner", ""]
  ];


  public function initProgress($projectId, $userId)
  {
    $em = $this->getEntityManager();

    foreach (self::$TASK_DEPENDENCIES as $tasklabel => $properties) {
      if (!in_array($tasklabel, array_values(self::$USER_TASKS))) {
        $progress = new Progress();
        $progress->setTask($tasklabel);
        $progress->setProject($projectId);
        $progress->setCompleted(FALSE);
        $progress->setDisabled(TRUE);
        $progress->setUser($userId);
        $progress->setNum(array_search($tasklabel, array_keys(self::$TASK_DEPENDENCIES)));

        $em->persist($progress);
      }
    }
    $em->flush();
    $this->updateProgress(self::CREATION_TASK, $projectId, TRUE, $userId, null, null);
  }

  /**
   * get true if task is valid
   * @return boolean
   */
  public function isValidTask($tasklabel)
  {
    if (array_key_exists($tasklabel, self::$TASK_DEPENDENCIES)) {
      return true;
    }
    return false;
  }

  /**
   * get true if task is valid
   * @return boolean
   */
  public function isEnableTask($taskLabel, $projectId, $userId)
  {
    /** @var Progress $progress */
    if ($userId == null || in_array($taskLabel, array_values(self::$USER_TASKS))) {
      $progress = $this->findOneBy(
          array("task" => $taskLabel, "project" => $projectId, "user" => $userId));
    } else {
      $progress = $this->findOneBy(
          array("task" => $taskLabel, "project" => $projectId));
    }

    if ($progress && $progress->getCompleted() == 0 && $progress->getDisabled() == 0) {
      return true;
    }
    return false;
  }

  /**
   * get true if task is completed
   * @return boolean
   */
  public function isCompletedTask($taskLabel, $projectId)
  {
    if (count($this->findBy(array("task" => $taskLabel, "project" => $projectId, "completed" => 0))) == 0) {
      return true;
    }
    return false;
  }

  /**
   * get true if a project is completed
   * @return boolean
   */
  public function isCompletedProject($projectId)
  {
    if (count($this->findBy(array("project" => $projectId, "completed" => 0))) == 0) {
      return true;
    }
    return false;
  }

  /**
   * get an array with the due tasks for each project
   * @return array
   */
  public function dueTasks($userId)
  {
    $taskList = array();
    $uncompletedTask = array();
    $evaluationTask = array();
    $em = $this->getEntityManager();

    $dql = "SELECT p.project, p.task, p.user, p.duedate, p.completed, p.disabled, p.num
          FROM AppBundle:Progress AS p
          WHERE  (p.disabled=0 OR p.completed=0 OR p.task='evaluation')
            AND p.project IN (SELECT DISTINCT pr.id FROM AppBundle:Project AS pr
                  INNER JOIN AppBundle:Role AS r2 WHERE pr.id=r2.project AND pr.active=1 AND r2.user=:userid AND r2.role!='' AND r2.status='accepted')
          ORDER BY p.project, p.num, p.disabled";
    $query = $em->createQuery($dql);
    $query->setParameter('userid', $userId);

    $tasks = $query->getResult();
    if (count($tasks) > 0) {
      /** @var Progress $task */
      foreach ($tasks as $task) {
        if ($task["task"] == self::EVALUATION_TASK) {
          $evaluationTask[$task["project"]] = 1;
        }
        if ($task["completed"] == 0 && !array_key_exists($task["project"], $uncompletedTask)) {
          $uncompletedTask[$task["project"]] = $task["task"];
        }

        if (array_key_exists($task["project"], $taskList)) {
          $values = $taskList[$task["project"]];

          if ($task["disabled"] == 0) {
            $tname = explode(", ", $values[1]);
            if (!in_array($task["task"], $tname)) {
              $values[1] .= ", " . $task["task"];
            }

            if ($task["duedate"] != null) {
              if ($values[2] == null || $values[2] > $task["duedate"]) {
                $values[2] = $task["duedate"];
              }
            }

            $taskList[$task["project"]] = $values;
          }
        } else {
          if ($task["disabled"] == 0) {
            $taskList[$task["project"]] = array(1, $task["task"], $task["duedate"]);
          }
        }

        foreach ($uncompletedTask as $projectId => $utask) {
          if (!array_key_exists($projectId, $taskList)) {
            $taskList[$projectId] = array(0, $utask, "");
          }
        }
      }
    }
    foreach ($taskList as $projectId => $taskInfo) {
      if (($taskInfo[1] == self::REVIEW_TASK || $taskInfo[1] == self::FINALIZE_TASK) && !array_key_exists($projectId,$evaluationTask)) {
        $taskList[$projectId][1] = self::EVALUATION_TASK;
        $taskList[$projectId][2] = "";
      }
    }

    return $taskList;
  }

  /**
   * get enable tasks
   * @return array
   */
  public function enableTasks($projectId, $userId)
  {
    $taskList = array();
    $tasks = $this->findBy(array("project" => $projectId, "disabled" => 0));
    if (count($tasks) > 0) {

      /** @var Progress $task */
      foreach ($tasks as $task) {
        $taskname = $task->getTask(); // preg_replace("/_/"," ",$task->getTask());
        if (in_array($taskname, array_values(self::$USER_TASKS))) {
          if ($task->getUser() == $userId) {
            if ($task->getDuedate() == null) {
              $taskList[$taskname] = "";
            } else {
              $taskList[$taskname] = $task->getDuedate()->format('Y-m-d H:i:s');
            }
            continue;
          }
        } else {
          $em = $this->getEntityManager();
          /** @var RoleRepository $role_repository */
          $role_repository = $em->getRepository('AppBundle:Role');
          $roles = $role_repository->getUserRoles($userId, $projectId);

          if (in_array("admin", $roles) || in_array(self::$TASK_DEPENDENCIES[$taskname][0], $roles)) {
            if ($task->getDuedate() == null) {
              $taskList[$taskname] = "";
            } else {
              $taskList[$taskname] = $task->getDuedate()->format('Y-m-d H:i:s');
            }
          }
        }
      }
    }
    return array_unique($taskList);
  }

  /**
   * get dependent tasks
   * @return array
   */
  public function dependencyTasks($taskLabel)
  {
    $taskList = array();
    foreach (self::$TASK_DEPENDENCIES as $task => $properties) {
      for ($i = 2; $i < count($properties); $i++) {
        if ($taskLabel == $properties[$i]) {
          array_push($taskList, preg_replace("/_/", " ", $task));
        }
      }
    }

    return array_unique($taskList);
  }


  /**
   * get a specific task progress
   * @returns Progress
   */
  public function getProgress($task, $project, $user = null)
  {
    $qarray = array("task" => $task, "project" => $project);
    if ($user != null && in_array($task, array_values(self::$USER_TASKS))) {
      $qarray["user"] = $user;
    }
    return $this->findOneBy($qarray);
  }


  /**
   * create a new progress or update a progress already exists
   * @return Progress
   */
  public function updateProgress($taskLabel, $projectId, $isCompleted, $userId, $role=null, $duedate=null)
  {
    //TODO: controllo che l'utente abbia il ruolo assegnato al task
    if ($role != null && $role != "admin" && self::$TASK_DEPENDENCIES[$taskLabel][0] != $role) {
      return null;
    }
    /** @var Progress $progress */
    if ($userId != null && in_array($taskLabel, array_values(self::$USER_TASKS))) {
      $progress = $this->getProgress($taskLabel, $projectId, $userId);
    } else {
      $progress = $this->getProgress($taskLabel, $projectId);
    }
    if ($progress == null) {
      $progress = new Progress();
      try {
        $progress->setTask($taskLabel);
        $progress->setProject($projectId);
        $progress->setNum(array_search($taskLabel, array_keys(self::$TASK_DEPENDENCIES)));
      } catch (\PDOException $e) {
        // Will output an SQLSTATE[23000] message, similar to:
        // Integrity constraint violation: 1062 Duplicate entry 'x'
        if ($e->getCode() != '23000') {
          //echo $e->getMessage();
          throw $e;
        }
      }
    }

    if ($progress) {
      //$em is the entity manager
      $em = $this->getEntityManager();
      if ($userId != null) {
        $progress->setUser($userId);
      }
      $progress->setCompleted($isCompleted);
      $em->persist($progress);
      $em->flush();


      $isDisabled = FALSE;
      $disableNext = TRUE;
      $enableTask = array();
      $dql = "SELECT DISTINCT p.num AS num, p.task AS task, p.completed AS completed
              FROM AppBundle:Progress AS p
              WHERE p.project=:id ORDER BY p.num";
      $query = $em->createQuery($dql);
      $query->setParameter('id', $projectId);

      $result = $query->getResult();
      foreach ($result as $pitem) {
        if ($pitem["completed"]) {
          for ($i = 2; $i < count(self::$TASK_DEPENDENCIES[$pitem["task"]]); $i++) {
            array_push($enableTask, self::$TASK_DEPENDENCIES[$pitem["task"]][$i]);
          }
        }
        if ($pitem["task"] == $taskLabel) {
          if ($isCompleted) {
            $isDisabled = TRUE;
            if (count($this->findBy(array("task" => $taskLabel, "project" => $projectId, "completed" => 0))) == 0) {
              $disableNext = FALSE;
            }
          } else if (in_array($taskLabel, $enableTask)) {
            $isDisabled = FALSE;
          }
        } else {
          if ($pitem["num"] < $progress->getNum()) {
            if ($pitem["completed"] == 0) {
              $isDisabled = TRUE;
            }
          } else {
            $this->changeProgressStatus($pitem["task"], $projectId, FALSE, $disableNext);
            $disableNext = TRUE;
          }
        }
      }

      //enable the current task if all the previous dependent tasks are complete
      /*foreach (self::$TASK_DEPENDENCIES as $label => $properties) {
        if (count($properties) > 1) {
          for ($i = 2; $i < count($properties); $i++) {
            if (in_array($taskLabel, $properties)) {
              if (!$this->isCompletedTask($label, $projectId)) {
                $isDisabled = TRUE;
                break;
              }
            }
          }
          if ($isDisabled) {
            break;
          }
        } else {
          $isDisabled = TRUE;
        }
      }*/


      $progress->setDisabled($isDisabled);
      if ($duedate != null)
        $progress->setDuedate(new \DateTime($duedate, new \DateTimeZone('UTC')));
      $progress->setModified(new \DateTime("now"));

      $em->persist($progress);
      $em->flush();

      //enable next tasks if all $taskLabel tasks have been completed
      if ($taskLabel == self::CHECKCORPUS_TASK) {
        if (count($this->findBy(array("task" => $taskLabel, "project" => $projectId, "completed" => 0))) == 0) {
          for ($i = 2; $i < count(self::$TASK_DEPENDENCIES[$taskLabel]); $i++) {
            //if (self::$TASK_DEPENDENCIES[$taskLabel][$i] == self::EVALUATION_TASK) {
              //$this->changeProgressStatus(self::$TASK_DEPENDENCIES[$taskLabel][$i], $projectId, FALSE, TRUE);
            //} else {
              $this->changeProgressStatus(self::$TASK_DEPENDENCIES[$taskLabel][$i], $projectId, FALSE, FALSE);
            //}
          }
        }
      }

      //update the modified time of the project
      $em->getRepository('AppBundle:Project')->updateModifiedTime($projectId);
    }
    return $progress;
  }

  /**
   * update a task by a user
   */
  public function updateUserProgress($role, $project, $user, $status)
  {
    if (array_key_exists($role, self::$USER_TASKS)) {
      if ($status == "revoked") {
        //delete progress
        return $this->deleteProgress(self::$USER_TASKS[$role], $project, $user);
      } else if ($status == "accepted") {
        //update progress
        if ($this->updateProgress(self::$USER_TASKS[$role], $project, FALSE, $user, null, null) != null) {
          return true;
        }
      }
    }
    return false;
  }

  /**
   * update the duedate
   */
  public function setDueDate($taskid, $duedate)
  {
    /** @var Progress $progress */
    //$progress = $this->getProgress($taskLabel, $project, $userid);
    $progress = $this->findOneBy(array("id" => $taskid));
    if ($progress != null) {
      $em = $this->getEntityManager();

      if ($duedate != null)
        $progress->setDuedate(new \DateTime($duedate, new \DateTimeZone('UTC')));
      else
        $progress->setDuedate(null);

      $em->persist($progress);
      $em->flush();
    }
    return $progress;
  }

  /**
   * enable/disable a task
   */
  public function changeProgressStatus($taskLabel, $project, $complete, $disable)
  {
    $tasks = $this->findBy(array("task" => $taskLabel, "project" => $project));
    if (count($tasks) > 0) {
      $em = $this->getEntityManager();

      /** @var Progress $progress */
      foreach ($tasks as $progress) {
        $progress->setCompleted($complete);
        $progress->setDisabled($disable);
        $progress->setModified(new \DateTime("now"));

        $em->persist($progress);
        $em->flush();
      }

      if ($taskLabel == self::GUIDELINES_TASK && $complete == 0) {
          // remove all intra TUs
          $em->getRepository("AppBundle:Sentence")->deleteIntraCorpus($project);
      }
      //recursive call
      //for ($i = 1; $i < count(self::$TASK_DEPENDENCIES[$taskLabel]); $i++) {
      //$this->changeProgressStatus(self::$TASK_DEPENDENCIES[$taskLabel][$i], $project, $user, FALSE, TRUE);
      //}
    }
  }

  /**
   * delete a task
   */
  public function deleteProgress($task, $project, $user)
  {
    $taskObj = $this->findOneBy(array("task" => $task, "project" => $project, "user" => $user));
    if ($taskObj) {
      $em = $this->getEntityManager();
      $em->remove($taskObj);
      $em->flush();

      return true;
    }
    return false;
  }

  /**
   * delete a task
   */
  public function deleteAllProgress($projectId, $userId = null)
  {
    if ($userId == null) {
      $tasks = $this->findBy(array("project" => $projectId));
    } else {
      $tasks = $this->findBy(array("project" => $projectId, "user" => $userId));
    }
    if (count($tasks) > 0) {
      $em = $this->getEntityManager();
      /** @var Progress $task */
      foreach ($tasks as $task) {
        if (in_array($task->getTask(), array_values(self::$USER_TASKS))) {
          $em->remove($task);
        }
      }
      $em->flush();

      //update the modified time of the project
      $em->getRepository('AppBundle:Project')->updateModifiedTime($projectId);
      return true;
    }
    return false;
  }


  /**
   * tasks report
   */
  public function taskReport($projectId, $userId)
  {
    $em = $this->getEntityManager();

    $tasks = array();
    $eval_progress = array();
    if ($projectId != null) {
      $dql = "SELECT p.id, p.task, p.completed, p.disabled, p.modified, p.duedate, p.user, u.username
                        FROM AppBundle:Progress AS p
                        INNER JOIN AppBundle:User AS u
                        WHERE p.user=u.id AND p.project=:pid
                        ORDER BY p.num, p.id";
      /** @var Query $query */
      $query = $em->createQuery($dql);
      $query->setParameter('pid', $projectId);

      $tasks = $query->getResult();

	  /** @var RoleRepository $repository_role */
      $repository_role = $em->getRepository("AppBundle:Role");
      $joinUsers = $repository_role->getRoles($projectId, $userId);
      $users = array();
      $invitations = array();
      foreach ($joinUsers as $jUser) {
      	$users[$jUser["id"]] = [$jUser["username"], $jUser["inviter"]];
        if ($jUser["status"] == "accepted") {
          if (!array_key_exists($jUser["role"] . "_invitation", $invitations)) {
            $invitations[$jUser["role"] . "_invitation"] = array();
          }
          if (!array_key_exists($jUser["inviter"], $invitations[$jUser["role"] . "_invitation"])) {
            $invitations[$jUser["role"] . "_invitation"][$jUser["inviter"]] = 0;
          }
          $invitations[$jUser["role"] . "_invitation"][$jUser["inviter"]]++;
        }
      }
      
      /** @var AnnotationRepository $repository_annotation */
      $repository_annotation = $em->getRepository("AppBundle:Annotation");
      foreach ($tasks as $task) {
        if ($task["task"] == self::EVALUATION_TASK) {
          if (array_key_exists($task["user"], $users)) {
          	$eval_progress[$task["user"]] = $repository_annotation->countDone($projectId, $task["user"]);
          }
        }
      }
    }

    /** @var SentenceRepository $repository_sentence */
    $repository_sentence = $em->getRepository("AppBundle:Sentence");
    $tus = $repository_sentence->countTUs($projectId);

    return array(
      'tasks' => $tasks,
      'task_byuser' => array_values(self::$USER_TASKS),
      'task_list' => self::$TASK_DEPENDENCIES,
      'invitation_tasks' => self::$INVITATION_TASKS,
      'eval_progress' => $eval_progress,
      'users' => $users,
      'invitations' => $invitations,
      'tus' => $tus
    );
  }
}
?>