<?php
/**
 * Project: mtequal_ext
 * User: cgirardi
 * Date: 09/02/16
 */

namespace AppBundle\Repository;

use AppBundle\Entity\Agreement;
use AppBundle\Entity\Project;
use AppBundle\Entity\Sentence;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

/**
 * Agreement repository class
 */
class AgreementRepository extends EntityRepository
{
/*
 * update agreement
 */
  function updateAgreement($outputId, $projectId)
  {
    $em = $this->getEntityManager();
    /** @var Project $project */
    $project = $em->getRepository('AppBundle:Project')->findOneBy(array("id" => $projectId));

    /** @var SentenceRepository $sentence_repository */
    $sentence_repository = $em->getRepository("AppBundle:Sentence");

    //select eval,count(*) from annotation left join progress on annotation.output_id=progress.sentence where num=24 group by eval order by eval;
    $dql = "SELECT s.num, a.eval, COUNT(a.eval) as cnt, SUM(a.revuser) as revuser
        FROM AppBundle:Sentence AS s
        INNER JOIN AppBundle:Annotation AS a
        WHERE s.num=a.num AND s.id=:outputid AND s.project=:project
        AND a.confirmed IS NOT NULL AND a.active=1
        GROUP BY a.num, a.eval ORDER BY a.num, a.eval";
    /** @var Query $query */
    $query = $em->createQuery($dql);
    $query->setParameter('outputid', $outputId);
    $query->setParameter('project', $projectId);

    $rows = $query->getResult();

    $annotations = array();
    $evaluators_count = array();
    $precision_count=array();

    $numGroupedEval = 1;
    $isAgreeBeforeRev = true;
    if ($project->getType() == "tree") {
      $questions = json_decode($project->getRanges());
      $numGroupedEval = count($questions);
    }
    foreach ($rows as $row) {
      if (!array_key_exists($row["num"], $annotations)) {
        $evaluators_count[$row["num"]] = 0;
        $precision_count[$row["num"]] = array();
        $annotations[$row["num"]] = array();

        for ($n=0; $n < $numGroupedEval; $n++) {
          $precision_count{$row["num"]}{$n} = array();
        }
      }
      $evaluators_count[$row["num"]] += intval($row["cnt"]);
      $annotations[$row["num"]][] = ['eval' => $row["eval"], 'cnt' => intval($row["cnt"]), 'revuser' => $row["revuser"]];

      if ($row["revuser"] != 0) {
        $isAgreeBeforeRev=false;
      }
      for ($n=0; $n < $numGroupedEval; $n++) {
        $ev = $row["eval"];
        if ($ev != 0) {
          $ev = substr($row["eval"], $n, 1);
        }
        $precision_count{$row["num"]}{$n}{$ev} += intval($row["cnt"]);
      }
    }

    // add the sentences don't confirmed yet
    $outputs = $sentence_repository->getOutput($outputId, $projectId);
    foreach ($outputs as $output) {
      if (!array_key_exists($output["num"], $annotations)) {
        $annotations[$output["num"]] = [];
      }
    }

    $evaluators=0;
    $categories=0;
    foreach ($annotations as $sentence_num => $values) {
      /** @var Agreement $agreement */
      $agreement = $this->find($sentence_num);
      if (count($values) == 0) {
        if ($agreement) {
          $em->remove($agreement);
          $em->flush();
        }
      } else {
        if (!$agreement) {
          $agreement = new Agreement();
          $agreement->setNum($sentence_num);
        }

        //keep the max number of evaluators per TU
        if ($evaluators_count[$sentence_num] > $evaluators) {
          $evaluators = $evaluators_count[$sentence_num];
        }
        $agreement->setN($evaluators_count[$sentence_num]);
        //keep the max number of used values per TU
        if (count($values) > $categories) {
          $categories=count($values);
        }
        $agreement->setK(count($values));
        $agreement->setValues(json_encode($values));

        if ($evaluators_count[$sentence_num] > 1) {
          $obsAgree = 0;
          foreach ($precision_count{$sentence_num} as $n => $arr) {
            $precision = 0;
            foreach ($arr as $ev => $cnt) {
              $precision += $cnt * $cnt;
            }
            $obsAgree += (($precision - $evaluators_count[$sentence_num]) / ($evaluators_count[$sentence_num] * ($evaluators_count[$sentence_num] - 1)));
          }
          $agree = $obsAgree/$numGroupedEval;
          if ($agree != 0 && $agree != 1) {
            $agree = round($agree,3);
          }
          $agreement->setAgreement($agree);
          if ($isAgreeBeforeRev) {
            $agreement->setAgreementBeforeRev($agree);
          }
        }
        $em->persist($agreement);
        $em->flush();
      }
    }

    //update source agreement
    /** @var Sentence $source */
    $source = $sentence_repository->findOneBy(array("id" => $outputId, "type" => "source", "project" => $projectId));
    if ($source) {
      $agreement = $this->find($source->getNum());
      if ($agreement) {
        if ($evaluators == 0) {
          $em->remove($agreement);
        } else {
          $agreement->setN($evaluators);
          $agreement->setK($categories);
          $agreement->setValues("");
          $em->persist($agreement);
        }
      } else {
        $agreement = new Agreement();
        $agreement->setNum($source->getNum());
        $agreement->setN($evaluators);
        $agreement->setK($categories);
        $agreement->setValues("");
        $em->persist($agreement);
      }
      $em->flush();
    }

    //update the modified time of the project
    $em->getRepository('AppBundle:Project')->updateModifiedTime($projectId);
  }

  /*
   * get evaluation count according with the selected filters
   */
  function getAgreementCountEvaluation ($projectId, $evaluatorsClause) {
    $queryparam ['projectid'] = $projectId;
    $query = "SELECT an.eval, COUNT(an.eval) as cnt
        FROM AppBundle:Annotation AS an
        INNER JOIN AppBundle:Agreement AS ag
        WHERE an.num=ag.num AND an.active=1 AND an.confirmed IS NOT NULL";

    if ($evaluatorsClause > -1) {
      $query .= " AND ag.n = :n";
      $queryparam ['n'] = $evaluatorsClause;
    } else {
      $query .= " AND ag.n > 0";
    }

    $query .= " AND an.num IN (SELECT s.num FROM AppBundle:Sentence AS s WHERE s.project=:projectid AND s.isintra<=1) GROUP BY an.eval ORDER BY an.eval";

    $dql = $this->getEntityManager()->createQuery($query)->setParameters($queryparam);

    return $dql->getResult();
  }

  function getDisagreementCountEvaluation ($projectId, $evaluatorsClause) {
    $queryparam ['projectid'] = $projectId;
    $query = "SELECT an.eval, COUNT(an.eval) as cnt
        FROM AppBundle:Annotation AS an
        INNER JOIN AppBundle:Agreement AS ag
        WHERE an.num=ag.num AND an.active=1 AND an.confirmed IS NOT NULL AND ag.k > 1";

    if ($evaluatorsClause > -1) {
      $query .= " AND ag.n = :n";
      $queryparam['n'] = $evaluatorsClause;
    } else {
      $query .= " AND ag.n > 0";
    }

    $query .= " AND an.num IN (SELECT s.num FROM AppBundle:Sentence AS s WHERE s.project=:projectid AND s.isintra<=1) GROUP BY an.eval ORDER BY an.eval";

    $dql = $this->getEntityManager()->createQuery($query)->setParameters($queryparam);

    return $dql->getResult();
  }

  function getSentenceAgreement($sentenceNum)
  {
    $query = "SELECT s.text, a.agreement
        FROM AppBundle:Sentence AS s
        INNER JOIN AppBundle:Agreement AS a
        WHERE s.num=a.num AND s.num=:num";

    /** @var Query $dql */
    $dql = $this->getEntityManager()->createQuery($query)->setParameter("num", $sentenceNum);
    return $dql->getArrayResult();
  }


  function getAgreement($projectId, $evaluatorsClause)
  {
    $queryparam ['project'] = $projectId;
    $query = "SELECT s.num, s.id, s.type, s.text, a.vals, a.n, a.k, a.agreement
        FROM AppBundle:Sentence AS s
        INNER JOIN AppBundle:Agreement AS a
        WHERE s.num=a.num AND s.project=:project AND s.simto IS NULL AND s.isintra<=1";

    if ($evaluatorsClause > -1) {
      $query .= " AND a.n = :n OR (s.num=s.linked AND s.type='source')";
      $queryparam ['n'] = $evaluatorsClause;
    }
    $query .= " ORDER BY s.num";

    $dql = $this->getEntityManager()->createQuery($query)->setParameters($queryparam);

    //convert JSON to array
    //$hashVals = json_decode($agreement->getVals(), TRUE);
    return $dql->getResult();
  }

  function getDisagreement($projectId, $evaluatorsClause)
  {
    $queryparam ['project'] = $projectId;
    $query = "SELECT s.num, s.id, s.type, s.text, a.vals, a.n, a.k, a.agreement
        FROM AppBundle:Sentence AS s
        INNER JOIN AppBundle:Agreement AS a
        WHERE s.num=a.num AND s.project=:project AND s.simto IS NULL AND s.isintra<=1 AND a.k > 1";

    if ($evaluatorsClause > -1) {
      $query .= " AND a.n = :n";
      $queryparam['n'] = $evaluatorsClause;
    } else {
      $query .= " AND a.n > 0";
    }
    $query .= " ORDER BY s.num";

    $dql = $this->getEntityManager()->createQuery($query)->setParameters($queryparam);

    //convert JSON to array
    //$hashVals = json_decode($agreement->getVals(), TRUE);
    return $dql->getResult();
  }
}
?>