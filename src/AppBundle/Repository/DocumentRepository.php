<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Document;
use AppBundle\Entity\Project;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

/**
 * Document repository class
 */
class DocumentRepository extends EntityRepository
{
    /**
     * create/update a document
     *
     * @param array $values
     * @return Document
     */
    public function update(array $values)
    {
        $time = new \DateTime("now");
        $url = "";

        /** @var Document $document */
        if (array_key_exists('id',$values)) {
            $document = $this->find($values['id']);
        } else if (array_key_exists('url',$values) && array_key_exists('type',$values)) {
            //check the document's url (if it exists) to avoid duplication
            $url = trim($values['url']);
            $document = $this->findOneBy(array("url" => $url, "type" => $values['type']));
        }
        if ($document) {
            $document->setUsed(self::getProjectsCount($document->getId()));
        } else {
            $document = new Document();
            $document->setUpdated($time);
        }

        if (!empty($url)) {
            $document->setUrl($url);
        }
        //set the type (i.e. evalformat, guidelines, image, ...)
        if (array_key_exists('type',$values)) {
            $document->setType($values['type']);
        }

        /* set the format according with the type:
         * - for image: png, jpg, gif,...
         * - for evalformat: binary, scale, ranking, tree
         * - for guidelines: empty
         */
        if (array_key_exists('format',$values)) {
            $document->setFormat($values['format']);
        }

        if (array_key_exists('data',$values)) {
            $document->setData($values['data']);
        }

        if (array_key_exists('length',$values)) {
            $document->setLength($values['length']);
        }

        if (array_key_exists('width',$values)) {
            $document->setWidth($values['width']);
        }

        if (array_key_exists('height',$values)) {
            $document->setHeight($values['height']);
        }

        $em = $this->getEntityManager();
        $em->persist($document);
        $em->flush();

        return $document;
    }

    public function importDocument($documentId, $projectId) {
        $em = $this->getEntityManager();
        /** @var Project $project */
        $project = $em->getRepository('AppBundle:Project')->find($projectId);
        if ($project) {
            /** @var Document $document */
            $document = $this->find($documentId);
            if ($document) {
                $data = json_decode($document->getData(), true);

                $doctype = $document->getType();
                $prevDocId = 0;
                if ($doctype == "evalformat") {
                    //$project->setType($document->getFormat());
                    if ($project->getEvalformat() != $documentId) {
                        $prevDocId = $project->getEvalformat();
                    }

                    $project->setType($data["type"]);
                    $project->setRanges(json_encode($data["ranges"]));
                    $project->setRequiref($data["requiref"]);

                    $project->setEvalformat($documentId);
                } else if ($doctype == "guidelines") {
                    if ($project->getGuidelines() != $documentId) {
                        $prevDocId = $project->getGuidelines();
                    }

                    $project->setRandout($data["randout"]);
                    $project->setBlindrev($data["blindrev"]);
                    $project->setSentlist($data["sentlist"]);
                    $project->setIntragree($data["intragree"]);
                    $project->setMaxdontknow($data["maxdontknow"]);
                    $project->setPooling($data["pooling"]);
                    $project->setInstructions($data["instructions"]);

                    $project->setGuidelines($document->getId());
                }
                $em->persist($project);
                $em->flush();

                $document->setUsed(self::getProjectsCount($document->getId()));
                $em->persist($document);
                $em->flush();

                if ($prevDocId != 0) {
                    $document = $this->find($prevDocId);
                    if ($document) {
                        $document->setUsed(self::getProjectsCount($prevDocId));
                        $em->persist($document);
                        $em->flush();
                    }
                }
            }
        }
        return false;
    }

    public function getProjectsCount($documentId)
    {
        /** @var Document $document */
        $document = $this->find($documentId);

        if ($document) {
            $em = $this->getEntityManager();
            $projects = $em->getRepository('AppBundle:Project')
                ->findBy(array($document->getType() => $documentId));
            return count($projects);
        }

        return 0;
    }


    private function json_validate($string)
    {
        // decode the JSON data
        $result = json_decode($string);

        // switch and check possible JSON errors
        switch (json_last_error()) {
            case JSON_ERROR_NONE:
                $error = ''; // JSON is valid // No error has occurred
                break;
            case JSON_ERROR_DEPTH:
                $error = 'The maximum stack depth has been exceeded.';
                break;
            case JSON_ERROR_STATE_MISMATCH:
                $error = 'Invalid or malformed JSON.';
                break;
            case JSON_ERROR_CTRL_CHAR:
                $error = 'Control character error, possibly incorrectly encoded.';
                break;
            case JSON_ERROR_SYNTAX:
                $error = 'Syntax error, malformed JSON.';
                break;
            // PHP >= 5.3.3
            case JSON_ERROR_UTF8:
                $error = 'Malformed UTF-8 characters, possibly incorrectly encoded.';
                break;
            // PHP >= 5.5.0
            case JSON_ERROR_RECURSION:
                $error = 'One or more recursive references in the value to be encoded.';
                break;
            // PHP >= 5.5.0
            case JSON_ERROR_INF_OR_NAN:
                $error = 'One or more NAN or INF values in the value to be encoded.';
                break;
            case JSON_ERROR_UNSUPPORTED_TYPE:
                $error = 'A value of a type that cannot be encoded was given.';
                break;
            default:
                $error = 'Unknown JSON error occured.';
                break;
        }

        return $error;
    }


}
