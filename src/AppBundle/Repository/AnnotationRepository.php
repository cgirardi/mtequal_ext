<?php
/**
 * Project: mtequal_ext
 * User: cgirardi
 * Date: 02/02/16
 */

namespace AppBundle\Repository;

use AppBundle\Entity\Annotation;
use AppBundle\Entity\Project;
use AppBundle\Entity\Sentence;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Symfony\Component\HttpFoundation\Response;

/**
 * Annotation repository class
 */
class AnnotationRepository extends EntityRepository
{
  # get info about a source sentence: text, reference, ..
  function getTargetAnnotations($num)
  {
    return $this->findBy(array("num" => $num));
  }


  # get info about a source sentence: text, reference, ..
  function getUserAnnotations($id, $projectId, $user)
  {
    return $this->findBy(array("output_id" => $id, "project" => $projectId, "user" => $user, "revuser" => 0));
  }

  /*
   * return void
   */
  function dontKnow($sentenceId, $projectId, $user)
  {
    $em = $this->getEntityManager();
    /** @var Project $project */
    $project = $em->getRepository('AppBundle:Project')->findOneBy(array("id" => $projectId));

    /** @var SentenceRepository $repository_sentence */
    $repository_sentence = $em->getRepository('AppBundle:Sentence');
    $sentences = $repository_sentence->getOutput($sentenceId, $projectId);
    /** @var Sentence $sentence */
    foreach ($sentences as $sentence) {
      /** @var Annotation $annotation */
      $annotation = $this->findOneBy(array("num" => $sentence["num"], "user" => $user));

      if ($annotation == null) {
        $annotation = new Annotation();
        $annotation->setOutputId($sentenceId);
        $annotation->setNum(intval($sentence["num"]));
        $annotation->setUser($user);
        $annotation->setProject($project);
        $annotation->setEval(Annotation::DONTKNOW_VALUE);
        $em->persist($annotation);
      } else {
        if ($annotation->getEval() == Annotation::DONTKNOW_VALUE) {
          $em->remove($annotation);
        } else {
          $annotation->setEval(Annotation::DONTKNOW_VALUE);
          $em->persist($annotation);
        }
      }
      $em->flush();
    }
  }

  /**
 * confirm evaluation
 * @return boolean
 */
  public function confirmEvaluation($sentenceId, $projectId, $user)
  {
    $em = $this->getEntityManager();
    $annotations = $this->findBy(array("output_id" => $sentenceId, "project" => $projectId, "user" => $user));

    /** @var Annotation $annotation */
    foreach ($annotations as $annotation) {
      $annotation->setConfirmed(new \DateTime("now"));
      $em->persist($annotation);
    }
    $em->flush();

    //update Agreement
    /** @var  AgreementRepository $repository_agreement */
    $repository_agreement = $em->getRepository('AppBundle:Agreement');
    $repository_agreement->updateAgreement($sentenceId, $projectId);

    return true;
  }

  /**
   * unconfirm the evaluation done by a user
   * @return integer
   */
  public function unconfirmUserEvaluations($projectId, $user)
  {
    $unsetConfirmed = 0;

    $em = $this->getEntityManager();
    //update Agreement
    /** @var  AgreementRepository $repository_agreement */
    $repository_agreement = $em->getRepository('AppBundle:Agreement');

    $annotations = $this->findBy(array("project" => $projectId, "user" => $user));

    /** @var Annotation $annotation */
    foreach ($annotations as $annotation) {
      if ($annotation->getConfirmed() != null) {
        $annotation->setConfirmed(null);
        $em->persist($annotation);
        $em->flush();

        $repository_agreement->updateAgreement($annotation->getOutputId(), $projectId);
        $unsetConfirmed++;
      }
    }


    return $unsetConfirmed;
  }


  /**
   * remove a progress
   * @return boolean
   */
  public function unconfirmEvaluation($sentenceId, $projectId, $user)
  {
    $em = $this->getEntityManager();
    $annotations = $this->findBy(array("output_id" => $sentenceId, "project" => $projectId, "user" => $user));

    /** @var Annotation $annotation */
    foreach ($annotations as $annotation) {
      $annotation->setConfirmed(null);
      $em->persist($annotation);
      $em->flush();
    }

    //update Agreement
    /** @var  AgreementRepository $repository_agreement */
    $repository_agreement = $em->getRepository('AppBundle:Agreement');
    $repository_agreement->updateAgreement($sentenceId, $projectId);

    return true;
  }

  public function countRevAnnotation($projectId)
  {
    $em = $this->getEntityManager();
    if ($projectId) {
      $dql = "SELECT COUNT(a.revtime)
          FROM AppBundle:Annotation AS a
          WHERE a.project=:project AND a.revtime IS NOT NULL AND a.active=1";

      /** @var Query $query */
      $query = $em->createQuery($dql);
      $query->setParameter('project', $projectId);

      return $query->getSingleScalarResult();
    }
    return 0;
  }

    /**
   * get the number of dont know evaluation
   * @param $projectId
   * @param $user
   * @return int|mixed
   */
  public function countDontKnow($projectId, $user)
  {
    $em = $this->getEntityManager();
    if ($projectId) {

      $dql = "SELECT COUNT(DISTINCT a.output_id)
          FROM AppBundle:Annotation AS a
          WHERE a.eval=0 AND a.user=:user AND a.project=:project AND a.confirmed IS NOT NULL AND a.active=1";

      /** @var Query $query */
      $query = $em->createQuery($dql);
      $query->setParameter('user', $user);
      $query->setParameter('project', $projectId);

      return $query->getSingleScalarResult();
    }
    return 0;
  }

  /**
   * get the number of completed evaluation
   * @return integer
   */
  public function countDone($projectId, $user)
  {
    $em = $this->getEntityManager();
    if ($projectId) {
      $dql = "SELECT COUNT(DISTINCT a.output_id)
          FROM AppBundle:Annotation AS a
          WHERE a.user=:user AND a.project=:project AND a.confirmed IS NOT NULL AND a.revuser=0";

      /** @var Query $query */
      $query = $em->createQuery($dql);
      $query->setParameter('user', $user);
      $query->setParameter('project', $projectId);

      return $query->getSingleScalarResult();
    }
    return 0;
  }

  /**
   * get the number of evaluations done in the intra annotator sentences
   * @return integer
   */
  public function countIntraEvaluation($projectId)
  {
    if ($projectId) {
      $queryparam ['project'] = $projectId;

      $query = "SELECT COUNT(a.num)
          FROM AppBundle:Sentence AS s
          INNER JOIN AppBundle:Annotation AS a
          WHERE s.num=a.num AND s.project=:project AND s.isintra>1";

      /** @var Query $query */
      $dql = $this->getEntityManager()->createQuery($query)->setParameters($queryparam);

      return $dql->getSingleScalarResult();
    }
    return 0;
  }

  /**
   * get the number of evaluations done by a user or for entire project
   * @return integer
   */
  public function countEvaluation($projectId, $user = null)
  {
    if ($projectId) {
      $queryparam ['project'] = $projectId;

      $query = "SELECT COUNT(a.num)
          FROM AppBundle:Annotation AS a
          WHERE a.project=:project";

      if ($user != null) {
        $query .= " AND (a.user=:user OR a.revuser=:user)";
        $queryparam ['user'] = $user;
      }
      /** @var Query $query */
      $dql = $this->getEntityManager()->createQuery($query)->setParameters($queryparam);

      return $dql->getSingleScalarResult();
    }
    return 0;
  }

  /**
   * get the number of completed evaluation
   * @return array
   */
  public function listOfDone($projectId)
  {
    $em = $this->getEntityManager();

    if ($projectId) {
      $dql = "SELECT a.output_id AS id, COUNT(a.eval) AS evals, COUNT(a.confirmed) AS confirmed
            FROM AppBundle:Annotation AS a
            WHERE a.project=:project AND a.revuser=0
            GROUP BY a.output_id";

      /** @var Query $query */
      $query = $em->createQuery($dql);
      $query->setParameter('project', $projectId);
      //$query->setParameter('user', $user);

      return $query->getResult();
    }
    return array();
  }

  /**
   * check if a TU has been already completed by a user
   * @return boolean
   */
  public function isDone($sentenceId, $projectId, $user)
  {
    /** @var Annotation $annotation */
    $annotation = $this->findOneBy(
      array("output_id" => $sentenceId, "project" => $projectId, "user" => $user, "revuser" => 0));
    if ($annotation && $annotation->getConfirmed() != null) {
      return true;
    }
    return false;
  }

  /**
   * delete all annotations in a project
   */
  public function deleteAllEvaluations($projectId)
  {

    $em = $this->getEntityManager();
    /*$dql = "SELECT ag.num
            FROM AppBundle:Agreement AS ag
            INNER JOIN AppBundle:Sentence AS s
            WHERE s.project=:project AND s.num=ag.num";
*/
    /** @var AgreementRepository $repository_agreement */
    $repository_agreement = $em->getRepository('AppBundle:Agreement');

    $qb = $repository_agreement->createQueryBuilder("agreement");
    $query = $qb->join('AppBundle:Sentence','s')
    ->where('s.project=:project')
    ->andWhere('s.num=agreement.num')
    ->setParameter('project', $projectId)
    ->getQuery();
    $agreements = $query->getResult();
    foreach ($agreements as $agreement) {
      $em->remove($agreement);
    }


    $annotations = $this->findBy(array("project" => $projectId));
    if (count($annotations) > 0) {
      /** @var Annotation $annotation */
      foreach ($annotations as $annotation) {
        $em->remove($annotation);
      }
      $em->flush();
    }
  }

  /**
   * delete evaluations in a project for a specific user
   */
  public function deleteEvaluations($projectId, $user, $asEvaluatorOnly = true)
  {
    $em = $this->getEntityManager();
    $params = array("project" => $projectId, "user" => $user);
    if ($asEvaluatorOnly) {
      $params["revuser"] = 0;
    }
    $annotations = $this->findBy($params);
    if (count($annotations) > 0) {
      /** @var AgreementRepository $repository_agreement */
      $repository_agreement = $em->getRepository('AppBundle:Agreement');
      $listOutput = array();
      /** @var Annotation $annotation */
      foreach ($annotations as $annotation) {
        if (!in_array($annotation->getOutputId(), $listOutput)) {
          array_push($listOutput, $annotation->getOutputId());
        }
        $em->remove($annotation);
      }
      $em->flush();

      foreach ($listOutput as $outputId) {
        //run the agreement update
        $repository_agreement->updateAgreement($outputId, $projectId);
      }
    }
  }

  /**
   * delete all reviews in a project for a specific user
   */
  public function deleteRevEvaluations($projectId, $user)
  {
    $em = $this->getEntityManager();
    $revAnnotations = $this->findBy(array("project" => $projectId, "revuser" => $user));
    if (count($revAnnotations) > 0) {
      /** @var AgreementRepository $repository_agreement */
      $repository_agreement = $em->getRepository('AppBundle:Agreement');
      $listOutput = array();
      /** @var Annotation $annotation */
      foreach ($revAnnotations as $annotation) {
        /** @var Annotation $evaluator_annotation */
        $evaluator_annotation = $this->findOneBy(array("num" => $annotation->getNum(), "user" => $annotation->getUser(), "revuser" => 0));
        if ($evaluator_annotation) {
          $evaluator_annotation->setActive(true);
          $em->persist($evaluator_annotation);
        }

        $em->remove($annotation);

        if (!in_array($annotation->getOutputId(), $listOutput)) {
          array_push($listOutput, $annotation->getOutputId());
        }
      }
      $em->flush();

      foreach ($listOutput as $outputId) {
        //run the agreement update
        $repository_agreement->updateAgreement($outputId, $projectId);
      }
    }
  }


  /**
   * remove all annotations done by reviewers for an output
   */
  public function resetReview($num)
  {
    $em = $this->getEntityManager();
    $reviews = $this->findBy(array("num" => $num, "active" => 1));

    $outputId = null;
    $projectId = null;
    if (count($reviews) > 0) {
      /** @var Annotation $rev */
      foreach ($reviews as $rev) {
        if ($rev->getRevuser() != null) {
          $outputId = $rev->getOutputId();
          $projectId = $rev->getProject()->getId();

          //restore the user annotations
          $dql = "UPDATE AppBundle:Annotation as a
          SET a.active=1
          WHERE a.num=:num AND a.active=0 AND a.user=:userid";

          /** @var Query $query */
          $query = $em->createQuery($dql);
          $query->setParameter('num', $num);
          $query->setParameter('userid', $rev->getUser()->getId());

          //remove the revised annotation before to set active=1 to user one
          $em->remove($rev);
          $em->flush();

          $query->execute();
          $em->flush();
        }
      }
      //run the agreement update
      if ($outputId != null && $projectId != null) {
        $em->getRepository('AppBundle:Agreement')->updateAgreement($outputId, $projectId);
      }
      return true;
    }
    return false;
  }

  /**
   * get sentence+annotation fields
   * @return array
   */
  public function sentenceAnnotation($projectId)
  {
    if ($projectId) {
      $dql = "SELECT (s.id) AS id, s.language, s.type, s.text, a.eval, (a.user) AS user, (a.revuser) AS revuser
          FROM AppBundle:Sentence AS s
          INNER JOIN AppBundle:Annotation AS a
          WHERE s.num=a.num AND s.project=:project AND s.isintra<=1 AND a.active=1 AND a.confirmed IS NOT NULL
          ORDER BY s.id, s.type, a.user";

      /** @var Query $query */
      $query = $this->getEntityManager()->createQuery($dql);
      $query->setParameter('project', $projectId);

      return $query->getArrayResult();
    }
    return array();
  }

  public function getAnnotationProgress ($projectId) {
    if ($projectId) {
      //evaluators
      $dql = "SELECT IDENTITY(a.user) AS user, COUNT(a.user) AS num
          FROM AppBundle:Annotation AS a
          WHERE a.project=:project AND a.revuser=0
          GROUP BY a.user";

      /** @var Query $query */
      $query = $this->getEntityManager()->createQuery($dql);
      $query->setParameter('project', $projectId);

      $evaluators = $query->getArrayResult();

      //reviewers
      $dql = "SELECT a.revuser AS user, COUNT(a.revuser) AS num
          FROM AppBundle:Annotation AS a
          WHERE a.project=:project AND a.revuser > 0
          GROUP BY a.revuser";

      $query2 = $this->getEntityManager()->createQuery($dql);
      $query2->setParameter('project', $projectId);

      $reviewers = $query2->getArrayResult();

      return array_merge($evaluators, $reviewers);
    }
    return array();
  }

  /**
   * Get an array with stats about all done evaluations
   * @param Project $project
   * @param array $targetLabels
   * @param $countRanges
   * @param $reviewed
   * @return array
   */
  public function evaluationSummary(Project $project, $targetLabels, $ranges, $reviewed)
  {
    $report = array();
    foreach ($targetLabels as $targetName => $targetInfo) {
      $report[$targetInfo["type"]]["eval"] = array();
      for ($n = 0; $n <= count($ranges); $n++) {
        if ($project->getType() == "tree") {
          $report[$targetInfo["type"]]["eval"][$n] = array();
        } else {
          $report[$targetInfo["type"]]["eval"][$n] = 0;
        }
      }
      $report[$targetInfo["type"]]["sentences"] = intval($targetInfo["sentences"]);
    }

    $evalData = $this->evaluationResults($project->getId(), $reviewed);
    $evalCount = array();
    foreach ($evalData as $evalItem) {
      if (array_key_exists($evalItem["type"], $evalCount)) {
        $evalCount[$evalItem["type"]] = $evalCount[$evalItem["type"]] + $evalItem["num"];
      } else {
        $evalCount[$evalItem["type"]] = $evalItem["num"];
      }
      if ($project->getType() == "tree") {
        if ($evalItem["eval"] == 0) {
          $report[$evalItem["type"]]["eval"][$evalItem["eval"]][0] = intval($evalItem["num"]);
        } else {
          for( $i = 0; $i < strlen( $evalItem["eval"] ); $i++ ) {
            $ev = substr( $evalItem["eval"], $i, 1 );
            $report[$evalItem["type"]]["eval"][$i+1][$ev] += intval($evalItem["num"]);
          }
        }
      } else {
        $report[$evalItem["type"]]["eval"][$evalItem["eval"]] = intval($evalItem["num"]);
      }
    }

    //save count of the evaluations
    $averageList = array();
    foreach ($targetLabels as $targetName => $targetInfo) {
      $sum = 0;
      foreach ($report[$targetInfo["type"]]["eval"] as $pos => $num) {
        $sum += intval($pos) * intval($num);
      }
      $report[$targetInfo["type"]]["evaluations"] = $evalCount[$targetInfo["type"]];
      $report[$targetInfo["type"]]["average"] = round((intval($sum) / intval($evalCount[$targetInfo["type"]])), 2);
      $averageList[$targetInfo["type"]] = $report[$targetInfo["type"]]["average"];
    }

    //order the average
    asort($averageList);
    $rank = 1;
    foreach ($averageList as $targetName => $average) {
      if ($project->getType() == "ranking") {
        $report[$targetName]["rank"] = $rank++;
      } else {
        $floorAverage = round($report[$targetName]["average"], 0);
        $report[$targetName]["rank"] = $floorAverage;
        if ($floorAverage == 0) {
          $report[$targetName]["rank"] = "<span class=\"shadow\">don't know</span>";
        } else {
          foreach ($ranges as $range) {
            if ($range["val"] == $floorAverage) {
              $report[$targetName]["rank"] = "<span class=\"shadow\" style=\"background-color: #" . $range["color"] . "\"><b>" . $range["label"] . "</b></span>";
              break;
            }
          }
        }
      }
    }
    return $report;
  }

  /**
   * @param $projectId
   * @param $reviewed
   * @return array
   */
  public function evaluationResults($projectId, $reviewed)
  {
    if ($projectId) {
      if ($reviewed) {
        $revParam = "AND a.active=1";
      } else {
        $revParam = "AND a.revuser=0";
      }
      $dql = "SELECT s.type, a.eval, COUNT(a.eval) as num
          FROM AppBundle:Sentence AS s
          INNER JOIN AppBundle:Annotation AS a
          WHERE s.num=a.num
            AND s.project=:project AND s.type!='reference' AND s.type!='source'
            AND s.isintra<=1 AND a.confirmed IS NOT NULL " . $revParam . "
          GROUP BY s.type, a.eval
          ORDER BY s.type, a.eval";

      /** @var Query $query */
      $query = $this->getEntityManager()->createQuery($dql);
      $query->setParameter('project', $projectId);

      return $query->getResult();
    }
    return array();
  }

  public function reviewResults($projectId)
  {
    if ($projectId) {
      $dql = "SELECT IDENTITY(a.user) AS userid, count(a.active) AS numrev
              FROM AppBundle:Annotation AS a
              WHERE a.project=:project AND a.revuser>0 AND a.revuser IS NOT NULL AND a.active=1
              GROUP BY userid";

      /** @var Query $query */
      $query = $this->getEntityManager()->createQuery($dql);
      $query->setParameter('project', $projectId);

      return $query->execute();
    }
    return array();
  }

  public function getEvalReportParams ($projectId, $userId) {
    $report = array();
    $em = $this->getEntityManager();
    /** @var Project $project */
    $project = $em->getRepository('AppBundle:Project')->findOneBy(array("id" => $projectId));
    if ($project) {
      $report["intratus"] = $project->getIntratus();
      //$report["evaluators"] = $em->getRepository("AppBundle:Role")->getUserByRole($projectId, "evaluator");
      $report["evaluators"] = $em->getRepository("AppBundle:Role")->getUsers($projectId, $userId, "evaluator");

      $report["ranges"] = json_decode($project->getRanges(), TRUE);

      /** @var SentenceRepository $sentence_repository */
      $sentence_repository = $em->getRepository('AppBundle:Sentence');
      $targetLabels = $sentence_repository->targetLabels($projectId);
      //number of TUs must be evaluated
      $report["numtus"] = $sentence_repository->countTUs($projectId);
      $report["evalsummary"] = $this->evaluationSummary($project, $targetLabels, $report["ranges"], false);
      
      $kappa = $this->calculateKappaQuality($projectId);
      if (is_numeric($kappa)) {
      	$report["kQuality"] = round($kappa, 3);
      } else {
      	$report["kQuality"] = $kappa;
      }
      $report["kIntraEvaluators"] = $this->calculateKappaIntra($project, $userId);
      if ($this->countRevAnnotation($projectId) > 0) {
        $report["evalsummaryrev"] = $this->evaluationSummary($project, $targetLabels, $report["ranges"], true);
        $report["kQualityRev"] = round($this->calculateKappaQuality($projectId, false), 3);

        //reviewer evaluation divided by user + all
        $report["evaluationsrev"] = $this->reviewResults($projectId);
        $report["totevaluationsrev"] = 0;
        foreach ($report["evaluationsrev"] as $revInfo) {
          if ($report["evaluators"][$revInfo["userid"]]["evaluator"]) {
            $report["totevaluationsrev"] += $revInfo["numrev"];
          }
        }
      }
    }
    return $report;
  }

  /**
   * return the normalized sum of observed agreement
   */
  private function getObservedAgreement ($projectId, $beforeRev = true) {
    $queryparam ['projectid'] = $projectId;
    $agreeField = "agreement";
    if ($beforeRev) {
      $agreeField = "agreementbeforerev";
    }
    $query = "SELECT SUM(ag.".$agreeField.") AS Pi, COUNT(ag.".$agreeField.") as N
        FROM AppBundle:Agreement AS ag
        INNER JOIN AppBundle:Annotation AS an
        WHERE ag.num=an.num AND an.confirmed IS NOT NULL";

    if ($beforeRev) {
      $query.= " AND an.revuser=0";
    } else {
      $query.= " AND an.active=1";
    }
    $query .= " AND an.num IN (SELECT s.num FROM AppBundle:Sentence AS s WHERE s.project=:projectid AND s.isintra<=1)";

    $dql = $this->getEntityManager()->createQuery($query)->setParameters($queryparam);
    $result = $dql->getSingleResult();

    if ($result["Pi"])
      return $result["Pi"] / $result["N"];
    return 0;
  }

  /**
   * return the expected agreement
   */
  private function getExpectedAgreement ($projectId, $beforeRev=true) {
    $Pe = 0;
    $em = $this->getEntityManager();
    /** @var Project $project */
    $project = $em->getRepository('AppBundle:Project')->findOneBy(array("id" => $projectId));

    $numOfLabels = 0;
    if ($project) {
      $ranges = $project->getRanges();
      if (strlen(trim($ranges)) > 1) {
        $numOfLabels = count(json_decode($ranges));
      }

      $data = $this->getEvalsDistribution($projectId, $beforeRev);
      if ($project->getType() == "tree") {
        for ($i = 0; $i < $numOfLabels; $i++) {
          $annotationCount = array();
          $sumCellsQx = 0;
          foreach ($data as $value) {
            if ($value["eval"] == 0) {
              $annotationCount[0] += $value["cnt"];
            } else {
              $ev = substr($value["eval"], $i, 1);
              $annotationCount[$ev] += $value["cnt"];
            }
            $sumCellsQx += $value["cnt"];
          }
          $pi = 0;
          //$logpi="";
          foreach ($annotationCount as $ev => $cnt) {
            $pi += pow((intval($cnt) / $sumCellsQx), 2);
            //$logpi.="pi[$ev] = pow(($cnt / $sumCellsQx), 2) = ".pow((intval($cnt) / $sumCellsQx), 2) ."\n";
          }
          $Pe += $pi;
        }
        $Pe = $Pe / $numOfLabels;

      } else {
        /* ranking, scale, binary */
        $sumCellsQx = 0;
        foreach ($data as $value) {
          $sumCellsQx += $value["cnt"];
        }
        foreach ($data as $value) {
          $Pe += pow((intval($value["cnt"]) / $sumCellsQx), 2);
        }
      }
    }

    return $Pe;
  }

  /**
   * return the distribution of evaluations in the project as hash
   */
  private function getEvalsDistribution ($projectId, $beforeRev=true)
  {
    $queryparam ['project'] = $projectId;
    $query = "SELECT a.eval AS eval, COUNT(a.eval) AS cnt
        FROM AppBundle:Sentence AS s
        INNER JOIN AppBundle:Annotation AS a
        WHERE s.num=a.num AND s.project=:project AND s.simto IS NULL AND s.isintra<=1";

    if ($beforeRev) {
      $query.= " AND a.revuser=0";
    } else {
      $query.= " AND a.active=1";
    }
    $query .= " GROUP BY a.eval ORDER BY a.eval";

    $dql = $this->getEntityManager()->createQuery($query)->setParameters($queryparam);
    return $dql->getArrayResult();
  }

  //Claudio: changed to public
  
  /**
   * return the normalized sum of intra-annotator observed agreement
   */
  public function getObservedIntraAgreement ($projectId, $userId, $beforeRev=true) {
    $queryparam ['project'] = $projectId;
    $queryparam ['userid'] = $userId;
    $query = "SELECT s.id, s.type, a.eval, s.isintra
        FROM AppBundle:Sentence AS s
        INNER JOIN AppBundle:Annotation AS a
        WHERE s.num=a.num AND s.project=:project AND a.user=:userid AND a.confirmed IS NOT NULL AND s.isintra>=1";

    if ($beforeRev) {
      $query.= " AND a.revuser=0";
    } else {
      $query.= " AND a.active=1";
    }
    $query .= " ORDER BY s.num";

    $dql = $this->getEntityManager()->createQuery($query)->setParameters($queryparam);
    //return $dql->getResult();

    $N=0;
    $Pi=0;
    $evaluators = 2;
    $countEvals=array();
    $iterableResult = $dql->iterate();

    while (($rows = $iterableResult->next()) !== false) {
      foreach ($rows as $row) {
        $sentID = $row["id"];
        if ($row["isintra"] > 1) {
          $sentID = preg_replace('/^\{/','', $sentID);
          $sentID = preg_replace('/\}$/','', $sentID);
        }
        $sentID .= $row["type"];
        if (array_key_exists($sentID, $countEvals)) {
          $countEvals[$sentID][$row["eval"]] += 1;
        } else {
          $countEvals[$sentID][$row["eval"]] = 1;
        }
      }
    }

    foreach ($countEvals as $num => $evals) {
      $sumEvals = 0;
      $sumPowEvals = 0;
      foreach ($evals as $ev => $cnt) {
        $sumEvals += $cnt;
        $sumPowEvals += pow($cnt, 2);
      }

      if ($sumEvals == $evaluators) {
        $N++;
        $Pi += ($sumPowEvals - $evaluators) / ($evaluators * ($evaluators - 1));
      }
    }

    if ($N > 0)
      //normalized P
      return ($Pi/$N);
    return 0;
  }

  //Claudio: changed to public
  /**
   * return the expected intra-annotator agreement
   */
  public function getExpectedIntraAgreement ($projectId, $userid, $beforeRev=true) {
    $data = $this->getEvalsIntraDistribution($projectId, $userid, $beforeRev);
    $sumEvals = 0;
    $Pe = 0;
    foreach ($data as $value) {
      $sumEvals += $value["cnt"];
    }
    if ($sumEvals > 0) {
      foreach ($data as $value) {
        $Pe += pow(($value["cnt"] / $sumEvals), 2);
      }
    }
    return $Pe;
  }

  /**
   * return the distribution of intra-annotator evaluations in the project as hash
   */
  private function getEvalsIntraDistribution ($projectId, $userId, $beforeRev=true)
  {
    $queryparam ['project'] = $projectId;
    $queryparam ['userid'] = $userId;
    $query = "SELECT a.eval AS eval, COUNT(a.eval) AS cnt
        FROM AppBundle:Sentence AS s
        INNER JOIN AppBundle:Annotation AS a
        WHERE s.num=a.num AND s.project=:project AND a.user=:userid AND s.isintra>=1";

    if ($beforeRev) {
      $query.= " AND a.revuser=0";
    } else {
      $query.= " AND a.active=1";
    }
    $query .= " GROUP BY a.eval ORDER BY a.eval";

    $dql = $this->getEntityManager()->createQuery($query)->setParameters($queryparam);
    return $dql->getArrayResult();
  }

  public function calculateKappaQuality ($projectId, $beforeRev=true) {
    $queryparam ['project'] = $projectId;
    $em = $this->getEntityManager();

    /** @var ProgressRepository $repository_progress */
    $repository_progress = $em->getRepository("AppBundle:Progress");
    if (count($repository_progress->findBy(array("task" => $repository_progress::EVALUATION_TASK, "project" => $projectId, "completed" => 0))) == 0) {
    	//evaluators count
    	$queryparam['$evaluators'] = count($repository_progress->findBy(array("task" => $repository_progress::EVALUATION_TASK,
      		"project" => $projectId,
      		"completed" => 1)));

        if ($queryparam['$evaluators'] > 1) {
          $Pnorm = $this->getObservedAgreement($projectId, $beforeRev);
          $Pe = $this->getExpectedAgreement($projectId, $beforeRev);

          if ((1 - $Pe) != 0) {
            return (($Pnorm - $Pe) / (1 - $Pe));
          } else {
            return 1;
          }
        }
    }
    return "not available";
  }

  public function calculateKappaIntra ($projectId, $userId, $beforeRev=true) {
    $em = $this->getEntityManager();

    //get all evaluators
    //$evaluators = $em->getRepository("AppBundle:Role")->getUserByRole($projectId, "evaluator");
    $evaluators = $em->getRepository("AppBundle:Role")->getUsers($projectId, $userId, "evaluator");

    $kUsers = array();
    foreach ($evaluators as $uID => $uInfo) {
      $Pnorm = $this->getObservedIntraAgreement($projectId, $uID, $beforeRev);
      $Pe = $this->getExpectedIntraAgreement($projectId, $uID, $beforeRev);

      $kappa=1;
      if ((1 - $Pe) != 0) {
      	$kappa= round((($Pnorm - $Pe)/(1 - $Pe)), 3);
      }
      $kUsers{$uID} = $kappa;
    }

    return $kUsers;
  }

  public function getViewedEvaluations ($projectId, $userId) {
    $query = "SELECT v.num, v.modified
        FROM AppBundle:ViewedData AS v
        WHERE v.project=:project AND v.user=:user";
    /** @var Query $dql */
    $dql = $this->getEntityManager()->createQuery($query);
    $dql->setParameter("project", $projectId);
    $dql->setParameter("user", $userId);
    $data = $dql->getArrayResult();

    $hash = array();
    foreach ($data as $value) {
      $hash[$value["num"]] = $value["modified"];
    }
    return $hash;
  }

  public function isViewedEvaluation ($num, $userId) {
    $query = "SELECT v.modified
        FROM AppBundle:ViewedData AS v
        WHERE v.num=:num AND v.user=:user";
    /** @var Query $dql */
    $dql = $this->getEntityManager()->createQuery($query);
    $dql->setParameter("num", $num);
    $dql->setParameter("user", $userId);
    $result = $dql->getArrayResult();

    if (count($result)) {
      return true;
    }
    return false;
  }


  public function getEvaluations ($num, $active=true) {
    $query = "SELECT a.eval AS eval, COUNT(a.eval) AS cnt, SUM(a.revuser) as revuser
        FROM AppBundle:Annotation AS a
        WHERE a.num=:num AND a.active=:active
        GROUP BY a.eval";

    /** @var Query $dql */
    $dql = $this->getEntityManager()->createQuery($query);
    $dql->setParameter("num", $num);
    $dql->setParameter("active", $active);
    $data = $dql->getArrayResult();

    $hash = array();
    foreach ($data as $value) {
      $hash[$value["eval"]] = [$value["cnt"], $value["revuser"]];
    }
    return $hash;
  }


  public function exportEvaluation ($projectId, $projectType, $userId, $format)
  {
    $message = "An error occurred during export data!";
    $decisionOptions = array(
      -1 => "dont_know",
      0 => "--",
      1 => 'Yes',
      2 => 'No',
      3 => 'N/A');

    /** @var ProjectRepository $repository */
    $repository = $this->getEntityManager()->getRepository('AppBundle:Project');
    /** @var Project $project */
    $project = $repository->find($projectId);
    if ($project) {
      $evalRanges = json_decode($project->getRanges(), TRUE);
      $evalRanges[-1]["label"] = "dont_know";


      /** @var RoleRepository $repository_role */
      $repository_role = $this->getEntityManager()->getRepository("AppBundle:Role");
      $joinUsers = $repository_role->getRoles($projectId, $userId);
      $users = array();
      foreach ($joinUsers as $key => $jUser) {
        $users[$jUser["id"]] = $jUser["username"];
      }

      $anonymous=array();
      $listVal=array();

      $sources = $this->getEntityManager()->getRepository('AppBundle:Sentence')->getSentences($projectId, "source");
      $targets = $this->sentenceAnnotation($projectId);

      if (count($targets) > 0) {
        $headerFields = ["id", "source", "target-language", "target-name", "target", "label", "user"];
        if ($projectType == "tree") {
          array_splice($headerFields, count($headerFields) - 2, 1, ["ev1", "ev2", "ev3", "ev4", "ev5"]);
        }

        //inizialize the output file
        $fileout = "/tmp/mtequal_prj-" . $projectId . "_evdata-" . time() . "." .$format;
        $fp = fopen($fileout, 'w');
        if ($format == "csv") {
          fputcsv($fp, $headerFields, ',');
        } else if ($format == "json") {
          $listVal = array();
        }

        foreach ($targets as $key => $val) {
          //skip the evaluation from out of own users list
          /*if (!array_key_exists($val["user"], $users)) {
            continue;
          }*/
          $eval = $val["eval"];
          $iuser = $val["user"];
          if ($val["revuser"] > 0) {
            $iuser = $val["revuser"];
          }
          unset($val["revuser"]);

          $val = array_values($val);
          //replace user ID with username
          if (array_key_exists($iuser, $users)) {
            array_splice($val, count($val)-1, 1, $users[$iuser]);
          } else {
            if (!array_key_exists($iuser, $anonymous)) {
              $anonymous[$iuser] = count($anonymous)+1;
            }
            $aUser = "anonymous" . $anonymous[$iuser];
            array_splice($val, count($val)-1, 1, $aUser);
          }

          //add source
          if (array_key_exists($val[0], $sources)) {
            array_splice($val, 1, 0, $sources[$val[0]]);
          } else {
            array_splice($val, 1, 0, "");
          }
          //for decision tree format replace the one value with multiple values (one for each question)
          if ($projectType == "tree") {
            $answers = array();
            if ($eval == 0) {
              for ($i = 0; $i < 5; $i++) {
                array_push($answers, $decisionOptions[-1]);
              }
            } else {
              for ($i = 0; $i < strlen($eval); $i++) {
                $ev = substr($eval, $i, 1);
                array_push($answers, $decisionOptions[$ev]);
              }
            }
            array_splice($val, count($val) - 2, 1, $answers);
          } else if ($projectType != "ranking") {
            array_splice($val, count($val) - 2, 1, $evalRanges[$eval-1]["label"]);
          }

          if ($format == "csv") {
            fputcsv($fp, $val, ',');
          } else if ($format == "json") {
            $listVal[] = array_combine($headerFields, $val);
          }
        }
        if ($format == "json") {
          $response['data'] = $listVal;
          fwrite($fp, json_encode($response, JSON_PRETTY_PRINT));
        }
        fclose($fp);

        if (file_exists($fileout)) {
          /** @var Response $response */
          $response = new Response();
          $response->headers->set('Pragma', 'public');
          $response->headers->set('Cache-Control', 'public, must-revalidate, post-check=0, pre-check=0');
          //$response->headers->set('Cache-Control: private',false); // required for certain browsers
          $response->headers->set('Expires', '-1');
          $response->headers->set('Content-Type', 'application/excel');
          $response->headers->set('Content-Transfert-Encoding', 'binary');
          $response->headers->set('Content-Disposition', 'attachment; filename=' . basename($fileout));
          $response->headers->set('Content-Length', filesize($fileout));
          $response->setContent(file_get_contents($fileout));
          unlink($fileout);

          return $response;
        }
      } else {
        $message = "Sorry! No data found.";
      }
    } else {
      $message = "Sorry! No data found.";
    }

    return new Response($message, 404);

    //return $this->redirectToRoute('admin', array('view' => 'settings.export', "message" => $message));
  }
}
?>