<?php
/**
 * Project: mtequal_ext
 * User: cgirardi
 * Date: 12/01/16
 */

namespace AppBundle\Repository;
use AppBundle\Entity\Corpus;
use AppBundle\Entity\Project;
use AppBundle\Entity\Sentence;
use AppBundle\Importing\FileValidator;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\File\UploadedFile;


/**
   * Corpus repository class
   */
class CorpusRepository extends EntityRepository
{
  /**
   * get corpus object from ID
   * @param $id
   * @return null|object
   */
  public function getCorpusFromID($id)
  {
    return $this->findOneBy(array("id" => $id));
  }

  /**
   * save corpus info
   * @param $filepath
   * @param $filename
   * @param $projectId
   * @param $sentences
   * @param $errors
   * @return Corpus
   */
  public function newCorpus($filepath, $filename, $projectId, $sentences, $errors) {
    $em = $this->getEntityManager();
    $corpus = new Corpus();

    $corpus->setFilename($filename);
    $corpus->setFilepath($filepath);
    $corpus->setDate(new \DateTime("now"));
    $corpus->setProject($projectId);
    $corpus->setSentences($sentences);
    $corpus->setErrors($errors);

    $em->persist($corpus);

    //update the modified time of the project
    $em->getRepository('AppBundle:Project')->updateModifiedTime($projectId);

    $em->flush();

    return $corpus;
  }

  /**
   * Check the imported file format
   * @param $upfile
   * @param $projectId
   * @return FileValidator|null
   */
  public function validateFile ($upfile, $projectId)
  {
    $em = $this->getEntityManager();

    /** @var Project $project */
    $project = $em->getRepository('AppBundle:Project')->findOneBy(array("id" => $projectId));
    if ($project) {
      $fileValidator = new FileValidator($upfile, $project, true);

      //check if id is already present in the database
      if (array_key_exists("id", $fileValidator->header_fields)) {
        /** @var SentenceRepository $repository_sentence */
        $repository_sentence = $em->getRepository('AppBundle:Sentence');
        $outputIDs = $repository_sentence->getIds($projectId);
        foreach ($outputIDs as $item) {
          if (in_array($item["id"], $fileValidator->listIDs)) {
            array_push($fileValidator->message, "ERROR: ID " . $item["id"] . " is already present in the database.");
            $fileValidator->isWrong = true;
          }
        }
      }
      return $fileValidator;
    }
    return null;
  }


  // parse file and store each line as a row of the DB table
  public function addFileData (Corpus $corpus)
  {
    ini_set('max_execution_time', 0);
    if ($corpus) {
      $filepath = $corpus->getFilepath();
      /** @var Project $project */
      $project = $this->getEntityManager()->getRepository('AppBundle:Project')->findOneBy(array("id" => $corpus->getProject()));
      $savedSentences = 0;
      if ($project && file_exists($filepath) && $corpus->getSentences() <= 0) {
        $header_found = false;
        $uploadFile = new UploadedFile($filepath, $corpus->getFilename());
        $fileValidator = new FileValidator($uploadFile, $project);

        if (!$fileValidator->isWrong) {
          $hash_header = $fileValidator->header_fields;
          if ($fileValidator->char_separator != null && count($hash_header) > 0) {
            $handle = fopen($filepath, "r");
            if ($handle) {
              while (($line = fgets($handle)) !== false) {
                if (!empty(trim($line)) && !$header_found) {
                  //skip header
                  $header_found = true;
                  continue;
                }
                if ($header_found) {
                  try {
                    $items = str_getcsv(self::htmlentities2utf8($line), $fileValidator->char_separator);
                    $sent_id = trim($items[$hash_header["id"]]);
                    //generate a new ID
                    if (empty($sent_id)) {
                      $sent_id = time() . "-" . rand(1, 9999);
                    }
                    //store source sentence
                    $source_language = "";
                    if (array_key_exists("source-language", $hash_header)) {
                      $source_language = $items[$hash_header["source-language"]];
                    }
                    $comment = "";
                    if (array_key_exists("comment", $hash_header)) {
                      $comment = $items[$hash_header["comment"]];
                    }

                    $source_num = self::saveSentence($sent_id, 0, $source_language, $items[$hash_header["source"]], $corpus, "source", $comment, false);
                    if ($source_num > 0) {
                      $savedSentences++;
                      //store the references
                      if (array_key_exists("reference", $hash_header) || array_key_exists("reference-url", $hash_header)) {
                        $reference_language = "";
                        if (array_key_exists("reference-language", $hash_header)) {
                          $reference_language = $items[$hash_header["reference-language"]];
                        }
                        //add a reference as a link
                        if (array_key_exists("reference-url", $hash_header)) {
                          $referenceUrl = trim($items[$hash_header["reference-url"]]);
                          if (!empty($referenceUrl)) {
                            self::saveSentence($sent_id, $source_num, $reference_language, $referenceUrl, $corpus, "reference", "", true);
                          }
                        }
                        //add a text reference
                        if (array_key_exists("reference", $hash_header)) {
                          $reference = trim($items[$hash_header["reference"]]);
                          if (!empty($reference)) {
                            self::saveSentence($sent_id, $source_num, $reference_language, $reference, $corpus, "reference", "", false);
                          }
                        }
                      }
                      //store the target sentence/s
                      foreach ($hash_header["target"] as $target_name => $target_fields) {
                        $target_language = "";
                        if (array_key_exists("target-language", $hash_header)) {
                          $target_language = $items[$hash_header ["target-language"]];
                        }
                        $target_comment = "";
                        if (array_key_exists("comment", $target_fields)) {
                          $target_comment = trim($items[$target_fields["comment"]]);
                        }
                        self::saveSentence($sent_id, $source_num, $target_language, $items[$target_fields["target"]], $corpus, $target_name, $target_comment, false);
                      }
                    }
                  } catch (Exception $e) {
                    fclose($handle);
                  }
                }
              }
              fclose($handle);
            }
          } else {
            return null;
          }
        }
      }
      // update count of the stored sentences
      $em = $this->getEntityManager();
      $corpus = self::getCorpusFromID($corpus->getId());
      $corpus->setSentences($savedSentences);
      $em->persist($corpus);
      $em->flush();

      return $savedSentences;
    }
    return null;
  }

  private function saveSentence($id, $linked, $lang, $text, Corpus $corpus, $type, $comment, $isUrl)
  {
    /** @var Sentence $sentence */
    $sentence = new Sentence();

    $sentence->setId($id);
    $sentence->setLanguage($lang);
    $sentence->setText($text);
    $sentence->setCorpus($corpus->getId());
    $sentence->setProject($corpus->getProject());
    $sentence->setType($type);
    if ($linked != null) {
      $sentence->setLinked($linked);
    } else {
      $sentence->setLinked(0);
    }
    $sentence->setTokenization(0);
    $sentence->setComment($comment);
    if ($isUrl) {
      $sentence->setIsurl(1);
    } else {
      $sentence->setIsurl(0);
    }
    $time = new \DateTime("now");
    $sentence->setCreated($time);
    $sentence->setUpdated($time);

    $em = $this->getEntityManager();
    $em->persist($sentence);
    $em->flush();

    if ($type == "source") {
      $em->clear();
      return $sentence->getNum();
    }

    return null;
  }


  /**
   * get corpus stats
   * @param $id
   * @return array
   */
  public function statsCorpus($id)
  {
    $dql = "SELECT COUNT(c.id) AS files, SUM(c.sentences) AS sentences
        FROM AppBundle:Corpus AS c
        WHERE c.project=:id AND c.sentences>0";
    $em = $this->getEntityManager();
    $query = $em->createQuery($dql);
    $query->setParameter('id', $id);

    return $query->getResult();
  }

  /**
   * check if the corpus already exists
   * @param $filename
   * @param $projectid
   * @return null|object
   */
  public function findCorpus($filename, $projectid)
  {
    return $this->findOneBy(array("filename" => $filename, "project" => $projectid));
  }

  /**
   * @param $projectid
   * @return array
   */
  public function getAnnotatedFiles($projectid)
  {
  $dql = "SELECT s.corpus, COUNT(s.corpus) AS num FROM AppBundle:Annotation AS a
INNER JOIN AppBundle:Sentence AS s WHERE a.num=s.num AND s.project=:id GROUP BY s.corpus";
    $em = $this->getEntityManager();
    $query = $em->createQuery($dql);
    $query->setParameter('id', $projectid);

    return $query->getResult();
  }


  /**
   * Delete a corpus
   * @param $id
   * @return bool
   */
  public function deleteCorpus($id)
  {
    $em = $this->getEntityManager();
    //remove corpus file information
    /** @var Corpus $corpus */
    $corpus = self::getCorpusFromID($id);
    if ($corpus != null) {
      //remove sentences
      $sentences = $em->getRepository('AppBundle:Sentence')->findBy(array("corpus" => $corpus));
      foreach ($sentences as $sentence) {
        $em->remove($sentence);
      }
      // remove the original uploaded file in the uploads dir
      if (file_exists($corpus->getFilepath())) {
        unlink($corpus->getFilepath());
      }

      //update the modified time of the project
      $em->getRepository('AppBundle:Project')->updateModifiedTime($corpus->getProject());

      $em->remove($corpus);
      $em->flush();

      return true;
    }
    return false;
  }

  /**
   * Delete all corpus
   * @param $projectId
   * @return bool
   */
  public function deleteAllCorpus($projectId) {
    $corpora = $this->findBy(array("project" => $projectId));
    foreach ($corpora as $corpus) {
      if (!$this->deleteCorpus($corpus->getId())) {
        return false;
      }
    }
    return true;
  }

  private function htmlentities2utf8($string)
  {
    return str_replace("\xc2\xa0", ' ', $string);
    #return utf8_decode(mb_convert_encoding(str_replace("\xc2\xa0",' ',$string), 'UTF-8', 'HTML-ENTITIES'));
  }
}
?>