<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Project;
use AppBundle\Entity\Sentence;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

/**
 * Sentence repository class
 */
class SentenceRepository extends EntityRepository
{
  /*
   * get info about a TU: source, references and a specific output
   */
  function getTUOneSentence ($num, $outputid, $projectid)
  {
    $dql = "SELECT s.num, s.type, s.language, s.text, s.tokenization, s.comment, s.isurl
        FROM AppBundle:Sentence AS s
        WHERE s.id=:id AND s.project=:projectid AND (s.type='source' OR s.type='reference')
        ORDER BY s.num";
    //get system outputs
    $dqlSys = "SELECT s.num, s.type, s.language, s.text, s.tokenization, s.comment, s.isurl
        FROM AppBundle:Sentence AS s
        WHERE s.num=:num";
    /** @var Query $query */
    $query = $this->getEntityManager()->createQuery($dql);
    $query->setParameter('id', $outputid);
    $query->setParameter('projectid', $projectid);
    $sourceAndRefs = $query->getResult();

    $querySys = $this->getEntityManager()->createQuery($dqlSys);
    $querySys->setParameter('num', $num);
    $systemSys = $querySys->getResult();
    foreach ($systemSys as $key => $val) {
      array_push($sourceAndRefs, $val);
    }

    return $sourceAndRefs;
  }

  /*
   * get info about a TU: source, references and all output
   */
  function getTUSentences ($outputid, $projectid, $randout) {
    /*$dql = "SELECT s.num, s.type, s.language, s.text, s.tokenization, s.id
        FROM AppBundle:Sentence AS s
        WHERE (s.num=:num OR s.linked=:num) AND s.project=:projectid";*/
    //get source and references
    $dql = "SELECT s.num, s.type, s.language, s.text, s.tokenization, s.comment, s.isurl
        FROM AppBundle:Sentence AS s
        WHERE s.id=:id AND s.project=:projectid AND (s.type='source' OR s.type='reference')
        ORDER BY s.num";
    //get system outputs
    $dqlSys = "SELECT s.num, s.type, s.language, s.text, s.tokenization, s.comment, s.isurl
        FROM AppBundle:Sentence AS s
        WHERE s.id=:id AND s.project=:projectid AND s.type!='source' AND s.type!='reference'
        ORDER BY s.num";

    /** @var Query $query */
    $query = $this->getEntityManager()->createQuery($dql);
    $query->setParameter('id', $outputid);
    $query->setParameter('projectid', $projectid);
    $sourceAndRefs = $query->getResult();

    $querySys = $this->getEntityManager()->createQuery($dqlSys);
    $querySys->setParameter('id', $outputid);
    $querySys->setParameter('projectid', $projectid);
    $systemSys = $querySys->getResult();

    if ($randout) {
      #randomize and return
      foreach (self::shuffle_assoc($systemSys, rand(2,999)) as $key => $val) {
        array_push($sourceAndRefs, $val);
      }
    } else {
      foreach ($systemSys as $key => $val) {
        array_push($sourceAndRefs, $val);
      }
    }
    return $sourceAndRefs;
  }

  /*
  * get all sentence by type
  */
  function getSentences ($projectid, $type)
  {
    $result = array();

    $dql = "SELECT s.id, s.text
        FROM AppBundle:Sentence AS s
        WHERE s.project=:projectid AND s.type=:type
        ORDER BY s.num";
    /** @var Query $query */
    $query = $this->getEntityManager()->createQuery($dql);
    $query->setParameter('projectid', $projectid);
    $query->setParameter('type', $type);

    $sentences = $query->getResult();

    foreach ($sentences as $sentence) {
      $result[$sentence["id"]] = $sentence["text"];
    }
    return $result;
  }

  /*
    * get info TU ids
    */
  function getIds($projectid) {
    $dql = "SELECT DISTINCT s.id AS id
        FROM AppBundle:Sentence AS s
        WHERE s.project=:projectid AND s.simto IS NULL AND s.isintra <= 1";

    /** @var Query $query */
    $query = $this->getEntityManager()->createQuery($dql);
    $query->setParameter('projectid', $projectid);

    return $query->getResult();
  }

  /*
   * get just the output with a specific ID
   */
  function getOutput($id, $projectid) {
    $em = $this->getEntityManager();

    $dql = "SELECT s.num, s.type, s.language, s.text, s.tokenization, s.id
            FROM AppBundle:Sentence AS s
            WHERE s.id=:id AND s.project=:projectid
            AND s.type != 'source' AND s.type != 'reference'";

    /** @var Query $query */
    $query = $em->createQuery($dql);
    $query->setParameter('id', $id);
    $query->setParameter('projectid', $projectid);

    return $query->execute();
  }

  /*
   * count project TUs
   */
  function countTUs($projectId, $isIntra = false) {
    $dql = "SELECT COUNT(DISTINCT s.id)
            FROM AppBundle:Sentence AS s
            WHERE s.project=:projectid AND s.simto IS NULL";
    //to get intra-annotator sentences
    if ($isIntra) {
      $dql .= " AND s.isintra>1";
    }
    /** @var Query $query */
    $query = $this->getEntityManager()->createQuery($dql);
    $query->setParameter('projectid', $projectId);

    return $query->getSingleScalarResult();
  }

  /*
   * count target output
   */
  function countOutput($projectid) {
    $dql = "SELECT COUNT(s.num) as num
            FROM AppBundle:Sentence AS s
            WHERE s.project=:projectid
            AND s.type != 'source' AND s.type != 'reference' AND s.simto IS NULL";

    /** @var Query $query */
    $query = $this->getEntityManager()->createQuery($dql);
    $query->setParameter('projectid', $projectid);

    return $query->getSingleScalarResult();
  }

  /*
   * count project outputs
   */
  function targetLabels($projectid) {
    $dql = "SELECT s.type, COUNT(s.type) AS sentences
            FROM AppBundle:Sentence AS s
            WHERE s.project=:projectid
            AND s.type != 'source' AND s.type != 'reference' AND s.simto IS NULL
            GROUP BY s.type";

    /** @var Query $query */
    $query = $this->getEntityManager()->createQuery($dql);
    $query->setParameter('projectid', $projectid);

    return $query->getResult();
  }

  //
  function getFirstUndoneSentences($projectid, $userid) {
    $dql ="SELECT s.id FROM AppBundle:Sentence AS s
          WHERE s.project=:projectid AND s.simto IS NULL AND s.id NOT IN (SELECT a.output_id
        FROM AppBundle:Annotation AS a
        WHERE a.user=:userid AND a.project=:projectid AND a.confirmed IS NOT NULL AND a.revuser=0)";

    /** @var Query $query */
    $query = $this->getEntityManager()->createQuery($dql)->setMaxResults(1);
    $query->setParameter('projectid', $projectid);
    $query->setParameter('userid', $userid);

    return $query->execute();
  }

  /*
   * return an array whose elements are shuffled in random order.
   */
  private function shuffle_assoc($hash, $id) {
    if (!is_array($hash)) return $hash;

    $keys = array_keys($hash);
    //shuffle($keys);
    //each user will be a difference random output ordered
    self::seoShuffle($keys, $id);
    $randHash = array();
    foreach ($keys as $key)
      $randHash[$key] = $hash[$key];

    return $randHash;
  }

  /*
   * return the same randomized result each time that list is generated
   */
  private function seoShuffle(&$items, $seed) {
    mt_srand($seed);
    for ($i = count($items) - 1; $i > 0; $i--){
      $j = @mt_rand(0, $i);
      $tmp = $items[$i];
      $items[$i] = $items[$j];
      $items[$j] = $tmp;
    }
  }

  public function deleteIntraCorpus($projectId)
  {
    $em = $this->getEntityManager();
    /** @var Sentence $sentence */
    /*$result = $this->findBy(array("project" => $projectId, "isintra" => 1));
    foreach ($result as $sentence){
      $sentence->setIsintra(0);
      $em->persist($sentence);
    }*/

    $result = $this->findBy(array("project" => $projectId));
    foreach ($result as $sentence) {
      $isintra = $sentence->getIsintra();
      if ($isintra == 1) {
        $sentence->setIsintra(0);
        $em->persist($sentence);
      } else if ($isintra > 1) {
        $em->remove($sentence);
      }
    }
    $em->flush();
  }


  public function applyPooling($projectId)
  {
    $em = $this->getEntityManager();

    /** @var ProjectRepository $project_repository */
    $project_repository = $em->getRepository("AppBundle:Project");
    /** @var Project $project */
    $project = $project_repository->find($projectId);
    if ($project) {
      if ($project->getDonePooling() != null && $project->getDonePooling() != $project->getPooling()) {
        //reset pooled TUs and their annotation+agreement
        /** @var AnnotationRepository $annotation_repository */
        $annotation_repository = $em->getRepository("AppBundle:Annotation");
        /** @var AgreementRepository $agreement_repository */
        $agreement_repository = $em->getRepository("AppBundle:Agreement");

        //remove the pooled sentences already evaluated
        $dql = "SELECT s.num
            FROM AppBundle:Sentence AS s
            INNER JOIN AppBundle:Agreement AS a
            WHERE s.num=a.num AND s.project=:projectid AND s.simto IS NOT NULL";
        $query = $this->getEntityManager()->createQuery($dql);
        $query->setParameter('projectid', $projectId);

        $polledSentences = $query->getArrayResult();
        /** @var Sentence $sentence */
        foreach ($polledSentences as $sentence) {
          $annotations = $annotation_repository->findBy(array("num" => $sentence["num"]));
          foreach ($annotations as $annotation) {
            $em->remove($annotation);
          }
          $agreements = $agreement_repository->findBy(array("num" => $sentence["num"]));
          foreach ($agreements as $agreement) {
            $em->remove($agreement);
          }
        }
        $em->flush();

        //reset pooling
        $result = $this->findBy(array("project" => $projectId, "simto" => "NOT NULL"));
        foreach ($result as $sentence) {
          $sentence->setSimto(null);
          $em->persist($sentence);
        }
        $em->flush();


        /*
              $dql = "UPDATE AppBundle:Sentence AS s
                    SET s.simto = NULL
                    WHERE s.project=:projectid AND s.simto IS NOT NULL";
              $query = $this->getEntityManager()->createQuery($dql);
              $query->setParameter('projectid', $projectId);
              $query->setCacheable(false);
              $query->execute();
        */

        if ($project->getPooling() != "") {
          //calculate the similarity among source sentences
          $result = $this->findBy(array("project" => $projectId, "type" => "source"));

          foreach ($result as $sentence) {
            if ($sentence->getIsintra() > 1) {
              continue;
            }
            /** @var Sentence $sentence2 */
            foreach ($result as $sentence2) {
              if ($sentence2->getId() > $sentence->getId() && $sentence2->getSimto() == null) {

                if (self::isSimilar($sentence->getText(), $sentence2->getText(), $project->getPooling())) {
                  $sentence2->setSimto($sentence->getId());
                  $em->persist($sentence2);

                  $idsentences = $this->findBy(array("project" => $projectId, "id" => $sentence2->getId()));
                  /** @var Sentence $idsentence */
                  foreach ($idsentences as $idsentence) {
                    $idsentence->setSimto($sentence->getId());
                    $em->persist($idsentence);
                  }
                  $em->flush();
                }
              }
            }
          }
        }
        $project->setDonePooling($project->getPooling());
        $em->persist($project);
        $em->flush();

        return true;
      }
    }
    return false;
  }


  public function createIntraCorpus($projectId)
  {
    $em = $this->getEntityManager();

    /** @var ProjectRepository $project_repository */
    $project_repository = $em->getRepository("AppBundle:Project");
    /** @var Project $project */
    $project = $project_repository->find($projectId);
    if ($project) {
      //calculate the number of the sentence for intra-annotator agreement
      $intraTUs = 0;
      $outputIDs = $this->getIds($projectId);
      if (count($outputIDs) > 0) {
        $intraTUs = intval(ceil(count($outputIDs) * $project->getIntragree() / 100));
      }

      if ($intraTUs != $this->countTUs($projectId, true)) {
        //delete previous intra corpus
        $this->deleteIntraCorpus($projectId);
        $project->setIntratus(0);

        if ($intraTUs > 0) {
            $project->setIntratus($intraTUs);

//duplicate $numsentplus sentences
            $oneOn = floor(count($outputIDs) / $intraTUs);
            for ($i = 1; $i <= $intraTUs; $i++) {
              $item = $outputIDs[$i * $oneOn - 1];
              $sentences = $this->getTUSentences($item["id"], $projectId, false);
              $sourceID = "";
              foreach ($sentences as $sentence) {
                $newSent = new Sentence();
                $newSent->setId("{" . $item["id"] . "}");
                $newSent->setProject($projectId);
                $newSent->setLanguage($sentence["language"]);
                $newSent->setType($sentence["type"]);
                if ($sentence["type"] == "source") {
                  $newSent->setLinked(0);
                } else {
                  $newSent->setLinked($sourceID);
                }
                $newSent->setIsurl($sentence["isurl"]);
                $newSent->setText($sentence["text"]);
                $newSent->setTokenization($sentence["tokenization"]);
                $newSent->setComment($sentence["comment"]);
                $newSent->setIsintra($sentence["num"]);
                $newSent->setCreated(new \DateTime());
                $em->persist($newSent);
                $em->flush();
                if ($sentence["type"] == "source") {
                  $sourceID = $newSent->getNum();
                  //$sourceID = $sentence["num"];
                }
                $sent = $this->find($sentence["num"]);
                if ($sent) {
                  $sent->setIsintra(1);
                  $em->persist($sent);
                }
              }
            }

        }
        $em->persist($project);
        $em->flush();
        return true;
      }
    }
    return false;
  }

  private function isSimilar ($text1, $text2, $mode)
  {
    $word2 = self::tokenize($text2, $mode);
    if ($mode == "exact" || $mode == "exactins") {
      $word1 = self::tokenize($text1, $mode);
      if (count($word1) != count($word2)) {
        return false;
      }
      $i = 0;
      foreach ($word1 as $word) {
        if ($word2[$i] != $word) {
          return false;
        }
        $i++;
      }
    } else {
      $word1 = array_fill_keys(self::tokenize($text1, $mode), 0);

      //bag of words
      foreach ($word2 as $word) {
        if (array_key_exists($word, $word1)) {
          $word1[$word]++;
        } else {
          return false;
        }
      }

      foreach ($word1 as $word => $count) {
        if ($count == 0) {
          return false;
        }
      }
    }
    return true;
  }

  private function tokenize ($text, $mode) {
    mb_internal_encoding('UTF-8');
    $text = preg_replace('/\s+/', ' ', trim($text));

    if ($mode != "exact") {
      $text = strtolower($text);
    }

    preg_match_all("~([#@]?\\w+|\\pP+|\\p{Sc}+|\\S)~u", $text, $matches);
    if (count($matches[0]) > 0) {
      return $matches[0];
    }
    return explode(" ", $text);
  }
}