<?php
/**
 * Project: mtequal_ext
 * User: cgirardi
 * Date: 25/10/15
 */

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\Role;
use Doctrine\ORM\Query;

class RoleRepository extends EntityRepository
{

  /*
  * check if a role already exists
  * @return Role
  */
  public function getRole($userId, $projectId, $roleType)
  {
    return $this->findOneBy(array("user" => $userId, "project" => $projectId, "role" => $roleType));
  }

  /*
 * create a new role (it is not already exists)
 * @return Role
 */
  public function updateRole($createdBy, $userId, $projectId, $roleType, $status)
  {
    if ($roleType == "") {
      if (count($this->findBy(array("user" => $userId, "project" => $projectId))) > 0) {
        //already exists at least a role record for this user in the project
        return null;
      }
    }
    $time = new \DateTime("now");
    /** @var Role $role */
    $role = $this->getRole($userId, $projectId, $roleType);
    if ($role == null) {
      $role = new Role();
      try {
        $role->setUser($userId);
        $role->setProject($projectId);
        $role->setRole($roleType);
        $role->setCreated($time);
        $role->setCreatedBy($createdBy);
        $role->setStatus("");
      } catch (\PDOException $e) {
        // Will output an SQLSTATE[23000] message, similar to:
        // Integrity constraint violation: 1062 Duplicate entry 'x'
        // ... for key 'UNIQ_BB4A8E30E7927C74'
        if ($e->getCode() != '23000') {
          //echo $e->getMessage();
          throw $e;
        }
      }
    }


    if ($role) {
      $em = $this->getEntityManager();
      /** @var ProgressRepository $progress_repository */
      $progress_repository = $em->getRepository('AppBundle:Progress');

      if ($status != null) {
        $role->setStatus($status);
        if ($status == "revoked") {
          $this->removeInvitation($userId, $projectId, $role->getRole());
        }
        $progress_repository->updateUserProgress($role->getRole(), $role->getProject(), $role->getUser(), $status);
      } else {
        if ($role->getStatus() == "pending" || $role->getStatus() == "accepted") {
          $this->removeInvitation($userId, $projectId, $role->getRole());
          $role->setStatus("revoked");
          $progress_repository->updateUserProgress($role->getRole(), $role->getProject(), $role->getUser(), "revoked");
        } else {
          $role->setStatus("pending");
        }
      }
      $role->setHash(self::getHash());
      $role->setActivated($time);

      $em->persist($role);
      $em->flush();

      //update progress for invitation tasks
      $task = $progress_repository->findOneBy(array("task" => $roleType. "_invitation", "project" => $projectId, "user" => $role->getCreatedBy()));
      if ($task) {
        $users = self::getUserByRole($projectId, $roleType, $role->getCreatedBy());
        if (count($users) > 0) {
          $task->setCompleted(true);
          $task->setDisabled(true);
        } else {
          $task->setCompleted(false);
          $task->setDisabled(false);
        }
        $em->persist($task);
        $em->flush();
      }

      /** @var ProjectRepository $project_repository */
      $project_repository = $em->getRepository('AppBundle:Project');
      $project_repository->updateModifiedTime($projectId);
    }
    return $role;
  }


  public function getUserByRole($projectId, $roleLabel = null, $createdBy = null)
  {
    //return $em->getRepository("AppBundle:Role")->findBy(array("project" => $projectId, "role" => "evaluator", "status" => "accepted"))
    //get user for a specific roles in a project
    $dql = "SELECT r.user AS id, u.username, u.email, r.role
        FROM AppBundle:Role AS r
        INNER JOIN AppBundle:User AS u
          WHERE u.id=r.user
        WHERE r.project = :projectid AND r.status='accepted'";
    if ($roleLabel != null) {
      $dql .= " AND r.role = :role";
    }
    if ($createdBy != null) {
      $dql .= " AND r.created_by = :createdby";
    }
    $dql .= " ORDER BY r.activated";

    /** @var Query $query */
    $query = $this->_em->createQuery($dql);
    if ($roleLabel != null) {
      $query->setParameter('role', $roleLabel);
    }
    if ($createdBy != null) {
      $query->setParameter('createdby', $createdBy);
    }
    $query->setParameter('projectid', $projectId);

    return $query->execute();
  }

  /**
   * get user who invited the userId with a specific role
   * @param $projectId
   * @param $userId
   * @param $roleLabel
   * @return mixed
   */
  public function getInviter($projectId, $userId, $roleLabel)
  {
    $dql = "SELECT r.user AS id, u.username, u.email
        FROM AppBundle:Role AS r
        INNER JOIN AppBundle:User AS u
          WHERE u.id=r.created_by
        WHERE r.project = :projectid AND r.user = :userid AND r.role = :role AND r.created_by!=0";

    /** @var Query $query */
    $query = $this->_em->createQuery($dql);
    $query->setParameter('projectid', $projectId);
    $query->setParameter('userid', $userId);
    $query->setParameter('role', $roleLabel);

    return $query->execute();
  }

  /**
   * get all users according with your role
   * @param $projectId
   * @param $userId
   * @return mixed|null
   */
  public function getRoles($projectId, $userId)
  {
    $dql = "SELECT u.id,u.username,u.email,r.role,r.status,r.created,r.activated,r.created_by AS inviter
        FROM AppBundle:User AS u
        INNER JOIN AppBundle:Role AS r
        WHERE u.id=r.user AND r.project=:project";

    /** @var Query $query */
    $roles = self::getUserRoles($userId, $projectId);
    if (in_array("owner", $roles) || in_array("admin", $roles) || in_array("reviewer", $roles)) {
      $dql .= " ORDER BY u.username";
      $query = $this->_em->createQuery($dql);
      $query->setParameter('project', $projectId);

    } else {
      if (in_array("vendor", $roles)) {
        //limit to get info about evaluators who are invited by the vendor
        $dql .= " AND (r.created_by=:userid OR u.id=:userid) ORDER BY u.username";
      } else {
        $dql .= " AND u.id=:userid ORDER BY u.username";
      }
      $query = $this->_em->createQuery($dql);
      $query->setParameter('project', $projectId);
      $query->setParameter('userid', $userId);
    }
    return $query->execute();
  }

  /**
   * return an array with accepted or created roles for a user
   * @param $userId
   * @param $projectId
   * @return array
   */
  function getUserRoles($userId, $projectId)
  {
    $roles = array();
    //get user roles for a project
    $dql = "SELECT r.role
            FROM AppBundle:Role AS r
            WHERE r.project=:projectid AND r.user=:userid AND r.status='accepted'
            ORDER BY r.role";
      /** @var Query $query */
    $query = $this->_em->createQuery($dql);
    $query->setParameter('userid', $userId);
    $query->setParameter('projectid', $projectId);

    foreach( $query->getArrayResult() as $key => $value ) {
      array_push($roles, $value["role"]);
    }
    return $roles;
  }

  /**
   * all roles of a user (view the Requests panel)
   */
  public function getUserRequests ($userId) {
    if ($userId != null) {
      $dql = "SELECT r.project as id,ord.name,r.role,r.status as role_status,r.created,r.activated,r.hash
        FROM role AS r
        LEFT JOIN (
          SELECT p.name as name, r2.project, MAX(r2.activated) AS recentime
            FROM role AS r2 INNER JOIN project AS p
          WHERE p.id=r2.project AND p.active=1 AND r2.user=$userId
        group by r2.project order by recentime DESC
          ) AS ord
           ON r.project=ord.project WHERE ord.name IS NOT NULL AND r.user=$userId AND r.role!=''
        ORDER BY ord.recentime DESC, r.activated DESC";

      return $this->_em->getConnection()->query($dql)->fetchAll();
    }
    return null;
  }

  /**
   * get the number of a specific role involved in a project
   */
  public function roleCount($projectId = null, $role = null)
  {
    $roles = $this->getEntityManager()->findBy(array("project" => $projectId, "role" => $role));
    if ($role)
      return count($roles);
    return 0;
  }

  /**
   * get the number of the roles without reply by user (thus pending)
   */
  public function pendingCount($userId)
  {
    $dql = "SELECT COUNT(r.project)
              FROM AppBundle:Role AS r
              INNER JOIN AppBundle:Project AS p
              WHERE r.project=p.id AND p.active=1
               AND r.user=:userid AND r.status='pending' AND r.role!=''";
    /** @var Query $query */
    $query = $this->getEntityManager()->createQuery($dql);
    $query->setParameter('userid', $userId);

    return $query->getSingleScalarResult();
  }

  /**
   * get statistics about project roles
   */
  public function statsRoles($projectId = null, $status = null) {
    //select role,count(*) from role where project=1 and status="accepted" group by role;

    $query = "SELECT r.role, COUNT(r.role) AS num FROM AppBundle:Role AS r WHERE r.project=:project";
    $queryparam ['project'] = $projectId;

    if ($status != null) {
      $query .= " AND r.status=:status";
      $queryparam ['status'] = $status;
    }
    $query .= " GROUP BY r.role";

    $dql = $this->getEntityManager()->createQuery($query)->setParameters($queryparam);
    return $dql->execute();
  }

  /**
   * remove role for a user in a project
   */
  public function removeInvitation($userId = null, $projectId = null, $role = null) {
    $em = $this->getEntityManager();

    if ($userId != null && $projectId != null) {
      if ($projectId == null) {
        $user_roles = $this->findBy(array("user" => $userId));
      } else if ($role == null) {
        $user_roles = $this->findBy(array("user" => $userId, "project" => $projectId));
      } else {
        $user_roles = $this->findBy(array("user" => $userId, "project" => $projectId, "role" => $role));
      }
      if (count($user_roles) > 0) {
        /** @var ProgressRepository $progress_repository */
        $progress_repository = $em->getRepository('AppBundle:Progress');
        /** @var AnnotationRepository $annotation_repository */
        $annotation_repository = $em->getRepository('AppBundle:Annotation');
        /** @var Role $userRole */
        foreach ($user_roles as $userRole) {
         if (array_key_exists($userRole->getRole(), $progress_repository::$USER_TASKS)) {
            $progress_repository->deleteProgress($progress_repository::$USER_TASKS[$role], $userRole->getProject(), $userRole->getUser());
            if ($role == "evaluator") {
              $annotation_repository->deleteEvaluations($projectId, $userId);
            } else if ($role == "reviewer") {
              $annotation_repository->deleteRevEvaluations($projectId, $userId);
            }
          } else {
            $progress_repository->deleteAllProgress($userRole->getProject(), $userRole->getUser());
            $annotation_repository->deleteEvaluations($userRole->getProject(), $userRole->getUser(), false);
          }

          if ($role == null) {
            $em->remove($userRole);
          }
        }
        $em->flush();

        //update the modified time of the project
        $em->getRepository('AppBundle:Project')->updateModifiedTime($projectId);
        return true;
      }
    }
    return false;
  }


  /**
   * get a string starting from the current date and a random number
   */
  private function getHash() {
    $time = new \DateTime("now");
    return md5($time->format('Y-m-d H:i:s.u') ." ". rand(1, 9999));
  }

  /**
   * get an hash with the info about project users
   */
  public function getUsers ($projectId, $userId, $role = null) {
    $dql = "SELECT u.id,u.username,u.email,r.role,r.status,r.created,r.activated,r.created_by AS inviter
        FROM AppBundle:User AS u
        INNER JOIN AppBundle:Role AS r
        WHERE u.id=r.user AND r.project=:project AND r.status='accepted'";
    if ($role != null) {
      $dql .= " AND (r.user=:userid OR r.role=:role)";
    }
    $dql .= " ORDER BY r.activated";
    $query = $this->_em->createQuery($dql);
    $query->setParameter('project', $projectId);
    if ($role != null) {
      $query->setParameter('userid', $userId);
      $query->setParameter('role', $role);
    }
    $roles = $query->execute();

    //get my roles (avoiding to do another query)
    $myRoles = array();
    foreach ($roles as $rUser) {
      if ($rUser["id"] == $userId && $rUser["role"] != "") {
        array_push($myRoles, $rUser["role"]);
      }
    }

    $roletype = array();
    $users = array();
    foreach ($roles as $rUser) {
      if (array_key_exists($rUser["role"], $roletype)) {
        $roletype[$rUser["role"]]++;
      } else {
        $roletype[$rUser["role"]] = 1;
      }
      if (in_array("owner", $myRoles) ||
          in_array("admin", $myRoles) ||
          in_array("reviewer", $myRoles) ||
        (in_array("vendor", $myRoles) && $rUser["inviter"]==$userId)) {
        if (!$role || $rUser["role"] == $role) {
          //	$userColor = $self::stringToColour($rUser["username"]);
          $users[$rUser["id"]][$rUser["role"]] = array("username" => $rUser["username"],
            "anonymous" => $rUser["role"] . " " . $roletype[$rUser["role"]]);
        }
      }
    }

    return $users;
  }

  /**
   * get an hash with the info about who are the inviters of the project
   */
  public function getInviters () {

  }

  /* private function stringToColour($str) {
    $str = unpack('L', hash('adler32', $str, true))[1];
    $H = $str/0xFFFFFFFF;
    $S = 0.4;
    $V = 255;
    $H *= 6;
    $h = intval($H);
    $H -= $h;
    $m = $V*(1 - $S);
    $x = $V*(1 - $S*(1-$H));
    $y = $V*(1 - $S*$H);
    $a = [[$V, $x, $m], [$y, $V, $m],
      [$m, $V, $x], [$m, $y, $V],
      [$x, $m, $V], [$V, $m, $y]][$h];
    return sprintf("#%02X%02X%02X", $a[0], $a[1], $a[2]);
  }
  */
}

?>