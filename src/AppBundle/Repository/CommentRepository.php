<?php
/**
 * Project: mtequal_ext
 * User: cgirardi
 * Date: 10/04/16
 */

namespace AppBundle\Repository;


use AppBundle\Entity\Comment;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

/**
 * Annotation repository class
 */
class CommentRepository extends EntityRepository
{
  const MAX_COMMENTS = 4;
  /*
   * save a comment void
   * @return boolean
   */
  function saveComment($projectid, $userid, $text, $table, $refid)
  {
    if ($text != null && trim($text) != "") {
      $em = $this->getEntityManager();
      /** @var Comment $comment */
      $comment = new Comment($userid, $projectid);
      $comment->setComment(trim(strip_tags($text)));
      $comment->setObj($table);
      $comment->setRefID(intval($refid));

      $em->persist($comment);
      $em->flush();
      return true;
    }
    return false;
  }

  /*
   * get the last comments
   */
  function lastComment ($projectid, $obj, $refid, $userid = null, $showall = false) {
    $query = "SELECT c.comment, c.userid, u.username, c.date
        FROM AppBundle:Comment AS c
        INNER JOIN AppBundle:User AS u
        WHERE c.userid=u.id AND c.project=:projectid AND c.obj=:obj AND c.refid=:refid";
    $queryparam ['projectid'] = $projectid;
    $queryparam ['obj'] = $obj;
    $queryparam ['refid'] = $refid;

    if ($userid != null) {
      $query .= " AND c.userid=:userid";
      $queryparam ['userid'] = $userid;
    }
    $query .= " ORDER BY c.date DESC";

    $dql = $this->getEntityManager()->createQuery($query)->setParameters($queryparam);
    if (!$showall) {
      $dql->setMaxResults(self::MAX_COMMENTS);
    }
    return $dql->getResult();
  }

  /*
   * get the ditribution of the comments for a project
   */
  public function getCommentsDistribution ($projectId)
  {
    $hash = array();
    $queryparam ['project'] = $projectId;
    $query = "SELECT c.refid AS num, COUNT(c.refid) AS cnt
        FROM AppBundle:Comment AS c
        WHERE c.project=:project
        GROUP BY num";

    $dql = $this->getEntityManager()->createQuery($query)->setParameters($queryparam);
    foreach ($dql->getArrayResult() as $value) {
      $hash[$value["num"]] = $value["cnt"];
    }
    return $hash;

  }

  /*
   * get the number of comments for a specific sentence
   */
  public function getCommentCount ($sentenceNum)
  {
    $query = "SELECT COUNT(c.refid) AS cnt
        FROM AppBundle:Comment AS c
        WHERE c.refid=:num";

    $dql = $this->getEntityManager()->createQuery($query)->setParameter("num",$sentenceNum);

    return $dql->getSingleScalarResult();

  }
}
?>