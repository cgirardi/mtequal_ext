<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Document;
use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\Project;
use Doctrine\ORM\Query;
use Symfony\Component\Config\Definition\Exception\Exception;

/**
 * Project repository class
 */
class ProjectRepository extends EntityRepository
{
  /**
   * get project object from ID
   * @param $id
   * @return null|object
   */
  public function getProjectFromID($id)
  {
    return $this->findOneBy(array("id" => $id, "active" => true));
  }

  /**
  * returns an hash with project information
  */
  public function getProject($id)
  {
    /** @var Project $project */
    $project = self::getProjectFromID($id);
    if ($project != null && $project->getActive()) {
      return array(
        "id" => $project->getId(),
        "name" => $project->getName(),
        "type" => $project->getType(),
        "pooling" => $project->getPooling(),
        "description" => $project->getDescription(),
        "instructions" => $project->getInstructions(),
        "ranges" => $project->getRanges(),
        "requiref" => intval($project->getRequiref()),
        "randout" => intval($project->getRandout()),
        "blindrev" => intval($project->getBlindrev()),
        "sentlist" => intval($project->getSentlist()),
        "intragree" => intval($project->getIntragree()),
        "maxdontknow" => intval($project->getMaxdontknow()),
        "intratus" => intval($project->getIntratus()),
        "evalformat" => self::getDocumentName($project->getEvalformat()),
        "guidelines" => self::getDocumentName($project->getGuidelines()),
        "created" => $project->getCreated(),
        "modified" => $project->getModified()
      );
    }
    return null;
  }

  /**
  * returns an hash with information for all projects
  */
  public function getProjects()
  {
    $em = $this->getEntityManager();

    $dql = "SELECT p.id, p.name, p.type, p.ranges, p.active, p.modified
        FROM AppBundle:Project AS p
        ORDER BY p.name";

    /** @var Query $query */
    $query = $em->createQuery($dql);

    return $query->execute();
  }

  /**
   * create/update a new project
   * @param array $values
   * @return Project
   */
  public function createProject(array $values)
  {
    $time = new \DateTime("now");
    $em = $this->getEntityManager();
    /** @var Project $project */
    $project = self::getProjectFromID($values['id']);
    if (!$project) {
      $project = new Project();
      $project->setCreated($time);
    }
    //set the project's name
    if (array_key_exists('name',$values)) {
      $name = trim($values['name']);
      if (!empty($name)) {
        $project->setName($values['name']);
      } else {
        return null;
      }
    }
    //set the evaluation type
    if (array_key_exists('type',$values)) {
      $project->setType($values['type']);

      if (array_key_exists('ranges',$values)) {
        $project->setRanges($values['ranges']);
      } else {
        $project->setRanges("");
      }

      //set the values with import corpus costraint
      // Claudio: fixed the bug
      if (array_key_exists('requiref', $values)) {
        if ($values['requiref']) {
          $project->setRequiref(true);
        } else {
          $project->setRequiref(false);
        }
      }

      //save evalformat document
      $docData = array("type" => $project->getType(),
        "ranges" => json_decode($project->getRanges(), true),
        "requiref" => $project->getRequiref());
      $docValues = array("type" => "evalformat",
        "format" => $project->getType(),
        "url" => $project->getName(),
        "data" => json_encode($docData));
      /** @var Document $doc */
      $doc = $em->getRepository("AppBundle:Document")->update($docValues);
      $project->setEvalformat($doc->getId());
    }

    //set the guidelines
    if (array_key_exists('instructions',$values)) {
      $project->setInstructions(trim(preg_replace('/<br>$/', '', $values['instructions'])));
    }

    //set the values of guidelines specification
    if (array_key_exists('randout',$values)) {
      if ($values['randout']) {
        $project->setRandout(true);
      } else {
        $project->setRandout(false);
      }
    }

    if (array_key_exists('blindrev',$values)) {
      if ($values['blindrev']) {
        $project->setBlindrev(true);
      } else {
        $project->setBlindrev(false);
      }
    }

    if (array_key_exists('sentlist',$values)) {
      if ($values['sentlist']) {
        $project->setSentlist(true);
      } else {
        $project->setSentlist(false);
      }
    }

    if (array_key_exists('intragree',$values)) {
      if (intval($values['intragree']) > 0) {
        $project->setIntragree(intval($values['intragree']));
      } else {
        $project->setIntragree(0);
      }
    }

    if (array_key_exists('maxdontknow',$values)) {
      if (intval($values['maxdontknow']) >= 0) {
        $project->setMaxdontknow($values['maxdontknow']);
      } else {
        $project->setMaxdontknow(-1);
      }
    }

    if (array_key_exists('pooling',$values)) {
      if ($project->getDonePooling() == null) {
        $project->setDonePooling($project->getPooling());
      }
      if ($values['pooling'] != "") {
        $project->setPooling($values['pooling']);
      } else {
        $project->setPooling("");
      }
    }

    //save guidelines document
    $docData = array("randout" => $project->getRandout(),
      "blindrev" => $project->getBlindrev(),
      "sentlist" => $project->getSentlist(),
      "intragree" => $project->getIntragree(),
      "maxdontknow" => $project->getMaxdontknow(),
      "pooling" => $project->getPooling(),
      "instructions" => $project->getInstructions()
    );
    $docValues = array("type" => "guidelines",
      "format" => "",
      "url" => $project->getName(),
      "data" => json_encode($docData));

    /** @var Document $doc */
    $doc = $em->getRepository("AppBundle:Document")->update($docValues);
    $project->setGuidelines($doc->getId());


    //set the description
    if (array_key_exists('description', $values)) {
      $project->setDescription(trim($values['description']));
    }

    $project->setModified($time);
    $em->persist($project);
    $em->flush();

    return $project;
  }

  /*
  * change the modified time of a project
  */
  public function updateModifiedTime ($projectId)
  {
    /** @var Project $project */
    $project = self::getProjectFromID($projectId);
    if ($project) {
      $time = new \DateTime("now");
      $em = $this->getEntityManager();

      $project->setModified($time);
      $em->persist($project);
      $em->flush();
      return true;
    }
    return false;
  }

  /*
  * action can be: edit, delete, invite, annotate, evaluate
  */
  public function can($action, $projectid, $userid) {
    $dql = "SELECT count(r.role)
        FROM AppBundle:RoleType AS t
        INNER JOIN AppBundle:Role AS r
        WHERE r.project=:projectid AND r.user=:userid AND t.name=r.role AND t.can_" . $action . "=1";
    try {
      $em = $this->getEntityManager();
      $query = $em->createQuery($dql);
      $query->setParameter('projectid', $projectid);
      $query->setParameter('userid', $userid);
      //$query->setParameter('action', $action);

      return $query->getSingleScalarResult();
    } catch (Exception $e) {
      //$logger->warn('QUERY FAILED: ' .$dql);
    }
    return 0;
  }

  /**
   * set a project as not active instead of remove it from database
  */
  public function deleteProject($id)
  {
    $em = $this->getEntityManager();
    $project = self::getProjectFromID($id);
    if ($project != null) {
      //set the flag like "disabled" to the project row instead remove the project and its dependencies: delete annotation, comment, roles, progress, sentences,...
      $project->setActive(false);
      //$em->remove($project);
      $em->persist($project);
      $em->flush();
      return 1;
    }
    return 0;
  }

  public function findProject($userid)
  {
    if ($userid != null) {
      $em = $this->getEntityManager();

      $dql = "SELECT p.id, p.name, p.type, p.description, p.modified, GROUP_CONCAT(r.role SEPARATOR ', ') AS roles
        FROM AppBundle:Project AS p
        INNER JOIN AppBundle:Role AS r
        WHERE p.id=r.project AND p.active=1 AND r.user=:userid AND r.role!='' AND r.status='accepted'
        GROUP BY p.id
        ORDER BY p.modified DESC, p.name";

      /** @var Query $query */
      $query = $em->createQuery($dql);
      $query->setParameter('userid', $userid);

      return $query->execute();
    }
    return null;
  }

  /*
   * get document name
   */
  private function getDocumentName($docId)
  {
    if ($docId != 0) {
      /** @var Document $doc */
      $doc = $this->getEntityManager()->getRepository("AppBundle:Document")->find($docId);
      if ($doc) {
        return $doc->getUrl();
      }
    }
    return "";
  }
}