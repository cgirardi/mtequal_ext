<?php
/**
 * Project: mtequal_ext
 * User: cgirardi
 * Date: 12/01/15
 */
namespace AppBundle\Util;

abstract class Log
{
  const fileLog = "/tmp/mterr.log";
  const APPEND_FILE = true;

  public static function write ($message)
  {
    if (self::APPEND_FILE) {
      $time = new \DateTime("now");

      ob_start();
      var_dump($message);
      $result = ob_get_clean();

      $message = "[" . $time->format('Y-m-d H:i:s') . "] " . $result ."\n";
      //$message = "[" . $time->format('Y-m-d H:i:s') . "] " . $message ."\n";
      error_log($message, 3, self::fileLog);
    }
  }
}
?>